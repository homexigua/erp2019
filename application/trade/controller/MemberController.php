<?php
namespace app\trade\controller;

use app\common\controller\TradeBaseController;
use app\common\model\member\MemberModel;

class MemberController extends TradeBaseController
{
    /**
     * 初始化方法
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new MemberModel();
    }

    /**
     * 会员首页
     * @return mixed
     */
    public function index()
    {
        return $this->fetch();
    }
    /**
     * 变更密码
     * @return mixed
     */
    public function changepwd(){
        if($this->request->isAjax()){
            try{
                $newPassword = $this->request->param('new_password');
                $oldPassword = $this->request->param('old_password');
                if(empty($newPassword) || empty($oldPassword)){
                    exception('请输入新旧密码！');
                }
                $this->model->changepwd($this->member['id'],$newPassword,$oldPassword);
            }catch (\Exception $e){
                $this->error($e->getMessage());
            }
            $this->success('操作成功！',url('Public/login'));
        }
        return $this->fetch();
    }
    /**
     * 退出登录
     */
    public function loginOut(){
        try{
            $this->model->loginOut();
        }catch (\Exception $e){
            $this->error($e->getMessage());
        }
        $this->success('退出成功！',url('public/login'));
    }
}
