<?php

namespace app\trade\controller;

use app\common\controller\TradeBaseController;

class IndexController extends TradeBaseController {
    /**
     * 初始化方法
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * 首页
     * @return mixed
     */
    public function index() {
        return $this->fetch();
    }

}
