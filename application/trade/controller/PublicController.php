<?php
namespace app\trade\controller;

use app\common\controller\TradeBaseController;
use app\common\model\member\MemberModel;
use think\facade\Session;

class PublicController extends TradeBaseController
{
    /**
     * 初始化方法
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 用户登录
     * @return mixed
     */
    public function login(){
        $memberModel = new MemberModel();
        if($this->request->isAjax()){
            $redirect = $this->request->param('redirect'); //获取跳转地址
            $redirect = empty($redirect) ? '/' : urldecode($redirect);
            try{
                $account = $this->request->param('account');
                $password = $this->request->param('password');
                $memberModel->login($account,$password);
            }catch (\Exception $e){
                $this->error($e->getMessage());
            }
            $this->success('操作成功！',$redirect);
        }
        if(Session::has('member')){ //已登录直接跳转
            $this->redirect('Index/index');
        }
        return $this->fetch();
    }
}
