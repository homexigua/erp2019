<?php

namespace app\site\controller;

use app\common\controller\SiteBaseController;
use app\common\model\SpecialItemModel;
use app\common\model\SpecialModel;

class SpecialController extends SiteBaseController {
    private $specialItemModel;

    /**
     * 初始化方法
     */
    public function __construct() {
        parent::__construct();
        $this->model = new SpecialModel();
        $this->specialItemModel = new SpecialItemModel();
    }

    /**
     * 专题页
     */
    public function index() {
        $specialId = $this->request->param('special_id/d');
        $info = $this->model->where('id', $specialId)->find();
        $this->assign('info', $info);
        return $this->fetch($info['special_tpl']);
    }
}
