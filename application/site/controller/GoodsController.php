<?php
namespace app\site\controller;

use app\common\controller\SiteBaseController;
use app\common\model\mall\GoodsModel;
use app\common\traits\homeTraits;
use org\util\TreeUtil;
use think\facade\Cache;

class GoodsController extends SiteBaseController
{
    use homeTraits;
    /**
     * 初始化方法
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new GoodsModel();
    }

    /**
     * 列表页
     * @return mixed
     */
    public function index(){
        $classId = $this->request->param('class_id');
        $this->getCatInfo($classId);
        if($this->request->isAjax()){
            $pageSize = $this->request->param('pageSize/d'); //每页多少条
            $pageNumber = $this->request->param('pageNumber/d'); //当前页
            $sortName = $this->request->param('sortName','id'); //排序字段
            $sortOrder = $this->request->param('sortOrder', 'desc'); //升序降序
            $order = [$sortName => $sortOrder];
            $classId = $this->request->param('class_id');
            $filter = $this->request->param("filter"); //搜索字段值 json
            $op = $this->request->param("op"); //搜索条件 json
            $op = $op ? $op : [];
            $filter = $filter ? $filter : [];
            $filter['site_id']=$this->siteId;
            $op['site_id']='=';
            if($classId>0){
                $filter['class_id']=$this->goodsClass[$classId]['data_view'];
                $op['class_id']='in';
            }
            //搜索器配置
            $searchFields = ['common'];//参与搜索信息
            $searchData = [ //默认搜索数据
                'common' => [$filter, $op],
                'sort' => $order,
            ];
            if (method_exists($this, '_pageFilter')) {
                $params = [];
                $this->_pageFilter($params);
                if(!empty($params['custom'])){
                    $searchData['common'][0] = array_merge($searchData['common'][0],$params['custom']['filter']);
                    $searchData['common'][1] = array_merge($searchData['common'][1],$params['custom']['op']);
                }
                if (!empty($params['fields'])) {
                    $searchFields = array_merge($searchFields, $params['fields']);
                }
                if (!empty($params['data'])) {
                    $searchData = array_merge($searchData, $params['data']);
                }
            }
            try {
                $data = ['total' => 0, 'rows' => []];
                //分页不需要sort和field
                $countData = $searchData;
                if (isset($countData['sort'])) unset($countData['sort']);
                if (isset($countData['field'])) unset($countData['field']);
                $data['total'] = $this->model->withSearch($searchFields, $countData)->count();
                $data['sql'] = $this->model->getLastSql();
                if ($data['total'] > 0) {
                    $list = $this->model->withSearch($searchFields, $searchData)->page($pageNumber, $pageSize)->select();
                    //$data['sql'] = $this->model->getLastSql();
                    if (method_exists($this->model, '_formatList')) {
                        $list = $this->model->_formatList($list);
                    }
                    $data['rows'] = $list;
                }
            } catch (\Exception $e) {
                $data = ['total' => 0, 'rows' => [], 'err_msg' => $e->getMessage()];
            }
            return $data;
        }
        return $this->fetch();
    }

    /**
     * 内容页
     * @return mixed
     */
    public function goodsinfo(){
        $goodsId = $this->request->param('goods_id');
        $goodsInfo = $this->model->getInfo(['id'=>$goodsId]);
        $this->getCatInfo($goodsInfo['class_id']);
        $this->assign('info',$goodsInfo);
        return $this->fetch();
    }

    /**
     * 搜索页
     * @return mixed
     */
    public function search(){

    }


    /**
     * 根据栏目ID获取对应信息
     * @param $cid
     * @return mixed
     */
    private function getCatInfo($cid)
    {
        try {
            $category = $this->goodsClass;
            $currentCat = $category[$cid]; //当前栏目
            $locationCat = array_reverse(TreeUtil::parentChannel($category, $cid, 'id', 'pid'));
            unset($locationCat[0]);
            $this->assign('keyword', $currentCat['class_keywords']);
            $this->assign('description', $currentCat['class_desc']);
            $this->assign('current_cat', $currentCat); //当前栏目
            $this->assign('location_cat', $locationCat); //当前位置
            $this->assign('top_cat', $locationCat[0]); //当前最顶级栏目
            return $currentCat;
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }

}
