<?php

namespace app\site\controller;

use app\common\controller\SiteBaseController;

class IndexController extends SiteBaseController {

    /**
     * 初始化方法
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * 首页
     * @return mixed
     */
    public function index() {

        return $this->fetch();
    }

}
