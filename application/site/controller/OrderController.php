<?php
namespace app\site\controller;

use app\common\controller\SiteBaseController;

class OrderController extends SiteBaseController
{
    /**
     * 初始化方法
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * 列表页
     * @return mixed
     */
    public function items(){
        return $this->fetch();
    }

    /**
     *
     * @return mixed
     */
    public function search(){
        return $this->fetch();
    }

}
