<?php
namespace app\site\controller;

use app\common\controller\SiteBaseController;
use app\common\model\cms\ClassModel;

class CmsController extends SiteBaseController
{
    private $classModel;
    private $moudleId;

    /**
     * 初始化方法
     */
    public function __construct()
    {
        parent::__construct();
        $this->classModel = new ClassModel();
        $this->moudleId = $this->request->param('moudle_id/d',1);
        $this->assign('moudle_id',$this->moudleId);
    }

    /**
     * 列表页
     * @return mixed
     */
    public function items(){
        $classId = $this->request->param('class_id/d');
        $currentInfo = $this->cmsClass[$classId];
        $this->assign('currentInfo',$currentInfo);
        return $this->fetch($currentInfo['class_template']);
    }

    /**
     * 内容页
     * @return mixed
     */
    public function info(){
        $moudelId = $this->request->param('moudle_id/d',1);
        $id = $this->request->param('id/d');
        $map[] = ['id','=',$id];
        $info = cms_model($moudelId)->getInfo($map);
        $currentInfo = $this->cmsClass[$info['class_id']];
        $this->assign('currentInfo',$currentInfo);
        $this->assign('info',$info);
        return $this->fetch($currentInfo['content_template']);
    }

    /**
     * 内容页
     * @return mixed
     */
    public function search(){
        $moudelId = $this->request->param('moudle_id/d',1);
        $title = $this->request->param('title/s');
        $paginateModel = $this->request->isMobile() ? true : false;
        $list = cms_model($moudelId)->where('is_audit',1)->where('title','like','%'.$title.'%')->paginate(12,$paginateModel,[
            'query'=>$this->request->request()
        ]);
        $this->assign('list',$list);
        return $this->fetch();
    }
}
