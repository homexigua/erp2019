<?php
namespace app\site\controller\member;

use app\common\controller\SiteMemberBaseController;
use app\common\model\CollectionModel;

class CollectionController extends SiteMemberBaseController
{
    /**
     * 初始化方法
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new CollectionModel();
    }

    /**
     * 收藏列表
     */
    public function index(){
        $type = $this->request->param('type',999);
        $modellist = $this->model->getTypelist();
        $model = new $modellist[$type];
        $ids = $this->model->where('member_id',$this->member['id'])->where('model',$modellist[$type])->select(); //收藏列表
        $list=$model::where('id','in',$ids)->select();
        $this->assign('list',$list);
        return $this->fetch();
    }
    /**
     * 添加收藏
     */
    public function add(){
        $type = $this->request->param('type',999);
        $collectionId = $this->request->param('collection_id/d');
        $this->model->collection($this->member['id'],$type,$collectionId);
    }

    /**
     * 删除收藏
     */
    public function del(){
        $id = $this->request->param('id/d');
        $this->model->where('id',$id)->delete();
        $this->success('操作成功！');
    }

}
