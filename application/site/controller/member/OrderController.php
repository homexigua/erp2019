<?php
namespace app\site\controller\member;

use app\common\controller\SiteMemberBaseController;
use app\common\model\mall\SoOrderModel;

class OrderController extends SiteMemberBaseController
{
    /**
     * 初始化方法
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new SoOrderModel();
    }

    /**
     * 订单列表
     */
    public function index(){

    }

    /**
     * 订单详情
     */
    public function info(){

    }

    /**
     * 订单付款
     */
    public function pay(){

    }

    /**
     * 确认收货
     */
    public function confirm(){

    }

    /**
     * 删除订单
     */
    public function del(){

    }

    /**
     * 退货申请
     */
    public function refund(){

    }

}
