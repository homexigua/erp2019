<?php
namespace app\site\controller\member;

use app\common\controller\SiteMemberBaseController;
use app\common\model\mall\SoOrderModel;


class InvoiceController extends SiteMemberBaseController
{
    /**
     * 初始化方法
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new SoOrderModel();
    }

    /**
     * 发票列表
     */
    public function index(){
        $this->model->where('member_id',$this->member['id'])->where('is_invoice',1)->select();
        return $this->fetch();
    }

    /**
     * 申请发票
     */
    public function add(){
        if($this->request->isAjax()){
            $orderIds = $this->request->param('order_id');
            $this->model->where('id','in',$orderIds)->update([
                '发票抬头'=>'',
                '邮寄地址'=>'',
                '发票状态'=>'',
            ]);
        }
        return $this->fetch();
    }

}
