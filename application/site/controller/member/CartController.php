<?php
namespace app\site\controller\member;

use app\common\controller\SiteMemberBaseController;
use app\common\model\mall\CartModel;


class CartController extends SiteMemberBaseController
{
    /**
     * 初始化方法
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new CartModel();
    }

    /**
     * 购物车列表
     */
    public function index(){
        if($this->request->isAjax()){
            $list = $this->model->getListBySite($this->getSiteId(),$this->member['id']);
            return $list;
        }
        return $this->fetch();
    }

    /**
     * 添加购物车
     */
    public function add(){
        try{
            $skuSn = $this->request->param('sku_sn');
            $goodsNumber = $this->request->param('goods_number');
            $type = $this->request->param('type','plus');
            $this->model->cartAdd($this->member['id'],$skuSn,$goodsNumber,$type);
        }catch (\Exception $e){
            $this->error($e->getMessage());
        }
        $this->success('操作成功！');
    }

    /**
     * 删除购物车一条
     */
    public function del(){
        $id = $this->request->param('id/d');
        if($id<=0) $this->error('参数不正确');
        $this->model->where('member_id',$this->member['id'])->where('id',$id)->delete();
        $this->success('删除成功!');
    }

    /**
     * 清空购物车
     */
    public function clear(){
        $this->model->where('member_id',$this->member['id'])
                    ->where('site_id',$this->getSiteId())
                    ->delete();
        $this->success('操作成功！');
    }
}
