<?php
namespace app\site\controller\member;

use app\common\controller\SiteMemberBaseController;
use app\common\model\mall\EvaluateModel;


class EvaluateController extends SiteMemberBaseController
{
    /**
     * 初始化方法
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new EvaluateModel;
    }

    /**
     * 我的评价
     */
    public function index(){
        if($this->request->isAjax()){
            $pageNumber=$this->request->param('pageNumber/d',1);
            $pageSize=$this->request->param('pageSize/d',10);
            $total = $this->model->where('member_id',$this->member['id'])->count();
            $rows = $this->model->where('member_id',$this->member['id'])->page($pageNumber,$pageSize)->select();
            return ['total'=>$total,'rows'=>$rows];
        }
        return $this->fetch();
    }

    /**
     * 添加评价
     */
    public function add(){
        if($this->request->isAjax()){
            try{
                $orderId = $this->request->param('order_id/d');
                $this->model->addEvaluate($orderId,$evaluateData);
            }catch (\Exception $e){
                $this->error($e->getMessage());
            }
            $this->success('操作成功！');
        }
        return $this->fetch();
    }

    /**
     * 修改评价
     */
    public function edit(){
        if($this->request->isAjax()){
            try{
                $orderId = $this->request->param('order_id/d');
                $this->model->editEvaluate($this->member['id'],$orderId);
            }catch (\Exception $e){
                $this->error($e->getMessage());
            }
            $this->success('操作成功！');
        }
        return $this->fetch();
    }

    /**
     * 匿名
     */
    public function anonymous(){
        $id = $this->request->param('id/d');
        $this->model->anonymous($id);
        $this->success('操作成功');
    }

}
