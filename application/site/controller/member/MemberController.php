<?php
namespace app\site\controller\member;

use app\common\controller\SiteMemberBaseController;
use app\common\model\member\MemberModel;
use think\facade\Cookie;

class MemberController extends SiteMemberBaseController
{
    private $memberInfo;
    /**
     * 初始化方法
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new MemberModel();
        $this->memberInfo = $this->model->where('id',$this->member['id'])->find();
    }

    /**
     * 会员中心首页
     */
    public function index(){

        $this->assign('memberInfo',$this->memberInfo);
        return $this->fetch();
    }

    /**
     * 修改会员资料
     */
    public function edit(){
        if($this->request->isAjax()){
            try{
                $data = $this->request->param();
                $this->validate($data,'app\common\validate\member\MemberValidate.edit');
                $data['id']=$this->member['id'];
                $this->model->editData($data);
            }catch (\Exception $e){
                $this->error($e->getMessage());
            }
            $this->success('操作成功！');
        }
        $this->assign('memberInfo',$this->memberInfo);
        return $this->fetch();
    }

    /**
     * 账户安全
     */
    public function safe(){
        //细分
        return $this->fetch();
    }

    /**
     * 更换手机号
     */
    public function changeMobile(){
        return $this->fetch();
    }

    /**
     * 绑定手机号
     */
    public function bindMobile(){
        if ($this->request->isAjax()){
            $mobile =$this->request->param('mobile');
            $code =$this->request->request('code');
            if (empty($mobile)) $this->error('手机号不能为空！');
            if (empty($mobile)) $this->error('验证码不能为空！');
            $code1 =Cookie::get($mobile.'bind');
            if (empty($code1)) $this->error('验证码已过期！');
            if ($code!=$code1) $this->error('验证码错误！');
            $info = MemberModel::where('mobile',$mobile)->find();
            if (!empty($info)) $this->error('该手机号已注册！');
            MemberModel::where('id',$this->member['id'])->update(['mobile'=>$mobile]);
            $this->success('绑定成功！');
        }
        return $this->fetch();
    }
    /**
     * 更换邮箱
     */
    public function changeEmail(){
        return $this->fetch();
    }

    /**
     * 绑定邮箱
     */
    public function bindEmail(){
        try{
            /*邮箱验证首次验证输入验证码，更换验证需要先验证旧邮箱，然后验证新邮箱的验证码*/
            $data=$this->request->param();
            $data['uid']=$this->getUid();
            $this->userModel->checkEmailValid($data['email'],$data['email_vcode']);
            $result=$this->validate($data,[
                'email|验证邮箱'=>'require',
                'email_vcode|邮件验证码'=>'require',
            ]);
            if(true!==$result){
                exception($result,40001);
            }
            $is_binding=$this->userModel->where('email',$data['email'])->find();
            if($is_binding){
                exception('邮箱已经被其他账号绑定，请确认！');
            }
            $this->userModel->isUpdate(true)->allowField(true)->data($data)->save();
        }catch(\Exception $e){
            $this->error($e->getMessage());
        }
        $this->success('邮箱验证成功！','/member/userinfo/safe.html');
    }

    /**
     * 修改密码
     */
    public function changePass(){
        $data=$this->request->param();
        $userInfo=$this->userModel->get($this->getLoginData()['uid']);
        if(md5(sha1($data['old_password']))!=$userInfo['password']){
            $this->error('旧密码错误');
        }else{
            $data['password']=md5(sha1($data['new_password']));
            $data['uid']=$this->getLoginData()['uid'];
            $this->userModel->isUpdate(true)->allowField(true)->data($data)->save();
            $this->success('修改成功','/home/user/safe.html');
        }
    }

}
