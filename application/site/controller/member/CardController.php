<?php

namespace app\site\controller\member;

use app\common\controller\SiteMemberBaseController;
use app\common\model\CardModel;


class CardController extends SiteMemberBaseController {
    /**
     * 初始化方法
     */
    public function __construct() {
        parent::__construct();
        $this->model = new CardModel();
    }

    /**
     * 我的卡卷 //区分
     */
    public function index() {

    }

    /**
     * 领取卡券
     */
    public function receive() {

    }


    /**
     * 获取可使用卡券列表
     */
    public function uselist() {

    }

    /**
     * 赠送卡券
     */
    public function give() {

    }

}
