<?php

namespace app\site\controller\member;

use app\common\controller\SiteMemberBaseController;
use app\common\model\member\AddressModel;


class AddressController extends SiteMemberBaseController {

    /**
     * 初始化方法
     */
    public function __construct() {
        parent::__construct();
        $this->model = new AddressModel();
    }

    /**
     * 会员地址
     */
    public function index() {
        $list = $this->model->where('member_id', $this->member['id'])->select();
        $this->assign('list', $list);
        return $this->fetch();
    }

    /**
     * 添加地址
     */
    public function add() {
        if ($this->request->isAjax()) {
            try {
                $data = $this->request->param();
                $this->validate($data, 'app\common\validate\member\AddressValidate.add');
                $data['member_id'] = $this->member['id'];
                $info = $this->model->addData($data);
                if ($data['is_default'] == 1) $this->model->setDefault($this->member['id'], $info['id']);
            } catch (\Exception $e) {
                $this->error($e->getMessage());
            }
            $this->success('添加成功！',null,$info);
        }
        return $this->fetch();
    }

    /**
     * 修改地址
     */
    public function edit() {
        if ($this->request->isAjax()) {
            try {
                $data = $this->request->param();
                $this->validate($data, 'app\common\validate\member\AddressValidate.edit');
                $info = $this->model->editData($data);
                if ($data['is_default'] == 1) $this->model->setDefault($this->member['id'], $info['id']);
            } catch (\Exception $e) {
                $this->error($e->getMessage());
            }
            $this->success('添加成功！',null,$info);
        }
        return $this->fetch();
    }

    /**
     * 删除地址
     */
    public function del() {
        try {
            $id = $this->request->param('id/d');
            $this->model->where('id', $id)->delete();
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
        $this->success('操作成功！');
    }

    /*获取当前登录人的所有地址信息*/
    public function getAddrList(){
        $list = $this->model->where('member_id',$this->member['id'])->order('is_default desc')->select();
        if(empty($list)){
            return [];
        }else{
            return $list->toArray();
        }
    }

    /**
     * 设为默认
     */
    public function setdefault() {
        try {
            $id = $this->request->param('id');
            $this->model->setDefault($this->member['id'], $id);
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
        $this->success('操作成功！');
    }

    /**
     * 选择收货地址
     */
    public function select() {

    }
}
