<?php
namespace app\site\controller\member;

use app\common\controller\SiteMemberBaseController;


class MsgController extends SiteMemberBaseController
{
    /**
     * 初始化方法
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new MsgModel();
    }

    /**
     * 消息列表 已读 未读
     */
    public function index(){

    }

    /**
     * 意见反馈列表
     */
    public function feedback(){

    }

    /**
     * 提交意见反馈
     */
    public function add(){

    }

    /**
     * 删除意见反馈
     */
    public function del(){

    }

    /**
     * 清空消息/意见反馈
     */
    public function clear(){

    }

}
