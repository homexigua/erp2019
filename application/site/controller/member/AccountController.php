<?php
namespace app\site\controller\member;

use app\common\controller\SiteMemberBaseController;
use app\common\model\member\AccountModel;


class AccountController extends SiteMemberBaseController
{
    /**
     * 初始化方法
     */
    public function __construct() {
        parent::__construct();
        $this->model = new AccountModel();
    }

    /**
     * 流水
     */
    public function index(){
        try{
            $list = $this->model->where('member_id',$this->member['id'])->page()->select();
            return $list;
        }catch (\Exception $e){
            $this->error($e->getMessage());
        }
        $this->success('操作成功！');
    }

    /**
     * 在线充值
     */
    public function recharge(){
        return $this->fetch();
    }

    /**
     * 提现申请
     */
    public function cash(){

    }


}
