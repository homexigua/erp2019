<?php
namespace app\site\controller;

use app\common\controller\SiteBaseController;
use app\common\model\member\MemberModel;
use org\util\HelpUtil;
use think\facade\Cookie;
use think\facade\Session;

class PublicController extends SiteBaseController
{
    /**
     * 初始化方法
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 用户登录
     * @return mixed
     */
    public function login(){
        $memberModel = new MemberModel();
        if($this->request->isAjax()){
            $redirect = $this->request->param('redirect'); //获取跳转地址
            $redirect = empty($redirect) ? '/' : urldecode($redirect);
            try{
                $vcode=$this->request->param('vcode');
                $account = $this->request->param('account');
                $password = $this->request->param('password');
                if(!captcha_check($vcode)){
                    exception('验证码错误');
                };
                $memberModel->login($account,$password);
            }catch (\Exception $e){
                $this->error($e->getMessage());
            }
            $this->success('操作成功！',$redirect);
        }
        return $this->fetch();
    }

    /**
     * 注册
     */
    public function reg(){

        if($this->request->isAjax()){
            try{
                $data = $this->request->param();
                $data['site_id']=$this->getSiteId();
                $smsVcode = $this->request->param('sms_vcode'); //用户输入的手机验证码
                if (!captcha_check($data['vcode'])) { // 图片验证码
                    exception('验证码错误',40001);
                };
                $result = $this->validate($data, [
                    'mobile|手机号'=>'require',
                    'password|密码'=>'require',
                ]);
                $code = Cookie::get($data['mobile'].':reg');
                if(empty($code)) exception('验证码已过期');
                if($smsVcode!=$code) exception('手机验证码不一致');
                if (true !== $result) {
                    exception($result,40001);
                }
                if($data['agree']!=1){
                    exception('必须同意注册协议才能进行注册！', 40001);
                }
                $memberModel = new MemberModel();
                $memberModel->addData($data);
            }catch (\Exception $e){
                $this->error($e->getMessage());
            }
            $this->success('注册成功！', url('/home/Public/login'));
        }
        return $this->fetch();
    }

    /**
     * 找回密码
     */
    public function findPassword(){
        if($this->request->isAjax()){
            try {
                $memberModel = new MemberModel();
                $data = $this->request->param();
                $info = $memberModel->where('mobile',$data['mobile'])->find();
                $data['id']=$info['id'];
                $result = $this->validate($data, [
                    'password|密码'=>'require',
                ]);
                if (true !== $result) {
                    exception($result,40001);
                }
                $memberModel->editData($data);
            } catch (\Exception $e) {
                $this->error($e->getMessage());
            }
            $this->success('修改成功！');
        }
        return $this->fetch();
    }

    public function sendsms(){
        if($this->request->isAjax()){
            try{
                $mobile = $this->request->param('mobile');
                $desc = $this->request->param('desc');
                $code = HelpUtil::randString(6,1);
                $code = 123456;
//              send_sms($mobile,['template' => '281208', 'data' => [$code, $desc, '30']]);
                $arr = ["注册"=>'reg','找回密码'=>'find','更换手机号'=>'change','绑定手机'=>'bind'];
                $cookieKey = $mobile.':'.$arr[$desc];
                Cookie::set($cookieKey,$code,1800);
            }catch (\Exception $e){
                $this->error('验证码发送不成功！');
            }
            $this->success('发送成功');
        }
        $this->error('请求方式不正确');
    }
}
