<?php
namespace app\site\controller;

use app\common\controller\SiteMemberBaseController;
use app\common\model\mall\CartModel;

class CartController extends SiteMemberBaseController
{
    /**
     * 初始化方法
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new CartModel();
    }
    /**
     * 列表页
     * @return mixed
     */
    public function index(){
        if($this->request->isAjax()){
            $scene = $this->request->param('scene','cart');
            $card_ids = $this->request->param('cart_ids');//已选择的购物车商品
            $cartList = $this->model->getListBySite($this->getSiteId(),$this->member['id'],$scene,$card_ids);
            /*if($cartList && $selectCart!=''){
                $selectCart = explode(',',$selectCart);
                foreach ($cartList['item'] as $k=>$v){
                    if(in_array($v['id'],$selectCart) && $v['status']!=0){
                        $cartList['item'][$k]['check']='checked';
                    }else{
                        $cartList['item'][$k]['check']='';
                    }
                }
            }*/
            return $cartList;
        }
        return $this->fetch();
    }
    /**
     * 添加购物车
     */
    public function cartadd()
    {
        try {
            $skusn = $this->request->param('sku_sn');
            $number = $this->request->param('number/d');
            $type = $this->request->param('type', 'plus'); //plus增加 update更新
            if (!empty($skusn) && $number > 0) {
                $cartId = $this->model->cartAdd($this->member['id'],$skusn, $number, $type);
            } else {
                exception('参数错误！', 40001);
            }
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
        $this->success('加入购物车成功！', '', $cartId);
    }

    /**
     * 删除购物车
     */
    public function cartdel()
    {
        try {
            $cartId = $this->request->param('cart_id');
            if (empty($cartId)) {
                exception('请选择删除商品！', 40001);
            }
            $map[] = ['id','in',$cartId];
            $this->model->cartdel($map);
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
        $this->success('删除成功！');
    }

    /**
     * 清空购物车
     */
    public function cartclear()
    {
        try {
            $this->model->cartdel(['buyer_uid' => $this->member['id']]);
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
        $this->success('清空购物车成功！');
    }

    /*订单确认页*/
    public function orderConfirm(){
        if($this->request->isAjax()){
            $data=$this->request->param();
            $data['buyer_uid']=$this->member['id'];
            $orderModel = new OrderModel();
            $info = $orderModel->cart2order($data);
            $this->success('添加成功','ordersuccess',$info);
            /*$data =[
                'goods'=>[
                    '1000001'=>[
                        'goods_list'=>['220d1abce8446031c62a432561fd2350'=>1,'a0be5b68e52dee3be1af5715ea8713ed'=>2],
                        'bill_remark'=>'订单备注',
                        'use_points'=>1000,
                        'coupon_id'=>1,
                        'b_card'=>'1,2,3',
                    ],
                    '1000002'=>[
                        'goods_list'=>['c50861698b9a13b88512bd2fcc21a97d'=>1,'5da071c4fcdc9b836a282d0031d897b0'=>2],
                        'bill_remark'=>'订单备注',
                        'use_points'=>1000,
                        'coupon_id'=>1,
                        'b_card'=>'1,2,3',
                    ]
                ],
                'buyer_uid'=>14,
                'address_id'=>14,
                'address_info'=>'{"id":14,"buyer_uid":14,"consignee_name":"卫强","consignee_phone":"13399997778","prov":"北京市","city":"丰台区","dist":"","address":"测试地址","is_default":1}',
                'is_zt'=>0,
            ];*/
        }
        return $this->fetch();
    }
    /*订单完成页*/
    public function orderSuccess(){
        return $this->fetch();
    }

}



