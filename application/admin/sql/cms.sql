CREATE TABLE `@table@` (
     `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
     `moudle_id` int(10) DEFAULT '0' COMMENT '模型ID',
     `class_id` int(10) DEFAULT '0' COMMENT '栏目ID',
     `title` varchar(255) DEFAULT '' COMMENT '标题',
     `thumb_url` varchar(255) DEFAULT '' COMMENT '图片地址',
     `keywords` varchar(255) DEFAULT '' COMMENT '关键词',
     `description` varchar(255) DEFAULT '' COMMENT '描述',
     `content` mediumtext COMMENT '正文内容',
     `show_order` int(10) DEFAULT '0' COMMENT '排序',
     `hits` int(10) DEFAULT '0' COMMENT '点击量',
     `create_time` int(10) DEFAULT '0' COMMENT '创建时间',
     `update_time` int(10) DEFAULT '0' COMMENT '更新时间',
     `admin_id` int(10) DEFAULT '0' COMMENT '录入人',
     `is_audit` tinyint(1) DEFAULT '1' COMMENT '0未审核 1已审核',
     PRIMARY KEY (`id`),
     KEY `class_id` (`class_id`),
     KEY `moudle_id` (`moudle_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='@table_comment@';