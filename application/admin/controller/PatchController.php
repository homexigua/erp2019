<?php
namespace app\admin\controller;

use app\common\controller\AdminBaseController;
use app\common\model\PatchModel;
use app\common\traits\adminTraits;
use app\common\traits\selectPageTraits;

class PatchController extends AdminBaseController
{
    /**
     * 无需登录的方法,同时也就不需要鉴权了
     * @var array
     */
    protected $noNeedLogin = [];
    /**
     * 需要鉴权的方法,但需要登录
     * @var array
     */
    protected $needRight = [];

    use adminTraits;
    use selectPageTraits;

    /**
     * 初始化方法
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new PatchModel();
    }

    /**
     * 分页扩展
     * @param $params
     */
    public function _pageFilter(&$params){
        $params['data']=[
            'append'=>'patch_type_text',
            'hidden'=>'patch_content',
        ];
    }
}
