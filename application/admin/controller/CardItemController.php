<?php
namespace app\admin\controller;

use app\common\controller\AdminBaseController;
use app\common\model\CardItemModel;
use app\common\traits\adminTraits;
use app\common\traits\selectPageTraits;

class CardItemController extends AdminBaseController
{
    protected $appendQuery = [];
    use adminTraits;
    use selectPageTraits;
    /**
     * 初始化方法
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new CardItemModel();
    }




}
