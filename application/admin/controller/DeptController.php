<?php
namespace app\admin\controller;

use app\common\controller\AdminBaseController;
use app\common\model\DeptModel;
use app\common\traits\adminTreeTraits;
use app\common\traits\selectPageTraits;

class DeptController extends AdminBaseController
{
    /**
     * 无需登录的方法,同时也就不需要鉴权了
     * @var array
     */
    protected $noNeedLogin = [];
    /**
     * 需要鉴权的方法,但需要登录
     * @var array
     */
    protected $needRight = [];
    /**
     * 追加用户字段
     */
    protected $appendUserFields = ['site_id','trade_id'];

    use adminTreeTraits;
    use selectPageTraits;

    /**
     * 初始化方法
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new DeptModel();
    }

    /**
     * 刷新缓存
     */
    public function refresh(){
        try{
            $this->model->updateCache();
        }catch (\Exception $e){
            $this->error($e->getMessage());
        }
        $this->success('操作成功！');
    }

}
