<?php
namespace app\admin\controller;

use app\common\controller\AdminBaseController;

use app\common\model\TplModel;
use app\common\traits\selectPageTraits;
use org\util\FileUtil;
use think\facade\Cache;
use think\facade\Env;

class ThemeController extends AdminBaseController
{
    /**
     * 需要鉴权的方法,但需要登录
     * @var array
     */
    protected $needRight = [];

    private $specialItemModel;

    use selectPageTraits;

    /**
     * 初始化方法
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new TplModel();
    }

    /**
     * 首页
     * @return mixed
     */
    public function index(){
        $device = $this->request->param('device','pc');
        $list = $this->model->getList($device);
        $this->assign('device',$device);
        $this->assign('list',$list);
        return $this->fetch();
    }

    /**
     * 添加模板
     * @return mixed
     */
    public function add(){
        $device = $this->request->param('device');
        if($this->request->isAjax()){
            try{
                $filename = $this->request->param('filename');
                $content = $this->request->param('content');
                if(strstr($filename,'.php')===false) exception('模板后缀必须为php');
                $fullname = $this->getPath($device).'/'.$filename;
                file_put_contents($fullname,htmlspecialchars_decode($content));
            }catch (\Exception $e){
                $this->error($e->getMessage());
            }
            $this->success('操作成功！',url('index',['device'=>$device]));
        }
        return $this->fetch();
    }

    /**
     * 编辑模板
     * @return mixed
     */
    public function edit(){
        $device = $this->request->param('device');
        $filename = $this->request->param('filename');
        $fullname = $this->getPath($device).'/'.$filename;
        if($this->request->isAjax()){
            try{
                $content = $this->request->param('content');
                file_put_contents($fullname,htmlspecialchars_decode($content));
            }catch (\Exception $e){
                $this->error($e->getMessage());
            }
            $this->success('操作成功！',url('index',['device'=>$device]));
        }
        $content = file_get_contents($fullname);
        $this->assign('filename',$filename);
        $this->assign('content',$content);
        return $this->fetch();
    }

    /**
     * 删除模板
     */
    public function del(){
        try{
            $device = $this->request->param('device');
            $filename = $this->request->param('filename');
            $path = $this->getPath($device);
            FileUtil::unlinkFile($path.'/'.$filename);
        }catch (\Exception $e){
            $this->error($e->getMessage());
        }
        $this->success('操作成功！');
    }

    /**
     * 获取模板文件夹
     * @param $device
     * @return string
     */
    private function getPath($device){
        $webconfig = Cache::get('webconfig');
        $theme = $webconfig['theme'];
        if($device=='pc'){
            $path = Env::get('ROOT_PATH').'public/themes/'.$theme;
        }else{
            $path = Env::get('ROOT_PATH').'public/themes/'.$theme.'_mobile';
        }
        return $path;
    }
}
