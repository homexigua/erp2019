<?php
namespace app\admin\controller;

use app\common\controller\AdminBaseController;
use app\common\model\ConfigModel;
use app\common\traits\adminTraits;

class ConfigController extends AdminBaseController
{
    protected $appendQuery =['site_id','trade_id'];

    /**
     * 无需登录的方法,同时也就不需要鉴权了
     * @var array
     */
    protected $noNeedLogin = [];
    /**
     * 需要鉴权的方法,但需要登录
     * @var array
     */
    protected $needRight = [];
    /**
     * 不允许操作的方法
     * @var array
     */
    protected $noNeedView = ['multi','editable'];


    use adminTraits;
    /**
     * 初始化方法
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new ConfigModel();
    }

    /**
     * 配置首页
     */
    public function index(){
        if($this->request->isAjax()){
            try{
                $config = $this->request->param('config/a');
                if(empty($config) || !is_array($config)) exception('配置项不能为空！');
                $this->model->saveAll(array_values($config));
                $this->model->updateCache();
            }catch (\Exception $e){
                $this->error($e->getMessage());
            }
            $this->success('操作成功！');
        }
        return $this->fetch();
    }

}
