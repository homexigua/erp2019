<?php
namespace app\admin\controller\mall;

use app\common\controller\AdminBaseController;
use app\common\model\mall\ClassModel;
use app\common\traits\adminTraits;
use app\common\traits\selectPageTraits;
use think\facade\Cache;

class ClassController extends AdminBaseController
{
    /**
     * 无需登录的方法,同时也就不需要鉴权了
     * @var array
     */
    protected $noNeedLogin = [];
    /**
     * 无需鉴权的方法,但需要登录
     * @var array
     */
    protected $needRight = [];

    use adminTraits;
    use selectPageTraits;

    /**
     * 初始化方法
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new ClassModel();
    }
    /**
     * 刷新缓存
     */
    public function refresh(){
        try{
            $this->model->updateCache();
        }catch (\Exception $e){
            $this->error($e->getMessage());
        }
        $this->success('操作成功！');
    }
    /**
     * 获取zree列表
     * @return array
     * @throws \Exception
     */
    public function ztree(){
        $list=$this->model->getList();
        $list = array_values($list);
        $arr = [];
        foreach($list as $k=>$v){
            $arr[]=['id'=>$v['id'],'pid'=>$v['pid'],'name'=>$v['name'],'open'=>true,'data_view'=>$v['data_view']];
        }
        return $arr;
    }

    /*获取行业商品类目*/
    public function getselect(){
        $pid = $this->request->param('id');
        $pid = empty($pid)?0:$pid;
        $arealist = $this->model->where(['pid'=>$pid,'trade_id'=>$this->loginInfo['trade_id'],'site_id'=>0])->select();
        return $arealist;
    }

}