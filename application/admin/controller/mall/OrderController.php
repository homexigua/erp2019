<?php
namespace app\admin\controller\mall;

use app\common\controller\AdminBaseController;
use app\common\traits\adminTraits;

class OrderController extends AdminBaseController
{
    /**
     * 无需登录的方法,同时也就不需要鉴权了
     * @var array
     */
    protected $noNeedLogin = [];
    /**
     * 无需鉴权的方法,但需要登录
     * @var array
     */
    protected $needRight = [];

    use adminTraits;
    /**
     * 初始化方法
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = null;
    }

}
