<?php
namespace app\admin\controller\mall;

use app\common\controller\AdminBaseController;
use app\common\model\mall\GoodsSkuModel;
use app\common\traits\adminTraits;
use think\Db;

class GoodsSkuController extends AdminBaseController
{
    /**
     * 无需鉴权的方法,但需要登录
     * @var array
     */
    protected $needRight = [];

    use adminTraits;
    /*
     * 初始化
     */
    public function __construct()
    {
        parent::__construct();

        $this->model=new GoodsSkuModel();
    }

    public function _pageFilter(&$params){
        $params['fields']=['view'];
        $params['data']=[
            'view'=>['mall_goods','mall_goods_sku']
        ];
    }

    /**
     * 选择商品
     */
    public function select(){
        $pageSize = $this->request->param('pageSize/d',15); //每页多少条
        $pageNumber = $this->request->param('pageNumber/d',1); //当前页
        $sortName = $this->request->param('sortName'); //排序字段
        $sortOrder = $this->request->param('sortOrder'); //升序降序
        $order = [];
        if(!empty($sortName) && !empty($sortOrder)){
            $order = [$sortName => $sortOrder];
        }
        $map=[];
        $goodsName = $this->request->param('goods_name');
        if(!empty($goodsName)) $map[]=['goods_name','like','%'.$goodsName.'%'];
        $barCode = $this->request->param('bar_code');
        if(!empty($barCode)) $map[]=['bar_code','like','%'.$barCode.'%'];
        $total = Db::view('mall_goods_sku',true)
          ->view('mall_goods',true,"mall_goods_sku.goods_id=mall_goods.id")
            ->where($map)
          ->count();
        $rows = [];
        if($total>0){
            $rows = Db::view('mall_goods_sku',"id as sku_id,sku_sn,bar_code,goods_id,sku_name,sale_price,min_sale_price")
                      ->view('mall_goods',
                          "goods_name,unit,is_unit_group,unit_group,goods_rate,is_service",
                          "mall_goods_sku.goods_id=mall_goods.id")
                      ->where($map)
                      ->order($order)
                      ->page($pageNumber,$pageSize)
                      ->select()
                      ->toArray();
            foreach($rows as $k=>$v){
                $rows[$k]['group_unit'] = $this->model->formatUnit($v['unit'],$v['is_unit_group'],$v['unit_group']);
                $rows[$k]['goods_name']=$v['goods_name'].' '.$v['sku_name'];
                $rows[$k]['goods_price']=$v['sale_price'];
                $rows[$k]['goods_unit']=$v['unit'];
                $rows[$k]['unit_number']=1;
            }
        }
        return ['total'=>$total,'rows'=>$rows];
    }
}