<?php
namespace app\admin\controller\mall;

use app\common\controller\AdminBaseController;
use app\common\model\mall\GoodsAttrModel;
use app\common\traits\adminTraits;
use app\common\traits\selectPageTraits;

class GoodsAttrController extends AdminBaseController
{
    /**
     * 无需鉴权的方法,但需要登录
     * @var array
     */
    protected $needRight = [];

    use adminTraits;
    use selectPageTraits;
    /*
     * 初始化
     */
    public function __construct()
    {
        parent::__construct();
        $this->model=new GoodsAttrModel();
    }


}