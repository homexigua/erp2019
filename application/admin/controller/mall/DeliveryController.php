<?php
namespace app\admin\controller\mall;

use app\common\controller\AdminBaseController;
use app\common\model\mall\DeliveryModel;
use app\common\traits\adminTraits;
use app\common\traits\selectPageTraits;

class DeliveryController extends AdminBaseController
{

    /**
     * 无需登录的方法,同时也就不需要鉴权了
     * @var array
     */
    protected $noNeedLogin = [];

    /**
     * 无需鉴权的方法,但需要登录
     * @var array
     */
    protected $needRight = [];

    //引入对应traits
    use adminTraits;
    use selectPageTraits;

    /**
    * 初始化方法
    */
    public function __construct()
    {
        parent::__construct();
        $this->model = new DeliveryModel();
    }

    public function _pageFilter(&$params){
        $params['fields']=['data_view'];
        $params['data']=[
            'shop_id'=>$this->admin['shop_id']
        ];
    }


    public function reloadData(){
        if($this->request->isAjax()){
            $data = $this->model->order('show_order asc')->select();
            $options = '';
            foreach ($data as $k=>$v) {
                $options .= '<option value="'.$v['id'].'">'.$v['delivery_name'].'</option>';
            }
            return $options;
        }
    }
}