<?php
namespace app\admin\controller\mall;

use app\common\controller\AdminBaseController;
use app\common\model\mall\UnitModel;
use app\common\traits\adminTraits;
use app\common\traits\selectPageTraits;

class UnitController extends AdminBaseController
{

    /**
     * 无需登录的方法,同时也就不需要鉴权了
     * @var array
     */
    protected $noNeedLogin = [selectpage];

    /**
     * 无需鉴权的方法,但需要登录
     * @var array
     */
    protected $needRight = [];

    //引入对应traits
    use adminTraits;
    use selectPageTraits;

    /**
    * 初始化方法
    */
    public function __construct()
    {
        parent::__construct();
        $this->model = new UnitModel();
    }

    /*更改状态*/
    public function setStatus(){
         $id = $this->request->param('id');
         $status = $this->request->param('status');
         $this->model->where('id',$id)->setField('status',$status);
         $this->success('更改成功');
    }

    /*获取商品单位组*/
    public function getUnit(){
        $Isbase = $this->request->param('is_base');
        $list = $this->model->getUnit($Isbase);
        return $list;
    }

    /*获取所有可选择单位*/
    public function getAll(){
        $unit = $this->model->column('unit');
        $unit = implode(',',$unit);
        $unit = explode(',',$unit);
        return array_values(array_unique($unit));
    }


}