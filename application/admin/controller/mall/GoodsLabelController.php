<?php
namespace app\admin\controller\mall;

use app\common\controller\AdminBaseController;
use app\common\traits\adminTraits;

class GoodsLabelController extends AdminBaseController
{

    /**
     * 无需登录的方法,同时也就不需要鉴权了
     * @var array
     */
    protected $noNeedLogin = [];

    /**
     * 无需鉴权的方法,但需要登录
     * @var array
     */
    protected $noNeedRight = [];
    /**
     * 添加编辑打开方式
     * @var string
     */
    protected $openTab = 'dialog'; //iframe dialog
    /**
     * 模型对象
     * @var \think\Model
     */
    protected $model = null;
    //引入对应traits
    use adminTraits;

    /**
     * 初始化方法
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new \app\common\model\mall\GoodsLabelModel();
    }

    /**
     * 首頁
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function index(){
        if($this->request->isAjax()){
            $data=$this->request->param('item/a');
            $dataList=[];
            $i=0;
            foreach($data as $id=>$name){
                $i+=1;
                if($i<5 && empty($name)){
                    $this->error('内置规则不能为空');
                }
                $dataList[]=['id'=>$id,'name'=>$name];
            }
            $this->model->saveAll($dataList);
            $this->success('操作成功！');
        }
        $this->model->initData($this->loginInfo['site_id']);
        $colorList = $this->model->where('site_id',$this->loginInfo['site_id'])->select();
        $this->assign('colorList',$colorList);
        return $this->fetch();
    }

}