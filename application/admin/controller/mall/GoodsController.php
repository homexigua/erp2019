<?php
namespace app\admin\controller\mall;

use app\common\controller\AdminBaseController;
use app\common\model\mall\GoodsModel;
use app\common\model\mall\GoodsSkuModel;
use app\common\traits\adminTraits;

class GoodsController extends AdminBaseController
{
    /**
     * 无需鉴权的方法,但需要登录
     * @var array
     */
    protected $needRight = [];


    use adminTraits;
    /*
     * 初始化
     */
    public function __construct()
    {
        parent::__construct();
        $this->model=new GoodsModel();
    }

    /*列表编辑sku信息*/
    public function editSku(){
        if ($this->request->has('ids') && $this->request->has('params')) {
            try {
                $ids = $this->request->param('ids');
                $params = $this->request->param('params');
                $model= new GoodsSkuModel();
                $model->multi($ids, $params);
            } catch (\Exception $e) {
                $this->error($e->getMessage());
            }
            $this->success('操作成功！');
        }
        $this->error('参数错误！');
    }

    /*阶梯价*/
    public function ladderPrice(){
        return $this->fetch();
    }



}