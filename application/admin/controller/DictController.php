<?php
namespace app\admin\controller;

use app\common\controller\AdminBaseController;
use app\common\model\DictModel;
use app\common\traits\adminTraits;
use app\common\traits\selectPageTraits;

class DictController extends AdminBaseController
{
    /**
     * 无需登录的方法,同时也就不需要鉴权了
     * @var array
     */
    protected $noNeedLogin = [];
    /**
     * 需要鉴权的方法
     * @var array
     */
    protected $needRight = ['index'];

    use adminTraits;
    use selectPageTraits;

    /**
     * 初始化方法
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new DictModel();
    }

    /**
     * 刷新缓存
     */
    public function refresh(){
        try{
            $this->model->updateCache();
        }catch (\Exception $e){
            $this->error($e->getMessage());
        }
        $this->success('操作成功！');
    }
}
