<?php

namespace app\admin\controller\customer;

use app\common\controller\AdminBaseController;
use app\common\model\customer\LogTplModel;
use app\common\traits\adminTraits;

class LogTplController extends AdminBaseController
{
    /**
     * 需要鉴权的方法,但需要登录
     * @var array
     */
    protected $needRight = [];
    /**
     * 追加用户字段
     */
    protected $appendUserFields = ['site_id','trade_id'];

    use adminTraits;

    /**
     * 初始化方法
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new LogTplModel();
    }
}