<?php

namespace app\admin\controller\customer;

use app\common\controller\AdminBaseController;
use app\common\model\customer\LabelModel;
use app\common\traits\adminTraits;
use app\common\traits\selectPageTraits;

class LabelController extends AdminBaseController
{
    /**
     * 需要鉴权的方法,但需要登录
     * @var array
     */
    protected $needRight = [];
    /**
     * 追加用户字段
     */
    protected $appendUserFields = ['site_id','trade_id','admin_id'];

    use adminTraits;
    use selectPageTraits;
    /**
     * 初始化方法
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new LabelModel();
    }
}