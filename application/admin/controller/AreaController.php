<?php
namespace app\admin\controller;

use app\common\controller\AdminBaseController;
use app\common\model\AreaModel;
use app\common\traits\adminTraits;

class AreaController extends AdminBaseController
{
    /**
     * 无需登录的方法,同时也就不需要鉴权了
     * @var array
     */
    protected $noNeedLogin = [];
    /**
     * 需要鉴权的方法,但需要登录
     * @var array
     */
    protected $needRight = [];

    use adminTraits;

    /**
     * 初始化方法
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new AreaModel();
    }

    /**
     * 首页
     * @return array|mixed|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function index(){
        if($this->request->isAjax()){
            $pid = $this->request->param('pid/d',0);
            $list = $this->model->where('pid',$pid)->select();
            return $list;
        }
        return $this->fetch();
    }

}
