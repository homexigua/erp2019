<?php
namespace app\admin\controller;

use app\common\controller\AdminBaseController;
use app\common\model\DictClassModel;
use app\common\traits\adminTreeTraits;

class DictClassController extends AdminBaseController
{
    /**
     * 无需登录的方法,同时也就不需要鉴权了
     * @var array
     */
    protected $noNeedLogin = [];
    /**
     * 需要鉴权的方法,但需要登录
     * @var array
     */
    protected $needRight = [];
    /**
     * 不允许操作的方法
     * @var array
     */
    protected $noNeedView = [];

    use adminTreeTraits;

    /**
     * 初始化方法
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new DictClassModel();
    }

    /**
     * 获取数据列表
     */
    public function getlist(){
        $list=$this->model->getList();
        $list = array_values($list);
        foreach($list as $k=>$v){
            $list[$k]['name'] = $v['name']."({$v['code']})";
        }
        return $list;
    }

    /**
     * 刷新缓存
     */
    public function refresh(){
        try{
            $this->model->updateCache();
        }catch (\Exception $e){
            $this->error($e->getMessage());
        }
        $this->success('操作成功！');
    }
}
