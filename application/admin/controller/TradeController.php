<?php
namespace app\admin\controller;

use app\common\controller\AdminBaseController;
use app\common\model\TradeModel;
use app\common\traits\adminTraits;
use app\common\traits\selectPageTraits;

class TradeController extends AdminBaseController
{
    /**
     * 追加查询条件
     * @var array
     */
    protected $appendQuery = [];
    /**
     * 需要鉴权的方法,需要登录
     * @var array
     */
    protected $needRight = [];

    use adminTraits;
    use selectPageTraits;

    /**
     * 初始化方法
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new TradeModel();
    }
}
