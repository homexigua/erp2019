<?php
namespace app\admin\controller\member;

use app\common\controller\AdminBaseController;
use app\common\model\member\ChangeLogModel;
use app\common\traits\adminTraits;

class ChangeLogController extends AdminBaseController
{
    /**
     * 无需登录的方法,同时也就不需要鉴权了
     * @var array
     */
    protected $noNeedLogin = [];
    /**
     * 需要鉴权的方法,但需要登录
     * @var array
     */
    protected $needRight = [];

    use adminTraits;

    /**
     * 初始化方法
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new ChangeLogModel();
    }
    /**
     * 搜索器配置
     */
    public function _pageFilter(&$params){
        $params['fields'] = ['view'];
        $params['data'] = [
            'view'=>['member_change_log','member']
        ];
    }
}
