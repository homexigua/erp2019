<?php
namespace app\admin\controller\member;

use app\common\controller\AdminBaseController;
use app\common\model\member\AccountModel;
use app\common\traits\adminTraits;

class AccountController extends AdminBaseController
{
    /**
     * 无需登录的方法,同时也就不需要鉴权了
     * @var array
     */
    protected $noNeedLogin = [];
    /**
     * 需要鉴权的方法,但需要登录
     * @var array
     */
    protected $needRight = [];

    use adminTraits;

    /**
     * 初始化方法
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new AccountModel();
    }
    /**
     * 搜索器配置
     */
    public function _pageFilter(&$params){
        $params['fields'] = ['view'];
        $params['data'] = [
            'view'=>['member_account','member']
        ];
    }

    /**
     * 清理未付款订单
     */
    public function clear(){
        try{
            $this->model->where('is_pay',0)->delete();
        }catch (\Exception $e){
            $this->error($e->getMessage());
        }
        $this->success('操作成功！');
    }
}
