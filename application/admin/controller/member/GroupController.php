<?php
namespace app\admin\controller\member;

use app\common\controller\AdminBaseController;
use app\common\model\member\GroupModel;
use app\common\model\member\RuleModel;
use app\common\traits\adminTraits;

class GroupController extends AdminBaseController
{
    /**
     * 无需登录的方法,同时也就不需要鉴权了
     * @var array
     */
    protected $noNeedLogin = [];
    /**
     * 需要鉴权的方法,但需要登录
     * @var array
     */
    protected $needRight = [];

    use adminTraits;

    /**
     * 初始化方法
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new GroupModel();
    }

}
