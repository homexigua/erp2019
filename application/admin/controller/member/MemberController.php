<?php
namespace app\admin\controller\member;

use app\common\controller\AdminBaseController;
use app\common\model\member\MemberModel;
use app\common\traits\adminTraits;
use app\common\traits\selectPageTraits;

class MemberController extends AdminBaseController
{
    /**
     * 无需登录的方法,同时也就不需要鉴权了
     * @var array
     */
    protected $noNeedLogin = [];
    /**
     * 需要鉴权的方法,但需要登录
     * @var array
     */
    protected $needRight = [];
    /**
     * 追加查询条件
     * @var array
     */
    protected $appendQuery = [];
    use adminTraits;
    use selectPageTraits;

    /**
     * 初始化方法
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new MemberModel();
    }

}
