<?php
namespace app\admin\controller;

use app\common\controller\AdminBaseController;
use org\util\TreeUtil;

class IndexController extends AdminBaseController
{
    /**
     * 无需登录的方法,同时也就不需要鉴权了
     * @var array
     */
    protected $noNeedLogin = [];
    /**
     * 需要鉴权的方法
     * @var array
     */
    protected $needRight = [];
    /**
     * 不允许操作的方法
     * @var array
     */
    protected $noNeedView = [];
    /**
     * 初始化方法
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 后台首页
     */
    public function index(){
        $menus = $this->getMenus();
        $this->assign('menus',json_encode($menus,JSON_UNESCAPED_UNICODE));
        return $this->fetch();
    }

    /**
     * 后台主页
     */
    public function main(){
        return $this->fetch();
    }

    /**
     * 获取菜单
     */
    public function getMenus(){
        $auth = \org\Auth::instance('admin');
        $menuList = $auth->getAuthList($this->loginInfo['id'],2);
        $menu = [];
        foreach($menuList as $k=>$v){
            if($v['is_menu']!=1) continue;
            $row['id'] = $v['name'];
            $row['rid'] = $v['id'];
            $row['pid'] = $v['pid'];
            $row['text'] = $v['title'];
            $row['icon'] = empty($v['icon']) ? 'fa fa-circle-o' : $v['icon'];
            if(strpos($v['name'],'.html')!==false){ //有链接地址
                $row['url'] = html_out($v['name']);
                $row['close'] = true;
                $row['targetType'] = "iframe-tab";
            }
            $menu[] = $row;
        }
        $menu = TreeUtil::channelLevel($menu, 0, "children",'&nbsp;','rid');
        foreach($menu as $k=>$v){
            if(!empty($v['children']) && $k==0) $menu[$k]['isOpen'] = true;
        }
        return $menu;
    }

    /**
     * 子菜单地址路由
     */
    public function route(){
        $url = $this->request->param('url');
        return redirect($url);
    }
}
