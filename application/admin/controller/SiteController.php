<?php
namespace app\admin\controller;

use app\common\controller\AdminBaseController;
use app\common\model\SiteModel;
use app\common\traits\adminTraits;
use app\common\traits\selectPageTraits;

class SiteController extends AdminBaseController
{
    /**
     * 追加查询条件
     * @var array
     */
    protected $appendQuery = [];
    /**
     * 需要鉴权的方法,需要登录
     * @var array
     */
    protected $needRight = [];
    /**
     * 追加用户字段
     */
    protected $appendUserFields = ['site_id'];

    use adminTraits;
    use selectPageTraits;

    /**
     * 初始化方法
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new SiteModel();
    }

    /**
     * 分页检索
     * @param $params
     */
    public function _pageFilter(&$params){
        if($this->getAccountType()=='is_trade'){
            $params['custom'] = [
                'filter'=>['trade_id'=>$this->loginInfo['trade_id']],
                'op'=>['trade_id'=>'='],
            ];
        }
    }

    /**
     * 不分页检索
     * @param $params
     */
    public function _filter(&$params){
        if($this->getAccountType()=='is_trade'){
            $params['custom'] = [
                'filter'=>['trade_id'=>$this->loginInfo['trade_id']],
                'op'=>['trade_id'=>'='],
            ];
        }
    }

    /*店铺信息设置*/
    public function config(){
        $info = $this->model->getInfo(['id'=>$this->loginInfo['site_id']]);
        $this->assign('info',$info);
        return $this->fetch();
    }
    
}
