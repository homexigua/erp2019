<?php
namespace app\admin\controller;

use app\common\controller\AdminBaseController;
use app\common\model\cms\MoudleModel;
use app\common\model\SpecialItemModel;
use app\common\model\SpecialModel;
use app\common\traits\adminTraits;
use app\common\traits\selectPageTraits;

class SpecialController extends AdminBaseController
{
    /**
     * 需要鉴权的方法,但需要登录
     * @var array
     */
    protected $needRight = [];

    private $specialItemModel;

    use adminTraits;
    use selectPageTraits;

    /**
     * 初始化方法
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new SpecialModel();
        $this->specialItemModel = new SpecialItemModel();
    }

    /**
     * 查看专题下的文章
     * @return mixed
     */
    public function items(){
        $moudleModel = new MoudleModel();
        $specialId = $this->request->param('special_id/d');
        $moudleId = $this->request->param('moudle_id',1);
        $info = $this->model->where('id',$specialId)->find();
        $moudleList = $moudleModel->where('status',1)->order('show_order asc')->select();
        $contentIds = $this->specialItemModel->getContentIdsBySpecialId($moudleId,$specialId);
        $map[]=['id','in',$contentIds];
        $list = cms_model($moudleId)->where($map)->select();
        $this->assign('info',$info);
        $this->assign('moudle_id',$moudleId);
        $this->assign('moudle_list',$moudleList);
        $this->assign('list',$list);
        return $this->fetch();
    }

    /**
     * 删除专题内容
     */
    public function delitem(){
        try{
            $id = $this->request->param('id');
            $this->specialItemModel->where('content_id','in',$id)->delete();
        }catch (\Exception $e){
            $this->error($e->getMessage());
        }
        $this->success('操作成功！');
    }
}
