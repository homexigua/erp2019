<?php

namespace app\admin\controller;

use app\common\controller\AdminBaseController;
use app\common\model\ConfigGroupModel;
use app\common\traits\adminTraits;

class ConfigGroupController extends AdminBaseController {

    protected $appendQuery = [];
    use adminTraits;

    /**
     * 初始化方法
     */
    public function __construct() {
        parent::__construct();
        $this->model = new ConfigGroupModel();
    }

}
