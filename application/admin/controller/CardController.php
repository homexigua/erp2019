<?php

namespace app\admin\controller;

use app\common\controller\AdminBaseController;
use app\common\model\CardModel;
use app\common\traits\adminTraits;
use app\common\traits\selectPageTraits;

class CardController extends AdminBaseController
{
    /**
     * 追加查询条件
     * @var array
     */
    protected $appendQuery = ['trade_id','site_id'];
    /**
     * 需要鉴权的方法,需要登录
     * @var array
     */
    protected $needRight = [];

    use adminTraits;
    use selectPageTraits;

    /**
     * 初始化方法
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new CardModel();
    }

    /**
     * 卡券发送
     * @return mixed
     */
    public function send(){
        return $this->fetch();
    }
}