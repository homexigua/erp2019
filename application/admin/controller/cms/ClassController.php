<?php
namespace app\admin\controller\cms;

use app\common\controller\AdminBaseController;
use app\common\model\cms\ClassModel;
use app\common\traits\adminTreeTraits;

class ClassController extends AdminBaseController
{
    /**
     * 需要鉴权的方法,但需要登录
     * @var array
     */
    protected $needRight = [];

    use adminTreeTraits;
    /*
     * 初始化
     */
    public function __construct()
    {
        parent::__construct();
        $this->model=new ClassModel();
    }

    /**
     * 栏目选择
     */
    public function select(){
        $moudleId = $this->request->param('moudle_id/d',1);
        $list = $this->model->formatContentSelect($moudleId);
        return json($list);
    }
    /**
     * 扩展信息
     */
    public function extend(){
        if($this->request->isAjax()){
            try{
                $data=$this->request->param();
                $this->model->saveExtend($data);
                $this->model->updateCache();
            }catch (\Exception $e){
                $this->error($e->getMessage());
            }
            $this->success('操作成功！');
        }
        $id = $this->request->param('id/d');
        $map[]=['id','=',$id];
        $info = $this->model->getInfo($map);
        $extend = $this->model->formatExtend($info['extend']);
        $this->assign('extend',$extend);
        return $this->fetch();
    }
    /**
     * 刷新缓存
     */
    public function refresh(){
        try{
            $this->model->updateCache();
        }catch (\Exception $e){
            $this->error($e->getMessage());
        }
        $this->success('操作成功！');
    }
}