<?php
namespace app\admin\controller\cms;

use app\common\controller\AdminBaseController;
use app\common\model\cms\ClassGroupModel;
use app\common\traits\adminTraits;
use app\common\traits\selectPageTraits;

class ClassGroupController extends AdminBaseController
{
    use adminTraits;
    use selectPageTraits;
    /*
     * 初始化
     */
    public function __construct()
    {
        parent::__construct();
        $this->model=new ClassGroupModel();
    }

}