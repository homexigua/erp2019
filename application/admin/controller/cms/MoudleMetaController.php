<?php
namespace app\admin\controller\cms;

use app\common\controller\AdminBaseController;
use app\common\model\cms\MoudleMetaModel;
use app\common\traits\adminTraits;

class MoudleMetaController extends AdminBaseController
{
    use adminTraits;
    /*
     * 初始化
     */
    public function __construct()
    {
        parent::__construct();
        $this->model=new MoudleMetaModel();
    }

    /**
     * 更新缓存
     */
    public function refresh(){
        try{
            $moudleId = $this->request->param('moudle_id/d');
            $this->model->updateCache($moudleId);
        }catch (\Exception $e){
            $this->error($e->getMessage());
        }
        $this->success('操作成功！');
    }
}