<?php
namespace app\admin\controller;

use app\common\controller\AdminBaseController;
use org\ConfigHelper;

class TpConfigController extends AdminBaseController
{
    /**
     * 无需登录的方法,同时也就不需要鉴权了
     * @var array
     */
    protected $noNeedLogin = [];
    /**
     * 需要鉴权的方法,但需要登录
     * @var array
     */
    protected $needRight = [];


    /**
     * 初始化方法
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new ConfigHelper();
    }

    /**
     * 上传参数配置
     */
    public function upload(){
        if($this->request->isAjax()){
            try{
                $data = $this->request->param();
                if($data['type']=='local'){
                    if($data['local']['prefix']!='./upload') exception('本地上传不建议修改目录！');
                }
                if($data['type']=='alioss'){
                    if(empty($data['alioss']['ak'])) exception('ak参数不能为空！');
                    if(empty($data['alioss']['sk'])) exception('sk参数不能为空！');
                    if(empty($data['alioss']['endpoint'])) exception('endpoint参数不能为空！');
                    if(empty($data['alioss']['bucket'])) exception('bucket参数不能为空！');
                    if(empty($data['alioss']['prefix'])) exception('prefix参数不能为空！');
                }
                if($data['type']=='qiniu'){
                    if(empty($data['qiniu']['ak'])) exception('ak参数不能为空！');
                    if(empty($data['qiniu']['sk'])) exception('sk参数不能为空！');
                    if(empty($data['qiniu']['bucket'])) exception('bucket参数不能为空！');
                    if(empty($data['qiniu']['prefix'])) exception('prefix参数不能为空！');
                }
                $this->model->setConfig('upload',$data);
            }catch (\Exception $e){
                $this->error($e->getMessage());
            }
            $this->success('操作成功！');
        }
        $config = $this->model->getConfig('upload');
        $this->assign('config',$config);
        return $this->fetch();
    }

    /**
     * 路由参数设置
     * @return mixed
     */
    public function router(){
        if($this->request->isAjax()){
            try{
                $items=$this->request->param('items/a');
                $this->model->setHomeRoute($items);
            }catch (\Exception $e){
                $this->error($e->getMessage());
            }
            $this->success('操作成功！');
        }
        $route = $this->model->getHomeRoute();
        $this->assign('route',$route);
        return $this->fetch();
    }
}
