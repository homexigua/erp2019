{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="nav-tabs-custom">
            {:D('Rule','auth')->getSubMenu('8')}
            <div class="box-body">
                <div class="toolbar">
                    <div class="pull-left">
                        <a href="/admin/theme/index.html?device=pc" class="btn btn-default {eq name="device" value="pc"}active{/eq}">电脑模板</a>
                        <a href="/admin/theme/index.html?device=mobile" class="btn btn-default {eq name="device" value="mobile"}active{/eq}">手机模板</a>
                    </div>
                    <div class="pull-right">
                        <a href="/admin/theme/add.html?device={$device}" class="btn btn-primary"><i class="fa fa-plus"></i> 添加</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="table-responsive">
                    <table class="table" id="grid">
                        <tr>
                            <th>文件名</th>
                            <th width="180">创建时间</th>
                            <th width="180">操作</th>
                        </tr>
                        {volist name="list" id="vo"}
                        <tr>
                            <td>{$vo.basename}</td>
                            <td>{$vo.filemtime|date="Y-m-d H:i:s"}</td>
                            <td>
                                <a href="/admin/theme/edit.html?device={$device}&filename={$vo.basename}">编辑</a>
                                <a href="javascript:;" onclick="utils.doAction(this,'refreshPage')"
                                   data-params="filename={$vo.basename}&device={$device}"
                                   data-url="/admin/theme/del.html"
                                >删除</a>
                            </td>
                        </tr>
                        {/volist}
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>
</body>
{/block}