{extend name="admin@public/layout" /}
{block name="head"}
{:D('Static')->loadCodeEditor()}
{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content-header">
        <h1>模板风格</h1>
        <ol class="breadcrumb">
            <li>
                <a href="javascript:;" class="tab-href" data-url="/admin/index/main.html" data-title="首页">
                    <i class="fa fa-dashboard"></i> 首页
                </a>
            </li>
            <li><a href="/admin/theme/index.html?device={$Request.param.device}">模板风格</a></li>
            <li class="active">修改模板风格</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-body">
                <form id="form" data-callfn="url">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">模板文件名</span>
                            <input type="text" class="form-control" name="filename" value="{$filename}" />
                        </div>
                    </div>
                    <div class="form-group">
                        <textarea type="textarea" class="form-control" id="content" name="content"></textarea>
                    </div>
                    <div class="fixed-foot text-right">
                        <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                        {:FormBuild::_button('立即保存',['class'=>'btn btn-info btn-sub'])}
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
<div style="height:15px;"></div>
</body>
<script>
    $(function () {
        var codeEditor = formHelper.codeEditor('content','htmlmixed');
        codeEditor.setSize('auto','500px');
        utils.ajaxSubForm({
            beforeCheck: function (curform) {
                $('#content').val(codeEditor.getValue());
            }
        });
    });
</script>
{/block}