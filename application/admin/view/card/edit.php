{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
    <div class="wrapper">
        <section class="content">
            <div class="box">
                <div class="box-body">
                <form class="form-horizontal" id="form" data-callfn="refreshTable">
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">卡券名称</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" name="card_title" value="{$info.card_title}" placeholder="请输入卡券名称" datatype="*" />
                        </div>
                        <label class="col-sm-2 control-label require">卡券副标题</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" name="card_sub_title" value="{$info.card_sub_title}" placeholder="请输入卡券副标题" datatype="*" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">卡券类型</label>
                        <div class="col-sm-4">
                            <select name="card_type" class="form-control">
                                <option value="1">现金券</option>
                                <option value="2">折扣券</option>
                            </select>
                        </div>
                        <label class="col-sm-2 control-label">减免金额</label>
                        <div class="col-sm-4">
                            <div class="i-checks form-inline">
                                <label style="font-weight:normal;">
                                    <input type="text" class="form-control" placeholder="请输入减免金额" name="reduce_cost" data-type="*" value="{$info.reduce_cost}" style="width:80px;" />
                                    元
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">卡券类型</label>
                        <div class="col-sm-4">
                            <select name="card_type" class="form-control">
                                <option value="1">现金券</option>
                                <option value="2">折扣券</option>
                            </select>
                        </div>
                        <label class="col-sm-2 control-label">打折额度</label>
                        <div class="col-sm-4">
                            <div class="i-checks form-inline">
                                <label style="font-weight:normal;">
                                    <input type="text" class="form-control" placeholder="请输入打折额度" name="discount" data-type="*" value="{$info.discount}" style="width:80px;" />
                                    折
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">启用额度</label>
                        <div class="col-sm-4">
                            <div class="i-checks form-inline">
                                <label style="font-weight:normal;">
                                    <input type="text" class="form-control" placeholder="订单最低金额" name="discount" data-type="*" value="{$info.limit_cost}" style="width:165px;" />
                                    元
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">使用时间</label>
                        <div class="col-sm-10">
                            <div class="i-checks">
                                <label style="padding-bottom:10px;font-weight:normal;">
                                    <input type="radio" name="data_type" value="2" checked/> 固定日期区间
                                </label>
                                <label style="padding-bottom:10px;font-weight:normal;">
                                    <input class="form-control f-laydate" AUTOCOMPLETE="off" name="data_type" value="1"  data-option='{"range":true,"format":"yyyy/MM/dd"}' placeholder="请选择卡券的使用日期区间" datatype="*">
                                </label>
                            </div>
                            <div class="i-checks form-inline">
                                <label style="font-weight:normal;">
                                    <input type="radio" name=" " value="2" /> 领劵后 ，
                                    <input type="text" class="form-control" name="begin_day" data-type="*" value="{$info.begin_day}" style="width:80px;" />
                                    天生效 ，有效期
                                    <input type="text" class="form-control" name="fixed_day" data-type="*" value="{$info.fixed_day}" style="width:80px;" />
                                    天
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">卡券库存</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="card_stock" value="{$info.card_stock}" placeholder="请输入库存" datatype="*" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">卡券使用说明</label>
                        <div class="col-sm-9 form-control-static">
                            {:FormBuild::_textarea('channel_desc','',['placeholder'=>'请输入卡券使用说明','rows'=>'3'])}
                        </div>
                    </div>
                    <div class="fixed-foot text-right">
                        <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                        <button type="button" class="btn btn-info btn-sub">立即保存</button>
                    </div>
                </form>
            </div>
            </div>
        </section>
    </div>
    <div style="height:15px;"></div>
</body>
<script>
    $(function () {
        formHelper.initForm();
        utils.ajaxSubForm();
    });
</script>
{/block}