{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="box">
            <div class="box-body">
                <form class="form-horizontal" id="form" data-callfn="refreshTable">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">用户类型</label>
                        <div class="col-sm-9">
                            <select name="member_id" class="form-control">
                                <option value="1">全部会员</option>
                                <option value="2">VIP会员</option>
                                <option value="3">自定义会员</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">每人限领</label>
                        <div class="col-sm-9">
                            <div class="i-checks form-inline">
                                <label style="font-weight:normal;">
                                    <input type="text" class="form-control" placeholder="请输入限领次数" name="get_limit" data-type="*" value="{$info.get_limit}" style="width:165px;" />
                                    次
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">发放开始时间</label>
                        <div class="col-sm-3">
                            {:FormBuild::_laydate('date','')}
                        </div>
                        <label class="col-sm-2 control-label">发放结束时间</label>
                        <div class="col-sm-4">
                            {:FormBuild::_laydate('date','')}
                        </div>
                    </div>
                    <div class="fixed-foot text-right">
                        <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                        <button type="button" class="btn btn-info btn-sub">立即保存</button>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">发放数量</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" placeholder="请输入允许发放的卡券数量,0为不限制" name="staff_num" value=""  datatype="n" autocomplete="off"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
<div style="height:15px;"></div>
</body>
<script>
    $(function () {
        formHelper.initForm();
        utils.ajaxSubForm();
    });
</script>
{/block}