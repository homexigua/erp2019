<?php
/**
 * Created by PhpStorm.
 * User: xiuzhen
 * Date: 2019-02-20
 * Time: 18:49
 */
?>
{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="nav-tabs-custom">
            <div class="box-body">
                <div class="toolbar">
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-primary" onclick="doAction.add();">
                            <i class="fa fa-plus"></i> 添加卡券
                        </button>
                    </div>
                </div>
                <table id="grid"></table>
            </div>
        </div>
    </section>
</div>
</body>
<script>
utils.btable({
        url: 'index.html',
        columns: [
            {field: 'id', title: 'ID'},
            // {field: 'site_id', title: '站点ID'},
            {field: 'card_type', title: '卡券类型'},
            {field: 'card_title', title: '卡券名称'},
            {field: 'card_stock', title: '卡券库存'},
            {field: 'send_number', title: '发放数量'},
            // {field: 'begin_time', title: '开始日期'},
            // {field: 'end_time', title: '结束日期'},
            {field: 'limit_cost', title: '启用金额'},
            // {field: 'is_use', title: '是否使用'},
            {
                field: 'operate', title: '操作', width: 100,
                formatter: function (value, row, index) {
                var html = '';
                html += '<a href="javascript:;" onclick="doAction.send(' + row.id + ')">发放</a> ';
                html += '<a href="javascript:;" onclick="doAction.edit(' + row.id + ')">编辑</a> ';
                    html += '<a href="javascript:;" onclick="utils.doAction(this,\'refreshTable\')" ' +
                        'data-params="id=' + row.id + '" data-url="/admin/card/del.html">删除</a>';
                return html;
            }
            }
        ]
    });
    var doAction = {
    add: function () {
        var rows = $('#grid').bootstrapTable('getSelections');
        var pid = rows.length==0 ? 0 : rows[0].id;
        utils.openDialog({
                title: '添加卡券',
                area: ['800px', '550px'],
                content: '/admin/card/add.html'
            });
        },
    edit: function (id) {
        utils.openDialog({
                title: '修改卡券',
                area: ['800px', '550px'],
                content: '/admin/card/edit.html?id=' + id
            });
        },
    send: function (id) {
        utils.openDialog({
                title: '发放卡券',
                area: ['800px', '550px'],
                content: '/admin/card/send.html?id=' + id
            });
        },

};
</script>
{/block}