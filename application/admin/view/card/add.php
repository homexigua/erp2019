{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
    <div class="wrapper">
        <section class="content">
            <div class="box">
                <div class="box-body">
                    <form class="form-horizontal" id="form" data-callfn="refreshPage">
                 <div class="box-body">
                     <div class="form-group">
                         <label class="col-sm-2 control-label">卡券标题</label>
                         <div class="col-sm-4">
                             <input type="text" class="form-control" placeholder="请输入卡券名称" name="card_title" value=""  datatype="*" nullmsg="请输入卡券名称" />
                         </div>
                         <label class="col-sm-2 control-label">卡券副标题</label>
                         <div class="col-sm-4">
                             <input type="text" class="form-control" placeholder="请输入卡券副标题" name="card_sub_title" value=""  datatype="*" nullmsg="请输入卡券副标题" />
                         </div>
                     </div>
                     <div class="form-group">
                         <label class="col-sm-2 control-label">卡券类型</label>
                         <div class="col-sm-4">
                             <select name="card_type" id="card_type" class="form-control">
                                 <option value="1">现金券</option>
                                 <option value="2">折扣券</option>
                             </select>
                         </div>
                         <label class="col-sm-2 control-label">卡券库存</label>
                         <div class="col-sm-4">
                             <input type="text" class="form-control" placeholder="请输入卡券数量" name="card_stock" value=""  datatype="n" autocomplete="off" />
                         </div>
                     </div>
                     <div class="form-group">
                         <label class="col-sm-2 control-label">启用额度</label>
                         <div class="col-sm-4">
                             <div class="input-group">
                                 <input type="text" class="form-control" placeholder="订单最低金额" name="limit_cost" data-type="*" value="" />
                                 <span class="input-group-addon">元</span>
                             </div>
                         </div>
                         <div class="card-type card-type-1">
                         <label class="col-sm-2 control-label">减免金额</label>
                         <div class="col-sm-4">
                             <div class="input-group">
                                 <input type="text" class="form-control" placeholder="请输入减免金额" name="reduce_cost"value="0" />
                                 <span class="input-group-addon">元</span>
                             </div>
                         </div>
                         </div>
                         <div class="card-type card-type-2">
                         <label class="col-sm-2 control-label">打折额度</label>
                         <div class="col-sm-4">
                             <div class="input-group">
                                 <input type="text" class="form-control" placeholder="请输入打折额度" name="discount" value="0" />
                                 <span class="input-group-addon">折</span>
                             </div>
                         </div>
                         </div>
                     </div>
                     <div class="form-group" style="font-weight:normal">
                         <label class="col-sm-2 control-label">使用时间</label>
                         <div class="col-sm-10 form-control-static form-inline">
                             <label class="icheck"><input type="radio" class="f-icheck" name="data_type" value="1" checked />
                                 固定日期区间
                                 <input class="form-control f-laydate" name="begin_end" data-option='{"range":true}' placeholder="请选择卡券的使用日期区间" style="width:280px;" />
                             </label>
                         </div>
                     </div>
                     <div class="form-group" style="font-weight:normal">
                         <label class="col-sm-2 control-label"></label>
                         <div class="col-sm-10 form-control-static form-inline">
                             <label class="icheck"> <input type="radio"  class="f-icheck" name="data_type" value="2" />
                                 领劵后 ，
                                 <input type="text" class="form-control" name="begin_day" value="0" style="width:80px;" />
                                 天生效 ，有效期
                                 <input type="text" class="form-control" name="fixed_day" value="0" style="width:80px;" />
                                 天
                             </label>
                         </div>
                     </div>
                     <div class="form-group">
                         <label class="col-sm-2 control-label">卡券使用说明</label>
                         <div class="col-sm-9 form-control-static">
                             {:FormBuild::_textarea('card_desc',$info.card_desc,['placeholder'=>'请输入卡券使用说明','rows'=>'3'])}
                         </div>
                     </div>
                     <div class="fixed-foot text-right">
                         <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                         <button type="button" class="btn btn-info btn-sub">立即保存</button>
                     </div>
                 </div>
             </form>
                </div>
            </div>
        </section>
    </div>
    <div style="height:15px;"></div>
</body>
<script>
    $(function () {
        $('#card_type').change(function () {
            var card_type = $(this).val();
            $('.card-type').hide();
            $('.card-type-'+card_type).show();
        });
        $('#card_type').trigger('change');
        formHelper.initForm();
        utils.ajaxSubForm();
    });
</script>

{/block}
