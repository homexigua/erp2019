{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="nav-tabs-custom">
            {:D('Rule','auth')->getSubMenu('6')}
            <div class="box-body">
                <div class="toolbar">
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-primary" onclick="doAction.add();">
                            <i class="fa fa-plus"></i> 添加
                        </button>
                    </div>
                </div>
                <table id="grid"
                       data-id-field="id"
                       data-editable-emptytext=""
                       data-editable-url="/admin/config_group/editable.html"></table>
            </div>
        </div>
    </section>
</div>
</body>
<script>
    utils.btable({
        url: 'index.html?is_page=0',
        pagination: false,
        singleSelect: true, //是否启用单选
        columns: [
            {field: 'state', checkbox: true},
            {field: 'group_name', title: '组名',
                editable: {
                    type:'text',
                    validate: function (value) { //验证规则自定义
                        if (valid.isEmpty(value)) return {newValue:'',msg:'组名不能为空'};
                    }
                }
            },
            {field: 'is_platform', title: '平台',width:100,
                formatter(value,row,index){
                    var html = '';
                    if (value == 1) {
                        html = '<label class="label label-success" onclick="utils.doAction(this,\'refreshTable\')" ' +
                            'data-url="multi.html" ' +
                            'data-params="ids=' + row.id + '&params=is_platform:0">是</label>';
                    } else {
                        html = '<label class="label label-warning" onclick="utils.doAction(this,\'refreshTable\')" ' +
                            'data-url="multi.html" ' +
                            'data-params="ids=' + row.id + '&params=is_platform:1">否</label>';
                    }
                    return html;
                }
            },
            {field: 'is_trade', title: '行业',width:100,
                formatter(value,row,index){
                    var html = '';
                    if (value == 1) {
                        html = '<label class="label label-success" onclick="utils.doAction(this,\'refreshTable\')" ' +
                            'data-url="multi.html" ' +
                            'data-params="ids=' + row.id + '&params=is_trade:0">是</label>';
                    } else {
                        html = '<label class="label label-warning" onclick="utils.doAction(this,\'refreshTable\')" ' +
                            'data-url="multi.html" ' +
                            'data-params="ids=' + row.id + '&params=is_trade:1">否</label>';
                    }
                    return html;
                }
            },
            {field: 'is_site', title: '站点',width:100,
                formatter(value,row,index){
                    var html = '';
                    if (value == 1) {
                        html = '<label class="label label-success" onclick="utils.doAction(this,\'refreshTable\')" ' +
                            'data-url="multi.html" ' +
                            'data-params="ids=' + row.id + '&params=is_site:0">是</label>';
                    } else {
                        html = '<label class="label label-warning" onclick="utils.doAction(this,\'refreshTable\')" ' +
                            'data-url="multi.html" ' +
                            'data-params="ids=' + row.id + '&params=is_site:1">否</label>';
                    }
                    return html;
                }
            },
            {field: 'show_order', title: '排序',width:100,
                editable: {
                    type:'text',
                    validate: function (value) { //验证规则自定义
                        if (!valid.isNum(value)) return {newValue:'0',msg:'排序填写不正确'};
                    }
                }
            },
            {
                field: 'operate', title: '操作', width: 100,
                formatter: function (value, row, index) {
                    var html = '';
                    html += '<a href="javascript:;" onclick="doAction.edit(' + row.id + ')">编辑</a> ';
                    html += '<a href="javascript:;" onclick="utils.doAction(this,\'refreshTable\')" ' +
                        'data-params="id=' + row.id + '" data-url="del.html">删除</a>';
                    return html;
                }
            }
        ]
    });
    var doAction = {
        add: function () {
            utils.openDialog({
                title: '添加参数配置组',
                area: ['800px', '258px'],
                content: '/admin/config_group/add.html'
            });
        },
        edit: function (id) {
            utils.openDialog({
                title: '修改参数配置组',
                area: ['800px', '258px'],
                content: '/admin/config_group/edit.html?id=' + id
            });
        }
    };
</script>
{/block}