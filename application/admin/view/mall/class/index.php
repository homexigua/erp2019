{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content-header">
        <h1>商品分类<small>分类管理</small></h1>
        <ol class="breadcrumb">
            <li>
                <a href="javascript:;" class="tab-href" data-url="/admin/index/main.html" data-title="首页">
                    <i class="fa fa-dashboard"></i> 首页
                </a>
            </li>
            <li class="active">分类列表</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-default">
            <div class="box-body">
                <div class="toolbar">
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary" onclick="doAction.add()">
                            <i class="fa fa-plus"></i> 添加
                        </button>
                        <button type="button" class="btn btn-default" onclick="doAction.refresh();">
                            <i class="fa fa-refresh"></i> 更新缓存
                        </button>
                    </div>
                </div>
                <table id="grid" class="table table-no-bordered"></table>
            </div>
        </div>
    </section>
</div>
</body>
<script>
    utils.bTreeTable({
        url:'getlist.html',
        columns:[
            {field:'state',checkbox:true},
            {field:'id',title:'id',width:80},
            {field:'name',title:'栏目名称'},
            {field:'class_keywords',title:'关键词'},
            {field:'is_show',title:'显示',width:100,
                formatter:function (value, row, index) {
                    var html = '';
                    if (value == 1) {
                        html = '<label class="label label-success" onclick="utils.doAction(this,\'refreshTable\')" ' +
                            'data-url="/admin/mall.class/multi.html" ' +
                            'data-params="ids=' + row.id + '&params=is_show:0">是</label>';
                    } else {
                        html = '<label class="label label-default" onclick="utils.doAction(this,\'refreshTable\')" ' +
                            'data-url="/admin/mall.class/multi.html" ' +
                            'data-params="ids=' + row.id + '&params=is_show:1">否</label>';
                    }
                    return html;
                }
            },
            {field:'show_order',title:'排序',sortable:true,width:100},
            {field: 'operate', title: '操作', width:220,
                formatter: function (value, row, index) {
                    var html = '';
                    html += '<a href="javascript:;" class="btn btn-warning btn-xs" onclick="doAction.add(' + row.id + ')">添加下级</a> ';
                    html += '<a href="javascript:;" class="btn btn-primary btn-xs" onclick="doAction.edit(' + row.id + ')">编辑</a> ';
                    html += '<a href="javascript:;" class="btn btn-danger btn-xs" onclick="utils.doAction(this,\'refreshTable\')" ' +
                        'data-params="id=' + row.id + '" data-url="/admin/mall.class/del.html">删除</a>';
                    return html;
                }
            }
        ],
        onLoadSuccess: function(data) {
            $('#grid').treegrid({
                treeColumn: 2,
                saveState:true,
                initialState:'collapsed',
                onChange: function() {
                    $('#grid').bootstrapTable('resetWidth');
                }
            });
        }
    });
    var doAction={
        add:function(classId){
            utils.openDialog({
                title: '添加栏目',
                area: ['800px', '450px'],
                content: '/admin/mall.class/add.html?class_id='+classId
            })
        },
        edit:function(id){
            utils.openDialog({
                title: '修改栏目',
                area: ['800px', '450px'],
                content: '/admin/mall.class/edit.html?id='+id
            })
        },
        refresh:function () { //刷新缓存
            $.get("/admin/mall.class/refresh.html",function (res) {
                layer.alert(res.msg,function (index) {
                    layer.close(index);
                    $('#grid').bootstrapTable('refresh');
                });
            });
        }
    };
</script>
{/block}
