{extend name="admin@public/layout" /}
{block name="head"}
<style>
    .type-box{display:none;}
</style>
{/block}
{block name="body"}
<?php
$cat = D('Class','mall')->getList(); //栏目列表
?>
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <form class="form-horizontal" id="form" {empty name='$Request.param.source'}data-callfn="refreshTable"{/empty}>
            <div class="row">
                <div class="col-sm-12">
                    <div class="box box-solid">
                        <div class="box-body">
                            <div class="form-group">
                                <label class="col-sm-2 control-label require">上级栏目</label>
                                <div class="col-sm-9">
                                    <select class="form-control" name="pid">
                                        {volist name=":D('Class','mall')->getList()" id="vo"}
                                        <option value="{$vo.id}" {eq name="$Request.param.pid" value="$vo['id']"}selected{/eq}>{$vo._name|raw}</option>
                                        {/volist}
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label require">栏目名称</label>
                                <div class="col-sm-9">
                                    {:FormBuild::_text('name','',['placeholder'=>'请输入栏目名称','datatype'=>'*'])}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">栏目图片</label>
                                <div class="col-sm-9">
                                    {:FormBuild::_image('image_url','')}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">栏目关键词</label>
                                <div class="col-sm-9">
                                    {:FormBuild::_tags('class_keywords','',['placeholder'=>'请输入栏目关键词'])}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">栏目描述</label>
                                <div class="col-sm-9 form-control-static">
                                    {:FormBuild::_textarea('class_desc','',['placeholder'=>'请输入栏目描述','rows'=>'3'])}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">是否显示</label>
                                <div class="col-sm-9 form-control-static">
                                    {:FormBuild::_radio('is_show','1',['list-data'=>'fun:get_dict_list:105'])}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="fixed-foot">
                    <div class="text-right">
                        <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                        {:FormBuild::_button('立即保存',['class'=>'btn btn-info btn-sub'])}
                    </div>
                </div>
            </div>
        </form>
    </section>
</div>
</body>
<script >
    $(function () {
        formHelper.initForm();
        utils.ajaxSubForm();
    });
</script>
{/block}