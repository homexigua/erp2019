{extend name="admin@public/layout" /}
{block name="title"}运费模板列表{/block}
{block name="head"}{/block}
{block name="body"}
<!-- 内容 -->
<section class="content">
    <div class="nav-tabs-custom">
        {:D('Rule','auth')->getSubMenu('87')}
        <div class="box-header with-border">
            <form class="form-inline" id="searchForm">
                <div class="form-group">
                    <select class="form-control" id="searchKey">
                        <option value="delivery_name">模板名称</option>
                    </select>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="searchValue" placeholder="请输入检索关键词" />
                </div>
                <div class="form-group">
                    <button type="button" class="btn btn-default" onclick="$('#grid').bootstrapTable('refresh');">
                    <i class="fa fa-search"></i> 检索
                    </button>
                </div>
            </form>
        </div>
        <div class="box-body">
            <div class="toolbar">
                <button type="button" class="btn btn-primary" onclick="doAction.add()">
                    <i class="fa fa-plus"></i> 添加
                </button>
            </div>
            <table id="grid"
                   data-id-field="id"
                   data-editable-emptytext="none"
                   data-editable-url="/admin/mall.delivery/editable.html">
            </table>
        </div>
    </div>
</section>
<script>
    utils.btable({
        url:'/admin/mall.delivery/index.html',
        sortName:'show_order',
        sortOrder:'asc',
        columns:[
            { field:'state',checkbox:true },
            { field:'delivery_name',title:'运费模板',width:120 },
            { field:'freight_type',title:'计价方式',width:150,formatter:function (value,row,index) {
                    var text={0:'免运费',1:'固定运费',2:'按重量计算'};
                    return text[value];
                }},
            { field:'freight_type_text',title:'计价内容'},
            { field:'delivery_status',title:'状态',width:140,align:'center',
                formatter:function (value, row, index) {
                    var  checked = value==1?'checked':'';
                    var html='<input type="checkbox" name="delivery_status" class="f-switch"\n' +
                        'data-option=\'{"onText":"启用","offText":"禁用","onColor":"success","offColor":"default","callfn":"doAction.switchAction"}\'\n' +
                        'data-id="'+row.id+'"\n' +
                        'placeholder="" value="1" '+checked+'/>';
                    return html;
                }
            },
            { field:'show_order',title:'排序',width:80,sortable:true,align:'center',
                editable:{
                    type:'text',
                    emptytext:'',
                    onblur:'submit',
                    clear:false,
                    showbuttons:false,
                    mode:'inline',
                    validate: function (value) { //验证规则自定义
                        if (!valid.isNum(value)) return {newValue:'0',msg:'排序格式不正确'};
                    }
                }
            },
            {field: 'operate', title: '操作', width:120,
                formatter: function (value, row, index) {
                    var html = '';
                    html += '<a href="javascript:;" onclick="doAction.edit(' + row.id + ')">编辑</a> ';
                    html += '<a href="javascript:;" onclick="utils.doAction(this,\'refreshTable\')" ' +
                        'data-params="id=' + row.id + '" data-url="/admin/mall.delivery/del.html">删除</a>';
                    return html;
                }
            }
        ],
        onPostBody:function () {
            formHelper.switch();
        }
    });
    var doAction={
        add:function() {
            var index = utils.openDialog({
                title: '运费模板',
                area: ['800px', '268px'],
                content: '/admin/mall.delivery/add.html'
            });
            layer.full(index);
        },
        edit:function(id) {
            var index = utils.openDialog({
                title: '运费模板',
                area: ['800px', '268px'],
                content: '/admin/mall.delivery/edit.html?id=' + id
            });
            layer.full(index);
        },
        switchAction:function (event,state) {
            var id=event.currentTarget.dataset.id;
            var name=event.currentTarget.name;
            var value=state==true?1:0;
            doAction.setStatus(id,name+':'+value);
        },
        setStatus:function (ids,params,url,callfn) {
            url = url || 'multi.html';
            utils.getAjax({
                url:url,
                data:{ids:ids,params:params},
                success:function (res) {
                    if(res.code==0){
                        utils.msg(res.msg);
                    }else{
                        if (typeof callfn === "function") {
                            callfn();
                        } else {
                            if (callfn == "refresh") $('#grid').bootstrapTable('refresh');
                        }
                    }
                }
            })
        },
    };
</script>
{/block}