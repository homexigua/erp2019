{extend name="admin@public/layout" /}
{block name="title"}添加运费模板{/block}
{block name="head"}{/block}
{block name="body"}
<section class="content" style="padding:20px;">
    <form class="form-horizontal" id="form" {empty name='$Request.param.source'}data-callfn="refreshTable"{/empty}>
        <div class="form-group">
            <label class="col-sm-2 control-label require">模板名称</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" placeholder="请输入分类名称" name="delivery_name" value=""  datatype="*"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label require">计价类型</label>
            <div class="col-sm-9">
                <select name="freight_type" class="form-control" datatype="*" id="freight_type">
                    <option value="0">免运费</option>
                    <option value="1">固定运费</option>
                    <option value="2">按重量设置运费</option>
                </select>
            </div>
        </div>
        <!--固定运费-->
        <div id="freight_type_1" style="display:none;">
            <div class="form-group">
                <label class="col-sm-2 control-label require">运费金额</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" placeholder="请输入运费金额" name="freight" value="" />
                </div>
            </div>
        </div>
        <!--按重量设置运费-->
        <div id="freight_type_2" style="display:none;">
            <div class="form-group">
                <div class="form-inline">
                    <label class="col-sm-2 control-label">重量设置</label>
                    <div class="col-sm-9">
                        <div class="i-checks form-inline">
                            <label style="font-weight:normal;">
                                首重重量
                                <input type="text" class="form-control" name="first_heavy" value="" style="width:80px;" />
                                Kg，首重运费
                                <input type="text" class="form-control" name="first_freight" value="" style="width:80px;" />
                                元
                            </label>
                        </div>
                        <div class="i-checks form-inline">
                            <label style="font-weight:normal;">
                                续重重量
                                <input type="text" class="form-control" name="continue_heavy" value="" style="width:80px;" />
                                Kg，续重运费
                                <input type="text" class="form-control" name="continue_freight" value="" style="width:80px;" />
                                元
                            </label>
                        </div>
                        <div class="i-checks form-inline">
                            <label style="font-weight:normal;">
                                最大重量
                                <input type="text" class="form-control" name="max_weight" value="0" style="width:80px;" />
                                Kg，超出最大重量禁止下单，输入“0”不限制。
                            </label>
                        </div>
                        <div class="i-checks form-inline">
                            <label style="font-weight:normal;">
                                最高运费
                                <input type="text" class="form-control" name="limit_freight" value="0" style="width:80px;" />
                                元， 超出按最高计算，输入“0”不限制。
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="fixed-foot text-right">
            <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
            <button type="button" class="btn btn-info btn-sub">立即保存</button>
        </div>
    </form>
</section>
<script>
    $(function () {
        formHelper.initForm();
        utils.ajaxSubForm();
    });

    $(function () {
        $('#freight_type').change(function () {
            var freight_type=$(this).val();
            if(freight_type==0){
                $('#freight_type_1').hide();
                $('#freight_type_2').hide();
            }
            if(freight_type==1){
                $('#freight_type_1').show();
                $('#freight_type_2').hide();
            }
            if(freight_type==2){
                $('#freight_type_1').hide();
                $('#freight_type_2').show();
            }
        });
        $('#freight_type').trigger('change');
        $("input[name='freight_type']").on('ifChanged', function (event) {
            alert($(this).val());
        });
    });
</script>
{/block}