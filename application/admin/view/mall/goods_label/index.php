{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="nav-tabs-custom">
            {:D('Rule','auth')->getSubMenu('87')}
            <div class="box-body">
                <form class="form-horizontal" id="form">
                    <table class="table">
                        <tr>
                            <td>
                                内置颜色
                            </td>
                            <td>
                                名称
                            </td>
                        </tr>
                        {volist name="$colorList" id="vo"}
                        <tr>
                            <td width="100">
                                <div style="display: inline-block;width: 40px;height: 20px;margin-top:5px;background-color: {$vo.color};"></div>
                            </td>
                            <td >
                                <input type="text" class="form-control" name="item[{$vo.id}]" value="{$vo.name}" />
                            </td>
                        </tr>
                        {/volist}
                    </table>
                    <div class="fixed-foot text-right">
                        <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                        <button type="submit" class="btn btn-info">立即保存</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
</body>
{/block}
{block name="script"}
<script>
    utils.ajaxSubForm();
</script>
{/block}