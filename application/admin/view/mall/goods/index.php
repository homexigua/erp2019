{extend name="admin@public/layout" /}
{block name="head"}
<style>
    #grid td{vertical-align: middle;font-size:14px;position:relative}
    #grid .bootstrap-switch-mini{margin:3px;}
    #grid .bootstrap-switch-default{color:#989898;}
    .goods-box{position:relative;height:40px;overflow:hidden;padding:0 20px 0 50px;}
    .goods-box .goods-thumb{
        width: 40px;
        height: 40px;
        border:1px solid #e8e8e8;
        position:absolute;
        top:0;
        left:0;
    }
    .goods-box .goods-info .goods-name{font-size:14px;line-height:20px;margin:0;padding:0;}
    #grid td .action-box{position:absolute;right:0;top:0;line-height:20px;font-size:16px;color:#989898;display:none;}
    #grid td .action-box a{position:relative;z-index:99;color:#989898;}
    #grid td .action-box a:hover{color:#f60;}
    #grid td:hover .action-box{display:block}
    .edit-form{
        padding:8px;border:1px solid #e8e8e8;box-shadow: 0 10px 20px rgba(0, 0, 0, 0.1);-webkit-box-shadow: 0 10px 20px rgba(0, 0, 0, 0.1);
        position:absolute;top:-30px;left:50%;margin-left:-140px;background:#fff;z-index:99;
        width:286px;
    }
    .edit-form:after{content:'';width: 0;height: 0;
        border-left: 10px solid transparent;
        border-right: 10px solid transparent;
        border-top: 10px solid #fff;
        position:absolute;
        bottom:-9px;
        left:50%;margin-left:-5px;
    }
    .edit-form:before{content:'';width: 0;height: 0;
        border-left: 10px solid transparent;
        border-right: 10px solid transparent;
        border-top: 10px solid #e8e8e8;
        position:absolute;
        bottom:-10px;
        left:50%;margin-left:-5px;
    }
    .edit-form .input-sm{width:162px;}
    .sku-table{margin:0;}
    .sku-table td{position:relative;padding:8px 15px;}
    .sku-table td .edit-icon{color:#989898;position:absolute;right:0;top:8px;display:none;}
    .sku-table td:hover .edit-icon{display:block;}
    .sku-table-1 td:first-child  .edit-form{margin-left:-100px;}

</style>
{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper" style="padding-left:160px;">
    <div class="category-box show">
        <h5 class="title">
            商品分组
            <a href="javascript:;" id="z-tree-btn" class="z-tree-btn plus-icon pull-right" style="margin-right:10px;" title="全部折叠/展开"></a>
        </h5>
        <ul class="ztree" id="ztree"></ul>
        <a href="javascript:;" id="category-toggle" class="category-toggle" title="隐藏/显示"></a>
    </div>
    <section class="content-header">
        <h1 style="font-size:18px;">商品列表</h1>
        <ol class="breadcrumb">
            <li>
                <a href="javascript:;" class="tab-href" data-url="/admin/index/main.html" data-title="首页">
                    <i class="fa fa-dashboard"></i> 首页
                </a>
            </li>
            <li class="active">商品列表</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-default">
            <div class="box-body">
                <div class="box-header with-border">
                    <form class="form-inline" id="searchForm">
                        <input type="hidden" name="class_id" value="" data-op="in">
                        <div class="form-group">
                            <select class="form-control" id="searchKey">
                                <option value="goods_name">商品名称</option>
                                <option value="sub_name">商品副标题</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="searchValue" placeholder="请输入检索关键词" />
                        </div>
                        <div class="form-group">
                            <button type="button" class="btn btn-default" onclick="$('#grid').bootstrapTable('refresh');">
                                <i class="fa fa-search"></i> 检索
                            </button>
                        </div>
                    </form>
                </div>
                <div class="toolbar">
                    <div class="btn-group btn-group-sm">
                        <a type="button" class="btn btn-default tab-href" data-url="/admin/mall.goods/add.html" data-title="添加商品">
                            <i class="fa fa-plus"></i> 添加
                        </a>
                        <button type="button" class="btn btn-default" onclick="doAction.del()">
                            <i class="fa fa-trash"></i> 删除
                        </button>
                    </div>
                </div>
                <table id="grid" class="table table-bordered"></table>
            </div>
        </div>
    </section>
</div>
</body>
<script type="text/html" id="goods-name-tpl">
    <div class="goods-box">
        <img src="${thumb}" class="goods-thumb" />
        <div class="goods-info">
            <h3 id="goods-name-${id}" class="goods-name">${goods_name}</h3>
        </div>
        <div class="action-box">
            <a href="javascript:;" data-name="goods_name" data-id="${id}" data-value="${goods_name}" onclick="doAction.editlist(this)" title="编辑商品名称" class="edit-icon"><i class="fa fa-edit"></i></a><br/>
            <a href="javascript:;"  onclick="alert(123)" title="获取商品推广二维码" class="qrcode-icon"><i class="fa fa-qrcode"></i></a>
        </div>
    </div>
</script>
<script type="text/html" id="edit-tpl">
    <div class="form-inline edit-form">
        <div class="form-group">
            <input type="text" class="form-control input-sm" name="${name}" value="">
        </div>
        <a href="javascript:;" data-id="${id}" data-name="${name}"  onclick="doAction.submitEdit(this)" class="btn btn-info btn-sm">确定</a>
        <a href="javascript:;" onclick="doAction.removeEdit()" class="btn btn-default btn-sm">取消</a>
    </div>
</script>
<script type="text/html" id="sku-list-tpl">
    <div style="padding:15px;">
        <table class="table table-border sku-table text-center sku-table-${num}">
            <tr>
                {@if data['sku_item'][0]['sku_name']!=''}
                <td>SKU名称</td>
                {@/if}
                <td>商品条码</td>
                <td>商品价格</td>
                <td>商品销量</td>
                <td>库存量</td>
                <td>占位量</td>
            </tr>
            {@each data.sku_item as value}
            <tr>
                {@if value.sku_name!=''}
                <td>
                    ${value.sku_name}
                </td>
                {@/if}
                <td>
                    <span id="bar_code-${value.id}">${value.bar_code}</span>
                    <a href="javascript:;"  data-name="bar_code" data-id="${value.id}" data-value="${value.bar_code}" onclick="doAction.editlist(this)"  class="edit-icon"><i class="fa fa-edit"></i></a>
                </td>
                <td>
                    <span id="sale_price-${value.id}">${value.sale_price}</span>
                    <a href="javascript:;" data-name="sale_price" data-id="${value.id}" data-value="${value.sale_price}" onclick="doAction.editlist(this)"  class="edit-icon"><i class="fa fa-edit"></i></a>
                </td>
                <td>${value.sale_num}</td>
                <td>${value.goods_stock}</td>
                <td>${value.position_stock}</td>
            </tr>
            {@/each}
        </table>
    </div>
</script>
<script>
$(function () {
    /*树形分类*/
    In('ztree',function () {
        $.get('/admin/mall.class/ztree.html',function (nodes) {
            $.fn.zTree.init($('#ztree'), {
                data: {
                    view: { showLine: false },
                    simpleData: { enable: true, idKey: "id", pIdKey: "pid", rootPId: 0 }
                },
                callback: { onClick: doAction.setCid }
            }, nodes);
        });
        $('#z-tree-btn').click(function () {
            var zTree = $.fn.zTree.getZTreeObj("ztree");
            if($(this).hasClass('plus-icon')){
                zTree.expandAll(false);
                $(this).removeClass('plus-icon').addClass('minus-icon');
            }else{
                zTree.expandAll(true);
                $(this).addClass('plus-icon').removeClass('minus-icon');

            }
        });
    });
    $('#category-toggle').click(function () {
        if($(this).parents('.category-box').hasClass('show')){
            $(this).parents('.category-box').removeClass('show').addClass('hided').css('left','-150px');
            $(this).parents('.wrapper').css('paddingLeft','0');
        }else{
            $(this).parents('.category-box').removeClass('hided').addClass('show').css('left','0');
            $(this).parents('.wrapper').css('paddingLeft','160px');
        }
    })
    utils.btable({
        url:'/admin/mall.goods/index.html',
        singleSelect:false,
        clickToSelect:false,
        columns:[
            { field:'state',checkbox:true },
            { field:'goods_name',title:'商品信息',width:280,
                formatter:function (value, row, index) {
                    var html=juicer($('#goods-name-tpl').html(),row);
                    return html;
                }
            },
            { field:'min_price',title:'销售价',width:150,sortable:true,
                formatter:function (value, row, index) {
                    var minPrice = parseFloat(row.min_price);
                    var maxPrice = parseFloat(row.max_price);
                    var html=minPrice.toFixed(2);
                    if(row.sku_item.length>1){
                        html=minPrice.toFixed(2)+' - '+maxPrice.toFixed(2);
                    }
                    html+='<div class="action-box" style="padding:8px;line-height:40px;">' +
                        '<a href="javascript:;" data-info=\''+JSON.stringify(row)+'\' data-minprice="'+minPrice+'" data-maxprice="'+maxPrice+'" onclick="doAction.editSku(this)" title="'+row.goods_name+'" class="edit-icon"><i class="fa fa-edit"></i></a>' +
                        '</div>';
                    return html;
                }
            },
            { field:'total_goods_stock',title:'总库存量',width:100,sortable:true},
            { field:'total_position_stock',title:'总占位量',width:100,sortable:true},
            { field:'label',title:'状态',width:300,align:'center',
                formatter:function (value, row, index) {
                    var sale_checked = row.is_sale==0?'':'checked';
                    var new_checked = row.is_new==0?'':'checked';
                    var recommend_checked = row.is_recommend==0?'':'checked';
                    var hot_checked = row.is_hot==0?'':'checked';
                    var html='<input type="checkbox" name="is_sale" class="f-switch"\n' +
                        'data-option=\'{"onText":"电商","offText":"普通","onColor":"success","offColor":"default","callfn":"doAction.switchAction"}\'\n' +
                        'data-id="'+row.id+'"\n' +
                        'placeholder="" value="1" '+sale_checked+'/>';
                    html+='<input type="checkbox" name="is_new" class="f-switch"\n' +
                        'data-option=\'{"onText":"新品","offText":"新品","onColor":"danger","offColor":"default","callfn":"doAction.switchAction"}\'\n' +
                        'data-id="'+row.id+'"\n' +
                        'placeholder="" value="1" '+new_checked+'/>';
                    html+='<input type="checkbox" name="is_recommend" class="f-switch"\n' +
                        'data-option=\'{"onText":"推荐","offText":"推荐","onColor":"info","offColor":"default","callfn":"doAction.switchAction"}\'\n' +
                        'data-id="'+row.id+'"\n' +
                        'placeholder="" value="1" '+recommend_checked+'/>';
                    html+='<input type="checkbox" name="is_hot" class="f-switch"\n' +
                        'data-option=\'{"onText":"热销","offText":"热销","onColor":"warning","offColor":"default","callfn":"doAction.switchAction"}\'\n' +
                        'data-id="'+row.id+'"\n' +
                        'placeholder="" value="1" '+hot_checked+'/>';
                    return html;
                }
            },
            { field:'action',title:'操作',width:80,align:'center',formatter:function (value,row,index) {
                    var html = '<a  class="btn btn btn-primary btn-xs tab-href"  onclick="doAction.edit('+row.id+')" style="margin:0 5px;">编辑</a>';
                    html+='<a href="javascript:;" class="btn btn btn-danger btn-xs" onclick="doAction.del('+row.id+')" style="margin:0 5px;">删除</a>';
                    return html;
                }},
        ],
        onPostBody:function () {
            formHelper.switch();
        }
    });
})
var doAction={
    viewSku:function(goods_id){
    },
    setCid:function(event, treeId, treeNode){
        $("input[name='class_id']").val(treeNode.data_view);
        $('#grid').bootstrapTable('refresh');
    },
    edit:function(id) {
        var url ='/admin/mall.goods/edit.html?id='+id;
        top.addTabs({id:url,url:url, title:'编辑商品',close:true});
    },
    del:function(id) {
        utils.confirm('确认要删除该商品吗？',function (index) {
            utils.getAjax({
                url:'/admin/mall.goods/del.html',
                data:{id:id},
                success:function (res) {
                    if(res.code==1){
                        $('#grid').bootstrapTable('refresh');
                    }else{
                        layer.msg(res.msg);
                    }
                    layer.close(index);
                }
            });
        })
    },
    switchAction:function (event,state) {
        var id=event.currentTarget.dataset.id;
        var name=event.currentTarget.name;
        var value=state==true?1:0;
        doAction.setStatus(id,name+':'+value);
    },
    /*编辑列表项*/
    editlist:function (obj) {
        var id=$(obj).data('id');
        var value = $(obj).data('value');
        var name = $(obj).data('name');
        var html = juicer($('#edit-tpl').html(),{value:value,id:id,name:name});
        $('.edit-form').remove();
        $(obj).parents('td').append(html);
        $('.edit-form').find('input[name="'+name+'"]').focus().val(value);
    },
    /*取消列表*/
    removeEdit:function () {
        $('.edit-form').remove();
    },
    /*编辑商品sku*/
    editSku:function (obj) {
        var info =$(obj).data('info');
        var goods_name =$(obj).attr('title');
        var html =juicer($('#sku-list-tpl').html(),{data:info,num:info.sku_item.length});
        layer.open({
            title:goods_name,
            offset:'auto',
            area: '800px',
            content:html,
            success :function () {
                localStorage.setItem('currentGoods',JSON.stringify(info));
            }
        })
    },
    /*列表编辑提交*/
    submitEdit:function (obj) {
        var id=$(obj).data('id');
        var name=$(obj).data('name');
        var value=$(obj).parents('.edit-form').find('input').val();
        var currentGoods = JSON.parse(localStorage.getItem('currentGoods'));
        var minprice =currentGoods.min_price;
        var maxprice =currentGoods.max_price;
        var goods_id =currentGoods.id;
        /*编辑sku表*/
        if(name!='goods_name'){
            doAction.setStatus(id,name+':'+value,'editsku.html',function () {
                $(obj).parents('.edit-form').remove();
                $('#'+name+'-'+id).text(value);
                /*如果是sku价格变动*/
                if(name=='sale_price'){
                    if(parseFloat(value)<=parseFloat(minprice)) doAction.setStatus(goods_id,'min_price:'+value,'','refresh');
                    if(parseFloat(value)>=parseFloat(maxprice)) doAction.setStatus(goods_id,'max_price:'+value,'','refresh');
                }
            });
        }else{
            doAction.setStatus(id,name+':'+value,'','refresh');
        }
    },
    setStatus:function (ids,params,url,callfn) {
        url = url || 'multi.html';
        utils.getAjax({
            url:url,
            data:{ids:ids,params:params},
            success:function (res) {
                if(res.code==0){
                    utils.msg(res.msg);
                }else{
                    if (typeof callfn === "function") {
                        callfn();
                    } else {
                        if (callfn == "refresh") $('#grid').bootstrapTable('refresh');
                    }
                }
            }
        })
    },

};
</script>
{/block}
