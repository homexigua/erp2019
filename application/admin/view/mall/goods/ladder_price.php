{extend name="admin@public/layout" /}
{block name='head'}
<style>
    .step_price_row{margin-bottom:10px;}
</style>
{/block}
{block name='body'}
<body>
<div style="padding:20px;" id="step_price_box">
    <div class="input-group step_price_row">
        <span class="input-group-addon" id="basic-addon1">购买</span>
        <input type="text" class="form-control number" placeholder="" value="1" readonly>
        <span class="input-group-addon" id="basic-addon2">以上，单价</span>
        <input type="text" class="form-control price"  placeholder="">
        <span class="input-group-btn">
            <button class="btn btn-default" type="button" onclick="stepAction.addRow()">
                <i class="fa fa-plus"></i>
            </button>
        </span>
    </div>
</div>
</body>
<script type="text/html" id="step_price_row">
    {@if data!=''}
    {@each data as value,index}
    <div class="input-group step_price_row">
        <span class="input-group-addon" id="basic-addon1">购买</span>
        <input type="text" class="form-control number" value="${value.number}" placeholder="" {@if index==0}readonly{@/if}>
        <span class="input-group-addon" id="basic-addon2">以上，单价</span>
        <input type="text" class="form-control price" value="${value.price}" placeholder="">
        <span class="input-group-btn">
            {@if index==0}
             <button class="btn btn-default" type="button" onclick="stepAction.addRow()">
                    <i class="fa fa-plus"></i>
             </button>
            {@else}
              <button class="btn btn-default" type="button" onclick="stepAction.delRow(this)">
                    <i class="fa fa-trash"></i>
              </button>
            {@/if}
        </span>
    </div>
    {@/each}
    {@else}
    <div class="input-group step_price_row">
        <span class="input-group-addon" id="basic-addon1">购买</span>
        <input type="text" class="form-control number" placeholder="">
        <span class="input-group-addon" id="basic-addon2">以上，单价</span>
        <input type="text" class="form-control price" placeholder="">
        <span class="input-group-btn">
                <button class="btn btn-default" type="button" onclick="stepAction.delRow(this)">
                    <i class="fa fa-trash"></i>
                </button>
        </span>
    </div>
    {@/if}
</script>
<script>
    var oldPrice="{$Request.param.price}";
    $(function () {
        stepAction.setLadderPrice(oldPrice);
    });
    var stepAction={
        addRow:function () {
            var row = juicer($('#step_price_row').html(),{data:''});
            $('#step_price_box').append(row);
        },
        delRow:function (obj) {
            $(obj).closest('.step_price_row').remove();
        },
        /*获取数据*/
        getLadderPrice:function () {
            var result = [];
            var result_msg = [];
            $('.step_price_row').each(function(){
                var number =$(this).find('.number').val();
                var price =$(this).find('.price').val();
                if(number=='' || price=='' || !valid.isFloat(number) || !valid.isFloat(price)){
                    result = [];
                    layer.msg('请正确填写阶梯价规则！',{icon:2});
                    return false;
                }else{
                    result.push(number+':'+price);
                }
            });
            return result.join(',');
        },
        /*还原数据*/
        setLadderPrice:function (value) {
            if(value!=''){
                var data=[];
                value=value.split(',');
                $.each(value,function (i,v) {
                    var item = v.split(':');
                    data.push({number:item[0],price:item[1]});
                });
                var html = juicer($('#step_price_row').html(),{data:data});
                $('#step_price_box').empty().html(html);
            }
        }
    }
</script>
{/block}
