{extend name="admin@public/layout" /}
{block name="head"}
<style>
    .box{border-top:none;}
    #select-attr .form-group{margin-bottom:5px;padding-bottom:5px;position:relative;border-bottom:1px dashed #e8e8e8;}
    #select-attr .form-group:last-child{border-bottom:none;}
    #select-attr .checkbox-inline{padding-top:5px!important;}
    #form .form-group{margin-bottom:15px;}
    #form .form-group .control-label{font-weight:500;}
    #form .form-tit{font-weight:500;background:#f4f4f4;margin:10px auto;padding:5px 10px 5px 25px;position:relative}
    #form .form-tit:after{content:'';width:5px;height:15px;background:#3c8dbc;position:absolute;left:15px;top:15px;}
    .input-price .btn-price{display:none;}
    .step-price{padding-right:120px;position:relative;}
    .step-price .btn-price{display:block;width:100px;height:34px;line-height:34px;color:#333;text-align:center;background:#f2f2f2;position:absolute;right:15px;top:0;}
</style>
{/block}
{block name="body"}
<?php
if($loginInfo['site_id']>0){
    $class = D('Class','mall')->getClass(0);
}else{
    $class = D('Class','mall')->getCategory(0);
}
?>
<body class="">
<div class="wrapper">
    <section class="content">
        <form class="form-horizontal" id="form">
            <div name="基本信息">
                <div class="form-tit">
                    <h5>基本信息</h5>
                </div>
                <div class="form-group">
                    <label class="col-sm-1 control-label require">行业类目</label>
                    <div class="col-sm-3" id="cxselect">
                        <select name="category_id" class="form-control" datatype="*" >
                            <option value="" >选择行业类目</option>
                            {volist name="$class" id="vo"}
                            <option value="{$vo.id}" {$vo.selected} {$vo.disabled}>{$vo._name|raw}</option>
                            {/volist}
                        </select>
                    </div>
                    <div class="col-sm-4">
                        <small style="line-height:34px;"><a href="javascript:;">怎么选择行业类目？</a></small>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-1 control-label require">商品分类</label>
                    <div class="col-sm-3">
                        <input type="text" class="f-selectpage" name="class_id" value=""
                               data-url="/admin/mall.class/selectpage.html?type=tree"
                               data-show="_name"
                               data-key="id"
                               data-option='{"pagination":false}'/>
                    </div>
                    <div class="col-sm-4">
                        <small style="line-height:34px;">
                            <a href="javascript:;" onclick="doAction.addOther('新建分类','/admin/mall.class/add.html?source=goods')">新建分类</a>
                        </small>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-1 control-label require">商品名称</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="goods_name" value=""  datatype="*"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-1 control-label">关键词</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control f-tags" id="keywords" placeholder="请输入关键词" name="keywords" data-value="" />
                    </div>
                    <div class="col-sm-10 col-sm-offset-1">
                        <small class="help-block">商品关键词标签，按回车增加新的标签</small>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-1 control-label">商品描述</label>
                    <div class="col-sm-6">
                        <textarea class="form-control" name="desc" rows="3" placeholder="请输入商品描述"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-1 control-label">商品相册</label>
                    <div class="col-sm-6">
                        <div class="f-upload-images-box" data-upload="uploadAlbum" data-name="album">
                            <ul class="f-upload-images-item"></ul>
                            <a class="img-upload-box" id="uploadAlbum"></a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div name="规格属性">
                <div class="form-tit">
                    <h5>规格属性</h5>
                </div>
                <div class="form-group" id="sku_switch">
                    <label class="col-sm-1 control-label">属性设置</label>
                    <div class="col-sm-4">
                        <input type="checkbox" class="f-switch" data-option='{"onText":"关闭SKU","offText":"开启SKU","onColor":"default","offColor":"success","size":"lg","callfn":"doAction.switchSku"}' name="is_goods_attr_switch" checked>
                        <input type="checkbox" class="f-switch" data-option='{"onText":"一口价","offText":"阶梯价","onColor":"default","offColor":"success","size":"lg","callfn":"doAction.switchPrice"}' name="goods_price_type_switch" checked>
                        <input type="hidden" name="is_goods_attr" value="0">
                        <input type="hidden" name="goods_price_type" value="0">
                    </div>
                </div>
                <div class="form-group" id="sku-box" style="display:none">
                    <label class="col-sm-1 control-label">规格属性</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control f-selectpage" id="goods_attr_ids" name="goods_attr_ids" value=""
                               data-url="/admin/mall.goods_attr/selectpage.html"
                               data-show="attr_name"
                               data-key="id"
                               data-custom="site_id={$loginInfo.site_id} and is_sku=1"
                               data-option='{"pagination":false,"multiple":true,"multipleControlbar":false,"maxSelectLimit":3,"selectFun":"doAction.selectSkuAttr","tagRemove":"doAction.selectSkuAttr"}'/>
                    </div>
                </div>
                <div id="select-attr"></div>
                <div id="sku-table"></div>
            </div>
            <div name="库存销售">
                <div class="form-tit">
                    <h5>销售及库存</h5>
                </div>
                <div class="form-group">
                    <label class="col-sm-1 control-label">最小起订</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" placeholder="最小起订量" name="min_buy_num" value="1" />
                    </div>
                    <label class="col-sm-1 control-label">最小包装量</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" placeholder="购买时只能选择最小包装量的倍数" name="sale_step" value="" />
                    </div>
                    <label class="col-sm-1 control-label">显示销量</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" placeholder="商城显示的销量,0为显示真实销量" name="sale_num" value="0" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-1 control-label">销售库存</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" placeholder="商城虚拟库存，小于库存不能下单" name="sale_stock" value="9999" />
                    </div>
                    <label class="col-sm-1 control-label">低库存预警</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" placeholder="最低库存,0不限制" name="min_stock" value="0" />
                    </div>
                    <label class="col-sm-1 control-label">高库存预警</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" placeholder="最高库存,0不限制" name="max_stock" value="0" />
                    </div>
                </div>
            </div>
            <div name="其他信息">
               <div class="form-tit">
                   <h5>其他信息</h5>
               </div>
               <div class="form-group">
                   <label class="col-sm-1 control-label">商品品牌</label>
                   <div class="col-sm-2">
                       <input type="text" class="f-selectpage" name="brand_id" value="" placeholder="请选择商品品牌"
                              data-url="/admin/mall.brand/selectpage.html"
                              data-show="brand_name"
                              data-custom="site_id={$loginInfo['site_id']}"
                              data-key="id"
                              data-option='{"pagination":false}'
                       />
                   </div>
                   <div class="col-sm-4">
                       <small style="line-height:34px;">
                           <a href="javascript:;" onclick="doAction.addOther('新建品牌','/admin/mall.brand/add.html?source=goods')">新建品牌</a>
                       </small>
                   </div>
               </div>
               <div class="form-group">
                   <label class="col-sm-1 control-label">商品单位</label>
                   <div class="col-sm-2">
                       <input type="text" class="f-selectpage" name="unit" value="" placeholder="请选择商品单位"
                              data-url="/admin/mall.unit/selectpage.html"
                              data-show="unit"
                              data-key="unit"
                              data-custom="site_id={$loginInfo['site_id']}"
                              data-option='{"pagination":false}'
                       />
                   </div>
                   <div class="col-sm-4">
                       <small style="line-height:34px;">
                           <a href="javascript:;" onclick="doAction.addOther('新增单位','/admin/mall.unit/add.html?source=goods')">新增单位</a>
                       </small>
                   </div>
               </div>
               <div class="form-group">
                   <label class="col-sm-1 control-label ">商品属性</label>
                   <div class="col-sm-10 form-control-static">
                       <input type="checkbox" class="f-switch" name="is_new"
                              data-option='{"onText":"新品","offText":"新品","onColor":"success","offColor":"default","size":"lg"}'
                              placeholder="" value="1"/>
                       <input type="checkbox" class="f-switch" name="is_recommend"
                              data-option='{"onText":"推荐","offText":"推荐","onColor":"success","offColor":"default","size":"lg"}'
                              placeholder="" value="1"/>
                       <input type="checkbox" class="f-switch" name="is_hot"
                              data-option='{"onText":"热卖","offText":"热卖","onColor":"success","offColor":"default","size":"lg"}'
                              placeholder="" value="1"/>
                   </div>
               </div>
               <div class="form-group">
                   <label class="col-sm-1 control-label ">发布状态</label>
                   <div class="col-sm-10 form-control-static">
                       <input type="checkbox" class="f-switch" name="is_sale"
                              data-option='{"onText":"上架","offText":"下架","onColor":"success","offColor":"default","size":"lg"}'
                              placeholder="" value="1" checked/>
                   </div>
               </div>
           </div>
            <div name="物流信息">
                <div class="form-tit">
                    <h5>物流信息</h5>
                </div>
                <div class="form-group">
                    <label class="col-sm-1 control-label">运费模板</label>
                    <div class="col-sm-2">
                        <input type="text" class="f-selectpage" name="delivery_id" value="" placeholder="请选择运费模板"
                               data-url="/admin/mall.delivery/selectpage.html"
                               data-show="delivery_name"
                               data-custom="site_id={$loginInfo['site_id']}"
                               data-key="id"
                               data-option='{"pagination":false,"selectFun":"doAction.selectDelivery","clear":"doAction.clearDelivery"}'
                        />
                    </div>
                    <div class="col-sm-4">
                        <small style="line-height:34px;">
                            <a href="javascript:;" onclick="doAction.addOther('新增运费模板','/admin/mall.delivery/add.html?source=goods')">新增运费模板</a>
                        </small>
                    </div>
                </div>
                <div id="freight_type_msg">

                </div>
                <div class="form-group">
                    <label class="col-sm-1 control-label">物流重量</label>
                    <div class="col-sm-2">
                        <div class="input-group">
                            <input type="text" name="goods_weight" class="form-control" value="0" placeholder="请输入商品物流重量">
                            <span class="input-group-addon">KG</span>
                        </div>
                    </div>
                </div>
            </div>
            <div name="商品详情">
                <div class="form-tit">
                    <h5>商品详情</h5>
                </div>
                <div class="form-group">
                    <label class="col-sm-1 control-label ">商品详情</label>
                    <div class="col-sm-10">
                        {:FormBuild::_ckeditor('goods_content','')}
                    </div>
                </div>
            </div>
            <div style="height:60px;"></div>
            <div class="fixed-foot text-right">
                <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                <button type="button" class="btn btn-info btn-sub">立即保存</button>
            </div>
        </form>
    </section>
</div>
</body>
<script type="text/html" id="attr_group_tpl">
    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-1">
            <div style="padding:15px;background:#f2f2f2;margin-top:15px;">
                {@each data as value, index}
                <div class="form-group attr-item">
                    <label class="col-sm-1 control-label">
                        <input type="text" class="form-control" name="sku_attr[${value.id}]" value="${value.attr_name}">
                    </label>
                    <div class="col-sm-10">
                        <div class="checkbox">
                            {@each value.attr_value as val}
                            <label class="checkbox-inline i-checks">
                                <input type="checkbox" class="" value="${val}" />${val}
                            </label>
                            {@/each}
                        </div>
                    </div>
                </div>
                {@/each}
            </div>
        </div>
    </div>
</script>
<script type="text/html" id="no-sku-table-tpl">
    <div class="form-group">
        <label class="col-sm-1 control-label">
            商品条码
        </label>
        <div class="col-sm-3">
            <input type="text" name="sku[0][bar_code]" class="form-control" value="" />
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-1 control-label">
            市场价格
        </label>
        <div class="col-sm-3">
            <input type="text" name="sku[0][market_price]" class="form-control" value="" />
        </div>
        <div class="col-sm-10 col-sm-offset-1">
            <small class="help-block" style="line-height:20px">商品展示时的划线价格，仅做展示用</small>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-1 control-label">
            销售价格
        </label>
        <div class="col-sm-3 input-price {@if goods_price_type==1}step-price{@/if}">
            <input type="text" name="sku[0][sale_price]" class="form-control" value="" autocomplete="off"/>
            <a href="javascript:;" onclick="doAction.ladderPrice(this)" class="btn-price">设置阶梯价</a>
        </div>
        <div class="col-sm-10 col-sm-offset-1">
            <small class="help-block" style="line-height:20px">商品价格分为一口价、阶梯价两种形式，阶梯价可以实现根据购买数量调整单价</small>
        </div>
    </div>
</script>
<script type="text/html" id="sku-table-tpl">
    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-1">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th width="15%" class="text-center">SKU</th>
                    <th width="20%" class="text-center">商品条码</th>
                    <th width="20%" class="text-center">市场价 <a href="javascript:;" onclick="doAction.batchPrice('market_price')" style="font-weight:500;font-size:12px;line-height:20px;float:right;margin-right:8px;">批量</a></th>
                    <th width="40%" class="text-center">销售价 <a href="javascript:;" onclick="doAction.batchPrice('sale_price')" style="font-weight:500;font-size:12px;line-height:20px;float:right;margin-right:8px;">批量</a></th>
                    <th width="5%" class="text-center"></th>
                </tr>
                </thead>
                <tbody>
                {@if num>0 }
                {@each data as value,index}
                <tr class="item" data-index="${index}" data-value="${value}">
                    <td>
                        <input type="hidden" name="sku[${index}][sku_name]" data-name="sku_name" class="form-control" value="${value}" autocomplete="off"/>
                        <p style="line-height:30px;text-align:left;padding:0 10px;margin-bottom:0;color:#ff2222;">${value}</p>
                    </td>
                    <td><input type="text" name="sku[${index}][bar_code]" data-name="sku_sn" class="form-control" value="" placeholder="" autocomplete="off"/></td>
                    <td><input type="text" name="sku[${index}][market_price]" data-name="market_price" class="form-control" value="" placeholder="" autocomplete="off"/></td>
                    <td>
                        <div class="input-price {@if goods_price_type==1}step-price{@/if}">
                            <input type="text" name="sku[${index}][sale_price]" data-name="sale_price" class="form-control" value="" placeholder="" autocomplete="off"/>
                            <a href="javascript:;" onclick="doAction.ladderPrice(this)" class="btn-price">设置阶梯价</a>
                        </div>
                    </td>
                    <td class="text-center"><a href="javascript:;" onclick="doAction.delSkuItem(this)" style="line-height:30px;"><i class="fa fa-trash"></i></a></td>
                </tr>
                {@/each}
                {@/if}
                </tbody>
            </table>
        </div>
        <div class="col-sm-8 col-sm-offset-1">
            <div id="remove-sku-box"></div>
        </div>
    </div>
</script>
<script type="text/html" id="sku-item-tpl">
    <tr class="item" data-index="${index}" data-value="${value}">
        <td>
            <input type="hidden" name="sku[${index}][sku_name]" data-name="sku_name" class="form-control" value="${value}" autocomplete="off"/>
            <p style="line-height:30px;text-align:left;padding:0 10px;margin-bottom:0;color:#ff2222;">${value}</p>
        </td>
        <td><input type="text" name="sku[${index}][sku_sn]" data-name="sku_sn" class="form-control" value="" autocomplete="off"/></td>
        <td><input type="text" name="sku[${index}][market_price]" data-name="market_price" class="form-control" value="" autocomplete="off"/></td>
        <td>
            <div class="input-price {@if goods_price_type==1}step-price{@/if}">
                <input type="text" name="sku[${index}][sale_price]" data-name="sale_price" class="form-control" value="" autocomplete="off"/>
                <a href="javascript:;" onclick="doAction.ladderPrice(this)" class="btn-price">设置阶梯价</a>
            </div>
        </td>
        <td class="text-center"><a href="javascript:;" onclick="doAction.delSkuItem(this)" style="line-height:30px;"><i class="fa fa-trash"></i></a></td>
    </tr>
</script>
<script>
    formHelper.initForm();
    var scene='add';
    var skuAttr={};/*选中的sku组信息*/
    var removeskuItem ={};/*移除的sku项目，点击可恢复*/
</script>
<script src="/static/admin/js/goods.js"></script>
{/block}