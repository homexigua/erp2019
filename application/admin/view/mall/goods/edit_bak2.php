{extend name="admin@public/layout" /}
{block name="head"}
<style>
    .box{border-top:none;}
    #select-attr .form-group{margin-bottom:5px;padding-bottom:5px;position:relative;border-bottom:1px dashed #e8e8e8;}
    #select-attr .form-group:last-child{border-bottom:none;}
    #select-attr .checkbox-inline{padding-top:5px!important;}
    #form .form-group{margin-bottom:20px;}
    #form .form-group .control-label{font-weight:500;}
    #form .form-tit{font-weight:500;background:#f4f4f4;margin:10px auto;padding:5px 10px 5px 25px;position:relative}
    #form .form-tit:after{content:'';width:5px;height:15px;background:#3c8dbc;position:absolute;left:15px;top:15px;}
</style>
{/block}
{block name="body"}
<?php
$class = D('Class','mall')->formatSelectList($info['class_id']);
$goodsAttr = D('GoodsAttr','mall')->getsku([['id','in',$info['goods_attr_ids']]]);
$skuItem = json_encode($info['sku_item']);
?>

<body class="">
<div class="wrapper">
    <section class="content">
        <form class="form-horizontal" id="form">
            <div class="form-tit">
                <h5>基本信息</h5>
            </div>
            <div class="form-group">
                <label class="col-sm-1 control-label require">商品类目</label>
                <div class="col-sm-3">
                    <input type="text" class="f-selectpage" name="category_id" value="{$info.category_id}"
                           data-url="/admin/mall.class/selectpage.html?type=tree"
                           data-show="_name"
                           data-key="id"
                           data-search="name"
                           data-option='{"pagination":false}'/>
                </div>
                <div class="col-sm-4">
                    <small style="line-height:34px;"><a href="javascript:;">怎么选择商品类目？</a></small>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-1 control-label require">商品分组</label>
                <div class="col-sm-3">
                    <select name="class_id" class="form-control" style="width:100%;" datatype="*" >
                        <option value="" >选择商品分组</option>
                        {volist name="$class" id="vo"}
                        <option value="{$vo.id}" {$vo.selected} {$vo.content_disabled}>{$vo._name|raw}</option>
                        {/volist}
                    </select>
                </div>
                <div class="col-sm-4">
                    <small style="line-height:34px;">
                        <a href="javascript:;">新建分组</a> | <a href="javascript:;">如何创建分组？</a>
                    </small>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-1 control-label require">商品名称</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" name="goods_name" value="{$info.goods_name}"  datatype="*"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-1 control-label">关键词</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control f-tags" id="keywords" placeholder="请输入关键词" name="keywords" data-value="{$info.keywords}" />
                </div>
                <div class="col-sm-10 col-sm-offset-1">
                    <small class="help-block">商品关键词标签，按回车增加新的标签</small>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-1 control-label">商品描述</label>
                <div class="col-sm-4">
                    <textarea class="form-control" name="desc" rows="3" placeholder="请输入商品描述">{$info.desc}</textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-1 control-label">商品相册</label>
                <div class="col-sm-10">
                    <div class="f-upload-images-box" data-upload="uploadAlbum" data-name="album" >
                        <ul class="f-upload-images-item">
                            {volist name="$info.album" id="vo"}
                            <li class="item-row">
                                <input type="hidden" name="album[]" value="{$vo}" />
                                <div class="img-box" style="background-image:url('{$vo}')">
                                    <a href="javascript:;" onclick="utils.upMove(this);"><i class="fa fa-arrow-left"></i></a>
                                    <a href="javascript:;" onclick="utils.downMove(this);"><i class="fa fa-arrow-right"></i></a>
                                    <a href="javascript:;" onclick="utils.delRow(this);"><i class="fa fa-trash-o"></i></a>
                                </div>
                            </li>
                            {/volist}
                        </ul>
                        <a class="img-upload-box" id="uploadAlbum"></a>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="form-tit">
                <h5>规格属性</h5>
            </div>
            {notempty name='goodsAttr'}
            <div class="form-group">
                <label class="col-sm-1 control-label">商品SKU</label>
                <div class="col-sm-8" id="select-attr">
                    <div style="padding:15px;background:#f2f2f2;position:relative">
                        {volist name='goodsAttr' id='vo'}
                        <div class="form-group">
                            <label class="col-sm-1 control-label">
                                <input type="text" class="form-control input-sm" name="sku_attr[{$vo.id}]" value="{$vo.attr_name}">
                            </label>
                            <div class="col-sm-8">
                                <div class="checkbox">
                                    {volist name='vo.attr_value' id='v'}
                                    <label class="checkbox-inline i-checks">
                                        <input type="checkbox" onclick="doAction.resetSku()" {in name='v' value='$info.sku_names'}checked{/in} value="{$v}" />{$v}
                                    </label>
                                    {/volist}
                                </div>
                            </div>
                        </div>
                        {/volist}
                        <!--  <a href="javascript:;" class="btn btn-default btn-xs" onclick="doAction.resetSku()" style="position:absolute;right:15px;top:10px;"><i class="fa fa-refresh"></i> 重置sku</a>-->
                    </div>
                </div>
            </div>
            {/notempty}
            <div id="sku-table"></div>
            <div class="form-group">
                <label class="col-sm-1 control-label">最小起订</label>
                <div class="col-sm-1">
                    <input type="text" class="form-control" placeholder="最小起订量,0不限制" name="min_buy_num" value="{$info.min_buy_num}" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-1 control-label">最低库存</label>
                <div class="col-sm-1">
                    <input type="text" class="form-control" placeholder="最低库存,0不限制" name="min_stock" value="{$info.min_stock}" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-1 control-label">最高库存</label>
                <div class="col-sm-1">
                    <input type="text" class="form-control" placeholder="最高库存,0不限制" name="max_stock" value="{$info.max_stock}" />
                </div>
            </div>
            <div class="form-tit">
                <h5>其他信息</h5>
            </div>
            <input type="checkbox" class="f-switch" data-option='{"onText":"开启","offText":"关闭","onColor":"warning","offColor":"default","callfn":"doAction.switchUnit"}' name="is_unit_group" value="1">
            <div id="unit-box"></div>
            <div class="form-group">
                <label class="col-sm-1 control-label">商品品牌</label>
                <div class="col-sm-2">
                    <input type="hidden" name="brand_name" value="{$info.brand_name}" />
                    <input type="text" class="f-selectpage" name="brand_id" value="{$info.brand_id}"
                           data-url="/admin/mall.brand/selectpage.html"
                           data-show="brand_name"
                           data-key="id"
                           data-option='{"selectFun":"doAction.selectBrand"}'/>
                </div>
                <div class="col-sm-4">
                    <small style="line-height:34px;">
                        <a href="javascript:;">新建品牌</a>
                    </small>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-1 control-label">保质期</label>
                <div class="col-sm-10">
                    <input type="checkbox" class="f-switch" data-option='{"onText":"开启","offText":"关闭","onColor":"success","offColor":"default","size":"lg","callfn":"doAction.switchQuality"}' name="is_quality" value="{$info.is_quality}">
                </div>
                <div class="col-sm-3 col-sm-offset-1" id="quality-box" style="{eq name="info.is_quality" value="0"}display: none;{/eq}margin-top:15px;">
                    <div class="input-group" style="width:45%;float:left;">
                        <input type="text" class="form-control" placeholder="请输入保质期" name="quality_days" value="" />
                        <span class="input-group-addon" id="basic-addon2">天</span>
                    </div>
                    <div class="input-group" style="width:45%;float:left;margin-left:10%;">
                        <input type="text" class="form-control" placeholder="请输入预警期" name="warning_days" value="" />
                        <span class="input-group-addon" id="basic-addon2">天</span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-1 control-label">商品税率</label>
                <div class="col-sm-1">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="请输入税率" name="goods_rate" value="{$info.goods_rate}" />
                        <span class="input-group-addon" id="basic-addon2">%</span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-1 control-label">规格型号</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" placeholder="请输入规格型号" name="goods_spec" value="{$info.goods_spec}" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-1 control-label">包装方式</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" placeholder="请输入包装方式" name="goods_pack" value="{$info.goods_pack}" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-1 control-label ">状态</label>
                <div class="col-sm-10 form-control-static">
                    <label class="checkbox-inline i-checks"><input type="radio" name="is_sale" class="f-icheck" value="1" {eq name="info.is_sale" value="1"}checked{/eq}   /> 上架</label>
                    <label class="checkbox-inline i-checks"><input type="radio" name="is_sale" class="f-icheck" value="0" {eq name="info.is_sale" value="0"}checked{/eq} /> 下架</label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-1 control-label">劳务费用</label>
                <div class="col-sm-10 form-control-static">
                    {:FormBuild::_radio('is_service',$info.is_service,['list-data'=>'fun:get_dict_list:105'])}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-1 control-label">新品</label>
                <div class="col-sm-10 form-control-static">
                    {:FormBuild::_radio('is_new',$info.is_new,['list-data'=>'fun:get_dict_list:105'])}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-1 control-label">推荐</label>
                <div class="col-sm-10 form-control-static">
                    {:FormBuild::_radio('is_recommend',$info.is_recommend,['list-data'=>'fun:get_dict_list:105'])}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-1 control-label">热门</label>
                <div class="col-sm-10 form-control-static">
                    {:FormBuild::_radio('is_hot',$info.is_hot,['list-data'=>'fun:get_dict_list:105'])}
                </div>
            </div>
            <div class="form-tit">
                <h5>物流信息</h5>
            </div>
            <div class="form-group">
                <label class="col-sm-1 control-label">运费模板</label>
                <div class="col-sm-4">
                    <div class="btn-group">
                        <button type="button" class="btn btn-warning" onclick="top.addTabs({ id: '/admin/mall.delivery/index.html',title: '运费模板管理',close: true, url: '/admin/mall.delivery/index.html' });">新建运费模板</button>
                        <button type="button" class="btn btn-primary" onclick="doAction.reloadData()">重新加载</button>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-1 control-label">模板选择</label>
                <div class="col-sm-4">
                    <select name="delivery_id" id="delivery_id" class="form-control">
                        <option value="0">请选择</option>
                        {volist name="$delivery" id="vo"}
                        <option value="{$vo.id}" {eq name="info.delivery_id" value="$vo['id']"}selected{/eq}>{$vo.delivery_name}</option>
                        {/volist}
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-1 control-label">物流重量</label>
                <div class="col-sm-4">
                    <div class="input-group">
                        <input type="text" name="goods_weight" class="form-control" value="{$info.goods_weight}" placeholder="请输入商品物流重量">
                        <span class="input-group-addon">KG</span>
                    </div>
                </div>
            </div>
            <div class="form-tit">
                <h5>商品详情</h5>
            </div>
            <div class="form-group">
                <label class="col-sm-1 control-label ">商品详情</label>
                <div class="col-sm-10">
                    {:FormBuild::_ckeditor('goods_content',$info.goods_content)}
                </div>
            </div>
            <div style="height:60px;"></div>
            <div class="fixed-foot text-right">
                <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                <button type="button" class="btn btn-info btn-sub">立即保存</button>
            </div>
        </form>
    </section>
</div>
</body>
<script type="text/html" id="sku-table-tpl">
    {@if num==1}
    <input type="hidden" name="is_goods_attr" value="0">
    <div class="form-group">
        <label class="col-sm-1 control-label">
            商品条码
        </label>
        <div class="col-sm-2">
            <input type="text" name="sku[bar_code]" class="form-control input-sm" value="${data[0]['bar_code']}" />
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-1 control-label">
            销售价格
        </label>
        <div class="col-sm-2">
            <input type="text" name="sku[sale_price]" class="form-control input-sm" value="${data[0]['sale_price']}" />
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-1 control-label">
            市场价格
        </label>
        <div class="col-sm-2">
            <input type="text" name="sku[market_price]" class="form-control input-sm" value="${data[0]['market_price']}" />
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-1 control-label">
            最低售价
        </label>
        <div class="col-sm-2">
            <input type="text" name="sku[min_sale_price]" class="form-control input-sm" value="${data[0]['min_sale_price']}"/>
        </div>
    </div>
    {@else}
    <div class="form-group">
        <input type="hidden" name="is_goods_attr" value="1">
        <div class="col-sm-8 col-sm-offset-1">
            <table class="table table-bordered table-condensed">
                <tr>
                    <th width="15%" class="text-center">SKU</th>
                    <th width="20%" class="text-center">商品条码</th>
                    <th width="20%" class="text-center">市场价 <a href="javascript:;" onclick="doAction.batchPrice('market_price')" style="font-weight:500;font-size:12px;line-height:20px;float:right;margin-right:8px;">批量</a></th>
                    <th width="20%" class="text-center">销售价 <a href="javascript:;" onclick="doAction.batchPrice('sale_price')" style="font-weight:500;font-size:12px;line-height:20px;float:right;margin-right:8px;">批量</a></th>
                    <th width="20%" class="text-center">最低售价 <a href="javascript:;" onclick="doAction.batchPrice('min_sale_price')" style="font-weight:500;font-size:12px;line-height:20px;float:right;margin-right:8px;">批量</a></th>
                    <th width="5%" class="text-center"></th>
                </tr>
                {@each data as value,index}
                <tr class="item" data-index="${index}" data-value="${json2string(value)}">
                    <input type="hidden" name="sku[${index}][id]" data-name="sku_id" class="form-control input-sm" value="${value.id}"/>
                    <td>
                        <input type="hidden" name="sku[${index}][sku_name]" data-name="sku_name" class="form-control input-sm" value="${value.sku_name}"/>
                        <p style="line-height:30px;text-align:left;padding:0 10px;margin-bottom:0;color:#ff2222;">${value.sku_name}</p>
                    </td>
                    <td><input type="text" name="sku[${index}][bar_code]" data-name="bar_code" class="form-control input-sm" value="${value.bar_code}" placeholder="" /></td>
                    <td><input type="text" name="sku[${index}][market_price]" data-name="market_price" class="form-control input-sm" value="${value.market_price}" placeholder="" /></td>
                    <td><input type="text" name="sku[${index}][sale_price]" data-name="sale_price" class="form-control input-sm" value="${value.sale_price}" placeholder="" /></td>
                    <td><input type="text" name="sku[${index}][min_sale_price]" data-name="min_sale_price" class="form-control input-sm" value="${value.min_sale_price}" placeholder="" /></td>
                    <td >
                        {@if value.goods_stock==0}
                        <a href="javascript:;" onclick="doAction.delSkuItem(this)" title="库存为零的商品可以删除" style="line-height:30px;"><i class="fa fa-trash"></i></a>
                        {@/if}
                    </td>
                </tr>
                {@/each}
            </table>
        </div>
        <div class="col-sm-8 col-sm-offset-1">
            <div id="remove-sku-box"></div>
        </div>
    </div>
    {@/if}

</script>
<script type="text/html" id="sku-item-tpl">
    <tr class="item" data-index="${index}" data-value="${json2string(value)}">
        <td>
            <input type="hidden" name="sku[${index}][sku_id]" data-name="sku_id" class="form-control input-sm" value="${value.id}"/>
            <input type="hidden" name="sku[${index}][sku_name]" data-name="sku_name" class="form-control input-sm" value="${value.sku_name}"/>
            <p style="line-height:30px;text-align:left;padding:0 10px;margin-bottom:0;color:#ff2222;">${value.sku_name}</p>
        </td>
        <td><input type="text" name="sku[${index}][sku_sn]" data-name="sku_sn" class="form-control input-sm" value="${value.sku_sn}" placeholder="" /></td>
        <td><input type="text" name="sku[${index}][market_price]" data-name="market_price" class="form-control input-sm" value="${value.market_price}" placeholder="" /></td>
        <td><input type="text" name="sku[${index}][sale_price]" data-name="sale_price" class="form-control input-sm" value="${value.sale_price}" placeholder="" /></td>
        <td><input type="text" name="sku[${index}][min_sale_price]" data-name="min_sale_price" class="form-control input-sm" value="${value.min_sale_price}" placeholder="" /></td>
        <td class="text-center">
            {@if value.goods_stock==0}
            <a href="javascript:;" onclick="doAction.delSkuItem(this)" title="库存为零的商品可以删除" style="line-height:30px;"><i class="fa fa-trash"></i></a>
            {@/if}
        </td>
    </tr>
</script>
<script type="text/html" id="unit-tpl">
    <div class="form-group">
        {@if type=='1' }
        <label class="col-sm-1 control-label">单位组</label>
        {@else}
        <label class="col-sm-1 control-label">商品单位</label>
        {@/if}
        <div class="col-sm-4">
            {@if data=='' }
            <div class="btn-group" role="group" aria-label="...">
                <a href="javascript:;" onclick=" top.addTabs({id:'/admin/mall.unit/index.html',url:'/admin/mall.unit/index.html', title:'计量单位',close:true});utils.openPage('system/goods.unit/index.html','计量单位')" class="btn btn-info">添加商品单位</a>
                <a href="javascript:;" onclick="doAction.getUnit(${type})" class="btn btn-warning">重新加载</a>
            </div>
            {@else}
            {@if type=='1' }
            <select class="form-control" name="unit_group" datatype="*">
                <option value="">请选择单位组</option>
                {@each data as value}
                <option value="${value.unit_show_name}" ${value.selected}>${value.alias}[${value.unit_show_name}]</option>
                {@/each}
            </select>
            {@else}
            <select class="form-control" name="unit" datatype="*">
                <option value="">请选择商品单位</option>
                {@each data as value}
                <option value="${value.unit}"  ${value.selected}>${value.alias}</option>
                {@/each}
            </select>
            {@/if}
            {@/if}
        </div>
    </div>
</script>
<script>
    formHelper.initForm();
    var exskuItem = JSON.parse('{$skuItem|raw}');/*已存在的sku列表*/
    var editSku = [];
    editSku =exskuItem.concat(editSku);
    console.log(exskuItem);
    //var goodsAttr = JSON.parse('{$goodsAttr|raw}');
    var scene='edit';
    var unitType="{$info.is_unit_group}";
    var unit="{$info.unit}";
    var unitGroup="{$info.unit_group}";
    console.log(unitGroup);
</script>
<script src="/static/plugins/sku/system_sku.js"></script>
{/block}