{extend name="admin@public/layout" /}
{block name="head"}
<style>
    .hided{left:-150px;}
</style>
{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper" style="padding-left:0;">
    <div class="category-box hided">
        <h5 class="title">
            商品类目
            <a href="javascript:;" id="z-tree-btn" class="z-tree-btn plus-icon pull-right" style="margin-right:10px;" title="全部折叠/展开"></a>
        </h5>
        <ul class="ztree" id="ztree"></ul>
        <a href="javascript:;" id="category-toggle" class="category-toggle" title="隐藏/显示"></a>
    </div>
    <section class="content">
        <div class="box box-default">
            <div class="box-body">
                <div class="box-header with-border">
                    <form class="form-inline" id="searchForm">
                        <input type="hidden" name="class_id" value="" data-op="in">
                        <div class="form-group">
                            <select class="form-control" id="searchKey">
                                <option value="goods_name">商品名称</option>
                                <option value="sub_name">商品副标题</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="searchValue" placeholder="请输入检索关键词" />
                        </div>
                        <div class="form-group">
                            <button type="button" class="btn btn-default" onclick="$('#grid').bootstrapTable('refresh');">
                                <i class="fa fa-search"></i> 检索
                            </button>
                        </div>
                    </form>
                </div>
                <table id="select-goods-grid" class="table table-bordered table-condensed table-hover" style="table-layout: fixed"></table>
            </div>
        </div>
    </section>
</div>
</body>
<script>
    var multiple="{$Request.param.multiple}";
    var depot ="{$Request.param.depot}";
    var singleSelect =multiple==1?true:false;
$(function () {
    /*树形分类*/
    In('ztree',function () {
        $.get('/admin/mall.class/ztree.html',function (nodes) {
            $.fn.zTree.init($('#ztree'), {
                data: {
                    view: { showLine: false },
                    simpleData: { enable: true, idKey: "id", pIdKey: "pid", rootPId: 0 }
                },
                callback: { onClick: doAction.setCid }
            }, nodes);
        });
        $('#z-tree-btn').click(function () {
            var zTree = $.fn.zTree.getZTreeObj("ztree");
            if($(this).hasClass('plus-icon')){
                zTree.expandAll(false);
                $(this).removeClass('plus-icon').addClass('minus-icon');
            }else{
                zTree.expandAll(true);
                $(this).addClass('plus-icon').removeClass('minus-icon');

            }
        });
    });
    $('#category-toggle').click(function () {
        if($(this).parents('.category-box').hasClass('show')){
            $(this).parents('.category-box').removeClass('show').addClass('hided').css('left','-150px');
            $(this).parents('.wrapper').css('paddingLeft','0');
        }else{
            $(this).parents('.category-box').removeClass('hided').addClass('show').css('left','0');
            $(this).parents('.wrapper').css('paddingLeft','160px');
        }
    })
    utils.btable({
        url:'/admin/mall.goods_sku/index.html',
        singleSelect:singleSelect,
        clickToSelect:true,
        height:440,
        columns:[
            { field:'state',checkbox:true,width:50 },
            { field:'bar_code',title:'商品条码',width:120},
            { field:'goods_name',title:'品名规格',width:300,class:'cell-no-edit'},
            { field:'usable_stock',title:'可用量',width:120,sortable:true,class:'cell-no-edit'},
            { field:'goods_stock',title:'总库存',width:120,sortable:true,class:'cell-no-edit'},
            { field:'unit',title:'最小单位',width:80,align:'center',class:'cell-no-edit'},
            { field:'unit_group',title:'单位组',width:240,class:'cell-no-edit'},
            { field:'sale_price',title:'销售价',width:150, editable:{
                    type:'text',
                    emptytext:'',
                    validate: function (value) { //验证规则自定义
                        if (!valid.isFloat(value)) return {newValue:'0',msg:'价格必须为数字'};
                    }
                }},
            { field:'min_sale_price',title:'最低售价',width:150,editable:{
                    type:'text',
                    emptytext:'',
                    validate: function (value) { //验证规则自定义
                        if (!valid.isFloat(value)) return {newValue:'0',msg:'价格必须为数字'};
                    }
                }},
            { field:'cost_avg_price',title:'平均成本',width:150,sortable:true,class:'cell-no-edit'},
            { field:'cost_last_price',title:'最新成本',width:150,sortable:true,class:'cell-no-edit'},
        ]
    },'#select-goods-grid');
})
var doAction={
    setCid:function(event, treeId, treeNode){
        $("input[name='class_id']").val(treeNode.data_view);
        $('#select-goods-grid').bootstrapTable('refresh');
    },
    select:function () {
        var rows = $('#select-goods-grid').bootstrapTable('getSelections');
        if (rows.length <= 0) {
            console.log(11111);
            layer.msg('请选择商品！',{icon:2});
            return false;
        }
        if(multiple>0 && rows.length >multiple){
            layer.msg('只能选择'+multiple+'个商品！',{icon:2});
            return false;
        }
        return rows;
    }
};
</script>
{/block}
