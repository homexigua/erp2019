{extend name="admin@public/layout" /}
{block name="head"}
{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <form class="form-horizontal" id="form" {empty name='$Request.param.source'}data-callfn="refreshTable"{/empty}>
            <div class="box box-solid">
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">品牌名称</label>
                        <div class="col-sm-9">
                            {:FormBuild::_text('brand_name','',['placeholder'=>'请输入品牌名称','datatype'=>'*'])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">助记码</label>
                        <div class="col-sm-9">
                            {:FormBuild::_text('brand_code','',['placeholder'=>'请输入助记码'])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">LOGO</label>
                        <div class="col-sm-9">
                            {:FormBuild::_image('brand_logo','')}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">品牌描述</label>
                        <div class="col-sm-9 form-control-static">
                            {:FormBuild::_textarea('brand_desc','',['placeholder'=>'请输入品牌描述','rows'=>'3'])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">推荐</label>
                        <div class="col-sm-9 form-control-static">
                            {:FormBuild::_radio('is_remmend','0',['list-data'=>'fun:get_dict_list:105'])}
                        </div>
                    </div>
                </div>
            </div>
            <div class="fixed-foot">
                <div class="text-right">
                    <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                    {:FormBuild::_button('立即保存',['class'=>'btn btn-info btn-sub'])}
                </div>
            </div>
        </form>
    </section>
</div>
</body>
<script>
    $(function () {
        formHelper.initForm();
        utils.ajaxSubForm();
    });
</script>
{/block}