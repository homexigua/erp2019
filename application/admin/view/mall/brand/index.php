{extend name="admin@public/layout" /}
{block name="head"}
<style>
    #grid td{vertical-align: middle;}
    .brand-logo{width:60px;height:60px;
        background-repeat:no-repeat;background-position:center center;background-size:cover;background-image: url("/pubic/admin/img/user1-128x128.jpg");
        background-color: #fff;
        margin:5px auto;
    }
</style>
{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="nav-tabs-custom">
<!--            “87权限规则的编号”-->
            {:D('Rule','auth')->getSubMenu('87')}
            <div class="box-body">
                <div class="toolbar">
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary" onclick="doAction.add()">
                            <i class="fa fa-plus"></i> 添加
                        </button>
                    </div>
                </div>
                <table id="grid"></table>
            </div>
        </div>
    </section>
</div>
</body>
<script>
    $(function () {
        formHelper.initForm();
    });
    utils.btable({
        url:'index.html',
        columns:[
            { field:'state',checkbox:true },//复选框
            { field:'brand_logo',title:'品牌LOGO',width:60,algin:'center',
                formatter:function (value, row, index) {
                    return '<div class="brand-logo" style="background-image:url('+value+')"></div>';
                }
            },
            { field:'brand_name',title:'品牌名称',width:150,
                editable:{
                    type:'text',
                    emptytext:'',
                }
            },
            { field:'brand_code',title:'助记码',width:150,
                editable:{
                    type:'text',
                    emptytext:'',
                }
            },
            { field:'brand_desc',title:'品牌描述',class:'cell-no-edit'},
            {field:'is_remmend',title:'推荐', align:'center',width:130,formatter:function (value,row,index) {
                    var  checked = value==1?'checked':'';
                    var html='<input type="checkbox" name="is_remmend" class="f-switch"\n' +
                        'data-option=\'{"onText":"推荐","offText":"普通","onColor":"warning","offColor":"default","callfn":"doAction.switchAction"}\'\n' +
                        'data-id="'+row.id+'"\n' +
                        'placeholder="" value="1" '+checked+'/>';
                    return html;
                }},
            {field:'status',title:'状态', align:'center',width:130,formatter:function (value,row,index) {
                    var  checked = value==1?'checked':'';
                    var html='<input type="checkbox" name="status" class="f-switch"\n' +
                        'data-option=\'{"onText":"启用","offText":"禁用","onColor":"info","offColor":"default","callfn":"doAction.switchAction"}\'\n' +
                        'data-id="'+row.id+'"\n' +
                        'placeholder="" value="1" '+checked+'/>';
                    return html;
                }},
            { field:'show_order',title:'排序',width:100,sortable:true,align:'center',
                editable:{
                    type:'text',
                    emptytext:'',
                    validate: function (value) { //验证规则自定义
                        if (!valid.isNum(value)) return {newValue:'0',msg:'排序格式不正确'};
                    }
                }
            },
            {
                field: 'operate', title: '操作', width: 100,
                formatter: function (value, row, index) {
                    var html = '';
                    html += '<a href="javascript:;" onclick="doAction.edit(' + row.id + ')">编辑</a> ';
                    html += '<a href="javascript:;" onclick="utils.doAction(this,\'refreshTable\')" ' +
                        'data-params="id=' + row.id + '" data-url="/admin/mall.brand/del.html">删除</a>';
                    return html;
                }
            },
        ],
        onPostBody:function () {
            formHelper.initForm();
        }
    });
    var doAction={
        add:function(){
            utils.openDialog({
                title: '添加品牌',
                area: ['800px', '420px'],
                content: '/admin/mall.brand/add.html'
            });
        },
        edit:function(id){
            utils.openDialog({
                title: '修改品牌',
                area: ['800px', '420px'],
                content: '/admin/mall.brand/edit.html?id='+id
            });
        },
        switchAction:function (event,state) {
            var id=event.currentTarget.dataset.id;
            var name=event.currentTarget.name;
            var value=state==true?1:0;
            doAction.setStatus(id,name+':'+value);
        },
        setStatus:function (ids,params,url,callfn) {
            url = url || 'multi.html';
            utils.getAjax({
                url:url,
                data:{ids:ids,params:params},
                success:function (res) {
                    if(res.code==0){
                        utils.msg(res.msg);
                    }else{
                        if (typeof callfn === "function") {
                            callfn();
                        } else {
                            if (callfn == "refresh") $('#grid').bootstrapTable('refresh');
                        }
                    }
                }
            })
        },
    };
</script>
{/block}
