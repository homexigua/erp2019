{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="nav-tabs-custom">
            {:D('Rule','auth')->getSubMenu('87')}
            <div class="row">
                <div class="col-sm-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <button type="button" class="btn btn-primary" onclick="doAction.add();"><i class="fa fa-plus"></i> 添加属性</button>
                        </div>
                        <div class="box-body">
                            <div class="table-responsive">
                                <table id="grid"></table>
                            </div>
                        </div>
                    </div>
                </div>
                <table id="grid"></table>
            </div>
        </div>
    </section>
</div>
</body>
<script>
    utils.btable({
        url:'/admin/mall.goods_attr/index.html',
        showExport:false,
        pagination: true,
        showRefresh: false,
        showToggle: false,
        showColumns: false,
        columns:[
            {field:'attr_name',title:'属性名称',width:200},
            {field:'is_sku',title:'SKU属性', align:'center',width:140,formatter:function (value,row,index) {
                    var  checked = value==1?'checked':'';
                    var html='<input type="checkbox" name="is_sku" class="f-switch"\n' +
                        'data-option=\'{"onText":"启用","offText":"禁用","onColor":"info","offColor":"default","callfn":"doAction.switchAction"}\'\n' +
                        'data-id="'+row.id+'"\n' +
                        'placeholder="" value="1" '+checked+'/>';
                    return html;
                }},
            {field:'is_search',title:'搜索属性', align:'center',width:140,formatter:function (value,row,index) {
                    var  checked = value==1?'checked':'';
                    var html='<input type="checkbox" name="is_search" class="f-switch"\n' +
                        'data-option=\'{"onText":"启用","offText":"禁用","onColor":"info","offColor":"default","callfn":"doAction.switchAction"}\'\n' +
                        'data-id="'+row.id+'"\n' +
                        'placeholder="" value="1" '+checked+'/>';
                    return html;
                }},
            {field:'attr_value',title:'属性值'},
            {field: 'operate', title: '操作', align:'center', width:80,
                formatter: function (value, row, index) {
                    var html = '';
                    html += '<a href="javascript:;" class="btn btn-xs btn-primary" onclick="doAction.edit(' + row.id + ')">编辑</a> ';
                    html += '<a href="javascript:;" class="btn btn-xs btn-danger" onclick="utils.doAction(this,\'refreshTable\')" ' +
                        'data-params="id=' + row.id + '" data-url="/admin/mall.goods_attr/del.html">删除</a>';
                    return html;
                }
            }
        ],
        onPostBody:function () {
            formHelper.initForm();
        }
    });
    var doAction={
        add:function(){ //添加属性
            utils.openDialog({
                title: '添加品牌',
                area: ['800px', '458px'],
                content: '/admin/mall.goods_attr/add.html'
            });
        },
        edit:function(id){
            utils.openDialog({
                title: '修改属性',
                area: ['800px', '458px'],
                offset:100,
                content: '/admin/mall.goods_attr/edit.html?id=' + id
            });
        },
        switchAction:function (event,state) {
            var id=event.currentTarget.dataset.id;
            var name=event.currentTarget.name;
            var value=state==true?1:0;
            doAction.setStatus(id,name+':'+value);
        },
        setStatus:function (ids,params,url,callfn) {
            url = url || 'multi.html';
            utils.getAjax({
                url:url,
                data:{ids:ids,params:params},
                success:function (res) {
                    if(res.code==0){
                        utils.msg(res.msg,function () {
                            $('#grid').bootstrapTable('refresh');
                        });
                    }else{
                        if (typeof callfn === "function") {
                            callfn();
                        } else {
                            if (callfn == "refresh") $('#grid').bootstrapTable('refresh');
                        }
                    }
                }
            })
        },
    }
</script>
{/block}