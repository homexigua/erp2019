{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="box">
            <div class="box-body">
                <form class="form-horizontal" id="form" data-callfn="refreshTable">
                    <div class="form-group" style="margin-bottom:15px;">
                        <label class="col-sm-2 control-label require">属性名称</label>
                        <div class="col-sm-9">
                            {:FormBuild::_text('attr_name','',['datatype'=>'*'])}
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom:0;">
                        <label class="col-sm-2 control-label require">属性值</label>
                        <div class="col-sm-10">
                            {:FormBuild::_tags('attr_value','')}
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom:15px;">
                        <div class="col-sm-10 col-sm-offset-2">
                            <span id="helpBlock" class="help-block">输入属性值，回车输入下一个</span>
                        </div>
                    </div>
                   <!-- <div class="form-group" style="margin-bottom:15px;">
                        <label class="col-sm-2 control-label">录入方式</label>
                        <div class="col-sm-10 form-control-static">
                            {:FormBuild::_radio('input_mode','1',['list-data'=>
                                [['id'=>'1','name'=>'列表选择'],['id'=>'0','name'=>'手工录入']]
                            ])}
                        </div>
                    </div>-->
                    <div class="form-group" style="margin-bottom:15px;">
                        <label class="col-sm-2 control-label require">属性类型</label>
                        <div class="col-sm-10 ">
                            <input type="checkbox" class="f-switch" name="is_sku"
                                   data-option='{"onText":"SKU属性","offText":"SKU属性","onColor":"success","offColor":"default","size":"lg"}'
                                   placeholder="" value="1" checked/>
                            <input type="checkbox" class="f-switch" name="is_search"
                                   data-option='{"onText":"搜索属性","offText":"搜索属性","onColor":"success","offColor":"default","size":"lg"}'
                                   placeholder="" value="1"/>
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom:15px;">
                        <label class="col-sm-2 control-label">排序</label>
                        <div class="col-sm-4">
                            {:FormBuild::_text('show_order','0',['datatype'=>'n'])}
                        </div>
                    </div>
                    <div class="fixed-foot text-right">
                        <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                        {:FormBuild::_button('立即保存',['class'=>'btn btn-info btn-sub'])}
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
<div style="height:15px;"></div>
</body>
<script>
    $(function () {
        formHelper.initForm();
        utils.ajaxSubForm();
    });
</script>
{/block}