{extend name="admin@public/layout" /}
{block name="head"}
<script src="https://cdnjs.cloudflare.com/ajax/libs/numeric/1.2.6/numeric.min.js"></script>
{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div class="table-responsive">
                    <div id="edit-grid"></div>
                </div>
                <a href="javascript:;" onclick="getData()">获取数据</a>
                <div id="my"></div>
            </div>
        </div>
    </section>
</div>
<div style="height:15px;"></div>
</body>
<?php
$config = D('BillConfig', 'inventory')->getConfig('101');
?>
<script src="/static/plugins/jexcel/dist/js/jexcel-extend.js"></script>
<script>

    var goodsItem = [
        {
            "depot_id":2,
            "sku_id": 111,
            "goods_name": "汽车全包围双层定制脚垫",
            "sku_name": "红色 雪佛兰",
            "goods_unit": "包",
            "group_unit": [{"id": "套","name": "套","number":1}, {"id": "包","name": "包","number":12}, {"id": "箱","name": "箱","number":48}],
            "unit_number":12,
            "goods_price": 382.80,
            "base_goods_price": 382.80,
            "goods_rate": 0.03,
            "is_free": 1
        },
        {
            "sku_id": 222,
            "goods_name": "网易云音乐氧气蓝牙耳机Pro版",
            "sku_name": "",
            "goods_unit": "台",
            "group_unit": [{"id":"台","name": "台", "number": 1},{"id":"车","name": "车", "number": 24}],
            "unit_number":1,
            "goods_price": 198.02,
            "base_goods_price": 382.80,
            "goods_rate": 0.06,
            "is_free": 0
        }];
    var editGoodsItem = {
        "sku_id": 1,
        "goods_name": "我是修改的内容",
        "sku_name": "红色 雪佛兰",
        "goods_unit": "中国",
        "group_unit": [{"id": "套","name": "套","number":1}, {"id": "包","name": "包","number":12}],
        "goods_price": 998,
        "goods_rate": 0.03,
        "is_free": 0
    };

    var header = JSON.parse('{$config.detail_config|raw}');
    var items = [];
    var filter = {
        unit:function(instance, cell, col, row, source) {
            var item = items[row] || '';
            if(item=='') return [];
            return item.group_unit;
        }
    };
    In('jexcel', function () {
        jexcelExtend.init('#edit-grid',items,{
            tableOverflow:true,
            onchange:function (obj, cell, newValue,oldValue) {
                var position = $(cell).prop('id').split('-'); //获取单元格位置
                var col=parseInt(position[0]);
                var row=parseInt(position[1]);
                if(header[col].field=='goods_unit'){ //处理单位
                    var item = items[row] || '';
                    if(item!=''){
                        var selectUnit = $.grep(item.group_unit,function (row,index) {
                            return row.id==newValue;
                        });
                        var unit_number = selectUnit[0].number;
                        $(obj).jexcel('setValue', jexcelExtend.getCol('unit_number')+(row+1), unit_number,true);
                    }
                }
                //base_goods_price*baozhuangliang
                //$(obj).jexcel('setValue', jexcelExtend.getCol('goods_price')+(row+1), unit_goods_price,true);
            }
        });
        $('#edit-grid').jexcel('updateSettings', {
            table: function (instance, cell, col, row, val, id) {
                jexcelExtend.formatCols(instance, cell, col, row, val, id); //格式化列数据
                jexcelExtend.formatFooter(instance, cell, col, row, val, id); //格式化底部数据
            }
        });
    });

    /**
     * 添加行
     */
    function addRow() {
        jexcelExtend.addRow('#edit-grid',goodsItem);
    }
    /**
     * 编辑行
     */
    function editRow(row) {
        jexcelExtend.editRow('#edit-grid',row,editGoodsItem);
    }

    function getData() {
        //console.info($('#edit-grid').jexcel('getValue', 'F1'));
        //console.info($('#edit-grid').jexcel('getHeaders'));
        console.info($('#edit-grid').jexcel('getData', false));
    }
</script>
{/block}