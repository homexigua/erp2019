{extend name="admin@public/layout" /}
{block name="title"}商品单位列表{/block}
{block name="head"}{/block}
{block name="body"}
<!-- 内容 -->
<section class="content">
    <div class="nav-tabs-custom">
        {:D('Rule','auth')->getSubMenu('87')}
        <div class="box-body">
            <div class="toolbar">
                <button type="button" class="btn btn-primary" onclick="doAction.add()">
                    <i class="fa fa-plus"></i> 添加
                </button>
            </div>
            <table id="grid"></table>
        </div>
    </div>
</section>
<script>
    utils.btable({
        url:'/admin/mall.unit/index.html',
        columns:[
            { field:'state',checkbox:true },
            { field:'alias',title:'单位名称',width:140,align:'center'},
            { field:'unit',title:'单位',width:200,align:'center'},
            { field:'is_base',title:'类型',width:140,align:'center',formatter:function (value,row,index) {
                if(value==0){
                    return '<span class="label label-info">基本单位</span>';
                }else{
                    return '<span class="label label-warning">多单位组</span>';
                }
            }},
            { field:'unit_num_text',title:'单位组配置'},
            { field:'status',title:'状态',width:140,align:'center',formatter:function (value,row,index) {
                var  checked = value==1?'checked':'';
                var html='<input type="checkbox" name="status" class="f-switch"\n' +
                    'data-option=\'{"onText":"启用","offText":"禁用","onColor":"success","offColor":"default","callfn":"doAction.switchAction"}\'\n' +
                    'data-id="'+row.id+'"\n' +
                    'placeholder="" value="1" '+checked+'/>';
                return html;
            }},
            {field: 'operate', title: '操作', width:120,
                formatter: function (value, row, index) {
                    var html = '';
                    html += '<a href="javascript:;" onclick="doAction.edit(' + row.id + ')">编辑</a> ';
                    html += '<a href="javascript:;" onclick="utils.doAction(this,\'refreshTable\')" ' +
                        'data-params="id=' + row.id + '" data-url="/admin/mall.unit/del.html">删除</a>';
                    return html;
                }
            }
        ],
        onPostBody:function () {
            formHelper.switch();
        }
    });
    var doAction={
        add:function(){
            utils.openDialog({
                title: '类目分组',
                area: ['800px', '450px'],
                content: '/admin/mall.unit/add.html'
            });
        },
        edit:function(id){
            utils.openDialog({
                title: '类目分组',
                area: ['800px', '450px'],
                content: '/admin/mall.unit/edit.html?id=' + id
            });
        },
        switchAction:function (event,state) {
            var id=event.currentTarget.dataset.id;
            var name=event.currentTarget.name;
            var value=state==true?1:0;
            doAction.setStatus(id,name+':'+value);
        },
        setStatus:function (ids,params,url,callfn) {
            url = url || 'multi.html';
            utils.getAjax({
                url:url,
                data:{ids:ids,params:params},
                success:function (res) {
                    if(res.code==0){
                        utils.msg(res.msg);
                    }else{
                        if (typeof callfn === "function") {
                            callfn();
                        } else {
                            if (callfn == "refresh") $('#grid').bootstrapTable('refresh');
                        }
                    }
                }
            })
        },

    };
</script>
{/block}