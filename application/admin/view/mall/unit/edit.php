{extend name="admin@public/layout" /}
{block name="title"}编辑商品单位{/block}
{block name="head"}{/block}
{block name="body"}
<section class="content">
    <div class="box box-default">
        {eq name="openTab" value="iframe"}
        <div class="box-header with-border">
            <h3 class="box-title">添加商品单位</h3>
        </div>
        {/eq}
        <div class="box-body">
            <form class="form-horizontal" id="form">
                <input type="hidden" name="id" value="{$info.id}">
                <input type="hidden" name="is_base" value="{$info.is_base}">
               {eq name='info.is_base' value='0'}
                <div class="form-group">
                    <label class="col-sm-2 control-label require">计量单位</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="请输入计量单位" name="unit" datatype="*" value="{$info.unit}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">单位别名</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="请输入计量单位显示别名" name="alias" value="{$info.alias}"/>
                    </div>
                </div>
                {else}
                <div class="form-group">
                    <label class="col-sm-2 control-label">单位组别名</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="请输入计量单位组显示别名" name="alias" value="{$info.alias}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label require">
                        计量单位组
                    </label>
                    <div class="col-sm-10">
                        <div class="f-input-array mc-input-group2" data-name="unit_num">
                            <div class="data-row-title">
                                <dd>计量单位</dd>
                                <dd>数量</dd>
                            </div>
                            {volist name='info.unit_num' id='vo'}
                            <div class="data-row">
                                <input type="text" class="form-control input1" data-name="unit" datatype="*" value="{$vo.unit}" placeholder="请输入计量单位名称">
                                <input type="text" class="form-control input2" data-name="num" value="{$vo.num}" datatype="num"  placeholder="请输入包含的最小单位数量">
                            </div>
                            {/volist}
                            <a href="javascript:;" onclick="formHelper.inputArrayAddrow(this)" class="btn btn-info" style="margin-bottom:10px;">增加一行</a>
                            <span style="font-size:12px;color:#989898;margin-left:15px;">计量单位组请由小到大输入，数量为包含的最小单位的数量。第一行为最小单位，数量必须为1</span>
                        </div>
                    </div>
                </div>
                {/eq}
                <div class="fixed-foot text-right">
                    <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                    <button type="button" class="btn btn-sub btn-info">立即保存</button>
                </div>
            </form>
        </div>
    </div>
    <div style="height:30px;"></div>
</section>
<script>
    $(function () {
        formHelper.initForm();
        utils.ajaxSubForm(function (res) {
            if(res.code==1){
                if(openTab === 'dialog'){
                    var index = parent.layer.getFrameIndex(window.name);
                    parent.layer.close(index);
                    parent.$('#grid').bootstrapTable('refresh');
                }else{
                    utils.confirm(res.msg,{ btn: ['继续添加', '关闭页面'] }
                        ,function(index,layero){
                            layer.close(index);
                            utils.urlHref('/admin/mall.unit/edit.html');
                        }
                        ,function(index,layero){
                            layer.close(index);
                            top.closeCurrentTab();
                        }
                    );
                }
            }else{
                utils.msg(res.msg);
            }
        });
    });
</script>
{/block}