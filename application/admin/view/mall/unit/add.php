{extend name="admin@public/layout" /}
{block name="title"}添加商品单位{/block}
{block name="head"}{/block}
{block name="body"}
<section class="content">
    <div class="box box-default">
        {eq name="openTab" value="iframe"}
        <div class="box-header with-border">
            <h3 class="box-title">添加商品单位</h3>
        </div>
        {/eq}
        <div class="box-body">
            <form class="form-horizontal" id="form" {empty name='$Request.param.source'}data-callfn="refreshTable"{/empty}>
                <div class="form-group hidden">
                    <label class="col-sm-2 control-label require">单位类型</label>
                    <div class="col-sm-10">
                        <select name="is_base" class="form-control" id="is_base" onchange="getForm()">
                            <option value="0">基本单位</option>
                            <option value="1">多计量单位组</option>
                        </select>
                    </div>
                </div>
                <div id="unit-box"></div>
                <div class="fixed-foot text-right">
                    <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                    <button type="button" class="btn btn-sub btn-info">立即保存</button>
                </div>
            </form>
        </div>
    </div>
    <div style="height:30px;"></div>
</section>
<script type="text/html" id="unit-box-0">
    <div class="form-group">
        <label class="col-sm-2 control-label require">计量单位</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" placeholder="请输入计量单位" name="unit" datatype="*" value=""/>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">备注</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" placeholder="备注信息" name="alias" value=""/>
        </div>
    </div>
</script>
<script type="text/html" id="unit-box-1">
    <div class="form-group">
        <label class="col-sm-2 control-label">单位组别名</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" placeholder="请输入计量单位组显示别名" name="alias" value=""/>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label require">
            计量单位组
        </label>
        <div class="col-sm-10">
            <div class="f-input-array mc-input-group2" data-name="unit_num">
                <div class="data-row-title">
                    <dd>计量单位</dd>
                    <dd>数量</dd>
                </div>
                <div class="data-row">
                    <input type="text" class="form-control input1" data-name="unit" datatype="*" value="" placeholder="请输入计量单位名称">
                    <input type="text" class="form-control input2" data-name="num" value="1" datatype="num"  placeholder="请输入包含的最小单位数量">
                </div>
                <a href="javascript:;" onclick="formHelper.inputArrayAddrow(this)" class="btn btn-info" style="margin-bottom:10px;">增加一行</a>
                <span style="font-size:12px;color:#989898;margin-left:15px;">计量单位组请由小到大输入，数量为包含的最小单位的数量。第一行为最小单位，数量必须为1</span>
            </div>
        </div>
    </div>
</script>
<script>
    $(function () {
        getForm();
        utils.ajaxSubForm();
    });
    /*选择单位类型*/
    function getForm() {
        var value=$('#is_base').val();
        var html = juicer($('#unit-box-'+value).html(),{});
        $('#unit-box').empty().html(html);
        formHelper.initForm();
    }
</script>
{/block}