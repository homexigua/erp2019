{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="nav-tabs-custom">
            <div class="box-body">
                <div class="toolbar">
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-primary" onclick="doAction.add();">
                            <i class="fa fa-plus"></i> 添加店铺
                        </button>
                    </div>
                </div>
                <table id="grid"></table>
            </div>
        </div>
    </section>
</div>
</body>
<script>
    utils.btable({
        url: '/admin/site/index.html',
        columns: [
            {field: 'state', checkbox: true},
            {field: 'site_name', title: '站点名称'},
            {field: 'owner_id', title: '管理员',width:150},
            {field: 'status', title: '状态',width:130},
            {
                field: 'operate', title: '操作', width: 100,
                formatter: function (value, row, index) {
                    var html = '';
                    html += '<a href="javascript:;" onclick="doAction.edit(' + row.id + ')">编辑</a> ';
                    return html;
                }
            }
        ]
    });
    var doAction = {
        add: function () {
            var rows = $('#grid').bootstrapTable('getSelections');
            var pid = rows.length==0 ? 0 : rows[0].id;
            utils.openDialog({
                title: '添加站点',
                area: ['800px', '600px'],
                content: '/admin/site/add.html'
            });
        },
        edit: function (id) {
            utils.openDialog({
                title: '修改站点',
                area: ['800px', '550px'],
                content: '/admin/site/edit.html?id=' + id
            });
        },
    };
</script>
{/block}