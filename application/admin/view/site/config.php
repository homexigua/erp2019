{extend name="admin@public/layout" /}
{block name="head"}
<style>
    .form-group{margin-bottom:15px!important;}
</style>
{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="nav-tabs-custom">
            {:D('Rule','auth')->getSubMenu('6')}
            <div class="box-body">
                <form class="form-horizontal" id="form" action="/admin/site/edit.html">
                    <input type="hidden" name="id" value="{$info.id}" readonly>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">店铺名称</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" placeholder="请输入店铺名称" name="site_name" value="{$info.site_name}"  datatype="*" autocomplete="off" readonly/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">店铺简称</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" placeholder="" name="site_alias" value="{$info.site_alias}"  autocomplete="off"/>
                            <span class="help-block">店铺简称建议6字以内，方便排版显示</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">联系人</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" placeholder="" name="site_contact" value="{$info.site_contact}" datatype="*" nullmsg="请输入联系人姓名" autocomplete="off"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">联系电话</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" placeholder="" name="site_tel" value="{$info.site_tel}" datatype="*" nullmsg="请输入手机号码" autocomplete="off"/>
                            <span class="help-block">这里的联系人信息仅用于我们与您取得联系，不会显示到网站前端。</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">绑定域名</label>
                        <div class="col-sm-4">
                            <textarea class="form-control" name="domain" rows="3" placeholder="请输入站点域名">{$info.domain}</textarea>
                            <span class="help-block">请输入绑定的域名，多个域名一行一个</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">所在区域</label>
                        <div class="col-sm-4">
                            <div id="cxselect" class="f-cx-select">
                                <select class="prov" name="site_prov" data-value="{$info.site_prov}"></select>
                                <select class="city" name="site_city" data-value="{$info.site_city}"></select>
                                <select class="dist" name="site_dist" data-value="{$info.site_dist}"></select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">联系地址</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" placeholder="请输入联系地址" name="site_addr" value="{$info.site_addr}" autocomplete="off"/>
                        </div>
                    </div>
                    <div class="fixed-foot text-right">
                        <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                        <button type="submit" class="btn btn-info">立即保存</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
</body>

<script>
    $(function () {
        formHelper.initForm();
        utils.ajaxSubForm();
    });
</script>
{/block}