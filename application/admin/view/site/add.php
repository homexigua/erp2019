{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <form class="form-horizontal" id="form" data-callfn="refreshTable">
            <div class="form-group">
                <label class="col-sm-2 control-label require">店铺名称</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" placeholder="请输入店铺名称" name="site_name" value=""  datatype="*" autocomplete="off"/>
                </div>
                {if $accountType=='is_platform'}
                <label class="col-sm-2 control-label">所属行业</label>
                <div class="col-sm-3">
                    <select name="trade_id" class="form-control">
                        <option value="">请选择行业</option>
                        {volist name=":D('Trade')->where('status',1)->select()" id="vo"}
                        <option value="{$vo.id}">{$vo.trade_name}</option>
                        {/volist}
                    </select>
                </div>
                {/if}
            </div>
            <div id="admin-box">
                <div class="form-group">
                    <label class="col-sm-2 control-label require">管理账号</label>
                    <div class="col-sm-9">
                        <div class="input-group">
                            <input type="text" class="form-control"  name="username" value=""  datatype="*" placeholder="请输入管理账号" autocomplete="off">
                            <span class="input-group-btn">
                                <a href="javascript:;" onclick="changeAdmin(this)" data-type="1" class="btn btn-warning">已有管理账号</a>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label require">管理密码</label>
                    <div class="col-sm-9">
                        <input type="password" class="form-control" placeholder="请输入管理密码" name="password" value=""  datatype="*"/>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label require">联系人</label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" placeholder="请输入联系人" name="site_contact" value="" datatype="*" nullmsg="请输入联系人姓名" autocomplete="off"/>
                </div>
                <label class="col-sm-2 control-label require">联系电话</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" placeholder="请输入联系电话" name="site_tel" value="" datatype="*" nullmsg="请输入手机号码" autocomplete="off"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label require">店铺类型</label>
                <div class="col-sm-3">
                    <select name="site_level" class="form-control">
                        <option value="1">基础电商</option>
                        <option value="2">电商+ERP</option>
                        <option value="3">电商+ERP+客户管理</option>
                    </select>
                </div>
                <label class="col-sm-2 control-label require">员工数量</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" placeholder="最多员工数量,0为不限制" name="staff_num" value=""  datatype="n" autocomplete="off"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">开通时间</label>
                <div class="col-sm-9">
                    <input class="form-control f-laydate" AUTOCOMPLETE="off" name="begin_end" data-option='{"range":true}' placeholder="请选择店铺的开通日期区间" datatype="*">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">绑定域名</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="domain" rows="3" placeholder="请输入站点域名">{$info.domain_html|raw}</textarea>
                    <span class="help-block">请输入绑定的域名，多个域名一行一个</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">所在区域</label>
                <div class="col-sm-9">
                    <div id="cxselect" class="f-cx-select">
                        <select class="prov" name="site_prov"></select>
                        <select class="city" name="site_city"></select>
                        <select class="dist" name="site_dist"></select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">联系地址</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" placeholder="请输入联系地址" name="site_addr" value="" autocomplete="off"/>
                </div>
            </div>
            <div class="fixed-foot text-right">
                <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                <button type="submit" class="btn btn-info">立即保存</button>
            </div>
        </form>
    </section>
</div>
<div style="height:15px;"></div>
</body>
<script type="text/html" id="admin-tpl">
    {@if type==1}
    <div class="form-group">
        <label class="col-sm-2 control-label">管理账号</label>
        <div class="col-sm-9">
            <div class="input-group">
                <input type="text" class="form-control f-selectpage" name="admin_id" value=""
                       data-url="/admin/admin/selectpage.html"
                       data-show="username"
                       data-key="id"
                       data-search="name|mobile"
                       data-custom="session_key='site'"
                       data-option='{"multiple":false,"andOr":"OR","formatItemFun":"formatItem","selectFun":"selectFun"}'
                       placeholder="请选择管理账号"
                />
                <span class="input-group-btn">
                    <a href="javascript:;" onclick="changeAdmin(this)" data-type="2" class="btn btn-info">新建管理账号</a>
                </span>
            </div>
        </div>
    </div>
    {@else}
    <div class="form-group">
        <label class="col-sm-2 control-label">管理账号</label>
        <div class="col-sm-9">
            <div class="input-group">
                <input type="text" class="form-control"  name="username" value=""  datatype="*" placeholder="请输入管理账号" autocomplete="off">
                <span class="input-group-btn">
                <a href="javascript:;" onclick="changeAdmin(this)" data-type="1" class="btn btn-warning">已有管理账号</a>
            </span>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">管理密码</label>
        <div class="col-sm-9">
            <input type="password" class="form-control" placeholder="请输入管理密码" name="password" value=""  datatype="*"/>
        </div>
    </div>
    {@/if}
</script>
<script>
    $(function () {
        formHelper.initForm();
        utils.ajaxSubForm();
    });
    function changeAdmin(obj) {
        var type=$(obj).data('type');
        var html = juicer($('#admin-tpl').html(),{type:type});
        $('#admin-box').empty().html(html);
        formHelper.selectpage();
    }
    function formatItem(data) {
        return data.username+'<span class="pull-right">['+data.nickname+'&nbsp;&nbsp;'+data.mobile+']</span>';
    }
    function selectFun(data) {
        $('input[name="username"]').val(data.username);
        $('input[name="contact_man"]').val(data.name);
        $('input[name="contact_phone"]').val(data.mobile);
    }
</script>
{/block}