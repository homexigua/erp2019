{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="box">
            <div class="box-body">
                <form class="form-horizontal" id="form" data-callfn="refreshTable">
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">字典编码</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="dict_code" value="" placeholder="请输入字典编码" datatype="*" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">字典值</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="dict_value" value="" placeholder="请输入字典值" datatype="*" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">描述</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" name="dict_desc" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">排序</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="show_order" value="0" placeholder="请输入排序" datatype="n" />
                        </div>
                    </div>
                    <div class="fixed-foot text-right">
                        <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                        <button type="button" class="btn btn-info btn-sub">立即保存</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
<div style="height:15px;"></div>
</body>
<script>
    $(function () {
        formHelper.initForm();
        utils.ajaxSubForm();
    });
</script>
{/block}