{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="nav-tabs-custom">
            {:D('Rule','auth')->getSubMenu('8')}
            <div class="row">
                <div class="col-md-3 col-lg-2">
                    <div style="height:100%;background:#fff;overflow-y:auto;border-right:1px solid #e5e5e5">
                        <div style="padding:10px 8px 0 8px;">
                            {eq name='loginInfo.site_id' value='0'}
                            {eq name='loginInfo.trade_id' value='0'}
                            <div class="pull-left">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default" onclick="doAction.subAdd();"><i class="fa fa-plus"></i></button>
                                    <button type="button" class="btn btn-default" onclick="doAction.subEdit();"><i class="fa fa-edit"></i></button>
                                    <button type="button" class="btn btn-default" onclick="doAction.subDel();"><i class="fa fa-trash"></i></button>
                                </div>
                            </div>
                            {/eq}
                            {/eq}
                            <div class="pull-right">
                                <a href="javascript:;" onclick="doAction.refreshClass();" class="expand fa fa-refresh pull-right" style="color:#666"></a>
                                <a href="javascript:;" data-type="1" class="expand fa fa-plus-square pull-right" style="color:#666"></a>
                                <a href="javascript:;" data-type="0" class="expand fa fa-minus-square pull-right" style="color:#666"></a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <hr style="margin:10px 0;">
                        <ul class="ztree" id="ztree" style="padding-bottom:20px;"></ul>
                    </div>
                </div>
                <div class="col-md-9 col-lg-10">
                    <div class="box">
                        <div class="box-header">
                            <form id="searchForm" style="display:none;">
                                <input type="hidden" name="class_code" id="class_code" value="-1" data-op="=" />
                            </form>
                        </div>
                        <div class="box-body">
                            <div class="toolbar">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-primary" onclick="doAction.add();">
                                        <i class="fa fa-plus"></i> 添加
                                    </button>
                                    <button type="button" class="btn btn-danger" onclick="doAction.del();">
                                        <i class="fa fa-trash"></i> 删除
                                    </button>
                                    <button type="button" class="btn bg-gray" onclick="doAction.refresh();">
                                        <i class="fa fa-refresh"></i> 更新缓存
                                    </button>
                                </div>
                            </div>
                            <table id="grid"></table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<input type="hidden" id="class_id" />
</body>
<script>
    In('ztree',function () {
        $.get('/admin/dict_class/getlist.html',function (nodes) {
            $.fn.zTree.init($('#ztree'), {
                data: {
                    view: { showLine: false },
                    simpleData: { enable: true, idKey: "id", pIdKey: "pid", rootPId: 0 }
                },
                callback: { onClick: doAction.setCode }
            }, nodes);
            var zTree = $.fn.zTree.getZTreeObj("ztree");
            zTree.expandAll(true);
        });
        $('.expand').click(function () {
            var zTree = $.fn.zTree.getZTreeObj("ztree");
            var type=$(this).data('type');
            if(type==1) zTree.expandAll(true);
            if(type==0) zTree.expandAll(false);
        });
    });
    utils.btable({
        url:'/admin/dict/index.html?is_page=0',
        pagination: false,
        singleSelect: false, //是否启用单选
        pageSize: 15,
        sortName:'show_order',
        sortOrder:'asc',
        columns: [
            {field: 'state', checkbox: true},
            {field: 'dict_code', title: '字典编码',width:220},
            {field: 'dict_value', title: '字典名称',width:320},
            {field: 'dict_desc', title: '备注'},
            {field: 'show_order', title: '排序',width:100},
            {
                field: 'operate', title: '操作', width: 100,
                formatter: function (value, row, index) {
                    var html = '';
                    html += '<a href="javascript:;" onclick="doAction.edit(' + row.id + ')">编辑</a> ';
                    html += '<a href="javascript:;" onclick="utils.doAction(this,\'refreshTable\')" ' +
                        'data-params="id=' + row.id + '" data-url="del.html">删除</a>';
                    return html;
                }
            }
        ]
    });
    var doAction = {
        setCode:function(event, treeId, treeNode){
            $('#class_code').val(treeNode.code);
            $('#class_id').val(treeNode.id);
            $('#grid').bootstrapTable('refresh');
        },
        subAdd:function(){
            utils.openDialog({
                title: '添加字典分类',
                area: ['800px', '350px'],
                content: '/admin/dict_class/add.html'
            });
        },
        subEdit:function(){
            var class_id = $('#class_id').val();
            if(class_id==''){
                layer.msg('请选择字典分类');
                return false;
            }
            utils.openDialog({
                title: '修改字典分类',
                area: ['800px', '350px'],
                content: '/admin/dict_class/edit.html?id='+class_id
            });
        },
        subDel:function(){
            var class_id = $('#class_id').val();
            if(class_id==''){
                layer.msg('请选择字典分类');
                return false;
            }
            layer.confirm('确认删除吗？',{icon:3},function (index) {
                layer.close(index);
                $.post('/admin/dict_class/del.html',{id:class_id},function (res) {
                    if(res.code==1){
                        layer.msg('删除成功！',{icon:1,time:1200},function () {
                            utils.refreshPage();
                        });
                    }else{
                        layer.msg(res.msg);
                    }
                });
            });
        },
        add: function () {
            var class_code = $('#class_code').val();
            if(class_code==''){
                layer.msg('请选择字典分类');
                return false;
            }
            utils.openDialog({
                title: '添加字典',
                area: ['800px', '350px'],
                content: '/admin/dict/add.html?class_code='+class_code
            });
        },
        edit: function (id) {
            utils.openDialog({
                title: '修改字典',
                area: ['800px', '350px'],
                content: '/admin/dict/edit.html?id=' + id
            });
        },
        del: function () {
            var ids = $.map($('#grid').bootstrapTable('getSelections'),function (row,index) {
                return row.id;
            });
            if(ids.length==0){
                layer.msg('请选择操作字典');
                return false;
            }
            layer.confirm('确认删除吗？',{icon:3},function (index) {
                layer.close(index);
                $.post('/admin/dict/del.html',{id:ids},function (res) {
                    if(res.code==1){
                        layer.msg('删除成功！',{icon:1,time:1200},function () {
                            $('#grid').bootstrapTable('refresh');
                        });
                    }else{
                        layer.msg(res.msg);
                    }
                });
            });
        },
        refresh:function () {
            $.get("/admin/dict/refresh.html",function (res) {
                layer.alert(res.msg,function (index) {
                    $('#grid').bootstrapTable('refresh');
                    layer.close(index);
                });
            });
        },
        refreshClass:function () {
            $.get("/admin/dict_class/refresh.html",function (res) {
                layer.alert(res.msg,function (index) {
                    utils.refreshPage();
                    layer.close(index);
                });
            });
        }
    };
</script>
{/block}