{extend name="admin@public/layout" /}
{block name="head"}
<style>
    .row .box-body{background:#fff;}
</style>
{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="nav-tabs-custom">
            {:D('Rule','auth')->getSubMenu('8')}
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="prov-toolbar">
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary" data-label="省份" data-dom="#prov-grid" onclick="area.add(this)"><i class="fa fa-plus"></i> 添加</button>
                        <button type="button" class="btn btn-danger" data-label="省份" data-dom="#prov-grid" onclick="area.del(this)"><i class="fa fa-trash"></i> 删除</button>
                    </div>
                </div>
                <div class="box-body">
                    <table  id="prov-grid"></table>
                </div>
            </div>
            <div class="col-md-4">
                <div class="city-toolbar">
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary" data-label="城市" data-dom="#city-grid" onclick="area.add(this)"><i class="fa fa-plus"></i> 添加</button>
                        <button type="button" class="btn btn-danger" data-label="城市" data-dom="#city-grid" onclick="area.del(this)"><i class="fa fa-trash"></i> 删除</button>
                    </div>
                </div>
                <div class="box-body">
                    <table  id="city-grid"></table>
                </div>
            </div>
            <div class="col-md-4">
                <div class="dist-toolbar">
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary" data-label="地区" data-dom="#dist-grid" onclick="area.add(this)"><i class="fa fa-plus"></i> 添加</button>
                        <button type="button" class="btn btn-danger" data-label="地区" data-dom="#dist-grid" onclick="area.del(this)"><i class="fa fa-trash"></i> 删除</button>
                    </div>
                </div>
                <div class="box-body">
                    <table  id="dist-grid"></table>
                </div>
            </div>
        </div>
    </section>
</div>
{include file="area/add" /}
<script>
    In.config('serial',true); //串行加载
    In('btable','editable','btable-editable',function () {
        $('#prov-grid').bootstrapTable({
            url:'index.html?pid=0',
            toolbar:'.prov-toolbar',
            clickToSelect:false,
            singleSelect:true,
            selectItemName:'prov-radio',
            idField:'id',
            editableUrl:'editable.html',
            columns:[
                {field:'state',radio:true},
                {field:'name',title:'省份',width:100,editable:formatter.name},
                {field:'show_order',title:'排序',width:100,editable:formatter.show_order},
            ],
            onCheck:function (row) {
                $('#city-grid').bootstrapTable('refresh',{
                    url:'index.html?pid='+row.id
                });
                $('#dist-grid').bootstrapTable('refresh');
            }
        });
        $('#city-grid').bootstrapTable({
            toolbar:'.city-toolbar',
            clickToSelect:false,
            singleSelect:true,
            selectItemName:'city-radio',
            idField:'id',
            editableUrl:'editable.html',
            columns:[
                {field:'state',radio:true},
                {field:'name',title:'城市',width:100,editable:formatter.name},
                {field:'show_order',title:'排序',width:100,editable:formatter.show_order},
            ],
            onCheck:function (row) {
                $('#dist-grid').bootstrapTable('refresh',{
                    url:'index.html?pid='+row.id
                });
            }
        });
        $('#dist-grid').bootstrapTable({
            toolbar:'.dist-toolbar',
            clickToSelect:false,
            singleSelect:true,
            selectItemName:'dist-radio',
            idField:'id',
            editableUrl:'editable.html',
            columns:[
                {field:'state',radio:true},
                {field:'name',title:'地区',width:100,editable:formatter.name},
                {field:'show_order',title:'排序',width:100,editable:formatter.show_order},
            ]
        });
    });
    var area={
        add:function (obj) {
            var label=$(obj).data('label');
            var dom=$(obj).data('dom');
            var pid=0;
            if(dom=='#city-grid'){ //添加城市
                var rows = $('#prov-grid').bootstrapTable('getSelections');
                if(rows.length==0){
                    layer.msg('请选择省份');
                    return false;
                }
                pid=rows[0].id;
            }
            if(dom=='#dist-grid'){ //添加地区
                var rows = $('#city-grid').bootstrapTable('getSelections');
                if(rows.length==0){
                    layer.msg('请选择城市');
                    return false;
                }
                pid=rows[0].id;
            }
            var data={label:label,pid:pid};
            var html=juicer($('#area-add-tpl').html(), data);
            utils.openDialog({
                type:1,
                title: '添加区域',
                area: '500px',
                content:html,
                success: function(layero, index){
                    utils.ajaxSubForm(function (res) {
                        layer.close(index);
                        $(dom).bootstrapTable('refresh',{
                            url:'index.html?pid='+pid
                        });
                    });
                }
            });
        },
        del:function (obj) {
            var label=$(obj).data('label');
            var dom=$(obj).data('dom');
            var rows = $(dom).bootstrapTable('getSelections');
            if(rows.length==0){
                layer.msg('请选择'+label);
                return false;
            }
            var id=rows[0].id;
            layer.confirm('确认删除吗？',{icon:3},function (index) {
                layer.close(index);
                utils.getAjax({
                    url:'del.html',
                    data:{id:id},
                    success:function (res) {
                        if(res.code==1){
                            $(dom).bootstrapTable('refresh');
                        }else{
                            layer.msg(res.msg);
                        }
                    }
                });
            })
        }
    };
    var formatter={
        name:{
            type: 'text',
            validate: function (v) { //验证规则自定义
                if (valid.isEmpty(v)) return '区域名称不能为空！';
            }
        },
        show_order:{
            type: 'text',
            validate: function (v) { //验证规则自定义
                if (!valid.isNum(v)) return '排序只能为数字！';
            }
        }
    };
</script>
</body>
{/block}