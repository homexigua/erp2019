<script type="text/html" id="area-add-tpl">
    <div style="padding:15px;">
        <form class="form-horizontal" id="form" action="add.html">
            <input type="hidden" name="pid" value="${pid}" />
            <div class="form-group">
                <label class="col-md-2 control-label">${label}</label>
                <div class="col-md-10">
                    <input type="text" class="form-control" placeholder="请输入${label}" name="name" datatype="*" autocomplete="off" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">排序</label>
                <div class="col-md-10">
                    <input type="text" class="form-control" placeholder="请输入排序序号" name="show_order" value="0" datatype="n" autocomplete="off" />
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-10 col-md-offset-2">
                    <button class="btn btn-primary btn-sub" type="button">保存</button>
                    <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                </div>
            </div>
        </form>
    </div>
</script>