{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="box">
            <div class="box-body">
               文件分组/文件列表/文件选择<br/>
                <button onclick="selectFile();">Select File</button>
            </div>
        </div>
    </section>
</div>
</body>
<script>
    var funcNum = utils.getQueryString( 'CKEditorFuncNum' );
    function selectFile() {
        if(funcNum>0){ //ckeditor 回调
            var fileUrl = "/a/a/a/a.html"; //当前选择url
            returnFileUrl(fileUrl);
        }else{ //单选多选处理

        }
    }
    /**
     * ckeditor 返回选择文件
     * @param fileUrl
     */
    function returnFileUrl(fileUrl) {
        window.opener.CKEDITOR.tools.callFunction( funcNum, fileUrl );
        window.close();
    }
</script>
{/block}