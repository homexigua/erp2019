{extend name="admin@public/layout" /}
{block name="head"}
<style>
    .table>tbody>tr>td{vertical-align: middle;}
</style>
{/block}
{block name="body"}
<?php
$group = D('ConfigGroup')->where($accountType,1)->order('show_order asc')->select();
$theme = str_replace('is_','',$accountType);
$themeList = D('Tpl')->getThemeList($theme);
?>
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="nav-tabs-custom">
            {:D('Rule','auth')->getSubMenu('6')}
            <div class="box-body">
                {if $auth->check('/admin/config/add.html',$loginInfo['id'])}
                <div class="toolbar">
                    <button type="button" class="btn btn-primary" onclick="doAction.add();">
                        <i class="fa fa-plus"></i> 添加配置
                    </button>
                </div>
                {/if}
                <ul class="nav nav-tabs" style="margin-top:8px;">
                    {volist name="group" id="vo"}
                    <li {eq name="i" value="1"}class="active"{/eq}><a href="#tab-{$i}" data-toggle="tab">{$vo.group_name}</a></li>
                    {/volist}
                </ul>
                <form class="form-horizontal" id="form" data-callfn="refreshSelfPage">
                    <div class="tab-content">
                        {volist name="group" id="g" key="k"}
                        <div class="tab-pane {eq name="k" value="1"}active{/eq}" id="tab-{$k}">
                        <div class="table-responsive">
                            <table class="table" id="grid">
                                <thead>
                                <tr>
                                    <th width="80">排序</th>
                                    <th width="160" style="text-align:right">变量标题</th>
                                    <th>变量值</th>
                                    <th width="360">描述</th>
                                    <th width="180">变量名</th>
                                    <th width="80">操作</th>
                                </tr>
                                </thead>
                                <tbody>
                                {volist name=":D('config')->getListByGroupId($g['id'])" id="vo"}
                                <tr>
                                    <td>
                                        <input type="hidden" name="config[{$vo.id}][id]" value="{$vo['id']}"/>
                                        <input type="text" class="form-control" name="config[{$vo.id}][show_order]" value="{$vo.show_order}" />
                                    </td>
                                    <td style="text-align:right">{$vo.title}</td>
                                    <td>{$vo._html|raw}</td>
                                    <td>{$vo.tips_name}</td>
                                    <td>&#123;$webconfig.{$vo.name}&#125;</td>
                                    <td>
                                        {if $auth->check('/admin/config/edit.html',$loginInfo['id'])}
                                        <a href="javascript:;" onclick="doAction.edit('{$vo.id}')"><i class="fa fa-edit"></i></a>
                                        {/if}
                                        {if $auth->check('/admin/config/del.html',$loginInfo['id'])}
                                        {eq name="vo.is_system" value="0"}
                                        <a href="javascript:;" onclick="doAction.edit('{$vo.id}')"><i class="fa fa-times-circle text-danger"></i></a>
                                        {/eq}
                                        {/if}
                                    </td>
                                </tr>
                                {/volist}
                                </tbody>
                            </table>
                        </div>
                        {/volist}
                    </div>
                    <div class="fixed-foot text-right">
                        <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                        <button type="button" class="btn btn-info btn-sub">立即保存</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
</body>
<script>
    $(function () {
        formHelper.initForm();
        utils.ajaxSubForm();
    });
    var doAction = {
        add:function () {
            utils.openDialog({
                title: '添加配置',
                area: ['800px', '560px'],
                content: '/admin/config/add.html'
            });
        },
        edit:function (id) {
            utils.openDialog({
                title: '修改配置',
                area: ['800px', '520px'],
                content: '/admin/config/edit.html?id='+id
            });
        }
    };
</script>
{/block}