{extend name="admin@public/layout" /}
{block name="head"}
{:D('Static')->loadCodeEditor()}
{/block}
{block name="body"}
<?php
$group = D('ConfigGroup')->order('show_order asc')->select();
$formTypes = FormBuild::getFormTypes();
$validRules = json_encode(FormBuild::getValidRules());
$radioList = [['id'=>1,'name'=>'是'],['id'=>0,'name'=>'否']];
?>
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="box">
            <div class="box-body">
                <form class="form-horizontal" id="form" data-callfn="refreshPage">
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">配置分组</label>
                        <div class="col-sm-4">
                            {:FormBuild::_select('group_id', '',['list-data'=>$group,'items-key'=>'id','items-name'=>'group_name','empty-option'=>'|请选择配置分组'])}
                        </div>
                        <label class="col-sm-2 control-label require">排序</label>
                        <div class="col-sm-4">
                            {:FormBuild::_text('show_order',0,['datatype'=>'n','placeholder'=>'请输入排序'])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">表单类型</label>
                        <div class="col-sm-4">
                            <select name="form_type" class="form-control">
                                <option value="">请选择表单类型</option>
                                {foreach $formTypes as $vo}
                                {in name="vo.id" value="text,textarea,select,selectPage,image,file"}
                                <option value="{$vo.id}">{$vo.name}</option>
                                {/in}
                                {/foreach}
                            </select>
                        </div>
                        <label class="col-sm-2 control-label require">变量标题</label>
                        <div class="col-sm-4">
                            {:FormBuild::_text("title",'',["placeholder"=>"请输入变量标题","datatype"=>'*'])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">变量名</label>
                        <div class="col-sm-4">
                            {:FormBuild::_text("name",'',["placeholder"=>"请输入变量名","datatype"=>'*'])}
                        </div>
                        <label class="col-sm-2 control-label">变量值</label>
                        <div class="col-sm-4">
                            {:FormBuild::_text("value",'',["placeholder"=>"请输入变量值"])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">配置描述</label>
                        <div class="col-sm-4">
                            {:FormBuild::_text("tips_name",'',["placeholder"=>"请输入配置描述"])}
                        </div>
                        <label class="col-sm-2 control-label require">是否系统</label>
                        <div class="col-sm-4 form-control-static">
                            {:FormBuild::_radio("is_system",'1',["list-data"=>$radioList])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">验证规则</label>
                        <div class="col-sm-10">
                            <input type="input" class="form-control" id="valid" name="valid" data-init=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">表单配置</label>
                        <div class="col-sm-10 form-control-static">
                            <textarea class="form-control" placeholder="请填写表单配置" rows="3" name="form_config" id="form_config"></textarea>
                            <div class="help-block">表单配置json格式,键值对形式配置</div>
                        </div>
                    </div>
                    <div class="fixed-foot text-right">
                        <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                        <button type="button" class="btn btn-info btn-sub">立即保存</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
<div style="height:15px;"></div>
</body>
<script>
    $(function () {
        var codeEditor = formHelper.codeEditor('form_config','application/json');
        codeEditor.setSize('auto','160px');
        codeEditor.on("blur", function () {
            $('#form_config').val(codeEditor.getValue());
        });
        In('selectpage', function () {
            var validRules = JSON.parse('{$validRules|raw}');
            $('#valid').selectPage({
                showField: 'name',
                keyField: 'id',
                pagination: false,
                multiple: true,
                data: validRules
            });
        });
        formHelper.initForm();
        utils.ajaxSubForm();
    });
</script>
{/block}