{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="nav-tabs-custom">
            {:D('Rule','auth')->getSubMenu('22')}
            <div class="box-body">
                <div class="toolbar">
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-primary" onclick="doAction.add();">
                            <i class="fa fa-plus"></i> 添加
                        </button>
                        <button type="button" class="btn btn-default" onclick="doAction.refresh();">
                            <i class="fa fa-refresh"></i> 更新缓存
                        </button>
                    </div>
                </div>
                <table id="grid"></table>
            </div>
        </div>
    </section>
</div>
</body>
<script>
    utils.bTreeTable({
        url: '/admin/dept/index.html',
        columns: [
            {field: 'state', checkbox: true},
            {field: 'name', title: '部门名称'},
            {field: 'phone', title: '部门电话'},
            {field: 'show_order', title: '排序'},
            {
                field: 'operate', title: '操作', width: 100,
                formatter: function (value, row, index) {
                    var html = '';
                    html += '<a href="javascript:;" onclick="doAction.edit(' + row.id + ')">编辑</a> ';
                    html += '<a href="javascript:;" onclick="utils.doAction(this,\'refreshTable\')" ' +
                        'data-params="id=' + row.id + '" data-url="/admin/dept/del.html">删除</a>';
                    return html;
                }
            }
        ]
    });
    var doAction = {
        add: function () {
            var rows = $('#grid').bootstrapTable('getSelections');
            var pid = rows.length==0 ? 0 : rows[0].id;
            utils.openDialog({
                title: '添加部门',
                area: ['800px', '312px'],
                content: '/admin/dept/add.html?pid='+pid
            });
        },
        edit: function (id) {
            utils.openDialog({
                title: '修改部门',
                area: ['800px', '312px'],
                content: '/admin/dept/edit.html?id=' + id
            });
        },
        refresh:function () { //刷新缓存
            $.get("/admin/dept/refresh.html",function (res) {
                layer.alert(res.msg,function (index) {
                    layer.close(index);
                    $('#grid').bootstrapTable('refresh');
                });
            });
        }
    };
</script>
{/block}