{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="box">
            <div class="box-body">
                <form class="form-horizontal" id="form" data-callfn="refreshTable">
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">上级部门</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="pid">
                                {volist name=":D('Dept')->getList()" id="vo"}
                                <option value="{$vo.id}"
                                        {eq name="vo.id" value="$info['pid']"}selected{/eq}
                                        {in name="vo.id" value="$info['data_view']"}disabled{/in}
                                >{$vo._name|raw}</option>
                                {/volist}
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">部门名称</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="name" value="{$info.name}" placeholder="请输入部门名称" datatype="*" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">部门电话</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="phone" value="{$info.phone}" placeholder="请输入部门电话" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">排序</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="show_order" value="{$info.show_order}" placeholder="请输入排序" datatype="*" />
                        </div>
                    </div>
                    <div class="fixed-foot text-right">
                        <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                        <button type="button" class="btn btn-info btn-sub">立即保存</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
<div style="height:15px;"></div>
</body>
<script>
    $(function () {
        formHelper.initForm();
        utils.ajaxSubForm();
    });
</script>
{/block}