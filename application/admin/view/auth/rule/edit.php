{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="box">
            <div class="box-body">
                <form class="form-horizontal" id="form" data-callfn="refreshTable">
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">上级规则</label>
                        <div class="col-sm-4">
                            <select class="form-control" name="pid">
                                <option value="0">顶级规则</option>
                                {volist name=":D('Rule','auth')->getList()" id="vo"}
                                <option value="{$vo.id}"
                                        {eq name="vo.id" value="$info['pid']"}selected{/eq}
                                {in name="vo.id" value="$info['data_view']"}disabled{/in}
                                >{$vo._name|raw}</option>
                                {/volist}
                            </select>
                        </div>
                        <label class="col-sm-2 control-label require">规则名称</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" name="title" value="{$info.title}" placeholder="请输入规则名称" datatype="*" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">规则标识</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="name" value="{$info.name|raw}" placeholder="请输入规则标识" datatype="*" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">规则条件</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="condition" value="{$info.condition}" placeholder="请输入规则条件" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">是否菜单</label>
                        <div class="col-sm-10 form-control-static">
                            <label class="icheck"><input type="radio" class="f-icheck" name="is_menu" value="0"
                                                         {eq name="info.is_menu" value="0"}checked{/eq}/> 否</label>
                            <label class="icheck"><input type="radio" class="f-icheck" name="is_menu" value="1"
                                                         {eq name="info.is_menu" value="1"}checked{/eq} /> 导航菜单</label>
                            <label class="icheck"><input type="radio" class="f-icheck" name="is_menu" value="2"
                                                         {eq name="info.is_menu" value="2"}checked{/eq} /> 子菜单</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">图标</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="icon" value="{$info.icon}" placeholder="请输入图标" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">备注</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" placeholder="请填写规则备注" rows="3" name="remark">{$info.remark}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">状态</label>
                        <div class="col-sm-10 form-control-static">
                            <label class="icheck"><input type="radio" class="f-icheck" name="status" value="1"
                                                         {eq name="info.status" value="1"}checked{/eq} /> 启用</label>
                            <label class="icheck"><input type="radio" class="f-icheck" name="status" value="0"
                                                         {eq name="info.status" value="0"}checked{/eq} /> 禁用</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">排序</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="show_order" value="{$info.show_order}" placeholder="请输入排序" datatype="*" />
                        </div>
                    </div>
                    <div class="fixed-foot text-right">
                        <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                        <button type="button" class="btn btn-info btn-sub">立即保存</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
<div style="height:15px;"></div>
</body>
<script>
    $(function () {
        formHelper.initForm();
        utils.ajaxSubForm();
    });
</script>
{/block}