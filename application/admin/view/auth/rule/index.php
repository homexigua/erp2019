{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<?php
$classCode = get_dict_list('101','value','text');
$classCode = json_encode($classCode,JSON_UNESCAPED_UNICODE);
?>
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="nav-tabs-custom">
            {:D('Rule','auth')->getSubMenu('22')}
            <div class="box-body">
                <div class="toolbar">
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-primary" onclick="doAction.add();">
                            <i class="fa fa-plus"></i> 添加
                        </button>
                        <button type="button" class="btn bg-gray" onclick="doAction.refresh();">
                            <i class="fa fa-refresh"></i> 更新缓存
                        </button>
                    </div>
                </div>
                <table id="grid"
                       data-id-field="id"
                       data-editable-emptytext="empty"
                       data-editable-url="/admin/auth.rule/editable.html">
                </table>
            </div>
        </div>
    </section>
</div>
</body>
<script>

    utils.bTreeTable({
        url: '/admin/auth.rule/index.html',
        columns: [
            {field: 'state', checkbox: true},
            {field: 'id', title: '编号',width:50 },
            {field: 'title', title: '规则名称', width:180,
                editable: {
                    type:'text',
                    validate: function (value) { //验证规则自定义
                        if (valid.isEmpty(value)) return {newValue:'',msg:'规则名称不能为空'};
                    }
                }
            },
            {field: 'name', title: '权限规则',
                editable: {
                    type:'text',
                    validate: function (value) { //验证规则自定义
                        if (valid.isEmpty(value)) return {newValue:'',msg:'权限规则不能为空'};
                    }
                }
            },
            {field: 'remark', title: '备注',
                editable: {
                    type:'text'
                }
            },
            {field: 'status', title: '状态',width:160,
                formatter:function (value, row, index) {
                    var html = '';
                    if (value == 1) {
                        html = '<label class="label label-success" onclick="utils.doAction(this,\'refreshTable\')" ' +
                            'data-url="/admin/auth.rule/multi.html" ' +
                            'data-params="ids=' + row.id + '&params=status:0">启用</label>';
                    } else {
                        html = '<label class="label label-warning" onclick="utils.doAction(this,\'refreshTable\')" ' +
                            'data-url="/admin/auth.rule/multi.html" ' +
                            'data-params="ids=' + row.id + '&params=status:1">禁用</label>';
                    }
                    return html;
                }
            },
            {field: 'show_order', title: '排序',width:80,
                editable: {
                    type:'text',
                    validate: function (value) { //验证规则自定义
                        if (!valid.isNum(value)) return {newValue:'0',msg:'排序填写不正确'};
                    }
                }
            },
            {
                field: 'operate', title: '操作', width: 100,
                formatter: function (value, row, index) {
                    var html = '';
                    html += '<a href="javascript:;" onclick="doAction.edit(' + row.id + ')">编辑</a> ';
                    html += '<a href="javascript:;" onclick="utils.doAction(this,\'refreshTable\')" ' +
                        'data-params="id=' + row.id + '" data-url="/admin/auth.rule/del.html">删除</a>';
                    return html;
                }
            }
        ],
        onLoadSuccess: function(data) {
            $('#grid').treegrid({
                treeColumn: 2,
                saveState:true,
                initialState:'collapsed',
                onChange: function() {
                    $('#grid').bootstrapTable('resetWidth');
                }
            });
        }
    });
    var doAction = {
        add: function () {
            var rows = $('#grid').bootstrapTable('getSelections');
            var pid = rows.length==0 ? 0 : rows[0].id;
            utils.openDialog({
                title: '添加规则',
                area: ['800px', '530px'],
                content: '/admin/auth.rule/add.html?pid='+pid
            });
        },
        edit: function (id) {
            utils.openDialog({
                title: '修改规则',
                area: ['800px', '530px'],
                content: '/admin/auth.rule/edit.html?id=' + id
            });
        },
        refresh:function () {
            $.get("/admin/auth.rule/refresh.html",function (res) {
                layer.alert(res.msg,function (index) {
                    layer.close(index);
                    $('#grid').bootstrapTable('refresh');
                });
            });
        }
    };
</script>
{/block}