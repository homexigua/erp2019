{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="nav-tabs-custom">
            {:D('Rule','auth')->getSubMenu('22')}
            <div class="box-body">
                <div class="toolbar">
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-primary" onclick="doAction.add();">
                            <i class="fa fa-plus"></i> 添加
                        </button>
                    </div>
                </div>
                <table id="grid"
                       data-id-field="id"
                       data-editable-emptytext=""
                       data-editable-url="/admin/auth.group/editable.html"></table>
            </div>
        </div>
    </section>
</div>
</body>
<script>
    utils.btable({
        url: '/admin/auth.group/index.html',
        columns: [
            {field: 'state', checkbox: true},
            {field: 'name', title: '角色名称',
                editable: {
                    type:'text',
                    validate: function (value) { //验证规则自定义
                        if (valid.isEmpty(value)) return {newValue:'',msg:'角色名称不能为空'};
                    }
                }
            },
            {field: 'status', title: '状态',width:160,
                formatter:function (value, row, index) {
                    var html = '';
                    if (value == 1) {
                        html = '<label class="label label-success" onclick="utils.doAction(this,\'refreshTable\')" ' +
                            'data-url="/admin/auth.group/multi.html" ' +
                            'data-params="ids=' + row.id + '&params=status:0">启用</label>';
                    } else {
                        html = '<label class="label label-warning" onclick="utils.doAction(this,\'refreshTable\')" ' +
                            'data-url="/admin/auth.group/multi.html" ' +
                            'data-params="ids=' + row.id + '&params=status:1">禁用</label>';
                    }
                    return html;
                }
            },
            {field: 'show_order', title: '排序',width:80,
                editable: {
                    type:'text',
                    validate: function (value) { //验证规则自定义
                        if (!valid.isNum(value)) return {newValue:'0',msg:'排序填写不正确'};
                    }
                }
            },
            {
                field: 'operate', title: '操作', width: 120,
                formatter: function (value, row, index) {
                    var html = '';
                    html += '<a href="javascript:;" onclick="doAction.setRule(' + row.id + ')">授权</a> ';
                    html += '<a href="javascript:;" onclick="doAction.edit(' + row.id + ')">编辑</a> ';
                    html += '<a href="javascript:;" onclick="utils.doAction(this,\'refreshTable\')" ' +
                        'data-params="id=' + row.id + '" data-url="/admin/auth.group/del.html">删除</a>';
                    return html;
                }
            }
        ]
    });
    var doAction = {
        add: function () {
            var rows = $('#grid').bootstrapTable('getSelections');
            var pid = rows.length==0 ? 0 : rows[0].id;
            utils.openDialog({
                title: '添加分组',
                area: ['800px', '280px'],
                content: '/admin/auth.group/add.html?pid='+pid
            });
        },
        edit: function (id) {
            utils.openDialog({
                title: '修改分组',
                area: ['800px', '280px'],
                content: '/admin/auth.group/edit.html?id=' + id
            });
        },
        setRule:function (id) {
            utils.openDialog({
                title: '用户组授权',
                area: ['100%', '100%'],
                content: '/admin/auth.group/setrule.html?group_id=' + id
            });
        }
    };
</script>
{/block}