{extend name="admin@public/layout" /}
{block name="head"}
<style>
    body .ztree-title{margin:8px 0 0;font-size:15px;background:#efefef;padding:8px 15px 4px;font-weight:600;border:1px solid #e8e8e8;}
    body ul.ztree { padding: 0px; border:1px solid #e8e8e8;border-top:none;margin-bottom:15px;}
    body ul.ztree>li { background: #fff; padding:8px;margin:0 10px 10px;border-bottom:1px dashed #e8e8e8; }
    body ul.ztree>li ul li.level1{background: #f1f1f1; margin-top: 8px; padding:0 5px;box-sizing:border-box;margin:8px 1%;line-height:31px;}
    body ul.ztree>li ul li.level1:after{display:block;content:'';height:0;overflow:hidden;clear:both;}
    body ul.ztree>li ul:after{display:block;content:'';height:0;overflow:hidden;clear:both;}
    body ul.ztree>li ul.level1 li{line-height:31px;}
    body ul.ztree>li ul li ul li ul li { display: inline-block; vertical-align: top; }
</style>
{/block}
{block name="body"}
<?php $znodes = json_encode($rule); ?>
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div class="box-body">
                    <form class="form-horizontal" id="form">
                        <div class="form-group">
                            <label class="col-sm-2 control-label require">所属角色</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="group_id" id="groupId" datatype="*">
                                    <option value="">请选择角色</option>
                                    {volist name="group_list.data" id="vo"}
                                    <option value="{$vo.id}" {eq name="Request.param.group_id" value="$vo['id']"}selected{/eq}>{$vo.name}</option>
                                    {/volist}
                                </select>
                            </div>
                        </div>
                        {gt name="Request.param.group_id" value="0"}
                        <div class="form-group">
                            <label class="col-sm-2 control-label">权限设置</label>
                            <div class="col-sm-10">
                                <input type="hidden" name="rules" value="" />
                                <div class="ztree-title">
                                    <label><input type="checkbox" class="class-check" /> 全选</label>
                                </div>
                                <ul id="ztree" data-id="{$v.id}" class="ztree"></ul>
                            </div>
                        </div>
                        {/gt}
                        <div class="fixed-foot text-right">
                            <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                            <button type="button" class="btn btn-info btn-sub">立即保存</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
<div style="height:15px;"></div>
</body>
<script>
    var zNodes = {$znodes|raw};
    var group_id = "{$Request.param.group_id}";
    $(function () {
        In('ztree',function () {
            if(group_id>0){
                var setting = {
                    check: { enable: true },
                    view: { showLine: false, showIcon: false, dblClickExpand: false },
                    data: {
                        simpleData: { enable: true, pIdKey: 'pid', idKey: 'id' },
                        key: { name: 'title' }
                    }
                };
                $.fn.zTree.init($("#ztree"), setting, zNodes);
                setCheck();
            }
        });
        //分组跳转
        $('#groupId').change(function () {
            var group_id = $(this).val();
            location.href="/admin/auth.group/setrule.html?group_id="+group_id;
        });
        $('.class-check').click(function () {
            var checked=false;
            if ($(this).prop("checked")) {
                checked=true;
            }
            checkNodes(checked);
        });
        formHelper.initForm();
        utils.ajaxSubForm({
            beforeCheck: function (curform) { //表单验证前同步编辑器
                if(group_id>0){
                    var rules=getNodes();
                    $("input[name='rules']").val(rules);
                }else{
                    $("input[name='rules']").val('');
                }
            }
        });
    });
    function setCheck() {
        var zTree = $.fn.zTree.getZTreeObj('ztree');
        zTree.setting.check.chkboxType = { "Y": "ps", "N": "ps" };
    }
    //checked true 全 false 全不选
    function checkNodes(checked) {
        var treeObj = $.fn.zTree.getZTreeObj('ztree');
        treeObj.checkAllNodes(checked);
    }
    //获取选中节点
    function getNodes() {
        var ids='';
        var treeObj = $.fn.zTree.getZTreeObj('ztree');
        var nodes = treeObj.getCheckedNodes(true);
        if(nodes.length>0){
            ids+=utils.jsonCol(nodes,'id')+',';
        }
        if(!valid.isEmpty(ids)) ids = ids.substr(0, ids.length - 1); //去除最后一个逗号
        return ids;
    }
</script>
{/block}