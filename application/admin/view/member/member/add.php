{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="box">
            <div class="box-body">
                <form class="form-horizontal" id="form" data-callfn="refreshTable">
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">用户组</label>
                        <div class="col-sm-4">
                            {:FormBuild::_select('group_id','',['list-data'=>'D:Group:member:select'])}
                        </div>
                        <label class="col-sm-2 control-label require">昵称</label>
                        <div class="col-sm-4">
                            {:FormBuild::_text('nickname','',['placeholder'=>'请输入昵称','datatype'=>'*'])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">用户名</label>
                        <div class="col-sm-4">
                            {:FormBuild::_text('username','',['placeholder'=>'请输入用户名'])}
                        </div>
                        <label class="col-sm-2 control-label require">密码</label>
                        <div class="col-sm-4">
                            {:FormBuild::_password('password','',['placeholder'=>'请输入密码','datatype'=>'*'])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">EMAIL</label>
                        <div class="col-sm-4">
                            {:FormBuild::_text('email','',['placeholder'=>'请输入email'])}
                        </div>
                        <label class="col-sm-2 control-label">电话</label>
                        <div class="col-sm-4">
                            {:FormBuild::_text('mobile','',['placeholder'=>'请输入手机号'])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">性别</label>
                        <div class="col-sm-4 form-control-static">
                            {:FormBuild::_radio('sex','1',['list-data'=>'fun:get_dict_list:102'])}
                        </div>
                        <label class="col-sm-2 control-label">生日</label>
                        <div class="col-sm-4">
                            {:FormBuild::_laydate('birthday','')}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">问题</label>
                        <div class="col-sm-4">
                            {:FormBuild::_select('question','',['list-data'=>'fun:get_dict_list:108','empty-option'=>'|请选择问题'])}
                        </div>
                        <label class="col-sm-2 control-label">答案</label>
                        <div class="col-sm-4">
                            {:FormBuild::_text('answer','')}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">QQ</label>
                        <div class="col-sm-4">
                            {:FormBuild::_text('qq','',['placeholder'=>'请输入qq'])}
                        </div>
                        <label class="col-sm-2 control-label">会员等级</label>
                        <div class="col-sm-4">
                            {:FormBuild::_text('level','0',['placeholder'=>'请输入会员等级'])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">头像</label>
                        <div class="col-sm-10">
                            {:FormBuild::_image('avatar','')}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">推荐人</label>
                        <div class="col-sm-4">
                            {:FormBuild::_selectpage('ref_id','',[
                                'data-url'=>'/admin/member.member/selectpage.html',
                                'data-show'=>'nickname',
                                'data-key'=>'id',
                            ])}
                        </div>
                        <label class="col-sm-2 control-label ">账户状态</label>
                        <div class="col-sm-4">
                            <input type="checkbox" class="f-switch" name="status"
                                   data-option='{"onText":"启用","offText":"禁用","onColor":"info","offColor":"danger"}'
                                   placeholder="" value="1" checked/>
                        </div>
                    </div>
                    <div class="fixed-foot text-right">
                        <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                        <button type="button" class="btn btn-info btn-sub">立即保存</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
<div style="height:15px;"></div>
</body>
<script>
    $(function () {
        formHelper.initForm();
        utils.ajaxSubForm();
    });
</script>
{/block}