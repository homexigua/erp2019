{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="nav-tabs-custom">
            {:D('Rule','auth')->getSubMenu('27')}
            <div class="box-header with-border">
                <form class="form-inline" id="searchForm">
                    <div class="form-group">
                        <select class="form-control" id="searchKey">
                            <option value="username">用户名</option>
                            <option value="email">邮箱</option>
                            <option value="mobile">手机</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="searchValue" placeholder="请输入检索关键词"/>
                    </div>
                    <div class="form-group">
                        <label style="margin-left:8px;">会员状态</label>
                        <select class="form-control" name="status" data-op="=" onchange="$('#grid').bootstrapTable('refresh');">
                            <option value="">全部</option>
                            <option value="1">启用</option>
                            <option value="0">禁用</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="button" class="btn btn-default" onclick="$('#grid').bootstrapTable('refresh');">
                            <i class="fa fa-search"></i> 检索
                        </button>
                    </div>
                </form>
            </div>
            <div class="box-body">
                <div class="toolbar">
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-primary" onclick="doAction.add();">
                            <i class="fa fa-plus"></i> 添加
                        </button>
                    </div>
                </div>
                <table id="grid"></table>
            </div>
        </div>
    </section>
</div>
</body>
<script>
    utils.btable({
        url: '/admin/member.member/index.html',
        singleSelect: true, //是否启用单选
        columns: [
            {field: 'state', checkbox: true},
            {field: 'id', title: '会员编号'},
            {field: 'username', title: '用户名'},
            {field: 'nickname', title: '昵称'},
            {field: 'email', title: '电子邮箱'},
            {field: 'mobile', title: '手机号'},
            {field: 'level', title: '等级'},
            {field: 'amount', title: '金额'},
            {field: 'points', title: '积分'},
            {
                field: 'status', title: '状态', width: 120,
                formatter: function (value, row, index) {
                    var html = '';
                    if (value == 1) {
                        html = '<label class="label label-success" onclick="utils.doAction(this,\'refreshTable\')" ' +
                            'data-url="/admin/member.member/multi.html" ' +
                            'data-params="ids=' + row.id + '&params=status:0">启用</label>';
                    } else {
                        html = '<label class="label label-warning" onclick="utils.doAction(this,\'refreshTable\')" ' +
                            'data-url="/admin/member.member/multi.html" ' +
                            'data-params="ids=' + row.id + '&params=status:1">禁用</label>';
                    }
                    return html;
                }
            },
            {field: 'login_time', title: '登录时间',
                formatter:function (value, row, index) {
                    return utils.dateFormat(value,'Y-m-d H:i:s');
                }
            },
            {field: 'create_time', title: '注册时间',
                formatter:function (value, row, index) {
                    return utils.dateFormat(value,'Y-m-d H:i:s');
                }
            },
            {
                field: 'operate', title: '操作', width: 120,
                formatter: function (value, row, index) {
                    var html = '';
                    html += '<a href="javascript:;" onclick="doAction.edit(' + row.id + ')">编辑</a> ';
                    html += '<a href="javascript:;" onclick="utils.doAction(this,\'refreshTable\')" ' +
                        'data-params="id=' + row.id + '" data-url="/admin/member.member/del.html">删除</a>';
                    return html;
                }
            }
        ]
    });
    var doAction = {
        add: function () {
            utils.openDialog({
                title: '添加会员',
                area: ['800px', '500px'],
                content: '/admin/member.member/add.html'
            });
        },
        edit: function (id) {
            utils.openDialog({
                title: '编辑会员',
                area: ['800px', '500px'],
                content: '/admin/member.member/edit.html?id=' + id
            });
        }
    };
</script>
{/block}