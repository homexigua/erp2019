{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="box">
            <div class="box-body">
                <form class="form-horizontal" id="form" data-callfn="refreshTable">
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">会员组</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="name" value="{$info.name}" placeholder="请输入会员组" datatype="*" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">状态</label>
                        <div class="col-sm-10 form-control-static">
                            <label class="icheck"><input type="radio" class="f-icheck" name="status" value="1" {eq name="info.status" value="1"}checked{/eq} /> 启用</label>
                            <label class="icheck"><input type="radio" class="f-icheck" name="status" value="0" {eq name="info.status" value="0"}checked{/eq} /> 禁用</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">排序</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="show_order" value="{$info.show_order}" placeholder="请输入排序" datatype="*" />
                        </div>
                    </div>
                    <div class="fixed-foot text-right">
                        <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                        <button type="button" class="btn btn-info btn-sub">立即保存</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
<div style="height:15px;"></div>
</body>
<script>
    $(function () {
        formHelper.initForm();
        utils.ajaxSubForm();
    });
</script>
{/block}