{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="nav-tabs-custom">
            {:D('Rule','auth')->getSubMenu('27')}
            <div class="box-body">
                <div class="toolbar">
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-primary" onclick="doAction.add();">
                            <i class="fa fa-plus"></i> 添加
                        </button>
                    </div>
                </div>
                <table id="grid"
                       data-id-field="id"
                       data-editable-emptytext=""
                       data-editable-url="/admin/member.group/editable.html"></table>
            </div>
        </div>
    </section>
</div>
</body>
<script>
    utils.btable({
        url: '/admin/member.group/index.html',
        singleSelect: true, //是否启用单选
        columns: [
            {field: 'state', checkbox: true},
            {field: 'name', title: '会员组名'},
            {
                field: 'status', title: '状态', width: 120,
                formatter: function (value, row, index) {
                    var html = '';
                    if (value == 1) {
                        html = '<label class="label label-success" onclick="utils.doAction(this,\'refreshTable\')" ' +
                            'data-url="/admin/member.group/multi.html" ' +
                            'data-params="ids=' + row.id + '&params=status:0">启用</label>';
                    } else {
                        html = '<label class="label label-warning" onclick="utils.doAction(this,\'refreshTable\')" ' +
                            'data-url="/admin/member.group/multi.html" ' +
                            'data-params="ids=' + row.id + '&params=status:1">禁用</label>';
                    }
                    return html;
                }
            },
            {field: 'show_order', title: '排序',width:80,
                editable: {
                    type:'text',
                    validate: function (value) { //验证规则自定义
                        if (!valid.isNum(value)) return {newValue:'0',msg:'排序填写不正确'};
                    }
                }
            },
            {
                field: 'operate', title: '操作', width: 100,
                formatter: function (value, row, index) {
                    var html = '';
                    html += '<a href="javascript:;" onclick="doAction.edit(' + row.id + ')">编辑</a> ';
                    html += '<a href="javascript:;" onclick="utils.doAction(this,\'refreshTable\')" ' +
                        'data-params="id=' + row.id + '" data-url="/admin/member.group/del.html">删除</a>';
                    return html;
                }
            }
        ]
    });
    var doAction = {
        add: function () {
            utils.openDialog({
                title: '添加会员组',
                area: ['800px', '268px'],
                content: '/admin/member.group/add.html'
            });
        },
        edit: function (id) {
            utils.openDialog({
                title: '编辑会员组',
                area: ['800px', '268px'],
                content: '/admin/member.group/edit.html?id=' + id
            });
        },
        setRule:function (id) {
            utils.openDialog({
                title: '用户组授权',
                area: ['100%', '100%'],
                content: '/admin/member.group/setrule.html?group_id=' + id
            });
        }
    };
</script>
{/block}