{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<?php
$change = get_dict_list('109');
$changeType = get_dict_key_value('109');
$changeType = json_encode($changeType,JSON_UNESCAPED_UNICODE);
?>
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="nav-tabs-custom">
            {:D('Rule','auth')->getSubMenu('27')}
            <div class="box-body">
                <div class="toolbar">
                    <form class="form-inline" id="searchForm">
                        <div class="form-group">
                            <select class="form-control" id="searchKey">
                                <option value="username">账户</option>
                                <option value="mobile">手机号</option>
                                <option value="qq">qq</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="searchValue" placeholder="请输入检索关键词"/>
                        </div>
                        <div class="form-group">
                            <label>变动类型</label>
                            {:FormBuild::_select('change_type','',[
                            'list-data'=>$change,
                            'empty-option'=>'|全部',
                            'data-op'=>'=',
                            'onchange'=>"$('#grid').bootstrapTable('refresh');"
                            ])}
                        </div>
                        <div class="form-group">
                            <label>日期区间</label>
                            {:FormBuild::_laydate('change_time','',[
                            'data-op'=>'between time',
                            'data-option'=>["type"=>"date","range"=>true],
                            'style'=>'width:280px',
                            'readonly'
                            ])}
                        </div>
                        <div class="form-group">
                            <button type="button" class="btn btn-default" onclick="$('#grid').bootstrapTable('refresh');">
                                <i class="fa fa-search"></i> 检索
                            </button>
                        </div>
                    </form>
                </div>
                <table id="grid"></table>
            </div>
        </div>
    </section>
</div>
</body>
<script>
    formHelper.initForm();
    utils.btable({
        url: '/admin/member.change_log/index.html',
        singleSelect: true, //是否启用单选
        sortOrder:'desc',
        columns: [
            {field: 'username', title:'账户'},
            {field: 'mobile', title:'手机'},
            {field: 'qq', title:'qq'},
            {field: 'change_points', title:'积分',sortable:true,},
            {field: 'change_frozen_points', title:'冻结积分',sortable:true,},
            {field: 'change_amount', title:'金额',sortable:true,},
            {field: 'change_frozen_amount', title:'冻结金额',sortable:true,},
            {field: 'change_type', title:'变动类型',
                formatter:function (value, row, index) {
                    var changeType=JSON.parse('{$changeType|raw}');
                    return changeType[value];
                }
            },
            {field: 'change_desc', title:'变动原因'},
            {field: 'change_time', title:'变动时间',sortable:true,
                formatter:function (value, row, index) {
                    return utils.dateFormat(value,'Y-m-d H:i:s');
                }
            },
        ]
    });
</script>
{/block}