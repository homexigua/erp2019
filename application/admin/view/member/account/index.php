{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="nav-tabs-custom">
            {:D('Rule','auth')->getSubMenu('27')}
            <div class="box-header with-border">
                <form class="form-inline" id="searchForm">
                    <div class="form-group">
                        <select class="form-control" id="searchKey">
                            <option value="username">用户名</option>
                            <option value="email">邮箱</option>
                            <option value="mobile">手机</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="searchValue" placeholder="请输入检索关键词"/>
                    </div>
                    <div class="form-group">
                        <select class="form-control" name="is_pay" data-op="=" onchange="$('#grid').bootstrapTable('refresh');">
                            <option value="">全部</option>
                            <option value="1">已付款</option>
                            <option value="0">未付款</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="button" class="btn btn-default" onclick="$('#grid').bootstrapTable('refresh');">
                            <i class="fa fa-search"></i> 检索
                        </button>
                    </div>
                </form>
            </div>
            <div class="box-body">
                <div class="toolbar">
                    <button type="button" class="btn btn-primary" onclick="doAction.add();">
                        <i class="fa fa-plus"></i> 添加
                    </button>
                    <button type="button" class="btn btn-warning" onclick="doAction.clear();">
                        <i class="fa fa-trash"></i> 清理未付款
                    </button>
                </div>
                <table id="grid"></table>
            </div>
        </div>
    </section>
</div>
</body>
<script>
    utils.btable({
        url: '/admin/member.account/index.html',
        singleSelect: true, //是否启用单选
        columns: [
            {field: 'state', checkbox: true},
            {field: 'username', title: '会员账户',width:160},
            {field: 'mobile', title: '手机号',width:160},
            {field: 'change_type', title: '类型',width:100,
                formatter: function (value, row, index) {
                    if(value==1) return '<label class="label label-success">充值</label>';
                    if(value==2) return '<label class="label label-warning">提现</label>';
                }
            },
            {field: 'amount', title: '变动金额',width:180,sortable:true},
            {field: 'change_time', title: '变动时间',width:180,sortable:true,
                formatter: function (value, row, index) {
                    return utils.dateFormat(value,'Y-m-d H:i:s');
                }
            },
            {field: 'action_name', title: '操作人',width:120},
            {field: 'is_pay', title: '状态',width:100,
                formatter: function (value, row, index) {
                    if(value==1) return '<label class="label label-success">已结算</label>';
                    if(value==0) return '<label class="label label-danger">未结算</label>';
                }
            },
            {field: 'remark', title: '备注'},
            {
                field: 'operate', title: '操作', width: 100,
                formatter: function (value, row, index) {
                    var html = '';
                    if(row.is_pay==0){
                        html += '<a href="javascript:;" onclick="doAction.edit(' + row.id + ')">处理</a> ';
                    }else{
                        html += '<a href="javascript:;" onclick="doAction.edit(' + row.id + ',\'view\')">查看</a> ';
                    }
                    return html;
                }
            }
        ]
    });
    var doAction = {
        add: function () {
            utils.openDialog({
                title: '添加申请',
                area: ['800px', '400px'],
                content: '/admin/member.account/add.html'
            });
        },
        edit: function (id,key) {
            var tpl = key || '';
            utils.openDialog({
                title: '处理申请',
                area: ['800px', '400px'],
                content: '/admin/member.account/edit.html?id='+id+'&tpl='+tpl
            });
        },
        clear:function () {
            layer.confirm('确认要清理数据吗？',function (index) {
                $.get('/admin/member.account/clear.html',function (res) {
                    layer.msg(res.msg,{icon: 1, time: 1200},function () {
                        $('#grid').bootstrapTable('refresh');
                    })
                });
                layer.close(index);
            });
        }
    };
</script>
{/block}