{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="box">
            <div class="box-body">
                <form class="form-horizontal" id="form" data-callfn="refreshTable">
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">会员</label>
                        <div class="col-sm-10">
                            {:FormBuild::_selectpage('member_id','',[
                            'data-url'=>'/admin/member.member/selectpage.html',
                            'data-show'=>'nickname',
                            'data-key'=>'id',
                            'data-search'=>'username|mobile|nickname'
                            ])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">类型</label>
                        <div class="col-sm-4 form-control-static">
                            {:FormBuild::_radio('change_type','1',['list-data'=>'fun:get_dict_list:106'])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">金额</label>
                        <div class="col-sm-4">
                            {:FormBuild::_text('amount','')}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">备注</label>
                        <div class="col-sm-10 form-control-static">
                            {:FormBuild::_textarea('remark','')}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">操作人</label>
                        <div class="col-sm-4">
                            {:FormBuild::_text('action_name',$admin['nickname'])}
                        </div>
                        <label class="col-sm-2 control-label">到款状态</label>
                        <div class="col-sm-4 form-control-static">
                            {:FormBuild::_radio('is_pay','0',['list-data'=>'fun:get_dict_list:110'])}
                        </div>
                    </div>
                    <div class="fixed-foot text-right">
                        <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                        <button type="button" class="btn btn-info btn-sub">立即保存</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
<div style="height:15px;"></div>
</body>
<script>
    $(function () {
        formHelper.initForm();
        utils.ajaxSubForm();
    });
</script>
{/block}