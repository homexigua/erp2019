{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="box">
            <div class="box-body">
                <form class="form-horizontal" id="form" data-callfn="refreshTable">
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">会员</label>
                        <div class="col-sm-10">
                            {:FormBuild::_selectpage('member_id',$info['member_id'],[
                            'data-url'=>'/admin/member.member/selectpage.html',
                            'data-show'=>'nickname',
                            'data-key'=>'id',
                            'data-search'=>'username|mobile|nickname',
                            'disabled'
                            ])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">类型</label>
                        <div class="col-sm-4 form-control-static">
                            {:FormBuild::_radio('change_type',$info['change_type'],['list-data'=>'fun:get_dict_list:106', 'disabled'])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">金额</label>
                        <div class="col-sm-4">
                            {:FormBuild::_text('amount',abs($info['amount']),['disabled'])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">备注</label>
                        <div class="col-sm-10 form-control-static">
                            {:FormBuild::_textarea('remark',$info['remark'],['disabled'])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">操作人</label>
                        <div class="col-sm-4">
                            {:FormBuild::_text('action_name',$info['action_name'],['disabled'])}
                        </div>
                        <label class="col-sm-2 control-label">结算状态</label>
                        <div class="col-sm-4 form-control-static">
                            {:FormBuild::_radio('is_pay',$info['is_pay'],['list-data'=>'fun:get_dict_list:110','disabled'])}
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
<div style="height:15px;"></div>
</body>
<script>
    $(function () {
        formHelper.initForm();
        utils.ajaxSubForm();
    });
</script>
{/block}