{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="box">
            <div class="box-body">
                <form class="form-horizontal" id="form" data-callfn="refreshTable">
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">专题名称</label>
                        <div class="col-sm-10">
                            {:FormBuild::_text('special_name',$info.special_name,['datatype'=>'*','placeholder'=>'请输入专题名称'])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">专题图片</label>
                        <div class="col-sm-10">
                            {:FormBuild::_image('special_image',$info.special_image)}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">关键词</label>
                        <div class="col-sm-10">
                            {:FormBuild::_tags('special_keywords',$info.special_keywords)}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">描述</label>
                        <div class="col-sm-10 form-control-static">
                            {:FormBuild::_textarea('special_description',$info.special_description,['placeholder'=>'请输入描述'])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">专题模板</label>
                        <div class="col-sm-10">
                            {:FormBuild::_select('special_tpl',$info.special_tpl,[
                            'list-data'=>'D:Tpl::getTplList:special_',
                            'empty-option'=>'|请选择专题模板'
                            ])}
                        </div>
                    </div>
                    <div class="fixed-foot text-right">
                        <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                        <button type="button" class="btn btn-info btn-sub">立即保存</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
<div style="height:15px;"></div>
</body>
<script>
    $(function () {
        formHelper.initForm();
        utils.ajaxSubForm();
    });
</script>
{/block}