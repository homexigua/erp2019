{extend name="admin@public/layout" /}
{block name="head"}
<style>
    .pagination {margin: 0;}
</style>
{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="box">
            <div class="box-header">
                <ul class="nav nav-pills">
                    {volist name="moudle_list" id="vo"}
                    <li {eq name="moudle_id" value="$vo['id']"}class="active"{/eq}>
                        <a href="/admin/special/items.html?moudle_id={$vo.id}&special_id={$Request.param.special_id}">{$vo.name}</a>
                    </li>
                    {/volist}
                </ul>
            </div>
            <div class="box-body">
                <div style="height:300px;overflow-y: auto;">
                <table class="table" id="grid">
                    <tr>
                        <th width="80">#</th>
                        <th width="80">ID</th>
                        <th>标题</th>
                        <th width="80">操作</th>
                    </tr>
                    {volist name="list" id="vo"}
                    <tr>
                        <td>
                            <input type="checkbox" value="{$vo.id}" />
                        </td>
                        <td>{$vo.id}</td>
                        <td>{$vo.title}</td>
                        <td>
                            <a href="javascript:;" onclick="utils.doAction(this,'refreshPage')"
                               data-params="id={$vo.id}" data-url="/admin/special/delitem.html">删除</a>
                        </td>
                    </tr>
                    {/volist}
                </table>
                </div>
            </div>
        </div>
        <div class="pull-left">
            <button type="button" class="btn btn-default" onclick="utils.checkAll()">全</button>
            <button type="button" class="btn btn-default" onclick="utils.checkOther()">反</button>
            <button type="button" class="btn btn-primary" onclick="doAction.del()">批量从本专题移除</button>
        </div>
    </section>
</div>
</body>
<script>
    var doAction = {
        del:function () {
            var ids=utils.getCheckboxValue();
            if(valid.isEmpty(ids)){
                layer.msg('请选择要移除的内容！');
                return false;
            }
            layer.confirm('确认要删除吗？',function (index) {
                layer.close(index);
                utils.getAjax({
                    url:'/admin/special/delitem.html',
                    data:{id:ids},
                    success:function (res) {
                        if(res.code==1){
                            utils.refreshPage();
                        }else{
                            layer.msg(res.msg);
                        }
                    }
                });
            })
        }
    };
</script>
{/block}