{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div class="toolbar">
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-primary" onclick="doAction.add();">
                            <i class="fa fa-plus"></i> 添加
                        </button>
                    </div>
                </div>
                <table id="grid"></table>
            </div>
        </div>
    </section>
</div>
</body>
<script>
    utils.btable({
        url: '/admin/special/index.html',
        singleSelect: true, //是否启用单选
        columns: [
            {field: 'state', checkbox: true},
            {field: 'id', title: '#',width:60},
            {field: 'special_name', title: '专题名称',
                formatter:function (value, row, index) {
                    var html='';
                    html += '<a href="javascript:;" onclick="doAction.view(\''+row.id+'\')">'+value+'<span class="text-danger">('+row.total+')</span></a>';
                    return html;
                }
            },
            {field: 'special_tpl', title: '专题模板',width:160},
            {
                field: 'operate', title: '操作', width: 100,
                formatter: function (value, row, index) {
                    var html = '';
                    html += '<a href="javascript:;" onclick="doAction.edit(' + row.id + ')">编辑</a> ';
                    html += '<a href="javascript:;" onclick="utils.doAction(this,\'refreshTable\')" ' +
                        'data-params="id=' + row.id + '" data-url="/admin/special/del.html">删除</a>';
                    return html;
                }
            }
        ]
    });
    var doAction = {
        add: function () {
            utils.openDialog({
                title: '添加专题',
                area: ['800px', '400px'],
                content: '/admin/special/add.html'
            });
        },
        edit: function (id) {
            utils.openDialog({
                title: '修改专题',
                area: ['800px', '400px'],
                content: '/admin/special/edit.html?id=' + id
            });
        },
        view:function (specialId) {
            utils.openDialog({
                title: '查看专题下的文章',
                area: ['800px', '500px'],
                content: '/admin/special/items.html?special_id=' + specialId
            });
        }
    };
</script>
{/block}