{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="box">
            <div class="box-body">
                <form class="form-horizontal" id="form" data-callfn="refreshTable">
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">模板名称</label>
                        <div class="col-sm-10">
                            {:FormBuild::_text('tpl_name','',['datatype'=>'*','placeholder'=>'请输入模板名称'])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">模板标题</label>
                        <div class="col-sm-10">
                            {:FormBuild::_text('tpl_title','',['datatype'=>'*','placeholder'=>'请输入模板标题'])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">模板内容</label>
                        <div class="col-sm-10">
                            {:FormBuild::_textarea('tpl_content','',['datatype'=>'*','rows'=>'3'])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">排序</label>
                        <div class="col-sm-10">
                            {:FormBuild::_text('show_order','')}
                        </div>
                    </div>
                    <div class="fixed-foot text-right">
                        <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                        <button type="button" class="btn btn-info btn-sub">立即保存</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
</body>
<script>
    $(function () {
        formHelper.initForm();
        utils.ajaxSubForm();
    });
</script>
{/block}