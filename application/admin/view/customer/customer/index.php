{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="nav-tabs-custom">
            {:D('Rule','auth')->getSubMenu('101')}
            <div class="box-body">
                <div class="toolbar">
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-primary" onclick="doAction.add();">
                            <i class="fa fa-plus"></i> 添加
                        </button>
                    </div>
                </div>
                <table id="grid"></table>
            </div>
        </div>
    </section>
</div>
</body>
<script>
    utils.btable({
        url: '/admin/customer.customer/index.html',
        columns: [
            {field: 'state', checkbox: true},
            {field: 'name', title: '客户'},
            {field: 'mobile', title: '手机'},
            {field: '', title: '最近动态'},
            {field: 'create_time', title: '创建时间'},
            {field: 'company', title: '公司'},
            {field: 'job', title: '职务'},
            {
                field: 'operate', title: '操作', width: 100,
                formatter: function (value, row, index) {
                    var html = '';
                    html += '<a href="javascript:;" onclick="doAction.edit(' + row.id + ')">编辑</a> ';
                    html += '<a href="javascript:;" onclick="utils.doAction(this,\'refreshTable\')" ' +
                        'data-params="id=' + row.id + '" data-url="/admin/customer.customer/del.html">删除</a>';
                    return html;
                }
            }
        ]
    });
    var doAction = {
        add:function(){
            utils.openDialog({
                title: '新增客户',
                area: ['800px', '560px'],
                content: '/admin/customer.customer/add.html'
            });
        },
        edit:function(id){
            utils.openDialog({
                title: '编辑客户',
                area: ['800px', '560px'],
                content: '/admin/customer.customer/edit.html?id='+id
            });
        },
    }
</script>
{/block}