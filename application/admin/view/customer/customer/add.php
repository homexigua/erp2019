{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<?php
$sexList = [['id'=>1,'name'=>'男'],['id'=>2,'name'=>'女']];
?>
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="box box-solid">
            <div class="box-body">
                <form class="form-horizontal" id="form" data-callfn="refreshTable">
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">姓名</label>
                        <div class="col-sm-4">
                            {:FormBuild::_text('name','',['datatype'=>'*','placeholder'=>'请输入客户姓名','nullmsg'=>'请填写姓名!'])}
                        </div>
                        <label class="col-sm-2 control-label">称呼</label>
                        <div class="col-sm-4">
                            {:FormBuild::_text('call_name','')}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">手机</label>
                        <div class="col-sm-4">
                            {:FormBuild::_text('mobile','',['datatype'=>'m','ignore'=>'ignore','placeholder'=>'请输入手机号'])}
                        </div>
                        <label class="col-sm-2 control-label">邮箱</label>
                        <div class="col-sm-4">
                            {:FormBuild::_text('email','',['datatype'=>'e','ignore'=>'ignore'])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">性别</label>
                        <div class="col-sm-4 form-control-static">
                            {:FormBuild::_radio('sex','1',['list-data'=>$sexList])}
                        </div>
                        <label class="col-sm-2 control-label">生日</label>
                        <div class="col-sm-4">
                            {:FormBuild::_laydate('birthday','')}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">所在地区</label>
                        <div class="col-sm-6">
                            {:FormBuild::_cxselect('prov,city,dist','')}
                        </div>
                        <div  class="col-sm-4 ">
                            {:FormBuild::_text('addr','',['placeholder'=>'详细地址'])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">客户来源</label>
                        <div class="col-sm-4">
                            {:FormBuild::_select('source','',['list-data'=>'D:Source:customer:getlist', 'items-key'=>'id', 'items-name'=>'name', 'empty-option'=>'|请选择客户来源'])}
                        </div>
                        <label class="col-sm-2 control-label">QQ</label>
                        <div class="col-sm-4">
                            {:FormBuild::_text('qq','')}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">客户分组</label>
                        <div class="col-sm-4">
                            {:FormBuild::_select('group_id','',['list-data'=>'D:Group:customer:getlist', 'items-key'=>'id', 'items-name'=>'name', 'datatype'=>'*', 'empty-option'=>'|请选择客户分组','nullmsg'=>'请选择客户分组!'])}
                        </div>
                        <label class="col-sm-2 control-label">客户标签</label>
                        <div class="col-sm-4">
                            {:FormBuild::_selectpages('label_id','',[
                            'data-url'=>'/admin/customer.label/selectpage.html',
                            'data-show'=>'name','data-key'=>'id',
                            'data-custom'=>'site_id='.$loginInfo['site_id']
                            ])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">公司</label>
                        <div class="col-sm-4">
                            {:FormBuild::_text('company','')}
                        </div>
                        <label class="col-sm-2 control-label">行业</label>
                        <div class="col-sm-4">
                            {:FormBuild::_text('trade_name','')}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">职位</label>
                        <div class="col-sm-4">
                            {:FormBuild::_text('job','')}
                        </div>
                        <label class="col-sm-2 control-label">网址</label>
                        <div class="col-sm-4">
                            {:FormBuild::_text('company_url','')}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">固定电话</label>
                        <div class="col-sm-4">
                            {:FormBuild::_text('phone','')}
                        </div>
                        <label class="col-sm-2 control-label">传真</label>
                        <div class="col-sm-4">
                            {:FormBuild::_text('fax','')}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">备注</label>
                        <div class="col-sm-10">
                            {:FormBuild::_text('memo','')}
                        </div>
                    </div>
                    <div class="fixed-foot">
                        <div class="text-right">
                            <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                            {:FormBuild::_button('立即保存',['class'=>'btn btn-info btn-sub'])}
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
<div style="height:15px;"></div>
</body>
<script>
    $(function () {
        formHelper.initForm();
        utils.ajaxSubForm();
    });
</script>
{/block}