{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="box box-solid">
            <div class="box-body">
                <form class="form-horizontal" id="form" data-callfn="refreshTable">
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">标签名</label>
                        <div class="col-sm-10">
                            {:FormBuild::_text('name',$info['name'],['datatype'=>'*','placeholder'=>'请输入标签名'])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">标签颜色</label>
                        <div class="col-sm-10">
                            {:FormBuild::_color('color',$info['color'])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">排序</label>
                        <div class="col-sm-10">
                            {:FormBuild::_text('show_order',$info['show_order'])}
                        </div>
                    </div>
                    <div class="fixed-foot text-right">
                        <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                        <button type="button" class="btn btn-info btn-sub">立即保存</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
</body>
<script>
    $(function () {
        formHelper.initForm();
        utils.ajaxSubForm();
    });
</script>
{/block}