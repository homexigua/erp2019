{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="nav-tabs-custom">
			{:D('Rule','auth')->getSubMenu('101')}
            <div class="box-body">
                <div class="toolbar">
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-primary" onclick="doAction.add();">
                            <i class="fa fa-plus"></i> 添加
                        </button>
                    </div>
                </div>
                <table id="grid"></table>
            </div>
        </div>
    </section>
</div>
</body>
<script>
    utils.btable({
        url: '/admin/customer.label/index.html',
        columns: [
            {field: 'state', checkbox: true},
            {field: 'name', title: '标签名'},
            {field: 'color', title: '标签颜色'},
            {field: 'show_order', title: '排序'},
            {
                field: 'operate', title: '操作', width: 100,
                formatter: function (value, row, index) {
                    var html = '';
                    html += '<a href="javascript:;" onclick="doAction.edit(' + row.id + ')">编辑</a> ';
                    html += '<a href="javascript:;" onclick="utils.doAction(this,\'refreshTable\')" ' +
                        'data-params="id=' + row.id + '" data-url="/admin/customer.label/del.html">删除</a>';
                    return html;
                }
            }
        ]
    });
    var doAction = {
        add:function(){
            utils.openDialog({
                title: '添加标签',
                area: ['800px', '312px'],
                content: '/admin/customer.label/add.html'
            });
        },
        edit:function(id){
            utils.openDialog({
                title: '修改标签',
                area: ['800px', '312px'],
                content: '/admin/customer.label/edit.html?id='+id
            });
        },
    }
</script>
{/block}