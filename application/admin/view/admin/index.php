{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<?php
$topDept = D('Dept')->getTopDept();
?>
<div class="wrapper">
    <section class="content">
        <div class="nav-tabs-custom">
            {:D('Rule','auth')->getSubMenu('22')}
        </div>
        <div class="row">
            <div class="col-md-3 col-lg-2">
                <div style="height:100%;background:#fff;overflow-y:auto;border-right:1px solid #e5e5e5">
                    <h5 style="padding:10px 10px 0;margin:0;">
                        部门
                        <a href="javascript:;" data-type="1" class="expand fa fa-plus-square pull-right" style="color:#666"></a>
                        <a href="javascript:;" data-type="0" class="expand fa fa-minus-square pull-right" style="color:#666"></a>
                    </h5>
                    <hr style="margin:10px 0;">
                    <ul class="ztree" id="ztree" style="padding-bottom:20px"></ul>
                </div>
            </div>
            <div class="col-md-9 col-lg-10">
                <div class="box">
                    <div class="box-header with-border">
                        <form class="form-inline" id="searchForm">
                            <input type="hidden" name="dept_id" data-op="in" value="{$topDept.data_view}" />
                            <div class="form-group">
                                <select class="form-control" id="searchKey">
                                    <option value="username">用户名</option>
                                    <option value="email">邮箱</option>
                                    <option value="mobile">手机</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="searchValue" placeholder="请输入检索关键词"/>
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-default" onclick="$('#grid').bootstrapTable('refresh');">
                                    <i class="fa fa-search"></i> 检索
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="box-body">
                        <div class="toolbar">
                            <button type="button" class="btn btn-primary" onclick="doAction.add();">
                                <i class="fa fa-plus"></i> 添加管理员
                            </button>
                        </div>
                        <table id="grid"></table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<input type="hidden" id="select_dept_id" value="{$topDept.id}" />
</body>
<script>
    In('ztree',function () {
        $.get('/admin/dept/getlist.html',function (nodes) {
            $.fn.zTree.init($('#ztree'), {
                data: {
                    view: { showLine: false },
                    simpleData: { enable: true, idKey: "id", pIdKey: "pid", rootPId: 0 }
                },
                callback: { onClick: doAction.setDeptId }
            }, nodes);
        });
        $('.expand').click(function () {
            var zTree = $.fn.zTree.getZTreeObj("ztree");
            var type=$(this).data('type');
            if(type==1) zTree.expandAll(true);
            if(type==0) zTree.expandAll(false);
        });
    });
    utils.btable({
        url: '/admin/admin/index.html',
        columns: [
            {field: 'state', checkbox: true},
            {field: 'username', title: '用户名'},
            {field: 'email', title: '邮箱'},
            {field: 'mobile', title: '手机'},
            {field: 'nickname', title: '姓名'},
            {field: 'login_times', title: '登录次数',sortable:true,width:100 },
            {field: 'login_time', title: '登录时间',sortable:true,
                formatter: function (value, row, index) {
                    if(value>0){
                        return utils.dateFormat(value,'Y-m-d H:i:s');
                    }
                }
            },
            {
                field: 'status', title: '状态', width: 120,
                formatter: function (value, row, index) {
                    var html = '';
                    if (value == 1) {
                        html = '<label class="label label-success" onclick="utils.doAction(this,\'refreshTable\')" ' +
                            'data-url="/admin/admin/multi.html" ' +
                            'data-params="ids=' + row.id + '&params=status:0">启用</label>';
                    } else {
                        html = '<label class="label label-warning" onclick="utils.doAction(this,\'refreshTable\')" ' +
                            'data-url="/admin/admin/multi.html" ' +
                            'data-params="ids=' + row.id + '&params=status:1">禁用</label>';
                    }
                    return html;
                }
            },
            {
                field: 'operate', title: '操作', width: 100,
                formatter: function (value, row, index) {
                    var html = '';
                    html += '<a href="javascript:;" onclick="doAction.edit(' + row.id + ')">编辑</a> ';
                    html += '<a href="javascript:;" onclick="utils.doAction(this,\'refreshTable\')" ' +
                        'data-params="id=' + row.id + '" data-url="/admin/admin/del.html">删除</a>';
                    return html;
                }
            }
        ]
    });
    var doAction = {
        setDeptId:function(event, treeId, treeNode){
            console.info(treeNode);
            $('#select_dept_id').val(treeNode.id);
            $("input[name='dept_id']").val(treeNode.data_view);
            $('#grid').bootstrapTable('refresh');
        },
        add: function () {
            var deptId = $('#select_dept_id').val();
            utils.openDialog({
                title: '添加管理员',
                area: ['800px', '568px'],
                content: '/admin/admin/add.html?dept_id='+deptId
            });
        },
        edit: function (id) {
            utils.openDialog({
                title: '修改管理员',
                area: ['800px', '568px'],
                content: '/admin/admin/edit.html?id=' + id
            });
        }
    };
</script>
{/block}