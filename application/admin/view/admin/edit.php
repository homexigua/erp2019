{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<?php
$deptList = D('Dept')->getList();
$statusList = [['id'=>1,'name'=>'启用'], ['id'=>0,'name'=>'禁用']];
?>
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="box">
            <div class="box-body">
                <form class="form-horizontal" id="form" data-callfn="refreshTable">
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">所属部门</label>
                        <div class="col-sm-10">
                            {:FormBuild::_select('dept_id',$info['dept_id'],['list-data'=>$deptList,'datatype'=>'*', 'items-key'=>'id', 'items-name'=>'_name'])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">用户名</label>
                        <div class="col-sm-10">
                            {:FormBuild::_text('username',$info['username'],['datatype'=>'*','placeholder'=>'请输入用户名'])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">手机号</label>
                        <div class="col-sm-10">
                            {:FormBuild::_text('mobile',$info['mobile'],['datatype'=>'m','ignore'=>'ignore','placeholder'=>'请输入手机号'])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">邮箱</label>
                        <div class="col-sm-10">
                            {:FormBuild::_text('email',$info['email'],['datatype'=>'e','ignore'=>'ignore','placeholder'=>'请输入邮箱'])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">姓名</label>
                        <div class="col-sm-10">
                            {:FormBuild::_text('nickname',$info['nickname'],['datatype'=>'*','placeholder'=>'请输入姓名'])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">密码</label>
                        <div class="col-sm-10">
                            {:FormBuild::_password('password','',['placeholder'=>'不修改密码请留空'])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">头像</label>
                        <div class="col-sm-10">
                            {:FormBuild::_image('avatar',$info['avatar'])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">性别</label>
                        <div class="col-sm-10 form-control-static">
                            {:FormBuild::_radio('sex',$info['sex'],['list-data'=>'fun:get_dict_list:102'])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">权限分组</label>
                        <div class="col-sm-10">
                            {:FormBuild::_selectpage('group_id',$info['group_id'],[
                            'data-url'=>'/admin/auth.group/selectpage.html',
                            'data-show'=>'name',
                            'data-key'=>'id',
                            'data-custom'=>'site_id='.$loginInfo['site_id']
                            ] )}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">状态</label>
                        <div class="col-sm-10 form-control-static">
                            {:FormBuild::_radio('status',$info['status'],['list-data'=>$statusList])}
                        </div>
                    </div>
                    <div class="fixed-foot text-right">
                        <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                        {:FormBuild::_button('立即保存',['class'=>'btn btn-info btn-sub'])}
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
<div style="height:15px;"></div>
</body>
<script>
    $(function () {
        formHelper.initForm();
        utils.ajaxSubForm();
    });
</script>
{/block}