{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="nav-tabs-custom">
            {:D('Rule','auth')->getSubMenu('12')}
            <div class="box-body">
                <form class="form-horizontal" id="form">
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">数据表</label>
                        <div class="col-sm-10">
                            <select name="table_name" id="table_name" class="form-control" datatype="*">
                                <option value="">请选择数据表</option>
                                {volist name="tables" id="vo"}
                                <option value="{$vo.table_name}">{$vo.table_name}</option>
                                {/volist}
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">数据字段</label>
                        <div class="col-sm-10">
                            <select name="field_name" id="field_name" class="form-control" datatype="*">

                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">SQL条件</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="query" value="" placeholder="请输入SQL条件" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">替换模式</label>
                        <div class="col-sm-10 form-control-static">
                            <label class="icheck"><input type="radio" class="f-icheck" name="replace_type" value="1" checked /> 简单模式</label>
                            <label class="icheck"><input type="radio" class="f-icheck" name="replace_type" value="2" /> 高级模式</label>
                            <label class="icheck"><input type="radio" class="f-icheck" name="replace_type" value="3" /> 赋值模式</label>
                        </div>
                    </div>
                    <div id="replace-box"></div>
                    <div class="form-group">
                        <div class="col-sm-10 col-sm-offset-2">
                            <button type="button" class="btn btn-info btn-sub">立即替换</button>
                            <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
<script type="text/html" id="replace-box-tpl">
    {@if replace_type==1}
    <div class="form-group">
        <label class="col-sm-2 control-label require">将字符</label>
        <div class="col-sm-10">
            <textarea class="form-control" placeholder="" rows="3" name="old_str" datatype="*"></textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">替换成</label>
        <div class="col-sm-10">
            <textarea class="form-control" placeholder="" rows="3" name="new_str"></textarea>
        </div>
    </div>
    {@/if}
    {@if replace_type==2}
    <div class="form-group">
        <label class="col-sm-2 control-label require">开始字符</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="old_start_str" value="" datatype="*" placeholder="请输入开始字符" />
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label require">结束字符</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="old_end_str" value="" datatype="*" placeholder="请输入结束字符" />
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">替换成</label>
        <div class="col-sm-10">
            <textarea class="form-control" placeholder="" rows="3" name="new_str"></textarea>
        </div>
    </div>
    {@/if}
    {@if replace_type==3}
    <div class="form-group">
        <label class="col-sm-2 control-label">修改成</label>
        <div class="col-sm-10">
            <textarea class="form-control" placeholder="" rows="3" name="new_str"></textarea>
        </div>
    </div>
    {@/if}
</script>
</body>
<script>
    $(function () {
        //表单初始化
        formHelper.initForm();
        //替换模式
        $('#replace-box').html(juicer($('#replace-box-tpl').html(),{ replace_type:1 })); //初始替换
        $("input[name='replace_type']").on('ifChecked', function(event){ //ifCreated 事件应该在插件初始化之前绑定
            var replace_type=event.currentTarget.value;
            var html=juicer($('#replace-box-tpl').html(),{ replace_type:replace_type });
            $('#replace-box').html(html);
        });
        //字段选择
        $('#table_name').change(function () {
            var table_name = $(this).val();
            $('#field_name').empty();
            $('#field_name').append('<option value="">请选择数据字段</option>');
            if(table_name!=''){
                utils.getAjax({
                    url:'/admin/database/fields.html',
                    data:{table_name:table_name},
                    success:function (fields) {
                        for(var i=0;i<fields.length;i++){
                            $('#field_name').append('<option value="'+fields[i].column_name+'">'+fields[i].column_name+'</option>');
                        }
                    }
                });
            }
        });
        $('#table_name').trigger('change');
        //提交表单
        utils.ajaxSubForm(function (res) {
            layer.alert('替换成功！',function () {
                location.href=res.url;
            });
        });
    });
</script>
{/block}