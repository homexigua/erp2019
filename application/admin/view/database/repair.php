{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="nav-tabs-custom">
            {:D('Rule','auth')->getSubMenu('12')}
            <div class="box-body">
                <div class="toolbar" style="margin-bottom:8px;">
                    <div class="btn-group">
                        <button type="button" class="btn btn-default" onclick="utils.checkAll();">全</button>
                        <button type="button" class="btn btn-default" onclick="utils.checkOther();">反</button>
                    </div>
                    <button type="button" class="btn btn-primary" onclick="doAction.optimize();">
                        <i class="fa fa-check"></i> 优化
                    </button>
                    <button type="button" class="btn btn-primary" onclick="doAction.repair();">
                        <i class="fa fa-wrench"></i> 修复
                    </button>
                </div>
                <div class="table-responsive">
                    <table class="table" id="grid">
                        <tr>
                            <th>#</th>
                            <th>表名</th>
                            <th>表引擎</th>
                            <th>数据条数</th>
                            <th>编码</th>
                            <th>备注</th>
                            <th>操作</th>
                        </tr>
                        {volist name="tables" id="vo"}
                        <tr>
                            <td width="60">
                                <input type="checkbox" value="{$vo.table_name}" />
                            </td>
                            <td>{$vo.table_name}</td>
                            <td width="160">{$vo.engine}</td>
                            <td width="200">{$vo.table_rows}</td>
                            <td width="200">{$vo.table_collation}</td>
                            <td width="200">{$vo.table_comment}</td>
                            <td width="160">
                                <a href="javascript:;" onclick="utils.doAction(this,'refreshPage')"
                                   data-url="repair.html"
                                   data-params="table_name={$vo.table_name}&type=optimize">优化</a>
                                <a href="javascript:;" onclick="utils.doAction(this,'refreshPage')"
                                   data-url="repair.html"
                                   data-params="table_name={$vo.table_name}&type=repair">修复</a>
                            </td>
                        </tr>
                        {/volist}
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>
</body>
<script>
    var doAction = {
        optimize:function(){
            var table_name = utils.getCheckboxValue();
            if(valid.isEmpty(table_name)){
                layer.msg('请选择要操作数据表');
                return false;
            }
            utils.getAjax({
                url:'repair.html',
                data:{table_name:table_name,type:'optimize'},
                success:function (res) {
                    if(res.code==1){
                        layer.msg(res.msg,{icon:1,time:1200},function () {
                            utils.refreshPage();
                        })
                    }else{
                        layer.msg(res.msg);
                    }
                }
            });
        },
        repair:function () { //修复
            var table_name = utils.getCheckboxValue();
            if(valid.isEmpty(table_name)){
                layer.msg('请选择要操作数据表');
                return false;
            }
            utils.getAjax({
                url:'repair.html',
                data:{table_name:table_name,type:'repair'},
                success:function (res) {
                    if(res.code==1){
                        layer.msg(res.msg,{icon:1,time:1200},function () {
                            utils.refreshPage();
                        })
                    }else{
                        layer.msg(res.msg);
                    }
                }
            });
        }
    };
</script>
{/block}