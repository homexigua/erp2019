{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="nav-tabs-custom">
            {:D('Rule','auth')->getSubMenu('12')}
            <div class="box-body">
                <div class="toolbar" style="margin-bottom:8px;">
                    <button type="button" class="btn btn-info" onclick="doAction.backup();">
                        <i class="fa fa-database"></i> 备份
                    </button>
                </div>
                <div class="table-responsive">
                    <table class="table" id="grid">
                        <tr>
                            <th>备份文件名</th>
                            <th>文件大小</th>
                            <th>操作时间</th>
                            <th>操作</th>
                        </tr>
                        {volist name="filelist" id="vo"}
                        <tr>
                            <td>{$vo.name}</td>
                            <td width="160">{$vo.size}</td>
                            <td width="200">{$vo.time}</td>
                            <td width="160">
                                <a href="javascript:;" onclick="doAction.restore('{$vo.name}')">还原</a>
                                <a href="download.html?file_name={$vo.name}">下载</a>
                                <a href="javascript:;" onclick="utils.doAction(this,'refreshPage')"
                                   data-url="del.html"
                                   data-params="file_name={$vo.name}">删除</a>
                            </td>
                        </tr>
                        {/volist}
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>
</body>
<script>
    var doAction = {
        backup:function(){
            utils.getAjax({
                url:'backup.html',
                success:function (res) {
                    if(res.code==1){
                        layer.msg(res.msg,{ icon:1,time:1200 },function () {
                            utils.refreshPage();
                        });
                    }else{
                        layer.msg(res.msg);
                    }
                }
            });
        },
        restore:function (file_name) {
            layer.confirm('确认操作吗?',{icon:3},function () {
                utils.getAjax({
                    url:'restore.html',
                    data:{file_name:file_name},
                    success:function (res) {
                        if(res.code==1){
                            layer.msg(res.msg,{ icon:1,time:1200 },function () {
                                utils.refreshPage();
                            });
                        }else{
                            layer.msg(res.msg);
                        }
                    }
                });
            });
        }
    };
</script>
{/block}