{extend name="admin@public/layout" /}
{block name="head"}
<style>
    #result-box{height:500px;overflow-y: auto;}
</style>
{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="nav-tabs-custom">
            {:D('Rule','auth')->getSubMenu('12')}
            <div class="box-body">
                <form class="form-horizontal" id="form">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <textarea class="form-control" placeholder="" rows="3" id="sql"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-10">
                            <button type="button" class="btn btn-info btn-sub">立即执行</button>
                            说明：一次只能执行一条SQL语句，如果您对SQL语句不了解，建议不要使用．
                        </div>
                    </div>
                </form>
                <div id="result-box" class="bg-gray-light" style="padding:10px;"></div>
            </div>
        </div>
    </section>
</div>
</body>
<script>
    $(function () {
        $('.btn-sub').click(function () {
            var sql=$('#sql').val();
            if(valid.isEmpty(sql)){
                layer.msg('sql语句不能为空！');
                return false;
            }
            $.post('sql.html',{sql:sql},function (res) {
                if(res.code==1){
                    $('#result-box').html(res.data);
                }else{
                    $('#result-box').html(res.msg);
                }
            });
        });
    });
</script>
{/block}