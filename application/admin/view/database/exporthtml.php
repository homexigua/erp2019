<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>数据库设计文档 -- {$database}</title>
    <style>
        body, td {
            font-family: verdana;
            font-size: 12px;
            line-height: 150%
        }

        table {
            width: 100%;
            background-color: #ccc;
            margin: 5px 0
        }

        td {
            background-color: #fff;
            padding: 3px;
            padding-left: 10px
        }

        thead td {
            text-align: center;
            font-weight: bold;
            background-color: #eee
        }

        a:link, a:visited, a:active {
            color: #015fb6;
            text-decoration: none
        }
        a:hover {
            color: #333;
            text-decoration: none
        }
    </style>
</head>
<body style='text-align:center;'>
<div style='width:800px; margin:20px auto; text-align:left;'>
    <a name='index' />
    <H2 style='text-align:center; line-height:50px;'>数据库设计文档</H2>
    <div><b>数据库名：{$database}</b></div>
    <table cellspacing='1' cellpadding='0'>
        <thead>
        <tr>
            <td style='width:40px; '>序号</td>
            <td>表名</td>
            <td>引擎</td>
            <td>说明</td>
        </tr>
        </thead>
        <tbody>
        {foreach $tables as $index=>$table}
        <tr>
            <td style='text-align:center;'>{$index+1}</td>
            <td><a href='#{$table.table_name}'>{$table.table_name}</a></td>
            <td>{$table.table_collation}</td>
            <td>{$table.table_comment}</td>
        </tr>
        {/foreach}
        </tbody>
    </table>
    {foreach $tables as $index=>$table}
    <a name='{$table.table_name}' />
    <div style='margin-top:30px;'>
        <a href='#index' style='float:right; margin-top:6px;'>返回目录</a>
        <b>表名：{$table.table_name}</b>
    </div>
    <div>说明：{$table.table_comment}</div>
    <div>数据列：</div>
    <table cellspacing='1' cellpadding='0'>
        <thead>
        <tr>
            <td style='width:40px; '>序号</td>
            <td>名称</td>
            <td>数据类型</td>
            <td>索引</td>
            <td>默认值</td>
            <td>说明</td>
        </tr>
        </thead>
        <tbody>
        {foreach $table['fields'] as $fieldIndex=>$field}
        <tr>
            <td style='text-align:center;'>{$fieldIndex+1}</td>
            <td>{$field.column_name}</td>
            <td>{$field.column_type}</td>
            <td>{$field.column_key}</td>
            <td>{$field.column_default}</td>
            <td>{$field.column_comment}</td>
        </tr>
        {/foreach}
        </tbody>
    </table>
    {/foreach}
</div>
</body>
</html>