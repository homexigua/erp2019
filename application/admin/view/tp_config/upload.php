{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="nav-tabs-custom">
            {:D('Rule','auth')->getSubMenu('8')}
            <div class="box-body">
                <form class="form-horizontal" id="form" data-callfn="refreshPage">
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">上传方式</label>
                        <div class="col-sm-6">
                            <select class="form-control" name="type" id="type">
                                <option value="local">本地上传</option>
                                <option value="alioss">阿里云上传</option>
                                <option value="qiniu">七牛上传</option>
                            </select>
                        </div>
                    </div>
                    <div class="upload-box box-local">
                        <div class="form-group">
                            <label class="col-sm-2 control-label require">上传目录</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="local[prefix]" value="{$config.local.prefix}" readonly placeholder="请输入上传目录" />
                                <p class="help-block">必须以./开始，一般不建议修改</p>
                            </div>
                        </div>
                    </div>
                    <div class="upload-box box-alioss">
                        <div class="form-group">
                            <label class="col-sm-2 control-label require">ak</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="alioss[ak]" value="{$config.alioss.ak}" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label require">sk</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="alioss[sk]" value="{$config.alioss.sk}" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label require">endpoint</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="alioss[endpoint]" value="{$config.alioss.endpoint}" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label require">bucket</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="alioss[bucket]" value="{$config.alioss.bucket}" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label require">prefix</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="alioss[prefix]" value="{$config.alioss.prefix}" />
                                <p class="help-block">上传前缀，建议"http://+云存储域名+/upload"形式</p>
                            </div>
                        </div>
                    </div>
                    <div class="upload-box box-qiniu">
                        <div class="form-group">
                            <label class="col-sm-2 control-label require">ak</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="qiniu[ak]" value="{$config.qiniu.ak}" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label require">sk</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="qiniu[sk]" value="{$config.qiniu.sk}" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label require">bucket</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="qiniu[bucket]" value="{$config.qiniu.bucket}" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label require">prefix</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="qiniu[prefix]" value="{$config.qiniu.prefix}" />
                                <p class="help-block">上传前缀，建议"http://+云存储域名+/upload"形式</p>
                            </div>
                        </div>
                    </div>
                    <legend>图片上传配置</legend>
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">允许后缀</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="img[ext]" value="{$config.img.ext}" datatype="*" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">上传限制</label>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <input type="text" class="form-control" name="img[size]" value="{$config.img.size}" datatype="n" />
                                <span class="input-group-addon">b</span>
                            </div>
                        </div>
                    </div>
                    <legend>附件上传配置</legend>
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">允许后缀</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="file[ext]" value="{$config.file.ext}" datatype="*" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">上传限制</label>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <input type="text" class="form-control" name="file[size]" value="{$config.file.size}" datatype="n" />
                                <span class="input-group-addon">b</span>
                            </div>
                        </div>
                    </div>
                    <legend>视频上传配置</legend>
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">允许后缀</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="vedio[ext]" value="{$config.vedio.ext}" datatype="*" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">上传限制</label>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <input type="text" class="form-control" name="vedio[size]" value="{$config.vedio.size}" datatype="n" />
                                <span class="input-group-addon">b</span>
                            </div>
                        </div>
                    </div>
                    <div class="fixed-foot text-right">
                        <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                        <button type="button" class="btn btn-info btn-sub">立即保存</button>
                    </div>

                </form>
            </div>
        </div>
    </section>
</div>
</body>
<script>
$(function () {
    $('#type').change(function () {
        var type=$(this).val();
        $('.upload-box').hide();
        $('.box-'+type).show();
    });
    $('#type').trigger('change');
    utils.ajaxSubForm(function (res) {
        layer.msg('操作成功！');
    });
});
</script>
{/block}