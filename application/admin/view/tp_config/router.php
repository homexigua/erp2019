{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="nav-tabs-custom">
            {:D('Rule','auth')->getSubMenu('8')}
            <div class="box-body">
                <div class="alert bg-gray">
                    <p>1、伪静态地址和访问地址不填写默认以访问地址保存</p>
                    <p>2、伪静态标识不填默认取访问地址</p>
                    <p>3、前端调用:url('路由标识','参数1=1&参数2=2')</p>
                </div>
                <form class="form-horizontal" id="form" data-callfn="refreshPage">
                    <div class="form-group" style="padding:10px;">
                        <table id="grid" class="table f-input-array" data-name="items">
                            <thead>
                            <tr>
                                <th>伪静态地址</th>
                                <th>访问地址</th>
                                <th width="160">伪静态标识</th>
                                <th width="100">操作</th>
                            </tr>
                            </thead>
                            <tr class="data-row-tpl">
                                <td><input type="text" class="form-control item-input" data-name="rule" value="" placeholder="请输入路由地址"></td>
                                <td><input type="text" class="form-control item-input" data-name="route" value="" placeholder="请输入访问地址"></td>
                                <td><input type="text" class="form-control item-input" data-name="name" value="" placeholder="路由标识"></td>
                                <td><a href="javascript:;" class="btn btn-info btn-add"><i class="fa fa-plus"></i></a></td>
                            </tr>
                            <tbody class="data-row">
                            {volist name="route" id="vo"}
                            <tr>
                                <td><input type="text" class="form-control item-input" data-name="rule" name="items[{$i-1}][rule]" value="{$vo.rule}" placeholder="请输入路由地址"></td>
                                <td><input type="text" class="form-control item-input" data-name="route" name="items[{$i-1}][route]" value="{$vo.route}" placeholder="请输入访问地址"></td>
                                <td><input type="text" class="form-control item-input" data-name="name" name="items[{$i-1}][name]" value="{$vo.name}" placeholder="路由标识"></td>
                                <td><a href="javascript:;" class="btn btn-danger btn-del"><i class="fa fa-times"></i> </a></td>
                            </tr>
                            {/volist}
                            </tbody>
                        </table>
                    </div>
                    <div class="fixed-foot text-right">
                        <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                        <button type="button" class="btn btn-primary btn-sub">立即保存</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
</body>
<script>
    $(function () {
        formHelper.initForm();
        utils.ajaxSubForm(function (res) {
            layer.msg('操作成功!');
        });
    });
</script>
{/block}