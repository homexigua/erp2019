<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>控制台</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="/static/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/static/plugins/fontawesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/static/admin/css/AdminLTE.css">
    <link rel="stylesheet" href="/static/admin/css/skins/skin-blue.min.css">
    <style type="text/css">
        html {overflow: hidden;}
    </style>
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue fixed sidebar-mini">
<div class="wrapper">
    <header class="main-header">
        <!-- Logo -->
        <a href="javascript:;" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini">{$loginInfo.site_name}</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg">{$loginInfo.site_name}</span>
        </a>
        <!-- 头部导航 -->
        <nav class="navbar navbar-static-top">
            <!--左边栏切换按钮-->
            <a href="javascript:;" class="sidebar-toggle" data-toggle="offcanvas"><span class="icon-bar"></span></a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- 消息通知
                    <li class="dropdown notifications-menu">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-bell-o"></i>
                            <span class="label label-warning">6</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">您有6条系统通知</li>
                            <li>
                                <ul class="menu">
                                    <li><a href="javascript:;"><i class="fa fa-users text-red"></i> 今天有5个待办需要您处理</a></li>
                                    <li><a href="javascript:;"><i class="fa fa-warning text-yellow"></i> 会员下单100笔</a></li>
                                    <li><a href="javascript:;"><i class="fa fa-users text-red"></i> 今天有5个待办需要您处理</a></li>
                                    <li><a href="javascript:;"><i class="fa fa-warning text-yellow"></i> 会员下单100笔</a></li>
                                    <li><a href="javascript:;"><i class="fa fa-users text-red"></i> 今天有5个待办需要您处理</a></li>
                                    <li><a href="javascript:;"><i class="fa fa-warning text-yellow"></i> 会员下单100笔</a></li>

                                </ul>
                            </li>
                            <li class="footer"><a href="javascript:;">查看所有</a></li>
                        </ul>
                    </li>
                    -->
                    <li><a href="/" target="_blank"  style="padding:10px;"><i class="fa fa-home"></i></a></li>
                    <li class="active"><a href="javascript:;" onclick="loginOut();" style="padding:10px;"><i class="fa fa-sign-out"></i></a></li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- 左边栏 -->
    <aside class="main-sidebar">
        <section class="sidebar">
            <!--用户面板-->
            <div class="user-panel">
                <div class="pull-left image">
                    {eq name="$loginInfo.sex" value="0"}<img src="/static/admin/img/avatar1.png" class="img-circle" />{/eq}
                    {eq name="$loginInfo.sex" value="1"}<img src="/static/admin/img/avatar5.png" class="img-circle" />{/eq}
                    {eq name="$loginInfo.sex" value="2"}<img src="/static/admin/img/avatar3.png" class="img-circle" />{/eq}
                </div>
                <div class="pull-left info">
                    <p>{$loginInfo.nickname}</p>
                    <a href="javascript:;" onclick="changepwd();"><i class="fa fa-edit"></i>修改密码</a>
                </div>
            </div>
            <!-- 搜索表单 -->
            <form method="get" class="sidebar-form hidden">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                <button type="button" name="search" id="search-btn" class="btn btn-flat" onclick="search_menu()">
                    <i class="fa fa-search"></i>
                </button>
              </span>
                </div>
            </form>
            <!-- 左边菜单 -->
            <ul class="sidebar-menu"></ul>
        </section>
    </aside>
    <!-- 右侧内容区域 -->
    <div class="content-wrapper" id="content-wrapper" style="min-height: 421px;">
        <!--tab多标签-->
        <div class="content-tabs">
            <button class="roll-nav roll-left tabLeft" onclick="scrollTabLeft()">
                <i class="fa fa-backward"></i>
            </button>
            <nav class="page-tabs menuTabs tab-ui-menu" id="tab-menu">
                <div class="page-tabs-content" style="margin-left: 0px;"></div>
            </nav>
            <button class="roll-nav roll-right tabRight" onclick="scrollTabRight()">
                <i class="fa fa-forward" style="margin-left: 3px;"></i>
            </button>
            <div class="btn-group roll-nav roll-right">
                <button class="dropdown tabClose" data-toggle="dropdown">
                    页签操作<i class="fa fa-caret-down" style="padding-left: 3px;"></i>
                </button>
                <ul class="dropdown-menu dropdown-menu-right" style="min-width: 128px;">
                    <li><a class="tabReload" href="javascript:refreshTab();">刷新当前</a></li>
                    <li><a class="tabCloseCurrent" href="javascript:closeCurrentTab();">关闭当前</a></li>
                    <li><a class="tabCloseAll" href="javascript:closeOtherTabs(true);">全部关闭</a></li>
                    <li><a class="tabCloseOther" href="javascript:closeOtherTabs();">除此之外全部关闭</a></li>
                </ul>
            </div>
            <button class="roll-nav roll-right fullscreen" onclick="App.handleFullScreen()"><i class="fa fa-arrows-alt"></i></button>
        </div>
        <div class="content-iframe " style="background-color: #ffffff; ">
            <div class="tab-content " id="tab-content"></div>
        </div>
    </div>
    <!-- 添加侧栏的背景。必须放置此div紧接在控制侧边栏之后 -->
    <div class="control-sidebar-bg"></div>
</div>
<script src="/static/plugins/jquery/jquery-2.2.4.min.js"></script>
<script src="/static/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="/static/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="/static/plugins/layer/layer.js"></script>
<script src="/static/plugins/juicer/juicer-min.js"></script>
<script src="/static/admin/js/app.js"></script>
<script src="/static/admin/js/app_iframe.js"></script>
<script src="/static/plugins/in/in.min.js"></script>
<script src="/static/admin/js/in-config.js?v={php}echo time(){/php}"></script>
<script src="/static/admin/js/utils.js?v={php}echo time(){/php}"></script>
<script type="text/javascript">
    var menus = JSON.parse('{$menus|raw}');
    $(function () {
        addTabs({
            id: '/admin/index/main.html',
            title: '首页',
            close: false,
            //content:'<h1 style="height:1000px;">我是中国人</h1>',
            url: '/admin/index/main.html'
        });
        getMenus();
        $('.top-menu').click(function () {
            $(this).addClass('active').siblings().removeClass('active');
            var class_code=$(this).data('classcode');
            getMenus(class_code);
        });
        // 动态创建菜单后，可以重新计算 SlimScroll
        $.AdminLTE.layout.fixSidebar();
        if ($.AdminLTE.options.sidebarSlimScroll) {
            if (typeof $.fn.slimScroll != 'undefined') {
                var $sidebar = $(".sidebar");
                $sidebar.slimScroll({destroy: true}).height("auto");
                $sidebar.slimscroll({
                    height: ($(window).height() - $(".main-header").height()) + "px",
                    color: "rgba(0,0,0,0.2)",
                    size: "3px"
                });
            }
        }
    });
    /**
     * 获取对应分类菜单
     */
    function getMenus() {
        $('.sidebar-menu').empty().sidebarMenu({data: menus});
    }
    /**
     * 本地搜索菜单
     */
    function search_menu() {
        //要搜索的值
        var text = $('input[name=q]').val();
        var $ul = $('.sidebar-menu');
        $ul.find("a.nav-link").each(function () {
            var $a = $(this).css("border", "");
            //判断是否含有要搜索的字符串
            if ($a.children("span.menu-text").text().indexOf(text) >= 0) {
                //如果a标签的父级是隐藏的就展开
                $ul = $a.parents("ul");
                if ($ul.is(":hidden")) {
                    $a.parents("ul").prev().click();
                }
                //点击该菜单
                $a.click().css("border", "1px solid");
            }
        });
    }
    /**
     * 退出登录
     */
    function changepwd() {
        utils.openDialog({
            type:2,
            title:'修改密码',
            area:['800px','312px'],
            content:'/admin/admin/changepwd.html'
        });
    }
    /**
     * 退出登录
     */
    function loginOut(){
        layer.confirm('确认要退出登录吗?', {icon: 3, title:'退出提醒'}, function(index){
            $.get("/admin/admin/loginout.html",function (res) {
                if(res.code==1) location.href=res.url;
                else layer.msg(res.msg);
            });
            layer.close(index);
        });
    }
</script>

</body>
</html>