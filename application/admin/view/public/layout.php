<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>{block name="title"}控制台{/block}</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="/static/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/static/plugins/fontawesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/static/admin/css/AdminLTE.css">
    <link rel="stylesheet" href="/static/admin/css/skins/skin-blue.min.css">
    <link rel="stylesheet" href="/static/admin/css/style.css">
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="/static/plugins/jquery/jquery-2.2.4.min.js"></script>
    <script src="/static/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="/static/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <script src="/static/plugins/lodash/lodash.min.js"></script>
    <script src="/static/plugins/layer/layer.js"></script>
    <script src="/static/plugins/juicer/juicer-min.js"></script>
    <script src="/static/admin/js/app.js"></script>
    <script src="/static/plugins/in/in.min.js"></script>
    <script src="/static/admin/js/in-config.js?v={php}echo time(){/php}"></script>
    <script src="/static/admin/js/utils.js?v={php}echo time(){/php}"></script>
    {block name="head"}{/block}
</head>
{block name="body"}{/block}
</html>
