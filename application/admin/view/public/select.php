{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <div class="container page-main">
        <div class="jumbotron">
            <h1 class="text-center">选择店铺</h1>
        </div>
        <div class="row">
            {volist name="sites" id="vo"}
            <div class="col-md-6">
                <div class="box box-widget widget-user">
                    <div class="widget-user-header bg-aqua-active">
                        <h3 class="widget-user-username">{$vo.site_name}</h3>
                        <h4>{$vo.site_alias}</h4>
                        <h5 class="widget-user-desc">

                        </h5>
                    </div>
                    <div class="widget-user-image">
                        <img class="img-circle" src="{$vo.site_logo|default='/static/picture.png'}" alt="店铺LOGO">
                    </div>
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-sm-4 border-right">
                                <div class="description-block">
                                    <h5 class="description-header">2019-08-08</h5>
                                    <span class="description-text">到期</span>
                                </div>
                            </div>
                            <div class="col-sm-4 border-right">
                                <div class="description-block">
                                    <h5 class="description-header">{$vo.site_tel|default='0000-00000000'}</h5>
                                    <span class="description-text">电话</span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="description-block">
                                    <h5 class="description-header">{$vo.site_prov} {$vo.site_city} {$vo.site_dist}</h5>
                                    <span class="description-text">区域</span>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <button type="button" onclick="selectShop('{$vo.id}')" class="btn btn-primary btn-block">进入店铺</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/volist}
        </div>
    </div>
</div>
</body>
<script>
    function selectShop(siteId) {
        utils.getAjax({
            url:'/admin/public/select.html',
            data:{site_id:siteId},
            success:function (res) {
                if(res.code==1){
                    location.href=res.url;
                }else{
                    layer.msg(res.msg);
                }
            }
        })
    }
</script>
{/block}