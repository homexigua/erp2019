{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="box">
            <div class="box-body">
                <form class="form-horizontal" id="form" data-callfn="refreshTable">
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">行业名称</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="trade_name" value="" placeholder="请输入行业名称" datatype="*"  autocomplete="off" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">行业管理员</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="nickname" value="" placeholder="请输入行业管理员名称" datatype="*" autocomplete="off"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">管理账号</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="username" value="" placeholder="请输入管理账号" datatype="*" autocomplete="off"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">管理密码</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="password" value="" placeholder="请输入管理密码" datatype="*" autocomplete="off"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">绑定域名</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" name="domain" rows="3" placeholder="请输入行业域名"></textarea>
                            <span class="help-block">请输入绑定的域名，多个域名一行一个</span>
                        </div>
                    </div>
                    <div class="fixed-foot text-right">
                        <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                        <button type="button" class="btn btn-info btn-sub">立即保存</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
<div style="height:15px;"></div>
</body>
<script>
    $(function () {
        formHelper.initForm();
        utils.ajaxSubForm();
    });
</script>
{/block}