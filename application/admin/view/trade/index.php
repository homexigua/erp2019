{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="nav-tabs-custom">
            <div class="box-body">
                <div class="toolbar">
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-primary" onclick="doAction.add();">
                            <i class="fa fa-plus"></i> 添加行业
                        </button>
                    </div>
                </div>
                <table id="grid" ></table>
            </div>
        </div>
    </section>
</div>
</body>
<script>
    utils.btable({
        url: '/admin/trade/index.html',
        columns: [
            {field: 'state', checkbox: true},
            {field: 'trade_name', title: '行业名称'},
            {field: 'domain', title: '绑定域名'},
            {field: 'nickname', title: '管理员',width:150},
            {field: 'status', title: '状态',width:130,formatter:function (value,row,index) {
                    var checked = row.status==0?'':'checked';
                    var html='<input type="checkbox" name="status" class="f-switch"\n' +
                        'data-option=\'{"onText":"启用","offText":"禁用","onColor":"success","offColor":"default","callfn":"doAction.switchAction"}\'\n' +
                        'data-id="'+row.id+'"\n' +
                        'placeholder="" value="1" '+checked+'/>';
                    return html;
                }},
            {
                field: 'operate', title: '操作', width: 100,
                formatter: function (value, row, index) {
                    var html = '';
                    html += '<a href="javascript:;" onclick="doAction.edit(' + row.id + ')">编辑</a> ';
                    return html;
                }
            }

        ],
        onPostBody:function () {
            formHelper.switch();
        }
    });
    var doAction = {
        add: function () {
            var rows = $('#grid').bootstrapTable('getSelections');
            var pid = rows.length==0 ? 0 : rows[0].id;
            utils.openDialog({
                title: '添加行业站点',
                area: ['800px', '420px'],
                content: '/admin/trade/add.html'
            });
        },
        edit: function (id) {
            utils.openDialog({
                title: '修改行业站点',
                area: ['800px', '290px'],
                content: '/admin/trade/edit.html?id=' + id
            });
        },
        switchAction:function (event,state) {
            var id=event.currentTarget.dataset.id;
            var name=event.currentTarget.name;
            var value=state==true?1:0;
            doAction.setStatus(id,name+':'+value);
        },
        setStatus:function (ids,params,url,callfn) {
            url = url || 'multi.html';
            utils.getAjax({
                url:url,
                data:{ids:ids,params:params},
                success:function (res) {
                    if(res.code==0){
                        utils.msg(res.msg);
                    }else{
                        if (typeof callfn === "function") {
                            callfn();
                        } else {
                            if (callfn == "refresh") $('#grid').bootstrapTable('refresh');
                        }
                    }
                }
            })
        },
    };
</script>
{/block}