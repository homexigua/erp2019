{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<?php
$topClass = D('DictClass')->where('pid',0)->select();
?>
<div class="wrapper">
    <section class="content">
        <div class="box">
            <div class="box-body">
                <form class="form-horizontal" id="form">
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">分类编码</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="code" value="{$info.code}" placeholder="请输入分类编码" datatype="*" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">上级分类</label>
                        <div class="col-sm-10">
                            {:FormBuild::_select('pid',$info.pid,['list-data'=>$topClass,'empty-option'=>'0|顶级分类'])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">分类名称</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="name" value="{$info.name}" placeholder="请输入分类名称" datatype="*" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">操作范围</label>
                        <div class="col-sm-10">
                            <input type="checkbox" class="f-switch" name="is_platform"
                                   data-option='{"onText":"平台","offText":"平台","onColor":"success","offColor":"default","size":"lg"}'
                                   placeholder="" value="1" {eq name="info.is_platform" value="1"}checked{/eq} />
                            <input type="checkbox" class="f-switch" name="is_trade"
                                   data-option='{"onText":"行业","offText":"行业","onColor":"success","offColor":"default","size":"lg"}'
                                   placeholder="" value="1" {eq name="info.is_trade" value="1"}checked{/eq} />
                            <input type="checkbox" class="f-switch" name="is_site"
                                   data-option='{"onText":"店铺","offText":"店铺","onColor":"success","offColor":"default","size":"lg"}'
                                   placeholder="" value="1" {eq name="info.is_site" value="1"}checked{/eq} />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">排序</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="show_order" value="{$info.show_order}" placeholder="请输入排序" datatype="n" />
                        </div>
                    </div>
                    <div class="fixed-foot text-right">
                        <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                        <button type="button" class="btn btn-info btn-sub">立即保存</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
<div style="height:15px;"></div>
</body>
<script>
    $(function () {
        formHelper.initForm();
        utils.ajaxSubForm(function () {
            var index = parent.layer.getFrameIndex(window.name);
            parent.layer.close(index);
            parent.utils.refreshPage();
        });
    });
</script>
{/block}