{extend name="admin@public/layout" /}
{block name="head"}
{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <form class="form-horizontal" id="form" data-callfn="refreshTable">
            <div class="box box-solid">
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">卡券号</label>
                        <div class="col-sm-9">
                            {:FormBuild::_text('card_sn',$info.card_sn,['placeholder'=>'请输入卡劵号','datatype'=>'*'])}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label require">卡券类型</label>
                        <div class="col-sm-9">
                            {:FormBuild::_text('card_type',$info.card_type,['datatype'=>'*'])}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">活动名称</label>
                        <div class="col-sm-9">
                            {:FormBuild::_text('card_title',$info.card_title,['placeholder'=>'请输入活动名称'])}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">订单ID</label>
                        <div class="col-sm-9">
                            {:FormBuild::_text('order_id',$info.order_id,['placeholder'=>'请输入订单号'])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">发放客户</label>
                        <div class="col-sm-9">
                            {:FormBuild::_text('member_id',$info.member_id,['placeholder'=>'请输入客户'])}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">核销金额</label>
                        <div class="col-sm-9">
                            {:FormBuild::_text('use_money',$info.use_money)}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">卡劵状态</label>
                        <div class="col-sm-9 form-control-static">
                            {:FormBuild::_radio('card_status',$info.card_status,['list-data'=>'fun:get_dict_list:105'])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">使用时间</label>
                        <div class="col-sm-9 form-control-static">
                            {:FormBuild::_laydate('use_time',$info.use_time,['rows'=>'3'])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">核销类型</label>
                        <div class="col-sm-9">
                            {:FormBuild::_('check_type',$info.check_type)}
                        </div>
                    </div>
                </div>
            </div>
            <div class="fixed-foot">
                <div class="text-right">
                    <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                    {:FormBuild::_button('立即保存',['class'=>'btn btn-info btn-sub'])}
                </div>
            </div>
        </form>
    </section>
</div>
</body>
<script>
    $(function () {
        formHelper.initForm();
        utils.ajaxSubForm();
    });
</script>
{/block}