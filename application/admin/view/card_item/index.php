{extend name="admin@public/layout" /}
{block name="head"}
{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper" style="padding-left:160px;">

    <section class="content-header">
        <h1 style="font-size:18px;">优惠劵列表</h1>
        <ol class="breadcrumb">
            <li>
                <a href="javascript:;" class="tab-href" data-url="/admin/index/main.html" data-title="首页">
                    <i class="fa fa-dashboard"></i> 首页
                </a>
            </li>
            <li class="active">优惠劵核销</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-default">
            <div class="box-body">
                <div class="box-header with-border">
                    <form class="form-inline" id="searchForm">
                        <input type="hidden" name="class_id" value="" data-op="in">
                        <div class="form-group">
                            <select class="form-control" id="searchKey">
                                <option value="card_sn">卡劵码</option>
                                <option value="customer_id">发放客户</option>
                                <option value="card_type">卡劵类型</option>
                                <option value="use_time">使用时间</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="searchValue" placeholder="请输入检索关键词" />
                        </div>
                        <div class="form-group">
                            <button type="button" class="btn btn-default" onclick="$('#grid').bootstrapTable('refresh');">
                                <i class="fa fa-search"></i> 检索
                            </button>
                        </div>
                    </form>
                </div>
                <table id="grid"
                       data-id-field="id"
                       data-editable-emptytext="empty"
                       data-editable-url="/admin/card_item/editable.html">
                </table>
            </div>
        </div>
    </section>
</div>
</body>

<script>
    $(function () {
        formHelper.initForm();
    });
    utils.btable({
        url:'index.html',
        columns:[
            { field:'state',checkbox:true },

            { field:'card_sn',title:'卡劵号',width:60,algin:'center',
                editable: {
                    type:'number',
                    validate: function (value) { //验证规则自定义
                        if (valid.isEmpty(value)) return {newValue:'',msg:'卡劵不能为空'};
                    }
                }
            },

            { field:'card_type',title:'卡劵类型',width:150,
                editable:{
                    type:'text',
                    emptytext:'',
                }
            },

            { field:'card_title',title:'卡劵名称',width:150,
                editable:{
                    type:'text',
                    emptytext:'',
                }
            },

            { field:'order_id',title:'订单ID',width:150,
                editable:{
                    type:'number',
                    emptytext:'',
                }
            },

            { field:'member_id',title:'发放客户',width:150,
                editable:{
                    type:'text',
                    emptytext:'',
                }
            },
            { field:'use_money',title:'核销金额',width:150,
                editable:{
                    type:'text',
                    emptytext:'',
                }
            },

            { field:'card_status',title:'卡劵状态',width:150,
                editable:{
                    type:'text',
                    emptytext:'',
                }
            },

            { field:'use_time',title:'使用日期',width:150,
                editable:{
                    type:'text',
                    emptytext:'',
                }
            },
            { field:'check_type',title:'核销类型',width:150,
                editable:{
                    type:'text',
                    emptytext:'',
                }
            },
            {
                field: 'operate', title: '操作', width: 100,
                formatter: function (value, row, index) {
                    var html = '';
                    html += '<a href="javascript:;" onclick="doAction.edit(' + row.id + ')">编辑</a> ';
                    return html;
                }
            },
        ],
        onPostBody:function () {
            formHelper.initForm();
        }
    });
    var doAction={

        edit:function(id){
            utils.openDialog({
                title: '修改优惠劵',
                area: ['800px', '420px'],
                content: '/admin/card_item/edit.html?id='+id
            });
        },
        switchAction:function (event,state) {
            var id=event.currentTarget.dataset.id;
            var name=event.currentTarget.name;
            var value=state==true?1:0;
            doAction.setStatus(id,name+':'+value);
        },
        setStatus:function (ids,params,url,callfn) {
            url = url || 'multi.html';
            utils.getAjax({
                url:url,
                data:{ids:ids,params:params},
                success:function (res) {
                    if(res.code==0){
                        layer.msg(res.msg);
                    }else{
                        if (typeof callfn === "function") {
                            callfn();
                        } else {
                            if (callfn == "refresh") $('#grid').bootstrapTable('refresh');
                        }
                    }
                }
            })
        },
    };
</script>
{/block}
