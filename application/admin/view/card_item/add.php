{extend name="admin@public/layout" /}
{block name="head"}
{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <form class="form-horizontal" id="form" data-callfn="refreshTable" >
            <div class="box box-solid" >
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">卡券号</label>
                        <div class="col-sm-9">
                            {:FormBuild::_text('card_sn','',['placeholder'=>'请输入卡劵号','datatype'=>'*'])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">发放客户</label>
                        <div class="col-sm-9">
                            {:FormBuild::_selectpage('customer_id','',['data-url'=>'/admin/admin/selectpage.html','data-show'=>'nickname','data-key'=>'id'])}
                            {:FormBuild::_select('customer_id','',['placeholder'=>'请输入客户'])}
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">发放时间</label>
                        <div class="col-sm-9">
                            {:FormBuild::_laydate('send_time','')}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">生效日期</label>
                        <div class="col-sm-6">
                            {:FormBuild::_laydate('start_time','')}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">结束日期</label>
                        <div class="col-sm-6 form-control-static">
                            {:FormBuild::_laydate('finish_time','',['placeholder'=>'请输入结束时间','rows'=>'3'])}
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label">所在地区</label>
                        <div class="col-sm-6 ">
                            {:FormBuild::_cxselect('prov,city,dist','')}
                        </div>
                        <div  class="col-sm-4 ">
                            {:FormBuild::_text('addr','',['placeholder'=>'详细地址'])}
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label">是否使用</label>
                        <div class="col-sm-9 form-control-static">
                            {:FormBuild::_radio('is_use','0',['list-data'=>'fun:get_dict_list:105'])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">使用日期</label>
                        <div class="col-sm-9">
                            {:FormBuild::_laydate('use_time','')}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label require">核销金额</label>
                        <div class="col-sm-9">
                            {:FormBuild::_text('use_money','',['datatype'=>'*'])}
                        </div>
                    </div>

                </div>
            </div>
            <div class="fixed-foot">
                <div class="text-right">
                    <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                    {:FormBuild::_button('立即保存',['type'=>'button','class'=>'btn btn-info btn-sub'])}
                </div>
            </div>
        </form>
    </section>
</div>
</body>
<script>
    $(function () {
        formHelper.initForm();
       utils.ajaxSubForm();
    });
</script>
{/block}