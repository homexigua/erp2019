{extend name="admin@public/layout" /}
{block name="head"}
{:D('Static')->loadCodeEditor()}
{/block}
{block name="body"}
<?php
$moudleId = $info['moudle_id'];
$moudleInfo = D('moudle','cms')->where('id',$moudleId)->find();
if(empty($moudleInfo)) die('params error!');
$dataTypes = \org\util\DbHelper::getFieldTypes();
$formTypes = \FormBuild::getFormTypes();
$validRules = json_encode(FormBuild::getValidRules());
$statusList = [['id'=>1,'name'=>'启用'], ['id'=>0,'name'=>'禁用']];
$isList = [['id'=>1,'name'=>'是'], ['id'=>0,'name'=>'否']];
?>
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="box">
            <div class="box-body">
                <form class="form-horizontal" id="form" data-callfn="refreshTable">
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">表单类型</label>
                        <div class="col-sm-4">
                            {:FormBuild::_select('form_type','text',['list-data'=>$formTypes,'datatype'=>'*','disabled'])}
                        </div>
                        <label class="col-sm-2 control-label require">标签名称</label>
                        <div class="col-sm-4">
                            {:FormBuild::_text('display_name',$info.display_name,['datatype'=>'*','placeholder'=>'请输入标签名称'])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">字段名称</label>
                        <div class="col-sm-4">
                            <p class="form-control-static">{$info.field_name}</p>
                        </div>
                        <label class="col-sm-2 control-label">默认值</label>
                        <div class="col-sm-4">
                            {:FormBuild::_text('default_value',$info.default_value,['placeholder'=>'请输入默认值'])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">数据类型</label>
                        <div class="col-sm-4">
                            <p class="form-control-static">{$info.data_type}</p>
                        </div>
                        <label class="col-sm-2 control-label require">数据长度</label>
                        <div class="col-sm-4">
                            <p class="form-control-static">{$info.data_length}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">验证规则</label>
                        <div class="col-sm-10">
                            <input type="input" class="form-control" id="valid" name="valid" data-init="{$info.valid}"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">表单配置</label>
                        <div class="col-sm-10 form-control-static">
                            <textarea class="form-control" placeholder="请填写表单配置" rows="3" name="form_config" id="form_config">{$info.form_config|raw}</textarea>
                            <div class="help-block">表单配置json格式,键值对形式配置</div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">系统</label>
                        <div class="col-sm-4 form-control-static">
                            {eq name="info.is_system" value="1"}是{else/}否{/eq}
                        </div>
                        <label class="col-sm-2 control-label">状态</label>
                        <div class="col-sm-4 form-control-static">
                            {:FormBuild::_radio('status',$info.status,['list-data'=>$statusList])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">排序</label>
                        <div class="col-sm-4 form-control-static">
                            {:FormBuild::_text('show_order',$info.show_order,['datatype'=>'n','placeholder'=>'请输入排序'])}
                        </div>
                    </div>
                    <div class="fixed-foot text-right">
                        <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                        {:FormBuild::_button('立即保存',['class'=>'btn btn-info btn-sub'])}
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
<div style="height:15px;"></div>
</body>
<script>
    $(function () {
        $('#data_type').change(function () {
            var len = $('#data_type option:selected').data('len');
            $("input[name='data_length']").val(len);
        });
        $('#data_type').trigger('change');
        var codeEditor = formHelper.codeEditor('form_config','application/json');
        codeEditor.setSize('auto','160px');
        codeEditor.on("blur", function () {
            $('#form_config').val(codeEditor.getValue());
        });
        formHelper.initForm();
        utils.ajaxSubForm();
    });
    In('selectpage', function () {
        var validRules = JSON.parse('{$validRules|raw}');
        $('#valid').selectPage({
            showField: 'name',
            keyField: 'id',
            pagination: false,
            multiple: true,
            data: validRules
        });
    });
</script>
{/block}