{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<?php
$moudleId = request()->param('moudle_id/d',0);
$moudleInfo = D('moudle','cms')->where('id',$moudleId)->find();
if(empty($moudleInfo)) die('params error!');
?>
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div class="alert bg-gray">
                    <p>1、字段类型、长度建立后不在允许修改</p>
                    <p>2、字段设置为系统内置后不允许删除</p>
                    <p>3、调整完对应字段信息后，需返回模型重新更新缓存后才会生效</p>
                </div>
                <form id="searchForm" style="display:none;">
                    <input type="hidden" name="moudle_id" value="{$Request.param.moudle_id}" data-op="=" />
                </form>
                <div class="toolbar">
                    <div class="btn-group pull-right">
                        <a href="/admin/cms.moudle/index.html" class="btn btn-default">
                            <i class="fa fa-reply"></i> 返回
                        </a>
                        <button type="button" class="btn btn-primary" onclick="doAction.add();">
                            <i class="fa fa-plus"></i> 添加字段
                        </button>
                        <button type="button" class="btn btn-default" onclick="doAction.refresh('{$Request.param.moudle_id}');">
                            <i class="fa fa-refresh"></i> 更新缓存
                        </button>
                    </div>
                </div>
                <table id="grid"
                       data-id-field="id"
                       data-editable-emptytext=""
                       data-editable-url="/admin/cms.moudle_meta/editable.html"></table>
            </div>
        </div>
    </section>
</div>
</body>
<script>
    var moudle_id="{$Request.param.moudle_id}";
    utils.btable({
        url: '/admin/cms.moudle_meta/index.html?is_page=0',
        pagination: false,
        toolbar:'.toolbar',
        showExport: false,
        showRefresh: true,
        showToggle: false,
        showColumns: true,
        sortName:'show_order',
        sortOrder:'asc',
        columns: [
            {field: 'state', checkbox: true},
            {field: 'display_name', title: '显示名',width:120,
                editable: {
                    type:'text',
                    validate: function (value) { //验证规则自定义
                        if (valid.isEmpty(value)) return {newValue:'',msg:'显示名不能为空'};
                    }
                }
            },
            {field: 'field_name', title: '字段名',width:160},
            {field: 'default_value', title: '默认值',width:120},
            {field: 'data_type', title: '字段类型',width:120},
            {field: 'data_length', title: '字段长度',width:120},
            {field: 'form_type', title: '表单类型',width:120},
            {field: 'valid', title: '验证规则',width:180},
            {field: 'is_system', title: '系统', width:40,
                formatter:function (value, row, index) {
                    if(value==0) return '<i class="fa fa-close text-danger"></i>';
                    if(value==1) return '<i class="fa fa-check text-green"></i>';
                }
            },
            {field: 'status', title: '状态', width:80,
                formatter:function (value, row, index) {
                    var html = '';
                    if (value == 1) {
                        html = '<label class="label label-success" onclick="utils.doAction(this,\'refreshTable\')" ' +
                            'data-url="/admin/cms.moudle_meta/multi.html" ' +
                            'data-params="ids=' + row.id + '&params=status:0">启用</label>';
                    } else {
                        html = '<label class="label label-warning" onclick="utils.doAction(this,\'refreshTable\')" ' +
                            'data-url="/admin/cms.moudle_meta/multi.html" ' +
                            'data-params="ids=' + row.id + '&params=status:1">禁用</label>';
                    }
                    return html;
                }
            },
            {field: 'show_order', title: '排序', width:80,
                editable: {
                    type:'text',
                    validate: function (value) { //验证规则自定义
                        if (!valid.isNum(value)) return {newValue:'0',msg:'排序填写不正确'};
                    }
                }
            },
            {
                field: 'operate', title: '操作', width:60,
                formatter: function (value, row, index) {
                    var html = '';
                    html += '<a href="javascript:;" class="btn btn-info btn-xs" onclick="doAction.edit(' + row.id + ')"><i class="fa fa-edit"></i> </a> ';
                    if(row.is_system==0){
                        html += '<a href="javascript:;" class="btn btn-danger btn-xs" onclick="utils.doAction(this,\'refreshTable\')" ' +
                            'data-params="id=' + row.id + '" data-url="/admin/cms.moudle_meta/del.html"><i class="fa fa-trash"></i> </a>';
                    }
                    return html;
                }
            }
        ]
    });
    var doAction = {
        add: function () {
            utils.openDialog({
                title: '添加字段',
                area: ['800px', '590px'],
                content: '/admin/cms.moudle_meta/add.html?moudle_id='+moudle_id
            });
        },
        edit: function (id) {
            utils.openDialog({
                title: '修改字段',
                area: ['800px', '590px'],
                content: '/admin/cms.moudle_meta/edit.html?id=' + id
            });
        },
        refresh:function (moudleId) { //刷新缓存
            $.get("/admin/cms.moudle_meta/refresh.html?moudle_id="+moudleId,function (res) {
                layer.alert(res.msg,function (index) {
                    layer.close(index);
                    $('#grid').bootstrapTable('refresh');
                });
            });
        }
    };
</script>
{/block}