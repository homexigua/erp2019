{extend name="admin@public/layout" /}
{block name="head"}
{:D('Static')->loadCodeEditor()}
{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="box">
            <div class="box-body">
                <form class="form-horizontal" id="form" data-callfn="refreshTable">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <textarea type="textarea" class="form-control" id="tpl">{$info.tpl}</textarea>
                        </div>
                    </div>
                    <div class="fixed-foot text-right">
                        {:FormBuild::_button('代码重置',['class'=>'btn btn-warning btn-init'])}
                        {:FormBuild::_button('立即保存',['class'=>'btn btn-info btn-sub'])}
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
<div style="height:15px;"></div>
</body>
<script>
    $(function () {
        var codeEditor = formHelper.codeEditor('tpl','php');
        codeEditor.setSize('auto','500px');
        $('.btn-init').click(function () {
            var id="{$Request.param.id}";
            var type="{$Request.param.type}";
            $.post('/admin/cms.moudle/initcode.html',{id:id,type:type},function (html) {
                if(type=='model'){
                    codeEditor.setOption("mode",'php');
                }else{
                    codeEditor.setOption("mode",'htmlmixed');
                }
                codeEditor.setValue(html);
            });
        });
        $('.btn-sub').click(function () {
            var id="{$Request.param.id}";
            var type="{$Request.param.type}";
            var tpl = codeEditor.getValue();
            $.post('/admin/cms.moudle/viewtpl.html',{id:id,type:type,tpl:tpl},function (res) {
                if(res.code==1){
                    layer.msg('保存成功!');
                }else{
                    layer.msg(res.msg);
                }
            });
        });
    });
</script>
{/block}