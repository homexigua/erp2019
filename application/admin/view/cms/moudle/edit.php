{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<?php
$statusList = [['id'=>1,'name'=>'启用'], ['id'=>0,'name'=>'禁用']];
?>
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="box">
            <div class="box-body">
                <form class="form-horizontal" id="form" data-callfn="refreshTable">
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">模型名称</label>
                        <div class="col-sm-10">
                            {:FormBuild::_text('name',$info.name,['datatype'=>'*','placeholder'=>'请输入模型名称'])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">模型表名</label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon">cms_table_</span>
                                {:FormBuild::_text('table_name',$info.table_name,["disabled",'placeholder'=>'请输入模型表名'])}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">备注</label>
                        <div class="col-sm-10 form-control-static">
                            {:FormBuild::_textarea('remark',$info.remark,['datatype'=>'*','placeholder'=>'请输入备注'])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">排序</label>
                        <div class="col-sm-10 form-control-static">
                            {:FormBuild::_text('show_order',$info.show_order,['datatype'=>'n','placeholder'=>'请输入排序'])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">状态</label>
                        <div class="col-sm-10 form-control-static">
                            {:FormBuild::_radio('status','1',['list-data'=>$statusList])}
                        </div>
                    </div>
                    <div class="fixed-foot text-right">
                        <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                        {:FormBuild::_button('立即保存',['class'=>'btn btn-info btn-sub'])}
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
<div style="height:15px;"></div>
</body>
<script>
    $(function () {
        formHelper.initForm();
        utils.ajaxSubForm();
    });
</script>
{/block}