{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div class="toolbar">
                    <div class="pull-left">
                        <button type="button" class="btn btn-primary" onclick="doAction.add();">
                            <i class="fa fa-plus"></i> 添加
                        </button>
                        <button type="button" class="btn btn-default" onclick="doAction.refresh();">
                            <i class="fa fa-refresh"></i> 更新缓存
                        </button>
                    </div>
                </div>
                <table id="grid"></table>
            </div>
        </div>
    </section>
</div>
</body>
<script>
    utils.btable({
        url: '/admin/cms.moudle/index.html',
        columns: [
            {field: 'state', checkbox: true},
            {field: 'name', title: '模型名称',width:160},
            {field: 'table_name', title: '表名',width:220},
            {field: 'add_tpl', title: '添加',width:120,
                formatter:function(value,row,index){
                    return '<a href="javascript:;" onclick="doAction.showTpl(\''+row.id+'\',\'add\');">查看代码</a>';
                }
            },
            {field: 'edit_tpl', title: '编辑',width:120,
                formatter:function(value,row,index){
                    return '<a href="javascript:;" onclick="doAction.showTpl(\''+row.id+'\',\'edit\');">查看代码</a>';
                }
            },
            {field: 'list_tpl', title: '列表',width:120,
                formatter:function(value,row,index){
                    return '<a href="javascript:;" onclick="doAction.showTpl(\''+row.id+'\',\'list\');">查看代码</a>';
                }
            },
            {field: 'model_tpl', title: '模型',width:120,
                formatter:function(value,row,index){
                    return '<a href="javascript:;" onclick="doAction.showTpl(\''+row.id+'\',\'model\');">查看代码</a>';
                }
            },
            {field: 'status', title: '状态', width:80,
                formatter:function (value, row, index) {
                    if(value==0) return '<label class="label label-warning">禁用</label>';
                    if(value==1) return '<label class="label label-success">启用</label>';
                }
            },
            {field: 'remark', title: '备注'},
            {field: 'show_order', title: '排序', width:80},
            {field: 'operate', title: '操作', width:260,
                formatter: function (value, row, index) {
                    var html = '';
                    html += '<a href="/admin/cms.moudle_meta/index.html?moudle_id='+row.id+'" class="btn btn-info btn-xs"><i class="fa fa-database"></i> 字段列表</a> ';
                    html += '<a href="javascript:;" class="btn btn-primary btn-xs" onclick="doAction.edit(' + row.id + ')"><i class="fa fa-edit"></i> 编辑模型</a> ';
                    html += '<a href="javascript:;" class="btn btn-danger btn-xs" onclick="utils.doAction(this,\'refreshTable\')" ' +
                        'data-params="id=' + row.id + '" data-url="/admin/cms.moudle/del.html"><i class="fa fa-trash"></i> 删除模型</a>';
                    return html;
                }
            }
        ]
    });
    var doAction = {
        add: function () {
            utils.openDialog({
                title: '添加模型',
                area: ['800px', '420px'],
                content: '/admin/cms.moudle/add.html'
            });
        },
        edit: function (id) { //修改模型
            utils.openDialog({
                title: '修改模型',
                area: ['800px', '420px'],
                content: '/admin/cms.moudle/edit.html?id=' + id
            });
        },
        refresh:function () { //刷新缓存
            $.get("/admin/cms.moudle/refresh.html",function (res) {
                layer.alert(res.msg,function (index) {
                    layer.close(index);
                    $('#grid').bootstrapTable('refresh');
                });
            });
        },
        showTpl:function (id,type) {
            var index=utils.openDialog({
                title: '查看模板',
                area: ['800px', '420px'],
                content: '/admin/cms.moudle/viewtpl.html?id='+id+"&type="+type
            });
            layer.full(index);
        }
    };
</script>
{/block}