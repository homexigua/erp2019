{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
            {volist name="modelList" id="vo"}
            <li {eq name="Request.param.moudle_id" value="$vo['id']"}class="active"{/eq}><a href="/admin/cms.content/index.html?moudle_id={$vo.id}">{$vo.name}</a></li>
            {/volist}
            </ul>
            <div class="box-header">
                <form class="form-inline" id="searchForm">
                    <input type="hidden" id="moudle_id" name="moudle_id" data-own="1" value="{$Request.param.moudle_id}" data-op="=" />
                    <div class="form-group">
                        <label>栏目</label>
                        <select class="form-control" name="class_id" id="class_id" data-own="1" onchange="$('#grid').bootstrapTable('refresh');" style="width:220px;">
                            <option value="">全部栏目</option>
                            {volist name="classList" id="vo"}
                            <option value="{$vo.id}">{$vo._name}</option>
                            {/volist}
                        </select>
                    </div>
                    <div class="form-group">
                        <label>状态</label>
                        <select class="form-control" name="is_audit" data-op="=" onchange="$('#grid').bootstrapTable('refresh');">
                            <option value="">全部</option>
                            <option value="1">已审</option>
                            <option value="0">未审</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>标题</label>
                        <input type="text" class="form-control" id="title" name="title" value="" data-op="like" placeholder="请输入标题"/>
                    </div>
                    <div class="form-group">
                        <button type="button" class="btn btn-default" onclick="$('#grid').bootstrapTable('refresh');">
                            <i class="fa fa-search"></i> 检索
                        </button>
                    </div>
                </form>
            </div>
            <div class="box-body">
                <div class="toolbar">
                    <div class="pull-left">
                        <button type="button" class="btn btn-primary" onclick="doAction.add('{$Request.param.moudle_id}');">
                            <i class="fa fa-plus"></i> 添加
                        </button>
                        <button type="button" class="btn btn-danger" onclick="doAction.del('{$Request.param.moudle_id}');">
                            <i class="fa fa-trash"></i> 删除
                        </button>
                    </div>
                </div>
                <table id="grid"
                       data-id-field="id"
                       data-editable-emptytext="empty"
                       data-editable-url="/admin/cms.content/editable.html?moudle_id={$Request.param.moudle_id}"></table>
            </div>
        </div>
    </section>
</div>
</body>
<script>
    utils.btable({
        url: '/admin/cms.content/index.html',
        singleSelect: false, //是否启用单选
        sortName:'show_order',
        sortOrder:'asc',
        columns: [
            {field: 'state', checkbox: true},
            {field: 'title', title: '标题'},
            {field: 'is_audit', title: '审核', width:80,
                formatter: function (value, row, index) {
                    var html = '';
                    if (value == 1) {
                        html = '<label class="label label-success" onclick="utils.doAction(this,\'refreshTable\')" ' +
                            'data-url="/admin/cms.content/multi.html?moudle_id={$Request.param.moudle_id}" ' +
                            'data-params="ids=' + row.id + '&params=is_audit:0">已审</label>';
                    } else {
                        html = '<label class="label label-warning" onclick="utils.doAction(this,\'refreshTable\')" ' +
                            'data-url="/admin/cms.content/multi.html?moudle_id={$Request.param.moudle_id}" ' +
                            'data-params="ids=' + row.id + '&params=is_audit:1">未审</label>';
                    }
                    return html;
                }
            },
            {field: 'show_order', title: '排序', width:80,
                editable: {
                    type:'text',
                    validate: function (value) { //验证规则自定义
                        if (!valid.isNum(value)) return {newValue:'0',msg:'排序填写不正确'};
                    }
                }
            },
            {field: 'operate', title: '操作', width:120,
                formatter: function (value, row, index) {
                    var html = '';
                    html += '<a href="javascript:;" class="btn btn-primary btn-xs" onclick="doAction.edit('+row.moudle_id+',' + row.id + ')">编辑</a> ';
                    html += '<a href="javascript:;" class="btn btn-danger btn-xs" onclick="doAction.del('+row.moudle_id+',' + row.id + ')">删除</a>';
                    return html;
                }
            }
        ]
    });
    var doAction = {
        add: function () {
            var moudle_id = parseInt($('#moudle_id').val());
            var class_id = $('#class_id').val();
            var class_id = valid.isEmpty(class_id) ? '' : class_id;
            var index=utils.openDialog({
                title: '添加内容',
                area: ['800px', '420px'],
                content: '/admin/cms.content/add.html?moudle_id='+moudle_id+'&class_id='+class_id
            });
            layer.full(index);
        },
        edit: function (moudle_id,id) { //修改模型
            var index=utils.openDialog({
                title: '修改模型',
                area: ['800px', '420px'],
                content: '/admin/cms.content/edit.html?moudle_id='+moudle_id+'&id=' + id
            });
            layer.full(index);
        },
        del:function (moudle_id,ids) {
            if(typeof ids=='undefined'){
                var ids = $.map($('#grid').bootstrapTable('getSelections'),function (row,index) {
                    return row.id;
                });
                if(ids.length==0){
                    layer.msg('请选择删除数据');
                    return false;
                }
            }
            layer.confirm('确认删除吗？',{icon:3},function (index) {
                layer.close(index);
                $.post('/admin/cms.content/del.html',{id:ids.join(','),moudle_id:moudle_id},function (res) {
                    if(res.code==1){
                        layer.msg('删除成功！',{icon:1,time:1200},function () {
                            $('#grid').bootstrapTable('refresh');
                        });
                    }else{
                        layer.msg(res.msg);
                    }
                });
            });
        }
    };
</script>
{/block}