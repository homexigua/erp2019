{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <form class="form-horizontal" id="form" data-callfn="refreshTable">
            <div class="row">
                <div class="col-sm-8">
                    <div class="box">
                        <div class="box-body">
                            <div class="form-group ">
                                <label class="col-sm-2 control-label require">栏目分类</label>
                                <div class="col-sm-10 ">
{:FormBuild::_select('class_id',$Request.param.class_id,[
    'list-data'=>$classList,
    'items-name'=>'_name',
    'empty-option'=>'|请选择栏目',
    'datatype'=>'*'
])}
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="col-sm-2 control-label require">标题</label>
                                <div class="col-sm-10 ">
{:FormBuild::_text('title','',[
    "placeholder" => "请输入标题",
    "datatype" => "*"
])}
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="col-sm-2 control-label ">缩略图</label>
                                <div class="col-sm-10 ">
{:FormBuild::_image('thumb_url','')}
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="col-sm-2 control-label ">关键词</label>
                                <div class="col-sm-10 ">
{:FormBuild::_tags('keywords','')}
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="col-sm-2 control-label ">描述</label>
                                <div class="col-sm-10 form-control-static">
{:FormBuild::_textarea('description','',[
    "placeholder" => "请输入描述"
])}
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="col-sm-2 control-label ">内容</label>
                                <div class="col-sm-10 form-control-static">
{:FormBuild::_ckeditor('content','')}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="box">
                        <div class="box-body">
                            <div class="form-group ">
                                <label class="col-sm-2 control-label">专题</label>
                                <div class="col-sm-10">
{:FormBuild::_selectpages('special_ids','',[
    'data-url'=>'/admin/special/selectpage.html',
    'data-show'=>'special_name',
    'data-key'=>'id'
])}
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="col-sm-2 control-label require">排序</label>
                                <div class="col-sm-10 ">
{:FormBuild::_text('show_order','0',[
    "placeholder" => "请输入排序",
    "datatype" => "n"
])}
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="col-sm-2 control-label require">审核</label>
                                <div class="col-sm-10 form-control-static">
{:FormBuild::_radio('is_audit','1',[
    "list-data" => [
        [
            "id" => 1,
            "name" => "通过"
        ],
        [
            "id" => 0,
            "name" => "拒绝"
        ]
    ],
    "datatype" => "n"
])}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fixed-foot text-right">
                <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                {:FormBuild::_button('立即保存',['class'=>'btn btn-info btn-sub'])}
            </div>
        </form>
    </section>
</div>
<div style="height:15px;"></div>
</body>
<script>
    $(function () {
        formHelper.initForm();
        utils.ajaxSubForm();
    });
</script>
{/block}