{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <form class="form-horizontal" id="form" data-callfn="refreshTable">
            <div class="row">
                <div class="col-sm-7">
                    <div class="box box-solid">
                        <div class="box-body">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">图片扩展1</label>
                                <div class="col-sm-10">
                                    {:FormBuild::_image('extend_image1',$extend['extend_image1'])}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">图片扩展2</label>
                                <div class="col-sm-10">
                                    {:FormBuild::_image('extend_image2',$extend['extend_image2'])}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">图片扩展3</label>
                                <div class="col-sm-10">
                                    {:FormBuild::_image('extend_image3',$extend['extend_image3'])}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label require">图集扩展</label>
                                <div class="col-sm-10">
                                    {:FormBuild::_images_desc('extend_images',$extend['extend_images'])}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label require">项目列表扩展</label>
                                <div class="col-sm-10">
                                    {:FormBuild::_input_array('extend_items',$extend['extend_items'])}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="box box-solid">
                        <div class="box-body">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">文本扩展1</label>
                                <div class="col-sm-10">
                                    {:FormBuild::_text('extend_input1',$extend['extend_input1'])}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">文本扩展2</label>
                                <div class="col-sm-10">
                                    {:FormBuild::_text('extend_input2',$extend['extend_input2'])}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">文本扩展3</label>
                                <div class="col-sm-10">
                                    {:FormBuild::_text('extend_input3',$extend['extend_input3'])}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">多行扩展1</label>
                                <div class="col-sm-10">
                                    {:FormBuild::_textarea('extend_textarea1',html_out($extend['extend_textarea1']))}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">多行扩展2</label>
                                <div class="col-sm-10">
                                    {:FormBuild::_textarea('extend_textarea2',html_out($extend['extend_textarea2']))}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">多行扩展3</label>
                                <div class="col-sm-10">
                                    {:FormBuild::_textarea('extend_textarea3',html_out($extend['extend_textarea3']))}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fixed-foot">
                <div class="text-right">
                    <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                    {:FormBuild::_button('立即保存',['class'=>'btn btn-info btn-sub'])}
                </div>
            </div>
        </form>
    </section>
</div>
</body>
<script >
    $(function () {
        formHelper.initForm();
        utils.ajaxSubForm();
    });
</script>
{/block}