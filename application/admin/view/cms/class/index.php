{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="nav-tabs-custom">
            {:D('Rule','auth')->getSubMenu('17')}
            <div class="box-body">
                <div class="toolbar">
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary" onclick="doAction.add()">
                            <i class="fa fa-plus"></i> 添加
                        </button>
                        <button type="button" class="btn btn-default" onclick="doAction.refresh();">
                            <i class="fa fa-refresh"></i> 更新缓存
                        </button>
                    </div>
                </div>
                <table id="grid" class="table table-no-bordered"></table>
            </div>
        </div>
    </section>
</div>
</body>
<script>
    utils.bTreeTable({
        url:'getlist.html',
        columns:[
            {field:'state',checkbox:true},
            {field:'id',title:'id',width:80},
            {field:'name',title:'栏目名称'},
            {field:'content_num',title:'内容数量',width:120},
            {field:'is_class_add',title:'添加栏目',width:100,
                formatter:function (value, row, index) {
                    var html = '';
                    if (value == 1) {
                        html = '<label class="label label-success" onclick="utils.doAction(this,\'refreshTable\')" ' +
                            'data-url="/admin/cms.class/multi.html" ' +
                            'data-params="ids=' + row.id + '&params=is_class_add:0">允许</label>';
                    } else {
                        html = '<label class="label label-default" onclick="utils.doAction(this,\'refreshTable\')" ' +
                            'data-url="/admin/cms.class/multi.html" ' +
                            'data-params="ids=' + row.id + '&params=is_class_add:1">禁止</label>';
                    }
                    return html;
                }
            },
            {field:'is_content_add',title:'添加内容',width:100,
                formatter:function (value, row, index) {
                    var html = '';
                    if (value == 1) {
                        html = '<label class="label label-success" onclick="utils.doAction(this,\'refreshTable\')" ' +
                            'data-url="/admin/cms.class/multi.html" ' +
                            'data-params="ids=' + row.id + '&params=is_content_add:0">允许</label>';
                    } else {
                        html = '<label class="label label-default" onclick="utils.doAction(this,\'refreshTable\')" ' +
                            'data-url="/admin/cms.class/multi.html" ' +
                            'data-params="ids=' + row.id + '&params=is_content_add:1">禁止</label>';
                    }
                    return html;
                }
            },
            {field:'class_type',title:'栏目类型',width:120,
                formatter:function (value, row, index) {
                    if(value==0) return '<label class="label label-primary">普通栏目</label>';
                    if(value==1) return '<label class="label label-warning">外部链接</label>';
                }
            },
            {field:'show_order',title:'排序',sortable:true,width:100},
            {field: 'operate', title: '操作', width:220,
                formatter: function (value, row, index) {
                    var html = '';
                    html += '<a href="javascript:;" class="btn bg-primary btn-xs" onclick="doAction.add(' + row.id + ')">添加下级</a> ';
                    html += '<a href="javascript:;" class="btn bg-purple btn-xs" onclick="doAction.extend(' + row.id + ',\''+row.name+'\')">扩展</a> ';
                    html += '<a href="javascript:;" class="btn bg-navy btn-xs" onclick="doAction.edit(' + row.id + ')">编辑</a> ';
                    html += '<a href="javascript:;" class="btn bg-red-active btn-xs" onclick="utils.doAction(this,\'refreshTable\')" ' +
                        'data-params="id=' + row.id + '" data-url="/admin/cms.class/del.html">删除</a>';
                    return html;
                }
            }
        ],
        onLoadSuccess: function(data) {
            $('#grid').treegrid({
                treeColumn: 2,
                saveState:true,
                initialState:'collapsed',
                onChange: function() {
                    $('#grid').bootstrapTable('resetWidth');
                }
            });
        }
    });
    var doAction={
        add:function(classId){
            var index = utils.openDialog({
                title: '添加栏目',
                area: ['800px', '420px'],
                content: '/admin/cms.class/add.html?class_id='+classId
            });
            layer.full(index);
        },
        edit:function(id){
            var index = utils.openDialog({
                title: '修改栏目',
                area: ['800px', '420px'],
                content: '/admin/cms.class/edit.html?id='+id
            });
            layer.full(index);
        },
        extend:function(id,name){
            var index = utils.openDialog({
                title: name+'扩展信息',
                area: ['800px', '420px'],
                content: '/admin/cms.class/extend.html?id='+id
            });
            layer.full(index);
        },
        refresh:function () { //刷新缓存
            $.get("/admin/cms.class/refresh.html",function (res) {
                layer.alert(res.msg,function (index) {
                    layer.close(index);
                    $('#grid').bootstrapTable('refresh');
                });
            });
        }
    };
</script>
{/block}
