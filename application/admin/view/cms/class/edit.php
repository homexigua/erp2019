{extend name="admin@public/layout" /}
{block name="head"}
<style>
    .type-box{display:none;}
</style>
{/block}
{block name="body"}
<?php
$cat = D('Class','cms')->getList(); //栏目列表
$selectCat = D('Class','cms')->formatSelectList($cat,$info['id']);
$moudle = D('Moudle','cms')->order('show_order asc')->select();
$group = D('ClassGroup','cms')->order('show_order asc')->select();
?>
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <form class="form-horizontal" id="form" data-callfn="refreshTable">
            <div class="row">
                <div class="col-sm-8">
                    <div class="box box-solid">
                        <div class="box-body">
                            <div class="form-group">
                                <label class="col-sm-2 control-label require">上级栏目</label>
                                <div class="col-sm-10">
                                    {:FormBuild::_select('pid',$info.pid,['list-data'=>$selectCat,'datatype'=>'*','items-key'=>'id','items-name'=>'_name','empty-option'=>'0|===顶级栏目==='])}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label require">栏目名称</label>
                                <div class="col-sm-10">
                                    {:FormBuild::_text('name',$info.name,['placeholder'=>'请输入栏目名称','datatype'=>'*'])}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label require">栏目模型</label>
                                <div class="col-sm-10">
                                    {:FormBuild::_select('moudle_id',$info.moudle_id,['datatype'=>'*','list-data'=>$moudle,'items-key'=>'id','items-name'=>'name'])}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">栏目图片</label>
                                <div class="col-sm-10">
                                    {:FormBuild::_image('image_url',$info.image_url)}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">栏目分组</label>
                                <div class="col-sm-10">
                                    {:FormBuild::_selectpages('group_id_collection',$info['group_id_collection'],[
                                    'placeholder'=>'请选择栏目分组',
                                    'data-url'=>'/admin/cms.class_group/selectpage.html',
                                    'data-show'=>'name',
                                    'data-key'=>'id'])}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">栏目关键词</label>
                                <div class="col-sm-10">
                                    {:FormBuild::_tags('class_keywords',$info.class_keywords,['placeholder'=>'请输入栏目关键词'])}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">栏目描述</label>
                                <div class="col-sm-10 form-control-static">
                                    {:FormBuild::_textarea('class_desc',$info.class_desc,['placeholder'=>'请输入栏目描述','rows'=>'3'])}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">栏目正文</label>
                                <div class="col-sm-10 form-control-static">
                                    {:FormBuild::_ckeditor('class_content',html_out($info.class_content))}
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="box box-solid">
                        <div class="box-body">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">显示</label>
                                <div class="col-sm-9 form-control-static">
                                    {:FormBuild::_radio('is_show',$info.is_show,['list-data'=>'fun:get_dict_list:105'])}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">栏目类型</label>
                                <div class="col-sm-9">
                                    {:FormBuild::_select('class_type',$info.class_type,['list-data'=>'fun:get_dict_list:107','id'=>'class_type'])}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">允许添加栏目</label>
                                <div class="col-sm-9 form-control-static">
                                    {:FormBuild::_radio('is_class_add',$info.is_class_add,['list-data'=>'fun:get_dict_list:105'])}
                                </div>
                            </div>
                            <div class="form-group type-box type-box-0">
                                <label class="col-sm-3 control-label">允许添加内容</label>
                                <div class="col-sm-9 form-control-static">
                                    {:FormBuild::_radio('is_content_add',$info.is_content_add,['list-data'=>'fun:get_dict_list:105'])}
                                </div>
                            </div>
                            <div class="form-group type-box type-box-0 type-box-2">
                                <label class="col-sm-3 control-label">栏目模板</label>
                                <div class="col-sm-9">
                                    {:FormBuild::_select('class_template',$info.class_template,[
                                    'list-data'=>'D:Tpl::getTplList:cms_items_'
                                    ])}
                                </div>
                            </div>
                            <div class="form-group type-box type-box-0">
                                <label class="col-sm-3 control-label">内容页模板</label>
                                <div class="col-sm-9">
                                    {:FormBuild::_select('content_template',$info.content_template,[
                                    'list-data'=>'D:Tpl::getTplList:cms_info_'
                                    ])}
                                </div>
                            </div>
                            <div class="form-group type-box type-box-1">
                                <label class="col-sm-3 control-label">外部链接</label>
                                <div class="col-sm-9">
                                    {:FormBuild::_text('class_url',$info.class_url,['placeholder'=>'栏目类型外部链接时生效'])}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label require">操作范围</label>
                                <div class="col-sm-9">
                                    <input type="checkbox" class="f-switch" name="is_platform"
                                           data-option='{"onText":"平台","offText":"平台","onColor":"success","offColor":"default","size":"lg"}'
                                           placeholder="" value="1" {eq name="info.is_platform" value="1"}checked{/eq} />
                                    <input type="checkbox" class="f-switch" name="is_trade"
                                           data-option='{"onText":"行业","offText":"行业","onColor":"success","offColor":"default","size":"lg"}'
                                           placeholder="" value="1" {eq name="info.is_trade" value="1"}checked{/eq} />
                                    <input type="checkbox" class="f-switch" name="is_site"
                                           data-option='{"onText":"店铺","offText":"店铺","onColor":"success","offColor":"default","size":"lg"}'
                                           placeholder="" value="1" {eq name="info.is_site" value="1"}checked{/eq} />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label require">排序</label>
                                <div class="col-sm-9">
                                    {:FormBuild::_text('show_order',$info.show_order,['placeholder'=>'请输入排序','datatype'=>'n'])}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="fixed-foot">
                    <div class="text-right">
                        <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                        {:FormBuild::_button('立即保存',['class'=>'btn btn-info btn-sub'])}
                    </div>
                </div>
            </div>
        </form>
    </section>
</div>
</body>
<script >
    $(function () {
        $('#class_type').change(function () {
            $('.type-box').hide();
            var val=$(this).val();
            if(val==0){ //普通栏目
                $('.type-box-0').show();
            }
            if(val==1){ //外部链接
                $('.type-box-1').show();
            }
            if(val==2){ //单页面
                $('.type-box-2').show();
            }
        });
        $('#class_type').trigger('change');
        formHelper.initForm();
        utils.ajaxSubForm();
    });
</script>
{/block}