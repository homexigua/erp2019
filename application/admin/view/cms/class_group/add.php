{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="box">
            <div class="box-body">
                <form class="form-horizontal" id="form"  data-callfn="refreshTable">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">分组名称</label>
                        <div class="col-sm-10">
                            {:FormBuild::_text('name','',['datatype'=>'*'])}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">备注</label>
                        <div class="col-sm-10">
                            {:FormBuild::_text('remark','')}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">排序</label>
                        <div class="col-sm-10">
                            {:FormBuild::_text('show_order','0')}
                        </div>
                    </div>
                    <div class="fixed-foot text-right">
                        <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                        {:FormBuild::_button('立即保存',['class'=>'btn btn-info btn-sub'])}
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>

</body>
<script>
    $(function () {
        formHelper.initForm();
        utils.ajaxSubForm();
    })
</script>
{/block}
