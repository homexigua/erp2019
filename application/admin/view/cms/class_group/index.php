{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="nav-tabs-custom">
            {:D('Rule','auth')->getSubMenu('17')}
            <div class="box-header with-border">
                <form class="form-inline">
                    <div class="form-group">
                        <select class="form-control" id="searchKey">
                            <option value="name">分组名称</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="searchValue" placeholder="请输入检索关键词">
                    </div>
                    <div class="form-group">
                        <button type="button" class="btn btn-default" onclick="$('#grid').bootstrapTable('refresh');"><i class="fa fa-search"></i> 检索</button>
                    </div>
                </form>
            </div>
            <div class="box-body">
                <div class="toolbar">
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary" onclick="doAction.add()">
                            <i class="fa fa-plus"></i> 添加
                        </button>
                    </div>
                </div>
                <table id="grid" class="table table-no-bordered"></table>
            </div>
        </div>
    </section>
</div>
</body>
<script>
    utils.btable({
        url:'/admin/cms.class_group/index.html',
        columns:[
            {field:'state',checkbox:true},
            {field:'name',title:'分类名称'},
            {field:'remark',title:'备注'},
            {field:'show_order',title:'排序',width:100},
            {field: 'operate', title: '操作', width:120,
                formatter: function (value, row, index) {
                    var html = '';
                    html += '<a href="javascript:;" onclick="doAction.edit(' + row.id + ')">编辑</a> ';
                    html += '<a href="javascript:;" onclick="utils.doAction(this,\'refreshTable\')" ' +
                        'data-params="id=' + row.id + '" data-url="/admin/cms.class_group/del.html">删除</a>';
                    return html;
                }
            }
        ]
    });
    var doAction={
        add:function(){
            utils.openDialog({
                title: '添加栏目分组',
                area: ['800px', '268px'],
                content: '/admin/cms.class_group/add.html'
            });
        },
        edit:function(id){
            utils.openDialog({
                title: '修改栏目分组',
                area: ['800px', '268px'],
                content: '/admin/cms.class_group/edit.html?id=' + id
            });
        }
    }
</script>
{/block}