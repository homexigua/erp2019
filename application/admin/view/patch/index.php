{extend name="admin@public/layout" /}
{block name="head"}{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="nav-tabs-custom">
            {:D('Rule','auth')->getSubMenu('8')}
            <div class="box-body">
                <div class="toolbar">
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-primary" onclick="doAction.add();">
                            <i class="fa fa-plus"></i> 添加
                        </button>
                        <button type="button" class="btn btn-danger" onclick="doAction.del();">
                            <i class="fa fa-trash"></i> 删除
                        </button>
                    </div>
                </div>
                <table id="grid"></table>
            </div>
        </div>
    </section>
</div>
</body>
<script>
    utils.btable({
        url: 'index.html',
        singleSelect: false, //是否启用单选
        columns: [
            {field: 'state', checkbox: true},
            {field: 'patch_sn', title: '碎片编号',width:180},
            {field: 'patch_type_text', title: '碎片类型',width:220},
            {field: 'patch_title', title: '碎片标题',width:220},
            {field: 'patch_desc', title: '碎片描述'},
            {
                field: 'operate', title: '操作', width: 100,
                formatter: function (value, row, index) {
                    var html = '';
                    html += '<a href="javascript:;" onclick="doAction.edit(' + row.id + ')">编辑</a> ';
                    html += '<a href="javascript:;" onclick="utils.doAction(this,\'refreshTable\')" ' +
                        'data-params="id=' + row.id + '" data-url="del.html">删除</a>';
                    return html;
                }
            }
        ]
    });
    var doAction = {
        add: function () {
            var index = utils.openDialog({
                title: '添加碎片',
                area: ['800px', '568px'],
                content: '/admin/patch/add.html'
            });
            layer.full(index);
        },
        edit: function (id) {
            var index=utils.openDialog({
                title: '修改管理员',
                area: ['800px', '568px'],
                content: '/admin/patch/edit.html?id=' + id
            });
            layer.full(index);
        },
        del: function () {
            var ids = $.map($('#grid').bootstrapTable('getSelections'),function (row,index) {
                return row.id;
            });
            if(ids.length==0){
                layer.msg('请选择删除碎片');
                return false;
            }
            layer.confirm('确认删除吗？',{icon:3},function (index) {
                layer.close(index);
                $.post('/admin/patch/del.html',{id:ids},function (res) {
                    if(res.code==1){
                        layer.msg('删除成功！',{icon:1,time:1200},function () {
                            $('#grid').bootstrapTable('refresh');
                        });
                    }else{
                        layer.msg(res.msg);
                    }
                });
            });
        }
    };
</script>
{/block}