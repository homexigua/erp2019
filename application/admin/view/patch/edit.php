{extend name="admin@public/layout" /}
{block name="head"}
{:D('Static')->loadCodeEditor()}
{/block}
{block name="body"}
<body class="bg-gray-light">
<div class="wrapper">
    <section class="content">
        <div class="box">
            <div class="box-body">
                <form class="form-horizontal" id="form" data-callfn="refreshTable">
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">碎片编号</label>
                        <div class="col-sm-10">
                            <p class="form-control-static">{$info.patch_sn}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">碎片标题</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="patch_title" value="{$info.patch_title}" placeholder="请输入碎片标题" datatype="*" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">碎片描述</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" placeholder="" rows="3" name="patch_desc">{$info.patch_desc}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">碎片类型</label>
                        <div class="col-sm-10">
                            <p class="form-control-static">{$info.patch_type_text}</p>
                        </div>
                    </div>
                    {eq name="info.patch_type" value="1"}
                    <div class="form-group">
                        <label class="col-sm-2 control-label require">上传图片</label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <input type="text" class="form-control" id="input-uploadimg" name="patch_content1" value="{$info.patch_content}" placeholder="请上传缩略图" />
                                <span class="input-group-addon f-uploadimg" id="uploadimg"><i class="fa fa-upload"></i> 上传</span>
                                <span class="input-group-addon" onclick="utils.imgPreview($('#input-uploadimg').val())"><i class="fa fa-picture-o"></i> 预览</span>
                            </div>
                        </div>
                    </div>
                    {/eq}
                    {eq name="info.patch_type" value="2"}
                    <div class="form-group patch-content patch-content-2">
                        <label class="col-sm-2 control-label require">多图带描述</label>
                        <div class="col-sm-10">
                            <div class="f-upload-images-remark-box" data-upload="uploadmore" data-name="patch_content2">
                                <a class="btn btn-info" id="uploadmore" style="margin-bottom:10px;">上传图片</a>
                                <div class="clearfix"></div>
                                <div class="row f-upload-images-item">
                                    {volist name="info.patch_content" id="vo"}
                                    <div class="col-lg-6 col-md-12 col-sm-12 item-row">
                                        <div class="img-box"  style="background-image:url('{$vo.url}')">
                                            <a href="javascript:;" onclick="utils.upMove(this);"><i class="fa fa-arrow-left"></i></a>
                                            <a href="javascript:;" onclick="utils.downMove(this);"><i class="fa fa-arrow-right"></i></a>
                                            <a href="javascript:;" onclick="utils.delRow(this);"><i class="fa fa-trash-o"></i></a>
                                        </div>
                                        <div class="form">
                                            <div class="form-group">
                                                <input type="hidden" class="form-control" name="patch_content2[url][]" value="{$vo.url}">
                                                <input type="text" class="form-control" name="patch_content2[title][]" value="{$vo.title}" placeholder="请输入图片标题">
                                            </div>
                                            <div class="form-group">
                                                <textarea class="form-control" rows="2" name="patch_content2[remark][]" placeholder="请输入图片摘要">{$vo.remark}</textarea>
                                            </div>
                                            <div class="input-group link-input">
                                                <span class="input-group-addon"><i class="fa fa-link"></i></span>
                                                <input type="text" class="form-control" name="patch_content2[href][]" value="{$vo.href}">
                                            </div>
                                        </div>
                                    </div>
                                    {/volist}
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    {/eq}
                    {eq name="info.patch_type" value="3"}
                    <div class="form-group patch-content patch-content-3">
                        <label class="col-sm-2 control-label require">代码碎片</label>
                        <div class="col-sm-10 form-control-static">
                            <textarea type="textarea" class="form-control" name="patch_content3" id="patch_content3" style="height:400px;">{$info.patch_content}</textarea>
                        </div>
                    </div>
                    {/eq}
                    {eq name="info.patch_type" value="4"}
                    <div class="form-group patch-content patch-content-4">
                        <label class="col-sm-2 control-label require">富文本碎片</label>
                        <div class="col-sm-10 form-control-static">
                            <textarea class="f-ckeditor" name="patch_content4" id='patch_content4' data-option='{"height":"380px"}'>{$info.patch_content|raw}</textarea>
                        </div>
                    </div>
                    {/eq}
                    {eq name="info.patch_type" value="5"}
                    <div class="form-group patch-content patch-content-5">
                        <label class="col-sm-2 control-label require">项目碎片</label>
                        <div class="col-sm-10 form-control-static">
                            <table class="table f-input-array" data-name="items">
                                <thead>
                                <tr>
                                    <th width="180">键</th>
                                    <th width="180">值</th>
                                    <th>描述</th>
                                    <th width="100">操作</th>
                                </tr>
                                </thead>
                                <tr class="data-row-tpl">
                                    <td><input type="text" class="form-control item-input" data-name="key" value="" placeholder="请输入键"></td>
                                    <td><input type="text" class="form-control item-input" data-name="value" value="" placeholder="请输入值"></td>
                                    <td><input type="text" class="form-control item-input" data-name="remark" value="" placeholder="请输入描述"></td>
                                    <td><a href="javascript:;" class="btn btn-info btn-add"><i class="fa fa-plus"></i></a></td>
                                </tr>
                                <tbody class="data-row">
                                {volist name="info.patch_content" id="vo"}
                                <tr class="data-row-tpl">
                                    <td><input type="text" class="form-control item-input" data-name="key" name="items[{$i-1}][key]" value="{$vo.key}" placeholder="请输入键"></td>
                                    <td><input type="text" class="form-control item-input" data-name="value" name="items[{$i-1}][value]" value="{$vo.value}" value="" placeholder="请输入值"></td>
                                    <td><input type="text" class="form-control item-input" data-name="remark" name="items[{$i-1}][remark]" value="{$vo.remark}" value="" placeholder="请输入描述"></td>
                                    <td><a href="javascript:;" class="btn btn-danger btn-del"><i class="fa fa-times"></i></a></td>
                                </tr>
                                {/volist}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    {/eq}
                    <div class="fixed-foot text-right">
                        <span class="valid-msg text-danger" style="margin-left:10px;display:none;"></span>
                        <button type="button" class="btn btn-info btn-sub">立即保存</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
<div style="height:15px;"></div>
</body>
<script>
    $(function () {
        var patch_type="{$info.patch_type}";
        if(patch_type==3){
            var codeEditor = formHelper.codeEditor('patch_content3','htmlmixed');
            codeEditor.on("blur",function(){
                $('#patch_content3').val(codeEditor.getValue());
            });
        }
        formHelper.initForm();
        utils.ajaxSubForm();
    });
</script>
{/block}