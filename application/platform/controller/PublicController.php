<?php
namespace app\platform\controller;

use app\common\controller\PlatformBaseController;
use app\common\model\member\MemberModel;
use think\facade\Cookie;
use think\facade\Session;

class PublicController extends PlatformBaseController
{
    /**
     * 初始化方法
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new MemberModel();
    }

    /**
     * 用户登录
     * @return mixed
     */
    public function login(){
        $memberModel = new MemberModel();
        if($this->request->isAjax()){
            $redirect = $this->request->param('redirect'); //获取跳转地址
            $redirect = empty($redirect) ? '/' : urldecode($redirect);
            try{
                $account = $this->request->param('account');
                $password = $this->request->param('password');
                $memberModel->login($account,$password);
            }catch (\Exception $e){
                $this->error($e->getMessage());
            }
            $this->success('操作成功！',$redirect);
        }
        return $this->fetch();

    }
    /*注册*/
    public function reg(){
        if($this->request->isAjax()){
            try{
                $data = $this->request->param();
                //$smsModel = new SmsModel();
                //$smsModel->checkValid($data['mobile'],$data['sms_vcode']);
                $result = $this->validate($data, [
                    'password|密码'=>'require'
                ]);
                if (true !== $result) {
                    exception($result,40001);
                }
                if($data['agree']!=1){
                    exception('必须同意注册协议才能进行注册！', 40001);
                }
                /*//注册绑定微信资料
                if(config('base.wx_login')===true){
                    if(is_mobile()==3 && Cookie::has('wxuser')){
                        $wxuser = Cookie::get('wxuser');
                        $data['openid'] = $wxuser['openid'];
                        $data['nickname'] = $wxuser['nickname'];
                        $data['headimgurl'] = $wxuser['headimgurl'];
                        $data['sex'] = $wxuser['sex'];
                    }
                }*/
                $data['status']=1;
                $this->model->addData($data);
                Cookie::delete('sms_valid');
            }catch (\Exception $e){
                $this->error($e->getMessage());
            }
            $this->success('注册成功！', url('/home/Public/login'));
        }

        return $this->fetch();
    }
    /*找回密码*/
    public function findPassword(){

        return $this->fetch();
    }


    //短信模板
    public function smsen(){
        //{1}为您的{2}验证码
        $status = send_sms('13453222041',
            [
                'template' => 'SMS_152475505',
                'data' =>
                    [
                        'code'=>'134567'
                    ]
            ]);
        exit;
        return $this->fetch();
    }
}
