<?php

namespace app\platform\controller;

use app\common\controller\PlatformBaseController;

class IndexController extends PlatformBaseController {
    /**
     * 初始化方法
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * 首页
     * @return mixed
     */
    public function index() {
        return $this->fetch();
    }

}
