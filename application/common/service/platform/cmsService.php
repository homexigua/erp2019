<?php

namespace app\common\service\platform;

use think\facade\Request;

class cmsService {

    private $tradeId;
    private $siteId;
    private $moudleId;
    private $model;

    public function __construct($tradeId,$siteId,$moudleId) {
        $this->moudleId = $moudleId;
        $this->tradeId = $tradeId;
        $this->siteId = $siteId;
        $this->model = cms_model($moudleId);
    }

    /**
     * 获取文章列表
     * @param $limit
     * @param $order
     * @param int $classID
     * @return mixed
     * @throws \Exception
     */
    public function getlist($limit,$order=[],$query=[]){
        $map[]=['trade_id',0];
        $map[]=['site_id',0];
        $map = array_merge($map,$query);
        $list = $this->model->field('content',true)->where($map)->limit($limit)->order($order)->select();
        return $list;
    }

    /**
     * 文章分页
     * @param $pageSize
     * @param array $order
     * @param array $query
     * @param bool $simple
     * @return mixed
     */
    public function pagelist($pageSize,$order=[],$query=[],$simple=false){
        $map[]=['trade_id',0];
        $map[]=['site_id',0];
        $map = array_merge($map,$query);
        $list = $this->model->field('content',true)->where($map)->order($order)->paginate($pageSize,$simple,[
            'query'=>Request::query()
        ]);
        return $list;
    }

    /**
     * ajax分页
     * @param $pageNumber
     * @param $pageSize
     * @param array $order
     * @param array $query
     * @param bool $simple
     * @return mixed
     */
    public function ajaxpage($pageNumber,$pageSize,$order=[],$query=[],$simple=false){
        $map[]=['trade_id',0];
        $map[]=['site_id',0];
        $map = array_merge($map,$query);
        $total = $this->model->where($map)->count();
        $rows = [];
        if($total>0){
            $rows = $this->model->field('content',true)->where($map)->order($order)->page($pageNumber,$pageSize)->select();
            $rows = $rows->toArray();
        }
        return ['total'=>$total,'rows'=>$rows];
    }

}