<?php

namespace app\common\service\trade;

use app\common\model\mall\CartModel;

class cartService {

    private $tradeId;
    private $siteId;
    private $member;
    private $model;

    public function __construct($tradeId,$siteId,$member) {
        $this->tradeId = $tradeId;
        $this->siteId = $siteId;
        $this->member = $member;
        $this->model = new CartModel();
    }

    /**
     * 购物车列表
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getlist(){
        $siteIds = $this->model->where('member_id',$this->member['id'])
                               ->where('trade_id',$this->tradeId)
                               ->column('site_id');
        $siteIds = array_unique($siteIds); //去重
        $cartList=[];
        foreach($siteIds as $k=>$siteId){
            $cartList[] = $this->model->getListBySite($this->member['id'],$this->siteId);
        }
        $list = $cartList;
        return $list;
    }

    /**
     * 添加购物车
     * @param $skuSn
     * @param $goodsNumber
     * @param string $type
     */
    public function add($skuSn,$goodsNumber,$type='plus'){
        $this->model->cartAdd($this->member['id'],$skuSn,$goodsNumber,$type);
    }

    /**
     * 删除单件购物车商品
     * @param $cartId
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function del($cartId){
        $this->model->where('id',$cartId)->delete();
    }
    /**
     * 清空购物车
     */
    public function clear(){
        $this->model
            ->where('trade_id',$this->tradeId)
            ->where('member_id',$this->member['id'])
            ->delete();
    }
}