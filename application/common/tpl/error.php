<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>跳转提示</title>
    <style>
        html,body{height:100%;width:100%;}
        body{margin:0;padding:0;font:12px/2  'Microsoft YaHei',\5FAE\8F6F\96C5\9ED1,"微软雅黑", Arial, sans-serif;background:#f4f4f4;}
        .msg-box{width:80vw;height:300px;text-align:center;position:absolute;left:50%;top:50%;margin:-150px 0 0 -40vw;}
        .msg-box img{width:100px;height:100px;}
        .msg-box h2{font-size:36px;line-height:42px;font-weight:500;}
        .msg-box .jump{font-size:14px;line-height:32px;}
        .msg-box .btn a{padding:8px 20px;border:1px solid #e5e5e5;font-size:14px;color:#989898;margin:0 10px;border-radius:10px;text-decoration:none; }
        .msg-box .btn a:hover{-webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);  box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);}
        .msg-box .btn a.go{border-color:#9ad388;color:#9ad388}
        .msg-box .btn a.go:hover{background:#9ad388;color:#fff;}
    </style>
</head>
<body>
<div class="msg-box">
	<img src="/static/admin/img/error.svg" alt="">
    <h2 class="error" style="font-size:18px;color:#989898">
        {switch Request.param.type}
            {case 1}请登录后再操作{/case}
            {case 2}抱歉...该方法不允许操作！{/case}
            {case 3}抱歉...未找到对应模板！{/case}
            {case 404}抱歉...页面未找到~！{/case}
            {default /}抱歉...您没有权限操作，请联系管理员！
        {/switch}
    </h2>
</div>
</body>
</html>