<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <title>跳转提示</title>
    <style>
        html,body{height:100%;width:100px;}
        body{background:#edf1f4;margin:0;padding:0;font:12px/2  'Microsoft YaHei',\5FAE\8F6F\96C5\9ED1,"微软雅黑", Arial, sans-serif;}
        .msg-box{width:80vw;height:300px;margin:150px 0;margin-left:10vw;padding:50px 0;border:1px solid #e5e5e5;border-radius:10px;background:#fff;text-align:center;
            -webkit-box-shadow: 0 10px 20px rgba(0, 0, 0, 0.1);  box-shadow: 0 10px 20px rgba(0, 0, 0, 0.1);  -webkit-transform: translate3d(0, -1px, 0);  transform: translate3d(0, -1px, 0)}
        .msg-box img{width:100px;height:100px;}
        .msg-box h2{font-size:36px;line-height:42px;font-weight:500;}
        .msg-box .jump{font-size:14px;line-height:32px;}
        .msg-box .btn a{padding:8px 20px;border:1px solid #e5e5e5;font-size:14px;color:#989898;margin:0 10px;border-radius:10px;text-decoration:none; }
        .msg-box .btn a:hover{-webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);  box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);}
        .msg-box .btn a.go{border-color:#9ad388;color:#9ad388}
        .msg-box .btn a.go:hover{background:#9ad388;color:#fff;}
    </style>
</head>
<body>
<div class="msg-box">
    <?php
    switch($code){
        case 1:
        echo '<img src="/static/admin/img/success.svg">';
        echo '<h2 class="success">'.strip_tags($msg).'</h2>';
        break;
        case 0:
        echo '<img src="/static/admin/img/error.svg">';
        echo '<h2 class="error">'.strip_tags($msg).'</h2>';
        break;
    }
    ?>
    <p class="detail"></p>
    <p class="jump">
        页面将在 <span id="wait"><?php echo($wait);?></span> 秒后自动跳转
    </p>
    <div class="btn">
        <a class="back" href="javascript:history.go(-1)">返回上一步</a>
        <a class="go" id="href" href="<?php echo($url);?>">立即跳转</a>
    </div>
</div>
<script type="text/javascript">
    (function(){
        var wait = document.getElementById('wait'),
                href = document.getElementById('href').href;
        var interval = setInterval(function(){
            var time = --wait.innerHTML;
            if(time <= 0) {
                location.href = href;
                clearInterval(interval);
            };
        }, 1000);
    })();
</script>
</body>
</html>