<?php

namespace app\common\controller;

class PlatformBaseController extends BaseController {
    /**
     * 账户类型
     * @var string
     */
    protected $accountType='platform';
    /**
     * 初始化方法
     * AdminBaseController constructor.
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * 站点ID
     * @return int
     */
    public function getSiteId(){
        return 0;
    }

    /**
     * 行业ID
     * @return int
     */
    public function getTradeId(){
        return 0;
    }
}
