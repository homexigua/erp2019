<?php

namespace app\common\controller;

use think\facade\Session;

class PlatformMemberBaseController extends TradeBaseController {
    /**
     * 会员登录信息
     * @var null
     */
    protected $member = null;

    /**
     * 初始化方法
     * AdminBaseController constructor.
     */
    public function __construct() {
        parent::__construct();
        //是否登录
        if (!$this->isLogin()) {
            $redirect = $_SERVER['HTTP_REFERER']; //跳转前页面地址
            $redirect = empty($redirect) ? '/' : urlencode($redirect);
            $this->error('请登录后再操作!', url('public/login',['redirect'=>$redirect]));
        }
        $this->member = Session::get('member');
        $this->assign('member', $this->member);
    }

    /**
     * 是否登录
     * @return bool
     */
    public function isLogin() {
        return Session::has('member');
    }

}