<?php

namespace app\common\controller;

use app\common\model\DomainModel;
use think\facade\Cache;

class TradeBaseController extends BaseController {
    /**
     * 账户类型
     * @var string
     */
    protected $accountType='trade';
    /**
     * 初始化方法
     * AdminBaseController constructor.
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * 站点ID
     * @return int
     */
    public function getSiteId(){
        return 0;
    }

    /**
     * 行业ID
     * @return int
     */
    public function getTradeId(){
        $domain = $this->request->rootDomain();
        $tradeId = DomainModel::getRefId($domain,2);
        if($tradeId>0) return $tradeId;
        die('未找到该行业站点');
    }
}
