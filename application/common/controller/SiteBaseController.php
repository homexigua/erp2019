<?php

namespace app\common\controller;

use app\common\model\DomainModel;
use app\common\model\SiteModel;
use org\Auth;
use org\util\TreeUtil;
use think\Db;
use think\facade\Cache;
use think\facade\Session;

class SiteBaseController extends BaseController {
    /**
     * 账户类型
     * @var string
     */
    protected $accountType='site';
    public $goodsClass;
    public $goodsTreeClass;
    protected $member = null;
    /**
     * 初始化方法
     * AdminBaseController constructor.
     */
    public function __construct() {
        parent::__construct();
        /*获取商品分类*/
        $this->goodsClass = Cache::get('mall_class_'.$this->getSiteId().'_'.$this->getTradeId());
        $class = array_values($this->goodsClass);
        /*商品树形分类*/
        $this->goodsTreeClass = TreeUtil::channelLevel($class,$class[0]['id']);
        $this->assign('site_id',$this->getSiteId());
        $this->assign('trade_id',$this->getTradeId());
        $this->member=Session::get('member');
        $this->assign('member',$this->member);
        $this->assign('goods_class',$this->goodsClass);
        $this->assign('goods_tree_class',$this->goodsTreeClass);
        $this->assign('memberInfo',$this->member);
    }

    /**
     * 站点ID
     * @return int
     */
    public function getSiteId(){
        $domain = $this->request->rootDomain();
        $siteId = DomainModel::getRefId($domain,3);
        if($siteId>0) return $siteId;
        die('未找到该站点');
    }

    /**
     * 行业ID
     * @return int
     */
    public function getTradeId(){
        $siteId = $this->getSiteId();
        $tradeId = SiteModel::where('id',$siteId)->value('trade_id');
        return $tradeId;
    }
}
