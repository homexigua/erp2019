<?php

namespace app\common\controller;

use app\common\model\ConfigModel;
use think\Controller;
use think\facade\Cache;

class BaseController extends Controller {
    /**
     * @var bool 验证失败是否抛出异常
     */
    protected $failException = true;
    /**
     * 网站配置
     * @var mixed
     */
    protected $webconfig;
    /**
     * 模型
     * @var
     */
    protected $model;

    /**
     * 初始化方法
     * AdminBaseController constructor.
     */
    public function __construct() {
        parent::__construct();
        //echo 'webconfig_'.$this->getSiteId().'_'.$this->getTradeId();
        $this->webconfig = Cache::get('webconfig_'.$this->getSiteId().'_'.$this->getTradeId());
        //dump( $this->webconfig);
        if(empty($this->webconfig)) die();
        $theme = $this->webconfig['theme'];
        $viewPath = "theme_".$this->accountType.DIRECTORY_SEPARATOR."{$theme}".DIRECTORY_SEPARATOR;
        $viewMobilePath = "theme_".$this->accountType.DIRECTORY_SEPARATOR."{$theme}_mobile".DIRECTORY_SEPARATOR;
        $this->view->config('view_depr', '/'); //设置模板分隔符
        //PC端主题设置
        $this->view->config('view_path', $viewPath);
        $realMobile = 0;
        //手机端主题设置
        if ($this->request->isMobile()) {
            $this->view->config('view_path', $viewMobilePath);
            $realMobile = 1;
            if (!$this->view->exists('')) { //不存在手机模板使用PC模板
                $this->view->config('view_path', $viewPath);
                $realMobile = 0;
            }
        }
        //视图过滤
        $this->view->filter(function($content) use($theme,$realMobile){
            if($realMobile===0){
                return str_replace("../static_skins/","/theme_{$theme}/static_skins/",$content);
            }else{
                return str_replace("../static_skins/","/theme_{$theme}_mobile/static_skins/",$content);
            }
        });
        $this->assign('webconfig',$this->webconfig);
    }

}
