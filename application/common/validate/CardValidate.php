<?php

namespace app\common\validate;

use think\Validate;

class CardValidate extends Validate
{
    protected $rule =   [
        'card_title|卡券标题'=>'require',
        'card_type|卡券类型'=>'require',
        'card_stock|卡券库存'=>'require',
//        'send_number|发放数量'=>'require',
        'limit_cost|启用金额'=>'require',


    ];

    /**
     * 添加验证场景
     * @return SystemDictType
     */
    public function sceneAdd()
    {
        return $this;
    }
    /**
     * 编辑验证场景
     * @return SystemDictType
     */
    public function sceneEdit()
    {
        return $this;
    }

}