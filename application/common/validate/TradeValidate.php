<?php

namespace app\common\validate;

use think\Db;
use think\Validate;

class TradeValidate extends Validate
{
    protected $rule =   [
        'trade_name|行业名称'=>'require',
        'nickname|行业管理员'=>'require',
        'username|管理账号'=>'require',
        'password|管理密码'=>'require',
        'domain|站点域名'=>'require',
    ];


    /**
     * 添加验证场景
     * @return SystemDictType
     */
    public function sceneAdd()
    {
        return $this;
    }
    /**
     * 编辑验证场景
     * @return SystemDictType
     */
    public function sceneEdit()
    {
        return $this->only(['trade_name','domain']);
    }

}