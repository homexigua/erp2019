<?php

namespace app\common\validate;

use think\Db;
use think\Validate;

class SiteValidate extends Validate
{
    protected $rule =   [
        'site_name|店铺名称'=>'require',
        'site_level|店铺类型'=>'require',
        'begin_end|开通时间'=>'require',
        'domain|绑定域名'=>'require',
    ];
    /**
     * 添加验证场景
     * @return SystemDictType
     */
    public function sceneAdd()
    {
        return $this;
    }
    /**
     * 编辑验证场景
     * @return SystemDictType
     */
    public function sceneEdit()
    {
        return $this->only(['site_name','domain']);
    }

}