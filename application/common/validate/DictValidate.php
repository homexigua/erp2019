<?php

namespace app\common\validate;

use think\Validate;

class DictValidate extends Validate
{
    protected $rule =   [
        'class_code|分类'=>'require',
        'dict_code|字典编码'=>'require',
        'dict_value|字典名称'=>'require',
        'show_order|排序'=>'number'
    ];

    /**
     * 添加验证场景
     * @return SystemDictType
     */
    public function sceneAdd()
    {
        return $this;
    }
    /**
     * 编辑验证场景
     * @return SystemDictType
     */
    public function sceneEdit()
    {
        return $this->only(['dict_code','dict_value','show_order']);
    }

}