<?php
namespace app\common\validate\mall;

use think\Validate;

class BrandValidate extends Validate
{
    protected $rule =   [
        'brand_name|品牌'=>'require',
    ];
    /**
     * 添加验证场景
     * @return SystemDictType
     */
    public function sceneAdd()
    {
        return $this;
    }
    /**
     * 编辑验证场景
     * @return SystemDictType
     */
    public function sceneEdit()
    {
        return $this;
    }

}