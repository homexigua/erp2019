<?php
namespace app\common\validate\mall;

use think\Validate;

class ClassValidate extends Validate
{
    protected $rule =   [
        'dept_id|部门'=>'number',
        'name|栏目名称'=>'require',
    ];
    /**
     * 添加验证场景
     * @return SystemDictType
     */
    public function sceneAdd()
    {
        return $this;
    }
    /**
     * 编辑验证场景
     * @return SystemDictType
     */
    public function sceneEdit()
    {
        return $this;
    }

}