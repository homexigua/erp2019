<?php
namespace app\common\validate\mall;

use think\Validate;

class GoodsAttrValidate extends Validate
{
    protected $rule =   [
        'attr_name|属性名称'=>'require',
        'attr_value|属性值'=>'require|checkName',
    ];
    /*自定义验证*/
    protected function checkName($value,$rule,$data)
    {
        if(!isset($data['is_sku']) && !isset($data['is_search'])){
            return '请选择属性类型';
        }else{
            return true;
        }
    }
    /**
     * 添加验证场景
     * @return SystemDictType
     */
    public function sceneAdd()
    {
        return $this;
    }
    /**
     * 编辑验证场景
     * @return SystemDictType
     */
    public function sceneEdit()
    {
        return $this;
    }

}