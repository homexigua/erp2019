<?php
namespace app\common\validate\mall;

use think\Validate;

class DeliveryValidate extends Validate
{
    protected $rule =   [
        'delivery_name|模板名称'=>'require',
        'freight_type|计价类型'=>'require|checkName',
    ];
    /*自定义验证*/
    protected function checkName($value,$rule,$data)
    {
        if($data['freight_type']==1){
            return $data['freight']!='' ? true : '请输入运费金额';
        }
        if($data['freight_type']==2){
            $msg = $data['first_heavy']!='' ? true : '请输入首重';
            $msg = is_numeric($data['first_heavy']) ? true : '重量必须为数字';
            $msg = $data['first_freight']!='' ? true : '请输入首重运费';
            $msg = is_numeric($data['first_freight']) ? true : '运费必须为数字';
            $msg = $data['continue_heavy']!='' ? true : '请输入续重';
            $msg = is_numeric($data['continue_heavy']) ? true : '续重重量必须为数字';
            $msg = $data['continue_freight']!='' ? true : '请输入续重运费';
            $msg = is_numeric($data['continue_freight']) ? true : '续重运费必须为数字';
            $msg = $data['max_weight']!='' ? true : '请输入续重运费';
            $msg = is_numeric($data['max_weight']) ? true : '最大重量必须为数字';
            $msg = $data['limit_freight']!='' ? true : '请输入最高运费';
            $msg = is_numeric($data['limit_freight']) ? true : '最高运费必须为数字';
            return $msg;
        }
    }
    /**
     * 添加验证场景
     * @return SystemDictType
     */
    public function sceneAdd()
    {
        return $this;
    }
    /**
     * 编辑验证场景
     * @return SystemDictType
     */
    public function sceneEdit()
    {
        return $this;
    }
}