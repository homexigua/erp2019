<?php
namespace app\common\validate\mall;

use think\Validate;

class UnitValidate extends Validate
{
    protected $rule =   [
    ];
    /**
     * 添加验证场景
     * @return SystemDictType
     */
    public function sceneAdd()
    {
        return $this;
    }
    /**
     * 编辑验证场景
     * @return SystemDictType
     */
    public function sceneEdit()
    {
        return $this;
    }
}