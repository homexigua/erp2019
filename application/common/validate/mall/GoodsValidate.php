<?php
namespace app\common\validate\mall;

use think\Validate;

class GoodsValidate extends Validate
{
    protected $rule =   [
        'category_id|行业类目'=>'require|number',
        'class_id|商品分类'=>'require|number',
        'goods_name|商品名称'=>'require',
        'unit|商品单位'=>'require',
        //'delivery_id|运费模板'=>'require|number',
        //'album|商品相册'=>'require',
    ];
    /**
     * 添加验证场景
     * @return SystemDictType
     */
    public function sceneAdd()
    {
        return $this;
    }
    /**
     * 编辑验证场景
     * @return SystemDictType
     */
    public function sceneEdit()
    {
        return $this;
    }

}