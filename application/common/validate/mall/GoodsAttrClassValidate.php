<?php
namespace app\common\validate\mall;

use think\Validate;

class GoodsAttrClassValidate extends Validate
{
    protected $rule =   [
        'class_name|属性组名称'=>'require',
    ];
    /**
     * 添加验证场景
     * @return SystemDictType
     */
    public function sceneAdd()
    {
        return $this;
    }
    /**
     * 编辑验证场景
     * @return SystemDictType
     */
    public function sceneEdit()
    {
        return $this;
    }

}