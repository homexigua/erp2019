<?php

namespace app\common\validate;

use think\Validate;

class PatchValidate extends Validate
{
    protected $rule =   [
        'patch_sn|碎片编号'=>'require',
        'patch_title|碎片标题'=>'require',
        'patch_type|碎片类型'=>'require',
    ];

    /**
     * 添加验证场景
     * @return SystemDictType
     */
    public function sceneAdd()
    {
        return $this;
    }
    /**
     * 编辑验证场景
     * @return SystemDictType
     */
    public function sceneEdit()
    {
        return $this->only(['patch_title']);
    }

}