<?php

namespace app\common\validate;

use think\Validate;

class AdminValidate extends Validate
{
    protected $rule =   [
        'dept_id|部门'=>'number',
        'username|账户'=>'require',
        'password|密码'=>'require',
        'nickname|姓名'=>'require'
    ];

    /**
     * 添加验证场景
     * @return SystemDictType
     */
    public function sceneAdd()
    {
        return $this;
    }
    /**
     * 编辑验证场景
     * @return SystemDictType
     */
    public function sceneEdit()
    {
        return $this->only(['dept_id','username','nickname']);
    }

}