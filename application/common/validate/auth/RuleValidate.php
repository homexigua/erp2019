<?php

namespace app\common\validate\auth;

use think\Validate;

class RuleValidate extends Validate
{
    protected $rule =   [
        'pid|上级规则'=>'number',
        'title|权限名称'=>'require',
        'name|权限规则'=>'require',
        'show_order|排序'=>'number',
    ];

    /**
     * 添加验证场景
     * @return SystemDictType
     */
    public function sceneAdd()
    {
        return $this;
    }
    /**
     * 编辑验证场景
     * @return SystemDictType
     */
    public function sceneEdit()
    {
        return $this;
    }

}