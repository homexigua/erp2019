<?php

namespace app\common\validate;

use think\Validate;

class DictClassValidate extends Validate
{
    protected $rule =   [
        'code|分类编码'=>'require',
        'name|分类名称'=>'require',
        'show_order|排序'=>'number'
    ];

    /**
     * 添加验证场景
     * @return SystemDictType
     */
    public function sceneAdd()
    {
        return $this;
    }
    /**
     * 编辑验证场景
     * @return SystemDictType
     */
    public function sceneEdit()
    {
        return $this->only(['name','show_order']);
    }

}