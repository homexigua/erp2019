<?php

namespace app\common\validate;

use think\Validate;

class SpecialValidate extends Validate
{
    protected $rule =   [
        'special_name|专题名称'=>'require',
        'special_tpl|专题模板'=>'require'
    ];

    /**
     * 添加验证场景
     * @return SystemDictType
     */
    public function sceneAdd()
    {
        return $this;
    }
    /**
     * 编辑验证场景
     * @return SystemDictType
     */
    public function sceneEdit()
    {
        return $this;
    }

}