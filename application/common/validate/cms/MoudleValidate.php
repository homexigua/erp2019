<?php

namespace app\common\validate\cms;

use think\Validate;

class MoudleValidate extends Validate
{
    protected $rule =   [
        'name|模型名称'=>'require',
        'table_name|模型表名'=>'require'
    ];

    /**
     * 添加验证场景
     * @return SystemDictType
     */
    public function sceneAdd()
    {
        return $this;
    }
    /**
     * 编辑验证场景
     * @return SystemDictType
     */
    public function sceneEdit()
    {
        return $this->only(['name']);
    }

}