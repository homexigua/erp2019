<?php

namespace app\common\validate\cms;

use think\Validate;

class MoudleMetaValidate extends Validate
{
    protected $rule =   [
        'moudle_id|模型'=>'require',
        'table_name|模型表名'=>'require',
        'display_name|标签'=>'require',
        'field_name|字段名'=>'alphaDash',
        'data_type|字段类型'=>'require',
        'data_length|字段长度'=>'number',
        'show_order|排序'=>'number',
    ];

    /**
     * 添加验证场景
     * @return SystemDictType
     */
    public function sceneAdd()
    {
        return $this;
    }
    /**
     * 编辑验证场景
     * @return SystemDictType
     */
    public function sceneEdit()
    {
        return $this->only(['display_name','show_order']);
    }

}