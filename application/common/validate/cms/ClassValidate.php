<?php
namespace app\common\validate\cms;

use think\Validate;

class ClassValidate extends Validate
{
    protected $rule =   [
        'pid|上级栏目'=>'require',
        'name|栏目名称'=>'require',
        'class_template|栏目模板'=>'require',
        'content_template|内容模板'=>'require',
        'show_order|排序'=>'number',
    ];
    /**
     * 添加验证场景
     * @return SystemDictType
     */
    public function sceneAdd()
    {
        return $this;
    }
    /**
     * 编辑验证场景
     * @return SystemDictType
     */
    public function sceneEdit()
    {
        return $this;
    }

}