<?php

namespace app\common\validate;

use think\Validate;

class ConfigValidate extends Validate
{
    protected $rule =   [
        'group_id|变量分组'=>'require',
        'name|变量名'=>'require',
        'title|变量标题'=>'require',
        'form_type|表单类型'=>'require',
    ];

    /**
     * 添加验证场景
     * @return SystemDictType
     */
    public function sceneAdd()
    {
        return $this;
    }
    /**
     * 编辑验证场景
     * @return SystemDictType
     */
    public function sceneEdit()
    {
        return $this->only(['title']);
    }

}