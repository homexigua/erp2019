<?php

namespace app\common\validate;

use think\Validate;

class DeptValidate extends Validate
{
    protected $rule =   [
        'name|部门名称'=>'require',
        'show_order|排序'=>'number'
    ];

    /**
     * 添加验证场景
     * @return SystemDictType
     */
    public function sceneAdd()
    {
        return $this;
    }
    /**
     * 编辑验证场景
     * @return SystemDictType
     */
    public function sceneEdit()
    {
        return $this;
    }

}