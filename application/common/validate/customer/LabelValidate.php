<?php

namespace app\common\validate\customer;

use think\Validate;

class LabelValidate extends Validate
{
    protected $rule =   [
        'name|标签名'=>'require',
        'color|标签颜色'=>'require',
    ];

    /**
     * 添加验证场景
     * @return SystemDictType
     */
    public function sceneAdd()
    {
        return $this;
    }
    /**
     * 编辑验证场景
     * @return SystemDictType
     */
    public function sceneEdit()
    {
        return $this;
    }

}