<?php
namespace app\common\validate\customer;

use think\Validate;

class GroupValidate extends Validate
{
    protected $rule =   [
        'name|分组名称'=>'require',
    ];
    /**
     * 添加验证场景
     * @return SystemDictType
     */
    public function sceneAdd()
    {
        return $this;
    }
    /**
     * 编辑验证场景
     * @return SystemDictType
     */
    public function sceneEdit()
    {
        return $this;
    }

}