<?php

namespace app\common\validate\customer;

use think\Validate;

class LogTplValidate extends Validate
{
    protected $rule =   [
        'tpl_name|模板名称'=>'require',
        'tpl_title|模板标题'=>'require',
        'tpl_content|模板内容'=>'require',
    ];

    /**
     * 添加验证场景
     * @return SystemDictType
     */
    public function sceneAdd()
    {
        return $this;
    }
    /**
     * 编辑验证场景
     * @return SystemDictType
     */
    public function sceneEdit()
    {
        return $this;
    }

}