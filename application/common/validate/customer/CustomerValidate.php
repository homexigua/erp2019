<?php
namespace app\common\validate\customer;

use think\Validate;

class CustomerValidate extends Validate
{
    protected $rule =   [
        'name|姓名'=>'require',
        'group_id|客户分组'=>'require',
    ];
    /**
     * 添加验证场景
     * @return SystemDictType
     */
    public function sceneAdd()
    {
        return $this;
    }
    /**
     * 编辑验证场景
     * @return SystemDictType
     */
    public function sceneEdit()
    {
        return $this;
    }

}