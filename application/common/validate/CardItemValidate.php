<?php

namespace app\common\validate;

use think\Validate;

class CardItemValidate extends Validate
{
    protected $rule =   [
        'card_sn|卡券号'=>'number',
        'customer_id|发放客户'=>'text',
        'send_time|发放时间'=>'require',
        'use_money|核销金额'=>'require',
    ];

    /**
     * 添加验证场景
     * @return SystemDictType
     */
    public function sceneAdd()
    {
        return $this;
    }
    /**
     * 编辑验证场景
     * @return SystemDictType
     */
    public function sceneEdit()
    {
        return $this;
    }

}