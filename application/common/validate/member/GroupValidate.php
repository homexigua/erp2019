<?php

namespace app\common\validate\member;

use think\Validate;

class GroupValidate extends Validate
{
    protected $rule =   [
        'name|分组名称'=>'require',
        'show_order|排序'=>'number'
    ];

    /**
     * 添加验证场景
     * @return SystemDictType
     */
    public function sceneAdd()
    {
        return $this;
    }
    /**
     * 编辑验证场景
     * @return SystemDictType
     */
    public function sceneEdit()
    {
        return $this;
    }

}