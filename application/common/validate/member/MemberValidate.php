<?php

namespace app\common\validate\member;

use think\Validate;

class MemberValidate extends Validate
{
    protected $rule =   [

        'password|密码'=>'require',
        'nickname|姓名'=>'require'
    ];

    /**
     * 添加验证场景
     * @return SystemDictType
     */
    public function sceneAdd()
    {
        return $this;
    }
    /**
     * 编辑验证场景
     * @return SystemDictType
     */
    public function sceneEdit()
    {
        return $this->only(['mobile','nickname']);
    }

}