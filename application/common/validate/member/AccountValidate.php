<?php

namespace app\common\validate\member;

use think\Validate;

class AccountValidate extends Validate
{
    protected $rule =   [
        'member_id|会员'=>'require',
        'amount|金额'=>'require|float|>:0',
        'action_name|操作人'=>'require',
        'change_type|变动类型'=>'require'
    ];

    /**
     * 添加验证场景
     * @return SystemDictType
     */
    public function sceneAdd()
    {
        return $this;
    }
    /**
     * 编辑验证场景
     * @return SystemDictType
     */
    public function sceneEdit()
    {
        return $this->only(['amount','action_name']);
    }

}