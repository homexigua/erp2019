<?php

namespace app\common\validate\member;

use think\Validate;

class AddressValidate extends Validate
{
    protected $rule =   [
        'consignee_name|收货人姓名'=>'require',
        'prov|省份'=>'require',
        'city|城市'=>'require',
        'address|地址'=>'require',
        'consignee_phone|收货人电话'=>'require'
    ];

    /**
     * 添加验证场景
     * @return SystemDictType
     */
    public function sceneAdd()
    {
        return $this;
    }
    /**
     * 编辑验证场景
     * @return SystemDictType
     */
    public function sceneEdit()
    {
        return $this;
    }

}