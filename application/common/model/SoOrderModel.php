<?php
namespace app\common\model\mall;

use app\common\model\BaseModel;
use app\common\model\member\AddressModel;
use app\common\model\member\MemberModel;

class SoOrderModel extends BaseModel
{
    protected $name = 'so_order';
    protected $pk='id';

    protected $autoWriteTimestamp = true;

    /**
     * 购物车转订单
     * @param $data
     */
    public function cart2order($data){
        self::startTrans();
        try{
            $detailModel = new OrderDetailModel();
            $skuModel = new GoodsSkuModel();
            $addressModel = new AddressModel();
            $memberModel = new MemberModel();
            $shopModel = new ShopModel();
            if(empty($data['buyer_uid'])) exception('买家不能为空！');
            if($data['is_zt']==0){
                $data['address_info'] = $addressModel->where('id',$data['address_id'])->find();
                if(empty($data['address_info'])) exception('收货地址已失效或删除!',40001);
            }else{
                $data['address_info'] =[
                    'prov'=>'',
                    'city'=>'',
                    'dist'=>'',
                    'address'=>'',
                    'consignee_name'=>'',
                    'consignee_phone'=>'',
                ];
            }
            $buyerInfo = $memberModel->where('id',$data['buyer_uid'])->find();
            $buyGoods = $data['goods'];
            foreach ($buyGoods as $shopId=>$orderInfo){
                $shopInfo = $shopModel->where('id',$shopId)->field('id,shop_name,trade_id')->find();
                //处理客户相关信息
                $skuList = $orderInfo['goods_list'];
                $goodsMoney = 0;
                $shippingMoney = 0;
                foreach($skuList as $skuSn=>$goodsNum){
                    $skuInfo = $skuModel->getinfo(['sku_sn'=>$skuSn]);
                    if(empty($skuInfo)) exception('对应商品不存在！');
                    //todo 活动再说
                    $goodsMoney += $skuInfo['sale_price']*$goodsNum;
                }
                /*计算运费*/
                /*订单金额=商品总价+运费-优惠券金额-积分金额-黑卡金额*/
                $orderMoney = $goodsMoney+$shippingMoney;
                $orderData=[
                    'order_sn'=>rand_number(),
                    'order_remark'=>$orderInfo['remark'],
                    'shop_id'=>$shopId,
                    'trade_id'=>$shopInfo['trade_id'],
                    'order_source'=>'电商订单',
                    'member_id'=>$data['buyer_uid'],
                    'shipping_type'=>$data['is_zt'], //配送方式
                    'shipping_code'=>'',
                    'consignee_prov'=>$data['address_info']['prov'],
                    'consignee_city'=>$data['address_info']['city'],
                    'consignee_dist'=>$data['address_info']['dist'],
                    'consignee_name'=>$data['address_info']['consignee_name'],
                    'consignee_phone'=>$data['address_info']['consignee_phone'],
                    'consignee_address'=>$data['address_info']['address'],
                    'order_money'=>$orderMoney,
                    'goods_money'=>$goodsMoney,
                    'shipping_money'=>$shippingMoney,
                    'is_online'=>1,
                ];
                $orderinfo = self::create($orderData);
                /*组装从表数据*/
                foreach($skuList as $skuSn=>$goodsNum){
                    $skuInfo = $skuModel->getinfo(['sku_sn'=>$skuSn]);
                    $item=[
                        'order_sn'=>$orderinfo['order_sn'],
                        'goods_num'=>$goodsNum,
                        'subtotal_goods_price'=>$goodsNum*$skuInfo['goods_price'],
                        'sku_id'=>$skuInfo['id'],
                    ];
                    unset($skuInfo['id']);
                    $itemData[]=$item+$skuInfo;
                }
                $detailModel->saveAll($itemData);
                /*删除购物车商品*/
                foreach($skuList as $skuSn=>$goodsNum){
                    CartModel::where(['sku_sn'=>$skuSn,'buyer_uid'=>$data['buyer_uid']])->delete();
                }
                $orderSn[] = $orderinfo['order_sn'];
            }
            self::commit();
        }catch (\Exception $e){
            self::rollback();
            exception($e->getMessage(),40001);
        }
        return $orderSn;
    }

    

}