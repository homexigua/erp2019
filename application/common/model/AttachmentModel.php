<?php
namespace app\common\model;

class AttachmentModel extends BaseModel
{
    protected $name = 'attachment';
    protected $pk='id';
    //开启自动写入时间戳
    protected $autoWriteTimestamp = false;

}