<?php
namespace app\common\model;

class SpecialItemModel extends BaseModel
{
    protected $name = 'special_item';
    protected $pk='id';
    //开启自动写入时间戳
    protected $autoWriteTimestamp = false;

    /**
     * 添加专题内容
     * @param $modelId
     * @param $contentId
     * @param $specialIds
     * @throws \Exception
     */
    public function addContent($modelId,$contentId,$specialIds){
        $this->where('content_id',$contentId)->delete();
        if(!empty($specialIds)){
            $specialIds = explode(',',$specialIds);
            $dataList = [];
            foreach ($specialIds as $specialId){
                $dataList[]=['special_id'=>$specialId,'model_id'=>$modelId,'content_id'=>$contentId];
            }
            $this->saveAll($dataList);
        }
    }

    /**
     * 根据内容获取专题ID
     * @param $contentId
     * @return array|string
     */
    public function getSpecialIdsByContentId($modelId,$contentId){
        $specialIds = $this->where('model_id',$modelId)->where('content_id',$contentId)->column('special_id');
        $specialIds = empty($specialIds) ? '' : implode(',',$specialIds);
        return $specialIds;
    }
    /**
     * 根据专题ID获取内容ID
     * @param $contentId
     * @return array|string
     */
    public function getContentIdsBySpecialId($modelId,$specialId){
        $contengIds = $this->where('model_id',$modelId)->where('special_id',$specialId)->column('content_id');
        $contengIds = empty($contengIds) ? '' : implode(',',$contengIds);
        return $contengIds;
    }
}