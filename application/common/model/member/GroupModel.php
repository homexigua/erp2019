<?php
namespace app\common\model\member;

use app\common\model\BaseModel;

class GroupModel extends BaseModel
{
    protected $name = 'member_group';
    protected $pk='id';
    //开启自动写入时间戳
    protected $autoWriteTimestamp = true;

    /**
     * 删除
     * @param $id
     */
    public function delData($id){
        $info = MemberModel::where('group_id',$id)->find();
        if($info) exception('该分组正在使用！');
        $this->where('id',$id)->delete();
    }
}