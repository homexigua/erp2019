<?php
namespace app\common\model\member;

use app\common\model\BaseModel;

class AddressModel extends BaseModel
{
    protected $name = 'member_address';
    protected $pk='id';
    //开启自动写入时间戳
    protected $autoWriteTimestamp = true;

    /**
     * 设置默认值
     * @param $memberId
     * @param $id
     */
    public function setDefault($memberId,$id){
        $this->where('member_id',$memberId)->setField('is_default',0);
        $this->where('id',$id)->setField('is_default',1);
    }
}