<?php
namespace app\common\model;

class SpecialModel extends BaseModel
{
    protected $name = 'special';
    protected $pk='id';
    //开启自动写入时间戳
    protected $autoWriteTimestamp = false;

    public function _formatList($list){
        foreach($list as $k=>$v){
            $total = SpecialItemModel::where('special_id',$v['id'])->count();
            $list[$k]['total'] = $total;
        }
        return $list;
    }
    /**
     * 删除专题
     * @param $id
     */
    public function delData($id) {
        $this->where('id',$id)->delete();
        SpecialItemModel::where('special_id',$id)->delete();
    }
}