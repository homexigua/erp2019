<?php
namespace app\common\model;


use app\common\model\mall\ClassModel;
use org\util\HelpUtil;
use think\db\Where;
use think\facade\Cache;
use think\facade\Session;
use think\facade\Validate;
use think\model\concern\SoftDelete;

class SiteModel extends BaseModel {

    protected $name = 'site';
    protected $pk='id';
    //开启自动写入时间戳
    protected $autoWriteTimestamp = true;
    //设置类型转换
    protected $type = [
    ];

    /**
     * 格式化列表
     * @param $list
     * @return mixed
     */
    public function formatList($list){
        
        return $list;
    }

    /*获取站点信息*/
    public function getInfo($map)
    {
        $domainModel = new DomainModel();
        $info = $this->where($map)->find();
        $domains = $domainModel->where('ref_id',$info['id'])->where('ref_type',3)->column('domain');
        $domains = empty($domains) ? '' : implode(PHP_EOL,$domains);
        if($info){
            $adminInfo = AdminModel::where('id',$info['owner_id'])->find();
            $info['username']=$adminInfo['username'];
            $info['nickname']=$adminInfo['nickname'];
            $info['begin_end']=date('Y-m-d',$info['begin_time']).' - '.date('Y-m-d',$info['end_time']);
            $info['domain'] = $domains;
        }
        return $info;
    }


    /**
     * 添加站点
     * @param $data
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function addData($data) {
        self::startTrans();
                try {
                    $loginInfo = $this->getLoginInfo();
                    if($loginInfo['site_id']>0 && $loginInfo['trade_id']>0) exception('非法操作！',40001);
                    if($loginInfo['site_id']==0){
                        $data['trade_id'] = $loginInfo['trade_id']>0 ? $loginInfo['trade_id'] : $data['trade_id'];
                        if(empty($data['trade_id'])) exception('请选择行业');
                    }
                    $begin_end = explode(' - ',$data['begin_end']);
                    $data['begin_time']=strtotime($begin_end[0]);
                    $data['end_time']=strtotime($begin_end[1]);
                    if(!isset($data['admin_id']) && ($data['username']=='' || $data['password']=='')){
                exception('请输入站点管理账号及密码',40001);
            }
            $info = self::create($data,true);
            $domainModel = new DomainModel();
            $domains = format_textarea($data['domain']);
            $domainModel->addDomain($domains,$info['id'],3);
            /*创建店铺管理员*/
            if(!isset($data['admin_id'])){
                $adminModel = new AdminModel();
                $adminData =[
                    'username'=>$data['username'],
                    'password'=>$data['password'],
                    'nickname'=>$data['site_contact'],
                    'session_key'=>'site',
                    'is_init'=>1
                ];
                $adminInfo = $adminModel->addData($adminData);
                $data['admin_id']=$adminInfo['id'];
            }

            //初始化

            /*创建店铺顶级部门*/
            $deptModel = new DeptModel();
            $deptData =[
                'pid'=>0,
                'name'=>$data['site_name'],
                'site_id'=>$info['id'],
                'trade_id'=>$data['trade_id']
            ];
            $deptInfo = $deptModel->addData($deptData);
            /*反写admin_id,dept_id,trade_id到权限表*/
            AdminAccessModel::create([
                'admin_id'=>$data['admin_id'],
                'site_id'=>$info['id'],
                'dept_id'=>$deptInfo['id'],
                'group_id'=>4,//暂时给与店铺权限
                'trade_id'=>$data['trade_id']
            ]);
            /*创建商品类目顶级*/
            ClassModel::create([
                'pid'=>0,
                'name'=>$data['site_name'],
                'site_id'=>$info['id'],
                'trade_id'=>$data['trade_id']
            ]);
            /*初始化文章类目顶级*/
            /*创建店铺站点配置*/
            $configModel = new ConfigModel();
            $configModel->saveAll(
                [
                    [
                        'group_id'=>1,'name'=>'web_title','title'=>'站点名称','tips_name'=>'网站前端页面显示名称','value'=>$data['site_name'],'form_type'=>'text',
                        'form_config'=>'{
                                            &quot;placeholder&quot;:&quot;请输入站点名称&quot;
                                        }',
                        'valid'=>'require','show_order'=>1,'is_system'=>1,'site_id'=>$info['id'],'trade_id'=>$data['trade_id']
                    ],
                    [
                        'group_id'=>1,'name'=>'keywords','title'=>'关键词','value'=>$data['site_name'],'form_type'=>'text',
                        'valid'=>'require','show_order'=>2,'is_system'=>1,'site_id'=>$info['id'],'trade_id'=>$data['trade_id']
                    ],
                    [
                        'group_id'=>1,'name'=>'desc','title'=>'描述','value'=>$data['site_name'],'form_type'=>'textarea',
                        'valid'=>'require','show_order'=>3,'is_system'=>1,'site_id'=>$info['id'],'trade_id'=>$data['trade_id']
                    ],
                    [
                        'group_id'=>1,'name'=>'desc','title'=>'LOGO','tips_name'=>'前端调用网站LOGO','form_type'=>'image',
                        'valid'=>'require','show_order'=>4,'is_system'=>1,'site_id'=>$info['id'],'trade_id'=>$data['trade_id']
                    ],
                    [
                        'group_id'=>1,'name'=>'theme','title'=>'主题风格','tips_name'=>'前端页面模板','value'=>'default','form_type'=>'select',
                        'form_config'=>'{
                                            &quot;list-data&quot;:&quot;D:Tpl::getThemeList:site&quot;,
                                            &quot;empty-option&quot;:&quot;|请选择主题风格&quot;
                                        }',
                        'valid'=>'require','show_order'=>5,'is_system'=>1,'site_id'=>$info['id'],'trade_id'=>$data['trade_id']
                    ]
                ]
            );
            /*创建店铺碎片配置*/




            /*反写admin_id到店铺表*/
            $this->where('id',$info['id'])->setField('owner_id',$data['admin_id']);
            self::commit();
        } catch (\Exception $e) {
            self::rollback();
            exception($e->getMessage(),40001);
        }
        return true;
    }

    /**
     * 修改站点信息
     * @param $data
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function editData($data) {
        self::startTrans();
        try {
            $loginInfo = $this->getLoginInfo();
            if($loginInfo['site_id']>0 && $loginInfo['trade_id']>0) exception('非法操作！',40001);
            if($loginInfo['site_id']==0){
                $data['trade_id'] = $loginInfo['trade_id']>0 ? $loginInfo['trade_id'] : $data['trade_id'];
                if(empty($data['trade_id'])) exception('请选择行业');
            }
            /*如果修改了店铺等级，需要过滤店铺已有权限*/
            /*如果修改了员工数量，比现在的数量少了，判断已有员工，超过修改的数量提示错误*/
            /*如果修改了起止时间*/
            /*更改域名后删除旧的域名缓存，添加新的缓存*/
            $info = self::update($data,true);
            $domainModel = new DomainModel();
            $domains = format_textarea($data['domain']);
            $domainModel->editDomain($domains,$info['id'],3);
            self::commit();
        } catch (\Exception $e) {
            self::rollback();
            exception($e->getMessage());
        }
        return true;
    }

}