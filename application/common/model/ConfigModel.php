<?php
namespace app\common\model;

use think\facade\Cache;

class ConfigModel extends BaseModel
{
    protected $name = 'config';
    protected $pk='id';
    //开启自动写入时间戳
    protected $autoWriteTimestamp = false;

    /**
     * 添加配置项
     * @param $data
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function addData($data) {
        $loginInfo = $this->getLoginInfo();
        $info = $this->where('name',$data['name'])
                     ->where('site_id',$loginInfo['site_id'])
                     ->where('trade_id',$loginInfo['trade_id'])
                     ->find();
        if($info) exception('该配置项已存在啊！',40001);
        self::create($data);
        $this->updateCache();
    }

    /**
     * 更新配置项
     * @param $data
     * @return BaseModel|void
     */
    public function editData($data) {
        self::update($data);
        $this->updateCache();
    }

    /**
     * 删除数据
     * @param $id
     * @return bool
     */
    public function delData($id){
        $this->where('id',$id)->delete();
        $this->updateCache();
    }
    /**
     * 更新缓存
     */
    public function updateCache(){
        $loginInfo = $this->getLoginInfo();
        $config = $this->where(['site_id'=>$loginInfo['site_id'],'trade_id'=>$loginInfo['trade_id']])->order('show_order asc')->column('value','name');
        Cache::set('webconfig_'.$loginInfo['site_id'].'_'.$loginInfo['trade_id'],$config);
    }

    /**
     * 获取对应组配置项
     * @param $groupId
     * @return array|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getListByGroupId($groupId){
        $loginInfo = $this->getLoginInfo();
        $list = $this->where('group_id',$groupId)
                     ->where('site_id',$loginInfo['site_id'])
                     ->where('trade_id',$loginInfo['trade_id'])
                     ->order('show_order asc')
                     ->select();
        if($list->isEmpty()) return [];
        foreach($list as $k=>$v){
            $fun = '_'.$v['form_type'];
            $options = json_decode(html_out($v['form_config']),true);
            $valid = \FormBuild::transformationValid($v['valid']);
            if(!empty($valid)) $options['datatype']=$valid;
            $v['_html'] = \FormBuild::$fun("config[{$v['id']}][value]",$v['value'],$options);
        }
        return $list;
    }

}