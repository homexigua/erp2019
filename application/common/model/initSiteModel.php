<?php
/**
 * 初始化站点
 */
namespace app\common\model;

use app\common\model\mall\ClassModel;
use think\Db;

class initSiteModel {

    private $siteId;
    private $tradeId;
    private $siteName;

    public function __construct($siteId,$tradeId) {
        $this->siteId = $siteId;
        $this->tradeId = $tradeId;
        $this->siteName = SiteModel::where('id',$this->siteId)->value('site_name');
    }

    /**
     * 安装站点
     */
    public function install(){
        Db::startTrans();
        try{
            $this->initDept();
            $this->initConfig();
            Db::commit();
        }catch (\Exception $e){
            Db::rollback();
        }
    }

    /**
     * 卸载站点
     */
    public function uninstall(){
        Db::startTrans();
        try{
            //删除对应配置项
            ConfigModel::where('site_id',$this->siteId)
                ->where('trade_id',$this->tradeId)
                ->delete();
            Db::commit();
        }catch (\Exception $e){
            Db::rollback();
        }
    }

    /**
     * 初始化部门
     * @param $siteName
     * @return DeptModel
     */
    public function initDept(){
        /*创建店铺顶级部门*/
        $deptModel = new DeptModel();
        $deptData =[
            'pid'=>0,
            'name'=>$this->siteName,
            'site_id'=>$this->siteId,
            'trade_id'=>$this->tradeId
        ];
        return $deptModel->addData($deptData);
    }

    /**
     * 初始化商品类目
     */
    public function initGoodsClass(){
        /*创建商品类目顶级*/
        ClassModel::create([
            'pid'=>0,
            'name'=>$this->siteName,
            'site_id'=>$this->siteId,
            'trade_id'=>$this->tradeId
        ]);
    }

    /**
     * 初始化配置信息
     */
    public function initConfig(){
        $configModel = new ConfigModel();
        $initData=[
            [
                'group_id'=>1,
                'name'=>'theme',
                'title'=>'主题风格',
                'tips_name'=>'站点模板风格',
                'value'=>'default',
                'form_type'=>'select',
                'form_config'=>'{
                    &quot;list-data&quot;:&quot;D:Tpl::getThemeList:site&quot;,
                    &quot;empty-option&quot;:&quot;|请选择主题风格&quot;
                }',
                'valid'=>'require',
                'show_order'=>3,
                'is_system'=>1,
                'site_id'=>$this->siteId,
                'trade_id'=>$this->tradeId,
            ]
        ];
        $dataList = [];
        foreach($initData as $v){
            $info = $configModel::where('name',$v['name'])
                        ->where('site_id',$this->siteId)
                        ->where('trade_id',$this->tradeId)
                        ->find();
            if(empty($info)) $dataList[] = $v;
        }
        if(!empty($dataList)){
            $configModel->saveAll($dataList);
        }
    }
}