<?php
namespace app\common\model;

class AdminAccessModel extends BaseModel
{
    protected $name = 'admin_access';
    protected $pk='id';
    //开启自动写入时间戳
    protected $autoWriteTimestamp = false;

}