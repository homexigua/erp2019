<?php
namespace app\common\model\customer;

use app\common\model\BaseModel;

class LogTplModel extends BaseModel
{
    protected $name = 'customer_log_tpl';
    protected $pk = 'id';
    //开启自动写入时间戳
    protected $autoWriteTimestamp = true;

}