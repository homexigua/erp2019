<?php
namespace app\common\model\customer;

use app\common\model\BaseModel;

class CustomerModel extends BaseModel
{
    protected $name = 'customer';
    protected $pk='id';
    protected $type=[
        "create_time"=>"timestamp:Y-m-d H:i",
        "update_time"=>"timestamp:Y-m-d H:i",
        "delete_time"=>"timestamp:Y-m-d H:i",
        "last_contact_time"=>"timestamp:Y-m-d H:i"
    ];
    //开启自动写入时间戳
    protected $autoWriteTimestamp = true;

}