<?php
namespace app\common\model\customer;

use app\common\model\BaseModel;

class LabelModel extends BaseModel
{
    protected $name = 'customer_label';
    protected $pk = 'id';
    //开启自动写入时间戳
    protected $autoWriteTimestamp = false;

}