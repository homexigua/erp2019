<?php
namespace app\common\model\customer;

use app\common\model\BaseModel;

class SourceModel extends BaseModel
{
    protected $name = 'customer_source';
    protected $pk='id';
    //开启自动写入时间戳
    protected $autoWriteTimestamp = true;


    public function getList($map=[]){
        $loginInfo = $this->getLoginInfo();
        $list = $this->where($map)->where('site_id',$loginInfo['site_id'])->order('show_order asc')->select();
        return $list;
    }
}