<?php
namespace app\common\model;

class PatchModel extends BaseModel
{
    protected $name = 'patch';
    protected $pk='id';
    //开启自动写入时间戳
    protected $autoWriteTimestamp = false;

    //碎片类型转换
    public function getPatchTypeTextAttr($value,$data){
        $arr = ['1'=>'单图碎片','2'=>'多图碎片','3'=>'代码碎片','4'=>'富文本碎片','5'=>'项目列表碎片'];
        return $arr[$data['patch_type']];
    }

    /**
     * 获取内容
     * @param $map
     * @return array|\PDOStatement|string|\think\Model|void|null
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getInfo($map) {
        $info = $this->append(['patch_type_text'])->where($map)->find();
        if(in_array($info['patch_type'],[2,5])){
            $info['patch_content'] = json_decode($info['patch_content'],true);
        }
        if(in_array($info['patch_type'],[3,4])){
            $info['patch_content'] = $this->formatContent($info['patch_content']);
        }
        return $info;
    }

    /**
     * 添加碎片
     * @param $data
     */
    public function addData($data){
        try{
            switch ($data['patch_type']){
                case '1':
                    if(empty($data['patch_content1'])) exception('请上传图片');
                    $data['patch_content'] = $data['patch_content1'];
                    break;
                case '2':
                    if(empty($data['patch_content2'])) exception('请上传图集');
                    $data['patch_content'] = $this->formatMultiImageDesc($data['patch_content2']);
                    break;
                case '3':
                    if(empty($data['patch_content3'])) exception('代码不能为空');
                    $data['patch_content'] = $data['patch_content3'];
                    break;
                case '4':
                    if(empty($data['patch_content4'])) exception('富文本内容不能为空');
                    $data['patch_content'] = $data['patch_content4'];
                    break;
                case '5':
                    if(empty($data['items'])) exception('项目列表不能为空！');
                    $items = [];
                    foreach($data['items'] as $v){
                        if(empty($v['key']) && empty($v['value']) && empty($v['remark'])){
                            continue;
                        }
                        $items[]=$v;
                    }
                    $data['patch_content'] = json_encode($items,JSON_UNESCAPED_UNICODE);
                    break;
            }
            $info = $this->where('patch_sn',$data['patch_sn'])->find();
            if($info) exception('碎片编号已存在！',40001);
            self::create($data);
        }catch (\Exception $e){
            exception($e->getMessage());
        }
        return true;
    }

    /**
     * 修改数据
     * @param $data
     */
    public function editData($data){
        $info = $this->where('id',$data['id'])->find();
        try{
            switch ($info['patch_type']){
                case '1':
                    if(empty($data['patch_content1'])) exception('请上传图片');
                    $data['patch_content'] = $data['patch_content1'];
                    break;
                case '2':
                    if(empty($data['patch_content2'])) exception('请上传图集');
                    $data['patch_content'] = $this->formatMultiImageDesc($data['patch_content2']);
                    break;
                case '3':
                    if(empty($data['patch_content3'])) exception('代码不能为空');
                    $data['patch_content'] = $data['patch_content3'];
                    break;
                case '4':
                    if(empty($data['patch_content4'])) exception('富文本内容不能为空');
                    $data['patch_content'] = $data['patch_content4'];
                    break;
                case '5':
                    if(empty($data['items'])) exception('项目列表不能为空！');
                    $items = [];
                    foreach($data['items'] as $v){
                        if(empty($v['key']) && empty($v['value']) && empty($v['remark'])){
                            continue;
                        }
                        $items[]=$v;
                    }
                    $data['patch_content'] = json_encode($items,JSON_UNESCAPED_UNICODE);
                    break;
            }
            self::update($data);
        }catch (\Exception $e){
            exception($e->getMessage());
        }
        return true;
    }
}