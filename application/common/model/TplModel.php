<?php
namespace app\common\model;

use org\util\FileUtil;
use think\facade\Cache;
use think\facade\Env;

class TplModel
{
    /**
     * 获取风格目录
     * @return array
     */
    public function getThemeList($theme){
        $arr = [];
        $path = Env::get('ROOT_PATH').'public/theme_'.$theme;
        $dir = FileUtil::getDir($path);
        foreach($dir as $k=>$v){
            if($v['type']=='file') continue; //排除文件
            if(strstr($v['filename'],'_mobile')!==false) continue; //排除手机风格
            $arr[]=['id'=>$v['filename'],'name'=>iconv('GB2312','UTF-8',$v['filename'])];
        }
        if(empty($arr)) $arr[]=['id'=>'default','name'=>'default'];
        return $arr;
    }
    /**
     * 获取模板文件名
     * @param $key
     * @return array
     */
    public function getTplList($type=null){
        $arr=[];
        $webconfig = Cache::get('webconfig');
        $theme = $webconfig['theme'];
        $path = Env::get('ROOT_PATH').'public/themes/'.$theme;
        $dir = FileUtil::getDir($path,['php']);
        foreach($dir as $k=>$v){
            if(!empty($type)){
                if(strstr($v['filename'],$type)!==false) $arr[]=['id'=>$v['filename'],'name'=>$v['filename']];
            }else{
                $arr[]=['id'=>$v['filename'],'name'=>$v['filename']];
            }
        }
        if(empty($arr)) $arr[]=['id'=>$type.'default','name'=>$type.'default'];
        return $arr;
    }

    /**
     * 获取模板文件列表
     * @return array
     */
    public function getList($device='pc'){
        $webconfig = Cache::get('webconfig');
        $theme = $webconfig['theme'];
        if($device=='pc'){
            $path = Env::get('ROOT_PATH').'public/themes/'.$theme;
        }else{
            $path = Env::get('ROOT_PATH').'public/themes/'.$theme.'_mobile';
        }
        $dir = FileUtil::getDir($path,['php']);
        return $dir;
    }
}