<?php
namespace app\common\model;

class ConfigGroupModel extends BaseModel
{
    protected $name = 'config_group';
    protected $pk='id';
    //开启自动写入时间戳
    protected $autoWriteTimestamp = false;

    public function addData($data) {
        $data['is_platform'] = isset($data['is_platform']) ? $data['is_platform'] : 0;
        $data['is_trade'] = isset($data['is_trade']) ? $data['is_trade'] : 0;
        $data['is_site'] = isset($data['is_site']) ? $data['is_site'] : 0;
        return self::create($data,true);
    }

    public function editData($data) {
        $data['is_platform'] = isset($data['is_platform']) ? $data['is_platform'] : 0;
        $data['is_trade'] = isset($data['is_trade']) ? $data['is_trade'] : 0;
        $data['is_site'] = isset($data['is_site']) ? $data['is_site'] : 0;
        return self::update($data,[],true);
    }

    /**
     * 删除分组
     * @param $id
     */
    public function delData($id){
        $info = ConfigModel::where('group_id',$id)->find();
        if($info) exception('分组下存在对应配置，不允许删除!',40001);
        $this->where('id',$id)->delete();
    }
}