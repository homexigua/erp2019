<?php

namespace app\common\model;

use app\common\model\mall\ClassModel;
use org\util\HelpUtil;
use think\facade\Cache;
use think\facade\Session;
use think\facade\Validate;
use think\model\concern\SoftDelete;

class TradeModel extends BaseModel {
    protected $name = 'trade';
    protected $pk = 'id';
    //开启自动写入时间戳
    protected $autoWriteTimestamp = true;
    //视图查询
    public function searchViewAttr($query, $value, $data) {

    }

    public function getInfo($map)
    {
        $domainModel = new DomainModel();
        $info = $this->where($map)->find();
        $domains = $domainModel->where('ref_id',$info['id'])->where('ref_type',2)->column('domain');
        $domains = empty($domains) ? '' : implode(PHP_EOL,$domains);
        if($info){
            $adminInfo = AdminModel::where('id',$info['admin_id'])->find();
            $info['username']=$adminInfo['username'];
            $info['nickname']=$adminInfo['nickname'];
            $info['domain'] = $domains;
        }
        return $info;
    }


    /**
     * 添加行业站点
     * @param $data
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function addData($data) {
        $domainModel = new DomainModel();
        self::startTrans();
        try {
            $info = self::create($data,true);
            //添加域名
            $domains = format_textarea($data['domain']);
            $domainModel->addDomain($domains,$info['id'],2);
            /*创建行业管理员*/
            $adminModel = new AdminModel();
            $adminData =[
                'username'=>$data['username'],
                'password'=>$data['password'],
                'nickname'=>$data['nickname'],
                'is_init'=>1
            ];
            $adminInfo = $adminModel->addData($adminData);
            /*创建行业顶级部门*/
            $deptModel = new DeptModel();
            $deptData =[
                'pid'=>0,
                'name'=>$data['trade_name'],
                'site_id'=>0,
                'trade_id'=>$info['id']
            ];
            $deptInfo = $deptModel->addData($deptData);
            /*创建商品类目顶级*/
            ClassModel::create([
                'pid'=>0,
                'name'=>$data['trade_name'],
                'site_id'=>0,
                'trade_id'=>$info['id']
            ]);
            /*创建文章类目顶级*/
            /*反写admin_id,dept_id,trade_id到权限表*/
            AdminAccessModel::create([
                'admin_id'=>$adminInfo['id'],
                'site_id'=>0,
                'dept_id'=>$deptInfo['id'],
                'group_id'=>1,//暂时给与超管权限
                'trade_id'=>$info['id']
            ]);
            /*反写admin_id,dept_id到行业表*/
            $this->save([
                'dept_id'=>$deptInfo['id'],
                'admin_id'=>$adminInfo['id'],
            ],['id'=>$info['id']]);
            /*创建店铺站点配置*/
            $configModel = new ConfigModel();
            $configModel->saveAll(
                [
                    [
                        'group_id'=>1,'name'=>'web_title','title'=>'站点名称','tips_name'=>'网站前端页面显示名称','value'=>$data['site_name'],'form_type'=>'text',
                        'form_config'=>'{
                                            &quot;placeholder&quot;:&quot;请输入站点名称&quot;
                                        }',
                        'valid'=>'require','show_order'=>1,'is_system'=>1,'site_id'=>$info['id'],'trade_id'=>$data['trade_id']
                    ],
                    [
                        'group_id'=>1,'name'=>'keywords','title'=>'关键词','value'=>$data['site_name'],'form_type'=>'text',
                        'valid'=>'require','show_order'=>2,'is_system'=>1,'site_id'=>$info['id'],'trade_id'=>$data['trade_id']
                    ],
                    [
                        'group_id'=>1,'name'=>'desc','title'=>'描述','value'=>$data['site_name'],'form_type'=>'textarea',
                        'valid'=>'require','show_order'=>3,'is_system'=>1,'site_id'=>$info['id'],'trade_id'=>$data['trade_id']
                    ],
                    [
                        'group_id'=>1,'name'=>'desc','title'=>'LOGO','tips_name'=>'前端调用网站LOGO','form_type'=>'image',
                        'valid'=>'require','show_order'=>4,'is_system'=>1,'site_id'=>$info['id'],'trade_id'=>$data['trade_id']
                    ],
                    [
                        'group_id'=>1,'name'=>'theme','title'=>'主题风格','tips_name'=>'前端页面模板','value'=>'default','form_type'=>'select',
                        'form_config'=>'{
                                            &quot;list-data&quot;:&quot;D:Tpl::getThemeList:site&quot;,
                                            &quot;empty-option&quot;:&quot;|请选择主题风格&quot;
                                        }',
                        'valid'=>'require','show_order'=>5,'is_system'=>1,'site_id'=>$info['id'],'trade_id'=>$data['trade_id']
                    ]
                ]
            );
            self::commit();
        } catch (\Exception $e) {
            self::rollback();
            exception($e->getMessage());
        }
        return true;
    }

    /**
     * 修改行业站点
     * @param $data
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function editData($data) {
        $domainModel = new DomainModel();
        self::startTrans();
        try {
            $info = $this->getInfo(['id'=>$data['id']]);
            //添加域名
            $domains = format_textarea($data['domain']);
            $domainModel->editDomain($domains,$info['id'],2);
            self::update($data,true);
            self::commit();
        } catch (\Exception $e) {
            self::rollback();
            exception($e->getMessage());
        }
        return true;
    }
}