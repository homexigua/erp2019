<?php
namespace app\common\model\cms;

use app\common\model\BaseModel;

class TablePictureModel extends BaseModel
{
    protected $name = 'cms_table_picture';
    protected $pk='id';
    //开启自动写入时间戳
    protected $autoWriteTimestamp = true;
    //设置类型转换
    protected $type = [

    ];
    //读取器
    protected function getAtlasAttr($value)
    {
        return json_decode($value,JSON_UNESCAPED_UNICODE);
    }
    protected function getContentAttr($value)
    {
        return $this->formatContent($value);
    }
    //修改器
    protected function setAtlasAttr($value)
    {
        return $this->formatMultiImageDesc($value);
    }

    /**
     * 获取内容
     */
    public function getInfo($map) {
        $info = $this->where($map)->find();
        $info['special_ids'] = D('SpecialItem')->getSpecialIdsByContentId($info['moudle_id'],$info['id']);
        return $info;
    }
    /**
     * 添加内容
     * @param $data
     * @return TableArticleModel|void
     * @throws \Exception
     */
    public function addData($data){
        try{
            if(!request()->has('atlas')){
                $data['atlas']='';
            }  
            $info = self::create($data);
        }catch (\Exception $e){
            exception($e->getMessage(),40001);
        }
        return $info;
    }
    /**
     * 编辑内容
     * @param $data
     * @return TableArticleModel|void
     * @throws \Exception
     */
    public function editData($data){
        try{
            if(!request()->has('atlas')){
                $data['atlas']='';
            }
            $info = self::update($data);
        }catch (\Exception $e){
            exception($e->getMessage(),40001);
        }
        return $info;
    }
    /**
     * 删除内容
     * @param $data
     * @return TableArticleModel|void
     * @throws \Exception
     */
    public function delData($id){
        try{
            $this->where('id','in',$id)->delete();
        } catch (\Exception $e){
            exception($e->getMessage(),40001);
        }
        return true;
    }
}