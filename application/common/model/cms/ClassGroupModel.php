<?php
namespace app\common\model\cms;

use app\common\model\BaseModel;

class ClassGroupModel extends BaseModel
{
    protected $name = 'cms_class_group';
    protected $pk='id';
    //开启自动写入时间戳
    protected $autoWriteTimestamp = false;
}