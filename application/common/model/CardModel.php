<?php
namespace app\common\model;

class CardModel extends BaseModel
{
    protected $name = 'card';
    protected $pk='id';
    //开启自动写入时间戳
    protected $autoWriteTimestamp = false;

    /**
     * 设置日期区间
     * @param $value
     * @param $data
     */
    public function setBeginEndAttr($value,$data){
        if($data['data_type']==1){ //固定日期区间

        }
        if($data['data_type']==2){ //领取后计算
            
        }
    }
    /**
     * 券状态查询
     */
    protected function searchQueryAttr(){

    }
    /**
     * 领券
     */
    public function receive($memberId,$cardId){
        $cardInfo = self::where('id',$cardId)->find();
        if(($cardInfo['send_number']+1)>$cardInfo['card_stock']) exception('优惠券已领完');
        if($cardInfo['get_limit']>0){
            $count = CardItemModel::where('member_id',$memberId)->where('card_id',$cardId)->count();
            if(($count+1)>$cardInfo['get_limit']) exception('一个人只能领取'.$cardInfo['get_limit'].'张卡券');
        }
        if($cardInfo['date_type']==1){
            if(time()>$cardInfo['end_time']) exception('该优惠券已失效');
            $startTime = $cardInfo['begin_time'];
            $finishTime = $cardInfo['end_time'];
        }else{ //领券后
            $startTime = time()+$cardInfo['begin_day']*24*60*60;
            $finishTime = time()+($cardInfo['begin_day']*24*60*60)+($cardInfo['fixed_day']*24*60*60);
        }
        $data = [
            'member_id'=>$memberId,
            'send_time'=>time(),
            'start_time'=>$startTime,
            'finish_time'=>$finishTime,
        ];
        $cardInfo = CardItemModel::where('card_id',$cardId)->where('member_id',0)->find();
        if(empty($cardInfo)) exception('优惠券已领完');
        $data['id'] = $cardInfo['id'];
        CardItemModel::update($data);
    }

    /**
     * 可用优惠券
     * @param $memberId
     * @param $goodsAccount
     */
    public function uselist($memberId,$goodsAccount){
        //limit_cost
    }

    /**
     * 核销卷
     * @param $memberId
     * @param $cardSn
     */
    public function useCard($memberId,$cardSn){

    }
}