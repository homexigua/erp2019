<?php
namespace app\common\model;

class AreaModel extends BaseModel
{
    protected $name = 'area';
    protected $pk='id';
    //开启自动写入时间戳
    protected $autoWriteTimestamp = false;

    /**
     * 删除
     * @param $id
     */
    public function delData($id) {
        if($this->where('pid',$id)->find()){
            exception('存在下级区域，不允许删除！');
        }
        $this->where('id',$id)->delete();
    }

}