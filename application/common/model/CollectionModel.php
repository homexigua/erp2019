<?php
namespace app\common\model;

class CollectionModel extends BaseModel
{
    protected $name = 'collection';
    protected $pk='id';
    //开启自动写入时间戳
    protected $autoWriteTimestamp = true;

    public function getTypelist(){
        $arr = [
            1=>'\app\common\cms\TableArticleModel',
            2=>'\app\common\cms\TablePictureModel',
            3=>'\app\common\cms\TableDownloadModel',
            998=>'\app\common\SiteModel',
            999=>'\app\common\mall\GoodsModel'
        ];
        return $arr;
    }

    /**
     * 添加收藏
     * @param $memberId
     * @param $type
     * @param $collectionId
     * @return CollectionModel
     */
    public function collection($memberId,$type,$collectionId){
        $modellist = $this->getTypelist();
        $model = new $modellist[$type];
        $field = $type==998 ? 'id as site_id,trade_id' : 'site_id,trade_id';
        $info = $model->where('id',$collectionId)->field($field)->find();
        return self::create([
            'model'=>$modellist[$type],
            'member_id'=>$memberId,
            'collection_id'=>$collectionId,
            'collection_time'=>time(),
            'site_id'=>$info['site_id'],
            'trade_id'=>$info['trade_id'],
        ]);
    }
}