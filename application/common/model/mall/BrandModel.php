<?php
namespace app\common\model\mall;

use app\common\model\BaseModel;

class BrandModel extends BaseModel
{
    protected $name = 'mall_goods_brand';
    protected $pk='id';
    //开启自动写入时间戳
    protected $autoWriteTimestamp = false;
    //设置类型转换
    protected $type = [
        'status'=>'integer',
        'is_remmend'=>'integer',
    ];

}