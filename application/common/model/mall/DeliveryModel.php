<?php
namespace app\common\model\mall;

use app\common\model\BaseModel;
use app\common\model\SiteModel;

class DeliveryModel extends BaseModel
{
    protected $name = 'mall_delivery';
    protected $pk = 'id';
    //开启自动写入时间戳
    protected $autoWriteTimestamp = false;
    //设置类型转换
    protected $type = [
        'promotion_info' => 'array'
    ];
    //读取器
    //运费类型
    protected function getFreightTypeTextAttr($value, $data)
    {
        $maxWeight = $data['max_weight']==0?'':'最大重量'.$data['max_weight'].'Kg；';
        $limitFreight = $data['limit_freight']==0?'':'最高运费'.$data['limit_freight'].'元；';
        $arr = [
            0 => '免运费',
            1 => '统一收取'.$data['freight'].'元运费',
            2 => '首重'.$data['first_heavy'].'Kg，运费'.$data['first_freight'].'元；续重每'.$data['continue_heavy'].'Kg增加运费'.$data['first_freight'].'元；'.$maxWeight.$limitFreight,
        ];
        return $arr[$data['freight_type']];
    }

    public function getPromotionTypeTextAttr($value, $data)
    {
        $promotionInfo = json_decode($data['promotion_info'], true);
        $arr = [
            0 => '不启用促销',
            1 => '在本商城购买满' . $promotionInfo[1]['goods_count'],
            2 => '在本商城购买满' . $promotionInfo[2]['goods_count'] . ',减免' . $promotionInfo[2]['discount_freight'] . '运费',
            3 => '在本商城购买满' . $promotionInfo[3]['goods_count'] . ',并且运费总额少于' . $promotionInfo[3]['total_freight'] . '时免运费',
        ];
        return $arr[$data['promotion_type']];
    }


    //修改器

    /**
     * 格式化列表
     * @param $list
     * @return mixed
     */
    public function _formatList($list){
        $list->append(['freight_type_text','promotion_type_text']);
        return $list;
    }

    /**
     * 计算运费
     */
    public function calcFreight($id, $totalWeight) {
        try {
            $deliveryInfo = $this->cache($id,30)->where('id', $id)->find(); //缓存30秒
            switch ($deliveryInfo['freight_type']) {
                case 0: //免运费
                    $freight = 0;
                    break;
                case 1: //固定运费
                    $freight = $deliveryInfo['freight'];
                    break;
                case 2: //按重量计算运费
                    /*
                    first_heavy:首重
                    continue_heavy:续重
                    first_freight:首重运费
                    continue_freight:续重运费
                    limit_freight:最高运费
                    */
                    if ($totalWeight > $deliveryInfo['first_heavy'] && $totalWeight <= $deliveryInfo['max_weight']) { //大于首重
                        $continueWeight = round(($totalWeight - $deliveryInfo['first_heavy']),2);
                        $continueNum = ceil($continueWeight / round($deliveryInfo['continue_heavy'],2)); //执行续重次数
                        $freight = $deliveryInfo['first_freight'] + ($continueNum * $deliveryInfo['continue_freight']);
                        $freight = $freight > $deliveryInfo['limit_freight'] ? $deliveryInfo['limit_freight'] : $freight;
                    } elseif($totalWeight > $deliveryInfo['max_weight']) {
                        exception('已经超出最大重量！');
                    } else { //小于首重取首重运费
                        $freight = $deliveryInfo['first_freight'];
                    }
                    break;
            }
        } catch (\Exception $e) {
            exception($e->getMessage(), 40001);
        }
        return $freight;
    }

    /**
     * 计算优惠后运费
     * @param $siteId
     * @param $goodsAmount
     * @param $calcFreight
     * @return int|mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function calcFreightDiscount($siteId,$goodsAmount,$calcFreight){
        $info = SiteModel::where('id',$siteId)
                         ->field('freight_discount_type,freight_goods_money,freight_limit_money,freight_discount_money')
            ->find();
        $freightGoodsMoney = $info->getData('freight_goods_money');
        $freightLimitMoney = $info->getData('freight_limit_money');
        $freight=$calcFreight;
        switch ($info->getData('freight_discount_type')){ //运费促销 0无优惠 1满免 2满减 3满并不超过
            case 0: //无优惠
                break;
            case 1: //满免
                if($goodsAmount>$freightGoodsMoney) $freight=0;
                break;
            case 2: //满减 freight_discount_money 减免金额 0为全免 大于0为减免金额
                if($goodsAmount>$freightGoodsMoney) $freight=$freightGoodsMoney>0 ? $calcFreight-$freightGoodsMoney : 0;
                break;
            case 3: //满并不超过 freight_limit_money 运费上限 freight_discount_money 减免金额 0为全免 大于0为减免金额
                if($goodsAmount>$freightGoodsMoney && $calcFreight<$freightLimitMoney){
                    $freight=$freightGoodsMoney>0 ? $calcFreight-$freightGoodsMoney : 0;
                }
                break;
            default:
                $freight=$calcFreight;
        }
        return $freight;
    }
}