<?php
namespace app\common\model\mall;

use app\common\model\BaseModel;
use think\Db;

class GoodsSkuModel extends BaseModel
{
    protected $name = 'mall_goods_sku';
    protected $pk='id';

    protected $autoWriteTimestamp = true;
    //设置类型转换
    protected $type = [];

    public function getInfo($map){
        $info = $this->where($map)->find();
        if(empty($info)) {
            return [];
        }else{
            $info = $info->toArray();
        }
        $goodsInfo = GoodsModel::where('id',$info['goods_id'])->field('goods_content',true)->find()->toArray();
        return array_merge($goodsInfo,$info);
    }

    /**
     * 获取sku列表
     * @param $map
     */
    public function getskulist($map){
        $list = $this->withSearch(['view'],[
            'view'=>['mall_goods_sku','mall_goods']])
            ->where($map)
            ->select();
        if(!empty($list)) $list=$list->toArray();
        //$list = $this->formatsku($list);
        return $list;
    }

    /*
 * 搜索器
 */
    public function searchViewAttr($query, $value, $data){
        //sku主表
        if(in_array('mall_goods_sku',$value)){
            $fields = $this->getTableFields('mall_goods_sku');
            $query->view('mall_goods_sku',$fields);
        }
        //商品表
        if(in_array('mall_goods',$value)){
            $fields = $this->getTableFields('mall_goods');
            $fields = array_merge(array_diff($fields, ['id','goods_content']));
            $query->view('mall_goods',$fields,'mall_goods.id=mall_goods_sku.goods_id');
        }
    }
    /*格式化列表*/
    public function _formatList($list){
        // "group_unit": [{"id": "套","name": "套=最小单位","number":1}, {"id": "包","name": "1包=12套","number":12}, {"id": "箱","name": "1箱=48套","number":48}],
        if($list){
            foreach ($list as $k=>$v){
                $list[$k]['group_unit']=$this->formatUnit($v['unit'],$v['is_unit_group'],$v['unit_group']);//select选择的单位
                $list[$k]['goods_price']=$v['sale_price'];
                $list[$k]['goods_unit']=$v['unit'];
                $list[$k]['unit_number']=1;
                $list[$k]['sku_id']=$v['id'];
            }
        }
        return $list;
    }



    /**
     * 格式化单位
     * @param $unit 基本单位
     * @param $isUnitGroup 是否单位组
     * @param $unitGroup 个:1,箱:10,包:100,批:1000
     * @param $currentUnit 当前单位
     * @return array [{"id": "套","name": "套=最小单位","number":1}, {"id": "包","name": "1包=12套","number":12}, {"id": "箱","name": "1箱=48套","number":48}],
     */
    public function formatUnit($unit,$isUnitGroup,$unitGroup){
        if($isUnitGroup==1){ //单位组解析
            $unitGroup = explode(',',$unitGroup);
            $arr=[];
            foreach($unitGroup as $k=>$v){
                $item = explode(':',$v);
                if($k==0){ //第一条
                    $arr[]=[
                        "id"=>$item[0],
                        "name"=>$item[0].'=最小单位',
                        "number"=>$item[1],
                    ];
                }else{
                    $arr[]=[
                        "id"=>$item[0],
                        "name"=>'1'.$item[0].'='.$item[1].$unit,
                        "number"=>$item[1],
                    ];
                }
            }
            return $arr;
        }else{
            return [["id"=>$unit,"name"=>"{$unit}=最小单位","number"=>1]];
        }
    }

}