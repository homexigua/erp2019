<?php
namespace app\common\model\mall;

use app\common\model\BaseModel;
use app\common\model\SiteModel;
use org\util\TreeUtil;
use think\facade\Cache;

class ClassModel extends BaseModel
{
    protected $name = 'mall_class';
    protected $pk = 'id';
    //开启自动写入时间戳
    protected $autoWriteTimestamp = true;
    /**
     * 获取缓存数据
     * @return mixed
     * @throws \Exception
     */
    public function getList(){
        if(!Cache::has($this->getCacheKey())){
            $this->updateCache();
        }
        $list = Cache::get($this->getCacheKey());
        return $list;
    }
    /**
     * 获取树型列表
     */
    public function getTreeList(){
        $list = $this->getList();
        $list = array_values($list);
        $treeList = TreeUtil::channelLevel($list,0);
        return $treeList;
    }
    /**
     * 缓存数据
     * @return bool
     * @throws \Exception
     */
    public function updateCache(){
        try{
            $loginInfo = $this->getLoginInfo();
            $list = $this->where('site_id',$loginInfo['site_id'])->where('trade_id',$loginInfo['trade_id'])->order('show_order asc')->select();
            $list = $list->isEmpty()?[]:$list->toArray();
            $list = TreeUtil::tree($list,'name'); //转换树型
            $cache=[];
            foreach($list as $k=>$v){
                //取所有下级，包含当前级
                $ids=$v['id']; //当前ID
                $hasChild = 0; //默认无下级
                $v['next_level']=[]; //下级分类
                $child = TreeUtil::channelList($list,$v['id']); //当前ID的所有下级
                $dataView = array_column($child,'id');
                if(!empty($dataView)){
                    $ids.=','.implode(',',$dataView);
                    $hasChild = 1;
                    foreach($child as $kk=>$vv){
                        if($vv['pid']==$v['id']) $v['next_level'][]=$vv;
                    }
                }
                $v['data_view'] = $ids;
                $v['has_child'] = $hasChild;
                //取所有上级
                $parents = array_reverse(TreeUtil::parentChannel($list, $v['id']));
                $path = array_column($parents,'id');
                $fullName = array_column($parents,'name');
                $v['path'] = implode(',',$path);
                $v['full_name'] = implode(',',$fullName);
                $cache[$v['id']] = $v;
            }
            Cache::set($this->getCacheKey(),$cache);
        }catch (\Exception $e){
            exception($e->getMessage());
        }
        return true;
    }
    /**
     * 删除店铺分类管理
     * @param $id
     */
    public function delData($id) {
        if($this->where('pid',$id)->find()) exception('存在下级分类，不允许删除！');
        if(SiteModel::where('id',$id)->find()) exception('分类存在店铺不允许删除');
        $this->where('id',$id)->delete();
    }

    /**
     * 获取行业商品类目列表
     */
    public function getCategory($id){
        $loginInfo = $this->getLoginInfo();

        $list = Cache::get('mall_class_0_'.$loginInfo['trade_id']);
        $category=[];
        foreach($list as $v){
            if($v['pid']==0){
                unset($v);
            }else{
                $v['selected'] = '';
                $v['disabled'] = '';
                if($v['is_content_add']==0) $v['disabled'] = 'disabled';  //禁用不允许添加内容的栏目
                //选中当前选择
                if($id>0){
                    if($id==$v['id']) {
                        $v['selected'] = ' selected';
                    }
                }
                $category[]=$v;
            }
        }
        return $category;
    }
    /**
     * 获取店内商品类目列表
     */
    public function getClass($id){
        $loginInfo = $this->getLoginInfo();
        $this->updateCache();
        $list = Cache::get('mall_class_'.$loginInfo['site_id'].'_'.$loginInfo['trade_id']);
        $category=[];
        foreach($list as $v){
            if($v['pid']==0){
                unset($v);
            }else{
                $v['selected'] = '';
                $v['disabled'] = '';
                if($v['is_content_add']==0) $v['disabled'] = 'disabled';  //禁用不允许添加内容的栏目
                //选中当前选择
                if($id>0){
                    if($id==$v['id']) {
                        $v['selected'] = ' selected';
                    }
                }
                $category[]=$v;
            }
        }
        return $category;
    }

    /**
     * 缓存标识
     * @return string
     */
    public function getCacheKey(){
        $loginInfo = $this->getLoginInfo();
        return 'mall_class_'.$loginInfo['site_id'].'_'.$loginInfo['trade_id'];
    }
}