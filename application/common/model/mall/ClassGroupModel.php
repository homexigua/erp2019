<?php
namespace app\common\model\mall;

use app\common\model\BaseModel;

class ClassGroupModel extends BaseModel
{
    protected $name = 'mall_class_group';
    protected $pk='id';
    //开启自动写入时间戳
    protected $autoWriteTimestamp = false;

}