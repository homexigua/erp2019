<?php
namespace app\common\model\mall;

use app\common\model\BaseModel;

class GoodsPriceModel extends BaseModel
{
    protected $name = 'mall_goods_price';
    protected $pk='id';
    //开启自动写入时间戳
    protected $autoWriteTimestamp = false;
}