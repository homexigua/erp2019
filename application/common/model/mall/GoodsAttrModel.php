<?php
namespace app\common\model\mall;

use app\common\model\BaseModel;

class GoodsAttrModel extends BaseModel
{
    protected $name = 'mall_goods_attr';
    protected $pk='id';
    //开启自动写入时间戳
    protected $autoWriteTimestamp = false;

    /*获取规格组*/
    public function getsku($map)
    {
        try{
            $attr = $this->where($map)->order('show_order asc')->select();
            if($attr){
                foreach ($attr as $k=>$v){
                    $attr[$k]['attr_value'] = explode(',',$v['attr_value']);
                }
            }
        }catch (\Exception $e){
            exception($e->getMessage(),40001);
        }
        return $attr;
    }

    public function _formatList($list){
        foreach($list as $k=>$v){
            $list[$k]['attr_value']=explode(',',$v['attr_value']);
        }
        return $list;
    }

}