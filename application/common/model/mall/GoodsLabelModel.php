<?php
namespace app\common\model\mall;

use app\common\model\BaseModel;

class GoodsLabelModel extends BaseModel
{
    protected $name = 'mall_goods_label';
    protected $pk='id';
    //开启自动写入时间戳
    protected $autoWriteTimestamp = false;
    //设置类型转换
    protected $type = [
        'is_base'=>'integer',
        'unit_num'=>'array',
    ];
    //读取器


    //修改器

    public function initData($siteId){
        if($this->where('site_id',$siteId)->find()) return;
        $datalist=[
            ['color'=>'#42D392','name'=>'主推','site_id'=>$siteId],
            ['color'=>'#92632B','name'=>'促销','site_id'=>$siteId],
            ['color'=>'#D52450','name'=>'滞销','site_id'=>$siteId],
            ['color'=>'#7F66DC','name'=>'残次品','site_id'=>$siteId],
            ['color'=>'#46D0F7','name'=>'','site_id'=>$siteId],
            ['color'=>'#FC6985','name'=>'','site_id'=>$siteId],
            ['color'=>'#FE8336','name'=>'','site_id'=>$siteId],
            ['color'=>'#86668D','name'=>'','site_id'=>$siteId],
        ];
        return self::saveAll($datalist);
    }

}