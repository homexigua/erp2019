<?php
namespace app\common\model\mall;

use app\common\model\BaseModel;

class UnitModel extends BaseModel
{
    protected $name = 'mall_goods_unit';
    protected $pk = 'id';
    //开启自动写入时间戳
    protected $autoWriteTimestamp = false;
    //设置类型转换
    protected $type = [
        'is_base'=>'integer',
        'unit_num'=>'array',
    ];
    //读取器


    //修改器


    /**
     * 添加数据
     * @param $data
     * @return BaseModel
     */
    public function addData($data){
        try{
            if($data['is_base']==0){
                if (empty($data['unit'])) exception('请填写计价单位', 40001);
                $info = $this->where(['unit'=>$data['unit'],'site_id'=>$data['site_id']])->find();
                if(!empty($info)) exception('计价单位已存在',40001);
                $data['alias']=$data['alias']==''?$data['unit']:$data['alias'];
            }else{
                if (count($data['unit_num'])<2) exception('请设置计价单位组', 40001);
                $unit = array_column($data['unit_num'],'unit');
                $data['unit']=implode(',',$unit);
                $info = $this->where(['unit'=>$data['unit'],'site_id'=>$data['site_id']])->find();
                if($info['unit_num']==$data['unit_num']) exception('计价单位已存在',40001);
                $data['alias']=$data['alias']==''?$data['unit']:$data['alias'];
            }
            $info = self::create($data);
        }catch (\Exception $e){
            exception($e->getMessage(),40001);
        }
        return $info;
    }
    /**
     * 编辑数据
     * @param $data
     * @return BaseModel
     */
    public function editData($data){
        try{
            if($data['is_base']==0){
                if (empty($data['unit'])) exception('请填写计价单位', 40001);
                $info = $this->where(['unit'=>$data['unit'],'shop_id'=>$data['shop_id']])->find();
                if(!empty($info)) exception('计价单位已存在',40001);
                $data['alias']=$data['alias']==''?$data['unit']:$data['alias'];
            }else{
                if (count($data['unit_num'])<2) exception('请设置计价单位组', 40001);
                $unit = array_column($data['unit_num'],'unit');
                $data['unit']=implode(',',$unit);
                $info = $this->where(['unit'=>$data['unit'],'shop_id'=>$data['shop_id']])->find();
                if($info['unit_num']==$data['unit_num']) exception('计价单位已存在',40001);
                $data['alias']=$data['alias']==''?$data['unit']:$data['alias'];
            }
            $info = self::update($data);
        }catch (\Exception $e){
            exception($e->getMessage(),40001);
        }
        return $info;
    }
    /**
     * 删除记录
     * @param $data
     * @return bool
     * @throws \Exception
     */
    public function delData($id)
    {
        try{
            $info = $this->where($id)->find();
            $this->where('id',$id)->delete();
        }catch (\Exception $e){
            exception($e->getMessage(),40001);
        }
        return true;
    }

    /*验证单位组*/
    public function validate($data){

    }
    /*获取单位列表*/
    public function getUnit($base=0){
        $list =$this->where(['is_base'=>$base,'status'=>1])->select();
        $list =$this->_formatList($list);
        return $list;
    }

    /**
     * 格式化列表
     * @param $list
     * @return mixed
     */
    public function _formatList($list){
        foreach ($list as $k=>$v){
            $text=[];
            $showUnit=[];
            if(!empty($v['unit_num'])){
                foreach ($v['unit_num'] as $kk=>$vv){
                    if($kk==0){
                        $text[]='最小单位：'.$v['unit_num'][0]['unit'];
                    }else{
                        $text[]='1'.$vv['unit'].'='.$vv['num'].$v['unit_num'][0]['unit'];
                    }
                    $showUnit[]=$vv['unit'].':'.$vv['num'];
                }
            }
            $list[$k]['unit_num_text']=implode('，',$text);
            $list[$k]['unit_show_name']=implode(',',$showUnit);

        }
        return $list;
    }
}