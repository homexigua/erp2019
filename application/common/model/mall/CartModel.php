<?php

namespace app\common\model\mall;

use app\common\model\BaseModel;
use app\common\model\DomainModel;
use app\common\model\SiteModel;

class CartModel extends BaseModel {
    protected $name = 'mall_cart';
    protected $pk = 'id';

    protected $autoWriteTimestamp = true;


    /*加入购物车*/
    public function cartAdd($memberId, $skuSn, $goodsNumber, $type = 'plus') {
        $cartId = null;
        try {
            if ((int)$memberId == 0) exception('买家不明确！', 40001);
            if (empty($skuSn)) exception('商品不存在！', 40001);
            if ($goodsNumber<0) exception('商品数量不正确！', 40001);
            $skuModel = new GoodsSkuModel();
            $skuInfo = $skuModel->getInfo(['sku_sn' => $skuSn]);
            if (empty($skuInfo)) exception('商品不存在！');
            $cartInfo = $this->where('member_id', $memberId)->where('sku_sn', $skuSn)->find();
            $oldNum = empty($cartInfo) ? 0 : $cartInfo['goods_number'];
            $number = $type == 'plus' ? $goodsNumber + $oldNum : $goodsNumber;
            $this->checkSku($skuInfo, $number);
            if ($cartInfo) {
                if ($type == 'plus') { //增加购买数量
                    $this->where('id', $cartInfo['id'])->setInc('goods_number', $goodsNumber);
                } else { //更新购买数量
                    $this->where('id', $cartInfo['id'])->setField('goods_number', $goodsNumber);
                }
                $cartId = $cartInfo['id'];
            } else { //插入购物车
                $cartData = ['member_id' => $memberId, 'sku_sn' => $skuSn, 'goods_number' => $goodsNumber, 'site_id' => $skuInfo['site_id'], 'trade_id' => $skuInfo['trade_id']];
                $info = self::create($cartData);
                $cartId = $info['id'];
            }
        } catch (\Exception $e) {
            exception($e->getMessage(), 40001);
        }
        return $cartId;
    }

    /**
     * 获取购物车列表（单店获取）
     * @param $siteId
     * @param $memberId
     * @param $scene 场景 cart 购物车列表 buy订单确认
     * @param $card_ids 购物车ids 如果是购物车列表设置选中，如果是订单确认则过滤掉没选择的购物商品
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * 场景scene  获取购物车列表
     */
    public function getListBySite($siteId,$memberId,$scene='cart',$card_ids) {
        $map[] = ['member_id', '=', $memberId];
        $map[] = ['site_id', '=', $siteId];
        if($scene=='buy'){
            $map[] = ['id','in',$card_ids];
        }else{
            $card_ids= $card_ids==''?'':explode(',',$card_ids);
        }
        $list = $this->where($map)->column('*','sku_sn');
        $deliveryModel = new DeliveryModel();
        if(empty($list)) return [];
        $cartList = [];
        //取出对应商品
        $goodsSkuModel = new GoodsSkuModel();
        $skuSNs = array_column($list,'sku_sn');
        $map=[];
        $map[] = ['sku_sn', 'in', $skuSNs];
        $skuList = $goodsSkuModel->getskulist($map);
        //店铺信息
        $siteInfo = SiteModel::where('id', $siteId)->field('id as site_id,site_name,site_alias,site_logo')->find();
        /*店铺域名*/
        $siteInfo['domain'] = DomainModel::where(['ref_id'=>$siteInfo['site_id'],'ref_type'=>3])->value('domain');
        if(empty($siteInfo)) exception('店铺不存在');
        $cartList['site_info'] = $siteInfo->toArray();
        //格式化购物车列表
        $totalWeight = 0; //商品总重量
        $totalGoodsAmount = 0; //商品总价
        $totalGoodsNumber = 0; //商品数量
        $totalPoints = 0; //可以获得的积分
        $totalFreight = 0; //总运费

        foreach ($skuList as $k => $v) {
            $v = array_merge($v, $list[$v['sku_sn']]); //合并购物车数据
            $v['status'] = 1; //设置状态生效
            $salePrice = $v['sale_price'];
            //todo 暂时使用商品销售价，计算阶梯价，后续活动未考虑 activity_type
            //活动商品处理,过期重置状态，否则更新销售价
            if($v['activity_type']>0){
                $v['status'] = 0;
                $v['fail_info'] = '商品已过期';
                continue;
            }
            //商品下架
            if ($v['is_sale'] == 0) {
                $v['status'] = 0;
                $v['fail_info'] = '商品已下架';
                continue;
            }
            //违规
            if ($v['is_against'] == 0) {
                $v['status'] = 0;
                $v['fail_info'] = '商品已违规下架';
                continue;
            }
            //库存不足
            if ($v['goods_number'] > $v['sale_stock']) {
                $v['status'] = 0;
                $v['fail_info'] = '库存不足';
                continue;
            }
            $v['sub_total'] = $v['goods_number'] * $salePrice;//金额小计
            $v['sub_points'] = $v['goods_number'] * $v['give_points'];//积分小计
            $totalWeight += $v['goods_weight']; //计算总重量
            $totalFreight += $deliveryModel->calcFreight($v['delivery_id'],$v['sub_total']);
            $totalPoints += $v['sub_points']; //计算总积分
            //计算商品总价
            $totalGoodsAmount += $v['sub_total']; //计算商品总价
            $totalGoodsNumber += $v['goods_number']; //计算商品总数量
            /*如果是购物车列表，还原选中状态*/
            if($scene=='cart' && !empty($card_ids)){
                if(in_array($v['id'],$card_ids) && $v['status']!=0){
                    $v['check']='checked';
                }else{
                    $v['check']='';
                }
            }
            $cartList['item'][] = $v;
        }
        //计算优惠后的运费
        $totalFreight=$deliveryModel->calcFreightDiscount($siteId,$totalGoodsAmount,$totalFreight);
        //购物车小计
        $cartList['total']=[
            'total_weight'=>$totalWeight,
            'total_goods_amount'=>$totalGoodsAmount,
            'total_goods_number'=>$totalGoodsNumber,
            'total_points'=>$totalPoints,
            'total_freight'=>$totalFreight,
        ];
        return $cartList;
    }

    /**
     * 删除购物车
     * @param $map
     */
    public function cartdel($map){
        try{
            $this->where($map)->delete();
            return true;
        }catch (\Exception $e){
            exception($e->getMessage(), 40001);
        }
    }


    /**
     * 检测加入购物车条件
     * @param $skuInfo
     * @param $number
     * @throws \Exception
     */
    public function checkSku($skuInfo, $number) {
        if ($skuInfo['is_sale'] == 0) exception('该商品已下架！', 40001);
        if ($number <= 0) exception('购买数量不能为空！', 40001);
        if ($number > $skuInfo['sale_stock']) exception("库存不足！", 40001);
        if ($number < $skuInfo['min_buy_num']) exception("低于最小起订量！", 40001);
        if ($number % $skuInfo['step'] != 0) exception("购买量必须为{$skuInfo['step']}的倍数", 40001);
        //todo 后续活动检测是否满足活动条件
        if($skuInfo['activity_type']>0){

        }
    }

}