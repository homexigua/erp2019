<?php

namespace app\common\model\mall;

use app\common\model\BaseModel;
use app\common\model\SiteModel;

class EvaluateModel extends BaseModel {
    protected $name = 'mall_evaluate';
    protected $pk = 'id';

    protected $autoWriteTimestamp = false;

    public function addEvaluate($orderId,$evaluateData){
        $orderInfo = SoOrderModel::where('id',$orderId)->find();
        //$orderInfo['member_id']
        //$orderInfo['site_id']
        //$orderInfo['trade_id']
        return self::create([
            'member_id'=>$orderInfo['member_id'],
            'site_id'=>$orderInfo['site_id'],
            'trade_id'=>$orderInfo['trade_id'],
            'content'=>$evaluateData['content']
        ]);
    }
}