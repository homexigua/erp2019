<?php
namespace app\common\model\mall;

use app\common\model\BaseModel;
use org\util\HelpUtil;

class GoodsModel extends BaseModel
{
    protected $name = 'mall_goods';
    protected $pk='id';

    protected $autoWriteTimestamp = true;
    //设置类型转换
    protected $type = [
        'is_base' => 'integer',
        'sku_attr' => 'array',
        'base_attr' => 'array'
    ];
    //读取器
    protected function getGoodsContentAttr($value)
    {
        return $this->formatContent($value);
    }

    protected function getAlbumAttr($value)
    {
        if (empty($value)) return [];
        return explode(',', $value);
    }

    //修改器
    protected function setAlbumAttr($value)
    {
        return implode(',', $value);
    }
    /*
     * 搜索器
     */
    public function searchViewAttr($query, $value, $data){
        //sku主表
        if(in_array('mall_goods_sku',$value)){
            $query->view('mall_goods_sku',"*,id as sku_id");
        }
        //商品表
        if(in_array('mall_goods',$value)){
            $fields = $this->getTableFields('mall_goods');
            $fields = array_merge(array_diff($fields, ['id','goods_content']));
            $query->view('mall_goods',$fields,'mall_goods.id=mall_goods_sku.goods_id');
        }
    }
    /**
     * 获取商品sku
     * @param $goodsInfo
     * @return array
     */
    public function getSkuSelect($goodsInfo)
    {
        $skuArr = [];
        if ($goodsInfo['is_goods_attr'] == 1) {
            $attrNames = array_values($goodsInfo['sku_attr']);
            foreach ($goodsInfo['sku_item'] as $k => $v) {
                $sku = explode(',', $v['sku_name']); //5g/每袋,8袋,5g/每瓶
                foreach ($sku as $key => $value) {
                    $skuArr[$key]['attr_name'] = $attrNames[$key];
                    $skuArr[$key]['attr_list'][$k] = $value;
                    $skuArr[$key]['attr_list'] = array_unique($skuArr[$key]['attr_list']);
                }
            }
        }
        return $skuArr;
    }

    /**
     * 格式化列表
     * @param $list 必须传入数据集
     * @return mixed
     */
    public function _formatList($list)
    {
        $list->hidden(['goods_content'])->toArray();
        $goodsIds = $list->column('id');
        $skuList = GoodsSkuModel::where('goods_id', 'in', $goodsIds)->order('sale_price asc')->select()->toArray();
        $skuList = arr_group($skuList, 'goods_id'); //按商品ID分组
        foreach ($list as $k => $v) {
            $v['sku_first'] = reset($skuList[$v['id']]); //取skuList第一条
            $v['sku_item'] = $skuList[$v['id']];
            $v['sku_end'] = end($skuList[$v['id']]);
            $v['sku_select'] = $this->getSkuSelect($v);
            $list[$k] = $v;
        }
        return $list;
    }

    /**
     * 获取商品详情
     * @param $map
     * @return array|null|\PDOStatement|string|\think\Model
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getInfo($map)
    {
        $info = $this->where($map)->find();
        $skuList = GoodsSkuModel::where('goods_id', $info['id'])->order('sale_price asc')->select()->toArray();
        /*循环获取价格*/
        foreach ($skuList as $k=>$v){
            $priceGroup = GoodsPriceModel::where('sku_sn',$v['sku_sn'])->select();
            $skuList[$k]['goods_price_group']=$priceGroup;
            if($info['goods_price_type']==0){
                $skuList[$k]['goods_price']=$priceGroup[0]['goods_price'];
                $skuList[$k]['sale_price']=$priceGroup[0]['goods_price'];
            }else{
                foreach ($priceGroup as $kk=>$vv){
                    if($vv['buy_number']==1){
                        $skuList[$k]['goods_price']=$vv['goods_price'];
                    }
                    $priceItem[$v['sku_sn']][$kk]=$vv['buy_number'].':'.$vv['goods_price'];
                }
                $skuList[$k]['sale_price']=implode(',',$priceItem[$v['sku_sn']]);
            }

        }
        $info['sku_first'] = reset($skuList);
        $info['sku_item'] = $skuList;
        $info['sku_names'] = implode(',',array_column($skuList,'sku_name'));
        $info['sku_end'] = end($skuList);
        $info['sku_select'] = $this->getSkuSelect($info);
        return $info;
    }

    /**
     * 添加数据
     * @param $data
     * @return BaseModel
     */
    public function addData($data)
    {
        $goodsSkuModel = new GoodsSkuModel();
        $goodsPriceModel = new GoodsPriceModel();
        self::startTrans();
        try {
            if (empty($data['sku'])) exception('请设置商品价格', 40001);
            $data['thumb'] = $data['album'][0]; //取第一张做为缩略图
            $skuData = $data['sku'];
            /*商品价格*/
            foreach ($skuData as $k=>$v){
                if($data['goods_price_type']==0){
                    if(!is_numeric($v['sale_price'])) exception('商品价格格式错误！',40001);
                    $skuData[$k]['sale_price_group']=[
                        [
                            'buy_number'=>1,
                            'goods_price'=>$v['sale_price']
                        ]
                    ];
                }else{
                    $price = explode(',',$v['sale_price']);
                    foreach ($price as $kk=>$value){
                        $item = explode(':',$value);
                        if(!is_numeric($item[0]) || !is_numeric($item[1])){
                            exception('商品价格格式错误！',40001);
                        };
                        $priceItem[$kk]=[
                            'buy_number'=>$item[0],
                            'goods_price'=>$item[1],
                        ];
                    }
                    $skuData[$k]['sale_price']=$priceItem[0]['goods_price'];
                    $skuData[$k]['sale_price_group']=$priceItem;
                }
            }
            $goodsPrice = arr_min_max($skuData,'sale_price');
            $data['min_price'] = $goodsPrice['min'];
            $data['max_price'] = $goodsPrice['max'];
            if ($data['is_goods_attr'] == 0) { //未开启规格
                $data['goods_attr_ids'] = '';
            }else{
                if (empty($data['goods_attr_ids'])) exception('请选择商品SKU属性！', 40001);
            }
            $goodsInfo = self::create($data);
            //验证明细
            foreach ($skuData as $v) {
                if (empty($v['sku_name']) && $data['is_goods_attr']==1) exception('请选择对应SKU', 40001);
                if (empty($v['market_price'])) exception('市场价不能为空', 40001);
                if (!is_numeric($v['market_price'])) exception('市场价格式不正确', 40001);
                if (empty($v['sale_price'])) exception('销售价不能为空', 40001);
                if (!is_numeric($v['sale_price'])) exception('销售价格式不正确', 40001);
                $v['goods_id'] = $goodsInfo['id'];
                $v['sku_sn'] = HelpUtil::keyGen();
                $skuList[] = $v;
                foreach ($v['sale_price_group'] as $kk=>$vv){
                    $vv['goods_id']=$goodsInfo['id'];
                    $vv['sku_sn']=$v['sku_sn'];
                    $priceList[]=$vv;
                }
            }
            $goodsSkuModel->SaveAll($skuList);
            $goodsPriceModel->SaveAll($priceList);
            self::commit();
        } catch (\Exception $e) {
            self::rollback();
            exception($e->getMessage(), 40001);
        }
    }

    /**
     * 编辑数据
     * @param $data
     * @return BaseModel
     */
    public function editData($data)
    {
        $goodsSkuModel = new GoodsSkuModel();
        $goodsPriceModel = new GoodsPriceModel();
        self::startTrans();
        try {

            if (empty($data['sku'])) exception('请设置商品价格', 40001);
            $data['thumb'] = $data['album'][0]; //取第一张做为缩略图
            //最低最高价
            $skuData = $data['sku'];
            /*商品价格*/
            foreach ($skuData as $k=>$v){
                if($data['goods_price_type']==0){
                    if(!is_numeric($v['sale_price'])) exception('商品价格格式错误！',40001);
                    $skuData[$k]['sale_price_group']=[
                        [
                            'buy_number'=>1,
                            'goods_price'=>$v['sale_price']
                        ]
                    ];
                }else{
                    $price = explode(',',$v['sale_price']);
                    foreach ($price as $kk=>$value){
                        $item = explode(':',$value);
                        if(!is_numeric($item[0]) || !is_numeric($item[1])){
                            exception('商品价格格式错误！',40001);
                        };
                        $priceItem[$v['sku_name']][]=[
                            'buy_number'=>$item[0],
                            'goods_price'=>$item[1],
                        ];
                    }
                    $skuData[$k]['sale_price']=$priceItem[$v['sku_name']][0]['goods_price'];
                    $skuData[$k]['sale_price_group']=$priceItem[$v['sku_name']];
                }
            }
            $goodsPrice = arr_min_max($skuData,'sale_price');
            $data['min_price'] = $goodsPrice['min'];
            $data['max_price'] = $goodsPrice['max'];
            $goodsInfo = self::update($data);
            $skuList = [];
            //取出原有SKU信息
            $oldSkuList = GoodsSkuModel::field('id,sku_sn')->where('goods_id', $goodsInfo['id'])->column('sku_sn','id');
            $oldSkuIds = array_keys($oldSkuList);
            //验证明细
            $newSkuIds = [];
            foreach ($skuData as $v) {
                if ($v['id'] > 0){
                    $newSkuIds[] = $v['id']; //当前编辑传入的sku_id
                    $v['sku_sn'] = empty($oldSkuList[$v['id']]) ? HelpUtil::keyGen():$oldSkuList[$v['id']];
                }else{
                    unset($v['id']);
                    $v['sku_sn'] = HelpUtil::keyGen();
                }
                //if (empty($v['sku_name'])) exception('请选择对应SKU', 40001);
                if (empty($v['market_price'])) exception('市场价不能为空', 40001);
                if (!is_numeric($v['market_price'])) exception('市场价格式不正确', 40001);
                if (empty($v['sale_price'])) exception('销售价不能为空', 40001);
                if (!is_numeric($v['sale_price'])) exception('销售价格式不正确', 40001);
                $v['goods_id'] = $goodsInfo['id'];
                $skuList[] = $v;
                foreach ($v['sale_price_group'] as $kk=>$vv){
                    $vv['goods_id']=$goodsInfo['id'];
                    $vv['sku_sn']=$v['sku_sn'];
                    $priceList[]=$vv;
                }
            }
            //需要删除的sku
            $diffSkuIds = array_diff($oldSkuIds, $newSkuIds);
            $goodsSkuModel->where('id', 'in', $diffSkuIds)->delete();
            $goodsPriceModel->where('goods_id',$goodsInfo['id'])->delete();
            $goodsSkuModel->SaveAll($skuList);
            $goodsPriceModel->SaveAll($priceList);
            self::commit();
        } catch (\Exception $e) {
            self::rollback();
            exception($e->getMessage(), 40001);
        }
    }

    /**
     * 删除记录
     * @param $data
     * @return bool
     * @throws \Exception
     */
    public function delData($id)
    {
        try {
            $skuList = GoodsSkuModel::where('goods_id',$id)->select();
            $stock=0;
            $order=0;
            foreach ($skuList as $k=>$v){
                /*库存合计*/
                $stock = $stock+$v['total_stock'];
                /*订单合计*/
            }
            if($stock!=0) exception('商品库存未清理，禁止删除！',40001);
            if($order>0) exception('商品已有交易订单，禁止删除！',40001);
            GoodsPriceModel::where('goods_id',$id)->delete();
            GoodsSkuModel::where('goods_id',$id)->delete();
            $this->where('id',$id)->delete();
        } catch (\Exception $e) {
            exception($e->getMessage(), 40001);
        }
        return true;
    }
    

}