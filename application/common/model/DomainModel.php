<?php
namespace app\common\model;

class DomainModel extends BaseModel
{
    protected $name = 'domain';
    protected $pk='domain';
    //开启自动写入时间戳
    protected $autoWriteTimestamp = false;

    /**
     * 添加域名
     * @param $domains
     * @param $refId
     * @param $refType
     * @throws \Exception
     */
    public function addDomain($domains,$refId,$refType){
        $existDomains = DomainModel::where('domain','in',$domains)->value('domain');
        if(!empty($existDomains)) exception(implode(',',$domains).'域名已存在！');
        $domainList=[];
        foreach($domains as $domain){
            $domainList[]=[
                'domain'=>$domain,
                'ref_id'=>$refId,
                'ref_type'=>$refType
            ];
        }
        $this->saveAll($domainList);
    }

    /**
     * 修改域名
     * @param $domains
     * @param $refId
     * @param $refType
     */
    public function editDomain($domains,$refId,$refType){
        //删除原有域名
        $this->where('ref_id',$refId)->where('ref_type',$refType)->delete();
        $existDomains = DomainModel::where('domain','in',$domains)->value('domain');
        if(!empty($existDomains)) exception(implode(',',$domains).'域名已存在！');
        $domainList=[];
        foreach($domains as $domain){
            $domainList[]=[
                'domain'=>$domain,
                'ref_id'=>$refId,
                'ref_type'=>$refType
            ];
        }
        $this->saveAll($domainList,false);
    }

    /**
     * 获取域名对应ID
     * @param $domain
     * @param $refType
     */
    static public function getRefId($domain,$refType){
        $refId = self::where('domain',$domain)->where('ref_type',$refType)->value('ref_id');
        return $refId;
    }
}