<?php
/**
 * 静态资源包
 */

namespace app\common\model;

class StaticModel
{
    /**
     * 加载代码编辑器资源
     * @return string
     */
    public function loadCodeEditor(){
        $res = <<<EOT
<link rel="stylesheet" href="/static/plugins/codemirror/codemirror.css">
<link rel="stylesheet" href="/static/plugins/codemirror/theme/monokai.css">
<!-- 引入CodeMirror核心文件 -->
<script src="/static/plugins/codemirror/codemirror.js"></script>
<!-- 引入CodeMirror语言包 -->
<script src="/static/plugins/codemirror/mode/clike/clike.js"></script>
<script src="/static/plugins/codemirror/mode/javascript/javascript.js"></script>
<script src="/static/plugins/codemirror/mode/xml/xml.js"></script>
<script src="/static/plugins/codemirror/mode/css/css.js"></script>
<script src="/static/plugins/codemirror/mode/htmlmixed/htmlmixed.js"></script>
<script src="/static/plugins/codemirror/mode/php/php.js"></script>
<!--支持代码折叠-->
<link rel="stylesheet" href="/static/plugins/codemirror/addon/fold/foldgutter.css"/>
<script src="/static/plugins/codemirror/addon/fold/foldcode.js"></script>
<script src="/static/plugins/codemirror/addon/fold/foldgutter.js"></script>
<script src="/static/plugins/codemirror/addon/fold/brace-fold.js"></script>
<script src="/static/plugins/codemirror/addon/fold/xml-fold.js"></script>
<script src="/static/plugins/codemirror/addon/fold/comment-fold.js"></script>
<!-- 引入当前行插件 -->
<script src="/static/plugins/codemirror/addon/selection/active-line.js"></script>
<!-- 引入编辑插件 -->
<script src="/static/plugins/codemirror/addon/edit/matchbrackets.js"></script>
<script src="/static/plugins/codemirror/addon/edit/closetag.js"></script>
EOT;
        return $res;
    }
}