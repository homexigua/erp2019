<?php

namespace app\common\model;

use org\util\TreeUtil;
use think\facade\Cache;

class DictClassModel extends BaseModel {
    protected $name = 'dict_class';
    protected $pk = 'id';
    //开启自动写入时间戳
    protected $autoWriteTimestamp = false;
    /**
     * 获取缓存数据
     * @return mixed
     * @throws \Exception
     */
    public function getList(){
        $cacheKey = 'dict_class_'.$this->getAccountType();
        if(!Cache::has($cacheKey)){
            $this->updateCache();
        }
        $list = Cache::get($cacheKey);
        return $list;
    }
    /**
     * 获取树型列表
     */
    public function getTreeList(){
        $list = $this->getList();
        $list = array_values($list);
        $treeList = TreeUtil::channelLevel($list,0);
        return $treeList;
    }

    /**
     * 获取部门信息
     * @param $map
     * @return array|\PDOStatement|string|\think\Model|void|null
     */
    public function getInfo($map){
        $info = $this->where($map)->find();
        $list = $this->getList();
        $deptInfo = $list[$info['id']];
        return $deptInfo;
    }
    /**
     * 缓存数据
     * @return bool
     * @throws \Exception
     */
    public function updateCache(){
        try{
            $accountType = $this->getAccountType();
            $cacheKey='dict_class_'.$accountType;
            $list = $this->where($accountType,1)->order('show_order asc')->select();
            $list = $list->isEmpty()?[]:$list->toArray();
            $list = TreeUtil::tree($list,'name'); //转换树型
            $cache=[];
            foreach($list as $k=>$v){
                //取所有下级，包含当前级
                $ids=$v['id']; //当前ID
                $hasChild = 0; //默认无下级
                $v['next_level']=[]; //下级分类
                $child = TreeUtil::channelList($list,$v['id']); //当前ID的所有下级
                $dataView = array_column($child,'id');
                if(!empty($dataView)){
                    $ids.=','.implode(',',$dataView);
                    $hasChild = 1;
                    foreach($child as $kk=>$vv){
                        if($vv['pid']==$v['id']) $v['next_level'][]=$vv;
                    }
                }
                $v['data_view'] = $ids;
                $v['has_child'] = $hasChild;
                //取所有上级
                $parents = array_reverse(TreeUtil::parentChannel($list, $v['id']));
                $path = array_column($parents,'id');
                $fullName = array_column($parents,'name');
                $v['path'] = implode(',',$path);
                $v['full_name'] = implode(',',$fullName);
                $cache[$v['id']] = $v;
            }
            Cache::set($cacheKey,$cache);
        }catch (\Exception $e){
            exception($e->getMessage());
        }
        return true;
    }

    /**
     * 添加字典分类
     * @param $data
     */
    public function addData($data){
        if($this->where('code',$data['code'])->find()){
            exception('分类编码已存在！');
        }
        $data['is_platform'] = isset($data['is_platform']) ? $data['is_platform'] : 0;
        $data['is_trade'] = isset($data['is_trade']) ? $data['is_trade'] : 0;
        $data['is_site'] = isset($data['is_site']) ? $data['is_site'] : 0;
        self::create($data);
    }
    /**
     * 编辑字典分类
     * @param $data
     */
    public function editData($data){
        $info = $this->where('id',$data['id'])->find();
        if($data['code']!=$info['code']){
            if($this->where('code',$data['code'])->find()){
                exception('分类编码已存在！');
            }
            //更新字典对应编码
            DictModel::where('class_code',$info['code'])->setField('class_code',$data['code']);
        }
        $data['is_platform'] = isset($data['is_platform']) ? $data['is_platform'] : 0;
        $data['is_trade'] = isset($data['is_trade']) ? $data['is_trade'] : 0;
        $data['is_site'] = isset($data['is_site']) ? $data['is_site'] : 0;
        self::update($data,[],true);
    }
    /**
     * 删除分类
     * @param $id
     */
    public function delData($id) {
        $list = $this->getList();
        $info = $list[$id];
        if($info['has_child']==1) exception('存在下级字典，不允许删除！');
        $dict = DictModel::where('class_code', $info['code'])->find();
        if ($dict) exception('存在对应字典，不允许删除！', 40001);
        $this->where('id', $id)->delete();
    }

}