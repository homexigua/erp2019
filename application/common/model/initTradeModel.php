<?php
/**
 * 初始化站点
 */
namespace app\common\model;

use think\Db;

class initTradeModel {

    private $siteId;
    private $tradeId;

    public function __construct($siteId,$tradeId) {
        $this->siteId = $siteId;
        $this->tradeId = $tradeId;
    }

    /**
     * 安装行业
     */
    public function install(){
        Db::startTrans();
        try{
            $this->initConfig();
            Db::commit();
        }catch (\Exception $e){
            Db::rollback();
        }
    }

    /**
     * 卸载行业
     */
    public function uninstall(){
        Db::startTrans();
        try{
            //删除对应配置项
            ConfigModel::where('site_id',$this->siteId)
                       ->where('trade_id',$this->tradeId)
                       ->delete();
            Db::commit();
        }catch (\Exception $e){
            Db::rollback();
        }
    }

    /**
     * 初始化配置信息
     */
    public function initConfig(){
        $configModel = new ConfigModel();
        $initData=[
            [
                'group_id'=>1,
                'name'=>'theme',
                'title'=>'主题风格',
                'tips_name'=>'行业模板风格',
                'value'=>'default',
                'form_type'=>'select',
                'form_config'=>'{
                    &quot;list-data&quot;:&quot;D:Tpl::getThemeList:trade&quot;,
                    &quot;empty-option&quot;:&quot;|请选择主题风格&quot;
                }',
                'valid'=>'require',
                'show_order'=>3,
                'is_system'=>1,
                'site_id'=>$this->siteId,
                'trade_id'=>$this->tradeId,
            ]
        ];
        $dataList = [];
        foreach($initData as $v){
            $info = $configModel::where('name',$v['name'])
                                ->where('site_id',$this->siteId)
                                ->where('trade_id',$this->tradeId)
                                ->find();
            if(empty($info)) $dataList[] = $v;
        }
        if(!empty($dataList)){
            $configModel->saveAll($dataList);
        }
    }
}