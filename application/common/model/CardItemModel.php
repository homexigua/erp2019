<?php
namespace app\common\model;


class CardItemModel extends BaseModel
{
    protected $name = 'card_item';
    protected $pk='id';
    //开启自动写入时间戳
    protected $autoWriteTimestamp = false;


   //存start_time时将日期补个0：00：00再转换时间戳
    public function setStartTimeAttr($value){
        return strtotime($value.' 0:00:00');
    }

    //读start_time时将时间戳转为年-月-日格式
    public function getStartTimeAttr($value){
        return date('Y-m-d',$value);
    }

   //存end_time时将日期补个23：59：59再转换时间戳
    public function setEndTimeAttr($value){
        return strtotime($value.' 23：59：59');
    }

    //读end_time时将时间戳转为年-月-日格式
    public function getEndTimeAttr($value){
        return date('Y-m-d',$value);
    }

}




