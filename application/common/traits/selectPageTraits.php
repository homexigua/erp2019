<?php

namespace app\common\traits;

use think\Db;

trait selectPageTraits
{

    /**
     * selectpage封装
     * @return mixed
     */
    public function selectpage()
    {
        $type = $this->request->param('type','page');
        if($type=='tree'){ //树型下拉
            $list = $this->model->getList();
            $searchKey = $this->request->param('searchKey'); //查询初始化数据 id
            $searchValue = $this->request->param('searchValue'); //查询初始化数据 7,5
            if(!empty($searchKey) && !empty($searchValue)){
                foreach($list as $k=>$v){
                    if(!in_array($v[$searchKey],explode(',',$searchValue))){
                        unset($list[$k]);
                    }
                }
            }
            $list = array_values($list);
            $data['list'] = $list;
            return $data;
        }
        $params = $this->formatSelectPageParams();
        $total = $this->model->where($params['map'])->count();
        $list = [];
        if($total>0){
            $fields=$this->request->param('fields',true);
            $list = $this->model->field($fields)->where($params['map'])->order($params['order'])->page($params['pageNumber'], $params['pageSize'])->select();
            if (method_exists($this->model, '_formatList')) {
                $list = $this->model->_formatList($list);
            }
        }
        $data['totalRow'] = $total;
        $data['list'] = $list;
        $data['sql'] = $this->model->getLastSql();
        return $data;
    }

    /**
     * 格式化selectpage参数
     */
    public function formatSelectPageParams(){
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        $pageNumber = $this->request->param('pageNumber/d'); //当前页数
        $pageSize = $this->request->param('pageSize/d',10); //每页记录数
        //处理排序
        $orderby = (array)$this->request->request("orderBy/a");
        $order = [];
        foreach ($orderby as $k => $v) {
            $order[$v[0]] = $v[1];
        }
        //处理默认选中
        //搜索
        $searchKey = $this->request->param('searchKey'); //查询初始化数据
        $searchValue = $this->request->param('searchValue'); //查询初始化数据
        if (!empty($searchKey) && !empty($searchValue)) {
            $map[] = [$searchKey, 'in', $searchValue];
        }
        //字段搜索
        $word = (array)$this->request->param("q_word/a"); //搜索关键词,客户端输入以空格分开,这里接收为数组
        $andor = $this->request->param("andOr", "and", "strtoupper"); //搜索条件
        $logic = $andor == 'AND' ? '&' : '|';
        $searchField = $this->request->request("searchField"); //搜索字段逗号隔开
        if(!empty($searchField) && !empty($word)){
            foreach ($word as $k => $v) {
                if(!empty($v)){
                    $map[] = [str_replace(',', $logic, $searchField), "like", "%{$v}%"];
                }
            }
        }
        //自定义搜索
        $query = $this->request->param('query');
        if(!empty($query)){
            $map[]=['','EXP',Db::raw($query)];
        }
        $custom = $this->request->param('custom');
        if(!empty($custom)){
            $map[]=['','EXP',Db::raw($custom)];
        }
        $data = [
            'pageNumber'=>$pageNumber,
            'pageSize'=>$pageSize,
            'order'=>$order,
            'map'=>$map
        ];
        return $data;
    }


}