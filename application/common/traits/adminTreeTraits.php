<?php

namespace app\common\traits;

trait adminTreeTraits
{
    /**
     * 列表方法
     * @return array
     */
    public function index()
    {
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            $list = $this->getlist();
            return $list;
        }
        return $this->fetch();
    }

    /**
     * 获取数据列表
     */
    public function getlist(){
        $list=$this->model->getList();
        $list = array_values($list);
        return $list;
    }
    /**
     * 添加
     * @return mixed
     */
    public function add()
    {
        if ($this->request->isAjax()) {
            try {
                $data = $this->request->param();
                //验证规格
                $validRule = str_replace(["\\model\\", "Model"], ["\\validate\\", 'Validate'], get_class($this->model));
                $this->validate($data, $validRule . '.add');
                $data = $this->appendUserData($data); //追加用户数据
                $info = $this->model->addData($data);
                $this->model->updateCache();
            } catch (\Exception $e) {
                $this->error($e->getMessage());
            }
            $this->success('操作成功！', null, $info);
        }
        return $this->fetch();
    }

    /**
     * 编辑
     * @return mixed
     */
    public function edit()
    {
        $id = $this->request->param('id/d');
        $map[]=['id','=',$id];
        $info = $this->model->getInfo($map);
        if (empty($info)) $this->error('未找到数据！');
        if ($this->request->isAjax()) {
            try {
                $data = $this->request->param();
                //验证规格
                $validRule = str_replace(["\\model\\", "Model"], ["\\validate\\", 'Validate'], get_class($this->model));
                $result = $this->validate($data, $validRule . '.edit');
                if(true !== $result) exception($result);
                $data = $this->appendUserData($data); //追加用户数据
                $info = $this->model->editData($data);
                $this->model->updateCache();
            } catch (\Exception $e) {
                $this->error($e->getMessage());
            }
            $this->success('操作成功！', null, $info);
        }
        $this->assign('info', $info);
        return $this->fetch();
    }

    /**
     * 删除
     * @return mixed
     */
    public function del()
    {
        try {
            $id = $this->request->param('id');
            if(empty($id)) exception('请选择删除数据！');
            $this->model->delData($id);
            $this->model->updateCache();
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
        $this->success('操作成功！');
    }

    /**
     * 批量更新数据
     */
    public function multi()
    {
        //ids: 1，3，5
        //params: status:1,is_boss=0
        if ($this->request->has('ids') && $this->request->has('params')) {
            try {
                $ids = $this->request->param('ids');
                $params = $this->request->param('params');
                $this->model->multi($ids, $params, $this->multiFields);
                $this->model->updateCache();
            } catch (\Exception $e) {
                $this->error($e->getMessage());
            }
            $this->success('操作成功！');
        }
        $this->error('参数错误！');
    }
    /**
     * editable修改内容
     */
    public function editable(){
        header("Content-type: text/html; charset=utf-8");
        try{
            $id = $this->request->param('pk');
            $name = $this->request->param('name');
            $value = $this->request->param('value');
            $this->model->where('id',$id)->setField($name,$value);
            $this->model->updateCache();
        }catch (\Exception $e){
            return response($e->getMessage(),40001);
        }
        return response('操作成功！');
    }
    /**
     * 追加用户数据
     */
    private function appendUserData($data)
    {
        if (!empty($this->appendUserFields)) {
            foreach ($this->appendUserFields as $v) {
                if($v=='admin_id'){
                    $data['admin_id'] = $this->loginInfo['id'];
                }else{
                    $data[$v] = $this->loginInfo[$v];
                }
            }
        }
        return $data;
    }

}