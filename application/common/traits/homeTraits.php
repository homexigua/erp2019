<?php

namespace app\common\traits;

trait homeTraits
{
    /**
     * 列表方法
     * @return array
     */
    public function index()
    {
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            $isPage = $this->request->param('is_page',1); //是否分页
            if($isPage==1){
                $data = $this->pageList();
                return $data;
            }else{
                $data = $this->getlist();
                return $data;
            }
        }
        return $this->fetch($this->request->param('tpl',''));
    }

    /**
     * 分页数据
     */
    public function pageList(){
        $pageSize = $this->request->param('pageSize/d'); //每页多少条
        $pageNumber = $this->request->param('pageNumber/d'); //当前页
        $sortName = $this->request->param('sortName','id'); //排序字段
        $sortOrder = $this->request->param('sortOrder', 'desc'); //升序降序
        $order = [$sortName => $sortOrder];
        $filter = $this->request->param("filter"); //搜索字段值 json
        $op = $this->request->param("op"); //搜索条件 json
        $op = $op ? $op : [];
        $filter = $filter ? $filter : [];
        if(!empty($this->appendQuery)){
            foreach($this->appendQuery as $field){
                $fieldArr = explode('.',$field);
                $value = count($fieldArr)>1 ? $fieldArr[1] : $field;
                $filter[$field] = $this->loginInfo[$value];
                $op[$field] = '=';
            }
        }
        //搜索器配置
        $searchFields = ['common'];//参与搜索信息
        $searchData = [ //默认搜索数据
            'common' => [$filter, $op],
            'sort' => $order,
        ];
        if (method_exists($this, '_pageFilter')) {
            $params = [];
            $this->_pageFilter($params);
            if(!empty($params['custom'])){
                $searchData['common'][0] = array_merge($searchData['common'][0],$params['custom']['filter']);
                $searchData['common'][1] = array_merge($searchData['common'][1],$params['custom']['op']);
            }
            if (!empty($params['fields'])) {
                $searchFields = array_merge($searchFields, $params['fields']);
            }
            if (!empty($params['data'])) {
                $searchData = array_merge($searchData, $params['data']);
            }
        }
        try {
            $data = ['total' => 0, 'rows' => []];
            //分页不需要sort和field
            $countData = $searchData;
            if (isset($countData['sort'])) unset($countData['sort']);
            if (isset($countData['field'])) unset($countData['field']);
            $data['total'] = $this->model->withSearch($searchFields, $countData)->count();
            $data['sql'] = $this->model->getLastSql();
            if ($data['total'] > 0) {
                $list = $this->model->withSearch($searchFields, $searchData)->page($pageNumber, $pageSize)->select();
                //$data['sql'] = $this->model->getLastSql();
                if (method_exists($this->model, '_formatList')) {
                    $list = $this->model->_formatList($list);
                }
                $data['rows'] = $list;
            }
        } catch (\Exception $e) {
            $data = ['total' => 0, 'rows' => [], 'err_msg' => $e->getMessage()];
        }
        return $data;
    }
    /**
     * 不分页数据
     */
    public function getlist(){
        $sortName = $this->request->param('sortName', 'id'); //排序字段
        $sortOrder = $this->request->param('sortOrder', 'desc'); //升序降序
        $order = [$sortName => $sortOrder];
        $filter = $this->request->param("filter"); //搜索字段值 json
        $op = $this->request->param("op"); //搜索条件 json
        $op = $op ? $op : [];
        $filter = $filter ? $filter : [];
        if(!empty($this->appendQuery)){
            foreach($this->appendQuery as $field){
                $fieldArr = explode('.',$field);
                $value = count($fieldArr)>1 ? $fieldArr[1] : $field;
                $filter[$field] = $this->loginInfo[$value];
                $op[$field] = '=';
            }
        }
        //站点ID
        if($this->sessionKey=='site'){
            $filter['site_id'] = $this->loginInfo['site_id'];
            $op['site_id'] = '=';
        }
        //搜索器配置
        $searchFields = ['common'];//参与搜索信息
        $searchData = [ //默认搜索数据
            'common' => [$filter, $op],
            'sort' => $order
        ];
        if (method_exists($this, '_filter')) {
            $params = [];
            $this->_filter($params);
            if(!empty($params['custom'])){
                $searchData['common'][0] = array_merge($searchData['common'][0],$params['custom']['filter']);
                $searchData['common'][1] = array_merge($searchData['common'][1],$params['custom']['op']);
            }
            if (!empty($params['fields'])) {
                $searchFields = array_merge($searchFields, $params['fields']);
            }
            if (!empty($params['data'])) {
                $searchData = array_merge($searchData, $params['data']);
            }
        }
        try {
            $list = $this->model->withSearch($searchFields, $searchData)->select();
            if (method_exists($this->model, '_formatList') && !$list->isEmpty()) {
                $list = $this->model->_formatList($list);
            }
            $data['data'] = $list;
            $data['sql'] = $this->model->getLastSql();
        } catch (\Exception $e) {
            $data['data'] = [];
            $data['err_msg'] = $e->getMessage();
        }
        return $data;
    }




}