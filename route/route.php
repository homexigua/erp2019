<?php

Route::get('qrcode', function ($text) {
    die(\org\util\HelpUtil::qrcode($text));
});
Route::view('error',\think\facade\Env::get('app_path').'common/tpl/error.php');

Route::view('/',function(){
    $domain = \think\facade\Request::rootDomain();
    $info = \app\common\model\DomainModel::where('domain',$domain)->find();
    if(empty($info)) return redirect('platform/Index/index');
    if($info['ref_type']==2) return redirect('trade/Index/index');
    if($info['ref_type']==3) return redirect('site/Index/index');
});