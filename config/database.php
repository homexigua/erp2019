<?php
return [
    // 数据库类型
    'type'            => 'mysql',
    // 服务器地址
    'hostname'        => Env::get('database.hostname','192.168.1.88'),
    // 数据库名
    'database'        => Env::get('database.database','crm2019'),
    // 用户名
    'username'        => Env::get('database.username','crm2019'),
    // 密码
    'password'        => Env::get('database.password','888999'),
    // 端口
    'hostport'        => '',
    // 连接dsn
    'dsn'             => '',
    // 数据库连接参数
    'params'          => [],
    // 数据库编码默认采用utf8
    'charset'         => 'utf8',
    // 数据库表前缀
    'prefix'          => Env::get('database.prefix',''),
    // 数据库调试模式
    'debug'           => Env::get('database.debug',false),
    // 数据库部署方式:0 集中式(单一服务器),1 分布式(主从服务器)
    'deploy'          => 0,
    // 数据库读写是否分离 主从式有效
    'rw_separate'     => false,
    // 读写分离后 主服务器数量
    'master_num'      => 1,
    // 指定从服务器序号
    'slave_no'        => '',
    // 是否严格检查字段是否存在
    'fields_strict'   => true,
    // 数据集返回类型 array 数组 collection Collection对象
    'resultset_type'  => 'collection',
    // 是否自动写入时间戳字段
    'auto_timestamp'  => false,
    //关闭输出时间自动格式转换
    'datetime_format' => false,
    // 是否需要进行SQL性能分析
    'sql_explain'     => false,
    ];