<?php
return [
    // HTTP 请求的超时时间（秒）
    'timeout' => 5.0,
    // 默认发送配置
    'default' => [
        // 网关调用策略，默认：顺序调用
        'strategy' => \Overtrue\EasySms\Strategies\OrderStrategy::class,
        // 默认可用的发送网关
        'gateways' => ['qcloud'],
    ],
    // 可用的网关配置
    'gateways' => [
        'errorlog' => [
            'file' => Env::get('runtime_path').'log/easy-sms.log',
        ],
        //阿里云 短信内容使用 template + data
        'aliyun' => [
            'access_key_id' => '',
            'access_key_secret' => '',
            'sign_name' => '',
        ],
        //阿里云Rest 短信内容使用 template + data
        'aliyunrest' => [
            'app_key' => '',
            'app_secret_key' => '',
            'sign_name' => '',
        ],
        //云片 短信内容使用 content
        'yunpian' => [
            'api_key' => '',
            'signature' => '【默认签名】', // 内容中无签名时使用
        ],
        //容联云通讯 短信内容使用 template + data
        'yuntongxun' => [
            'app_id' => '',
            'account_sid' => '',
            'account_token' => '',
            'is_sub_account' => false,
        ],
        //互亿无线 短信内容使用 content
        'huyi' => [
            'api_id' => '',
            'api_key' => '',
        ],
        //百度云 短信内容使用 template + data
        'baidu' => [
            'ak' => '',
            'sk' => '',
            'invoke_id' => '',
            'domain' => '',
        ],
        //融云 短信分为两大类，验证类和通知类短信。 发送验证类短信使用 template + data
        'rongcloud' => [
            'app_key' => '',
            'app_secret' => '',
        ],
        //腾讯云SMS 短信内容使用 content
        'qcloud' => [
            'sdk_app_id' => '', // SDK APP ID
            'app_key' => '', // APP KEY
            'sign_name' => '', // 短信签名，如果使用默认签名，该字段可缺省（对应官方文档中的sign）
        ]
    ],
];