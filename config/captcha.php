<?php
/**
 * Created by PhpStorm.
 * User: chaiy
 * Date: 2018/5/11
 * Time: 7:50
 */
return [
    // 验证码字体大小
    'fontSize'    =>    42,
    // 验证码位数
    'length'      =>    4,
    // 关闭验证码杂点
    'useNoise'    =>    false,
    'codeSet'  => '1234567890',
];