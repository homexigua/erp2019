<?php

namespace analysis;

use think\Facade;
/**
 * @see \analysis\Analysis
 * @mixin \analysis\Analysis
 * @method array ppl($key, $dict, $num = 5, $type=1) static 返回分词结果
 * 模式：1简单数组 2分词字符串 3完整数组
 * 使用方法：\analysis\AnalysisHelper::ppl('待分词字符串',["1.txt"],5,2);
 */
class AnalysisHelper extends Facade {
    protected static function getFacadeClass() {
        return 'analysis\Analysis';
    }
}