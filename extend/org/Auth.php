<?php

namespace org;

use think\Db;
use think\facade\Config;
use think\facade\Request;
use think\facade\Session;

/**
 * 权限认证类
 * 功能特性：
 * 1，是对规则进行认证，不是对节点进行认证。用户可以把节点当作规则名称实现对节点进行认证。
 *      $auth=new Auth();  $auth->check('规则名称','用户id')
 * 2，可以同时对多条规则进行认证，并设置多条规则的关系（or或者and）
 *      $auth=new Auth();  $auth->check('规则1,规则2','用户id','and')
 *      第三个参数为and时表示，用户需要同时具有规则1和规则2的权限。 当第三个参数为or时，表示用户值需要具备其中一个条件即可。默认为or
 * 3，我们需要设置每个用户组拥有哪些规则(auth_group 定义了用户组权限)
 *
 * 4，支持规则表达式。
 *      在auth_rule 表中定义一条规则时，如果type为1， condition字段就可以定义规则表达式。 如定义{score}>5  and {score}<100  表示用户的分数在5-100之间时这条规则才会通过。
 */
//数据库
/*
-- ----------------------------
-- auth_rule，规则表，
-- id:主键，name：规则唯一标识, title：规则中文名称 status 状态：为1正常，为0禁用，condition：规则表达式，为空表示存在就验证，不为空表示按照条件验证
-- ----------------------------
DROP TABLE IF EXISTS `auth_rule`;
CREATE TABLE `auth_rule` (
    `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
    `name` char(80) NOT NULL DEFAULT '',
    `title` char(20) NOT NULL DEFAULT '',
    `status` tinyint(1) NOT NULL DEFAULT '1',
    `condition` char(100) NOT NULL DEFAULT '',  # 规则附件条件,满足附加条件的规则,才认为是有效的规则
    PRIMARY KEY (`id`),
    UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
-- ----------------------------
-- auth_group 用户组表，
-- id：主键， title:用户组中文名称， rules：用户组拥有的规则id， 多个规则","隔开，status 状态：为1正常，为0禁用
-- ----------------------------
DROP TABLE IF EXISTS `auth_group`;
    CREATE TABLE `auth_group` (
    `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
    `name` char(100) NOT NULL DEFAULT '',
    `status` tinyint(1) NOT NULL DEFAULT '1',
    `rules` char(80) NOT NULL DEFAULT '',
    PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
 */

class Auth {
    /**
     * @var object 对象实例
     */
    protected static $instance;
    /**
     * 当前请求实例
     * @var Request
     */
    protected $request;
    //默认配置
    protected $config = [
        'auth_on' => 1, // 权限开关
        'auth_type' => 1, // 认证方式，1为实时认证；2为登录认证
        'auth_group' => 'auth_group', // 用户组数据表名
        'auth_rule' => 'auth_rule', // 权限规则表
        'auth_user' => 'admin', // 用户信息表
    ];

    /**
     * 类架构函数
     * Auth constructor.
     */
    public function __construct($key = '') {
        if (!empty($key)) {
            $auth = Config::get('auth.' . $key);
            $this->config = array_merge($this->config, $auth);
        }
        // 初始化request
        $this->request = Request::instance();
    }

    /**
     * 初始化
     * @access public
     * @param 配置项
     * @return \think\Request
     */
    public static function instance($configKey) {
        if (is_null(self::$instance)) {
            self::$instance = new static($configKey);
        }
        return self::$instance;
    }

    /**
     * 检查权限
     * @param $name  需要验证的规则列表,支持逗号分隔的权限规则或索引数组
     * @param $uid 认证用户的id
     * @param string $mode 执行check的模式
     * @param string $relation 如果为 'or' 表示满足任一条规则即通过验证;如果为 'and'则表示需满足所有规则才能通过验证
     * @return bool  通过验证返回true;失败返回false
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function check($name, $uid, $mode = 'url', $relation = 'or') {
        if (!$this->config['auth_on']) {
            return true;
        }
        // 获取用户需要验证的所有有效规则列表
        $authList = $this->getAuthList($uid);
        if (is_string($name)) {
            $name = strtolower($name);
            if (strpos($name, ',') !== false) {
                $name = explode(',', $name);
            } else {
                $name = [$name];
            }
        }
        $list = []; //保存验证通过的规则名
        if ('url' == $mode) {
            $REQUEST = unserialize(strtolower(serialize($this->request->param())));
        }
        foreach ($authList as $auth) {
            $query = preg_replace('/^.+\?/U', '', $auth);
            $baseName = preg_replace('/\?.*$/U', '', $name);
            if ('url' == $mode && $query != $auth) {
                parse_str($query, $param); //解析规则中的param
                $intersect = array_intersect_assoc($REQUEST, $param); //取参数交集
                $auth = preg_replace('/\?.*$/U', '', $auth);
                if (in_array($auth, $baseName) && $intersect == $param) {
                    //如果节点相符且url参数满足
                    $list[] = $auth;
                }
            } else {
                if (in_array($auth, $baseName)) {
                    $list[] = $auth;
                }
            }
        }
        if ('or' == $relation && !empty($list)) {
            return true;
        }
        $diff = array_diff($name, $list);
        if ('and' == $relation && empty($diff)) {
            return true;
        }
        return false;
    }

    /**
     * 获取权限列表
     * @param $uid
     * @param $type 1、获取权限列表 2、获取权限详细信息
     * @return array|mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getAuthList($uid,$type=1) {
        static $_authList = []; //保存用户验证通过的权限列表
        if (isset($_authList[$uid])) {
            return $_authList[$uid];
        }
        if (2 == $this->config['auth_type'] && Session::has('_auth_list_' . $uid)) {
            return Session::get('_auth_list_' . $uid);
        }
        $fullAuthList = []; //完整权限列表
        //获取用户组信息
        $group = $this->getGroup($uid);
        if (empty($group['group_id']) || empty($group['rules'])) return [];
        $map = array(
            ['id', 'in', $group['rules']],
            ['status', '=', 1],
        );
        //读取用户组所有权限规则
        $rules = Db::table($this->config['auth_rule'])->where($map)->field(true)->order('show_order asc')->select();
        //循环规则，判断结果
        $authList = []; //
        foreach ($rules as $rule) {
            if (!empty($rule['condition'])) { //条件判断
                $user = $this->getUserInfo($uid);
                //根据condition进行验证
                $command = preg_replace('/\{(\w*?)\}/', '$user[\'\\1\']', $rule['condition']);
                //dump($command); //debug
                $condition = null;
                @(eval('$condition=(' . $command . ');'));
                if ($condition) {
                    $fullAuthList[] = $rule;
                    $authList[] = strtolower($rule['name']);
                }
            } else {
                //只要存在就记录
                $fullAuthList[] = $rule;
                $authList[] = strtolower($rule['name']);
            }
        }
        if($type!=1) return $fullAuthList;
        $_authList[$uid] = $authList;
        if (2 == $this->config['auth_type']) {
            //规则列表结果保存到session
            Session::set('_auth_list_' . $uid, $authList);
        }
        return array_unique($authList);
    }

    /**
     * 获取用户组
     * @param $uid
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getGroup($uid) {
        static $groups = [];
        if (isset($groups[$uid])) {
            return $groups[$uid];
        }
        $sessionKey = Session::get('session_key');
        $loginInfo = Session::get($sessionKey);
        // 转换表名
        $auth_group = $this->config['auth_group'];
        $group = Db::table($auth_group, 'name,rules')
                   ->field('name,rules')
                   ->where('status',1)
                   ->where('id',$loginInfo['group_id'])
                   ->find();
        if(!empty($group)){
            $group['uid'] = $uid;
            $group['group_id'] = $loginInfo['group_id'];
        }
        $groups[$uid] = $group;
        return $groups[$uid];
    }

    /**
     * 获取用户信息
     * @param $uid
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getUserInfo($uid) {
        static $userinfo = [];
        $user = Db::name($this->config['auth_user']);
        // 获取用户表主键
        $_pk = is_string($user->getPk()) ? $user->getPk() : 'id';
        if (!isset($userinfo[$uid])) {
            $userinfo[$uid] = $user->where($_pk, $uid)->field("password", true)->find();
        }
        return $userinfo[$uid];
    }

    /**
     * 获取session权限用户资料
     * @param string $sessionKey
     * @return array|mixed
     */
    public function getSessionInfo($sessionKey) {
        if (Session::has($sessionKey)) {
            return Session::get($sessionKey);
        }
        return [];
    }
}