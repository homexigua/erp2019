<?php

namespace org\util;
/**
 * 操纵文件类
 *
 * 例子：
 * FileUtil::createDir('a/1/2/3');                    测试建立文件夹 建一个a/1/2/3文件夹
 * FileUtil::createFile('b/1/2/3');                    测试建立文件        在b/1/2/文件夹下面建一个3文件
 * FileUtil::createFile('b/1/2/3.exe');             测试建立文件        在b/1/2/文件夹下面建一个3.exe文件
 * FileUtil::copyDir('b','d/e');                    测试复制文件夹 建立一个d/e文件夹，把b文件夹下的内容复制进去
 * FileUtil::copyFile('b/1/2/3.exe','b/b/3.exe'); 测试复制文件        建立一个b/b文件夹，并把b/1/2文件夹中的3.exe文件复制进去
 * FileUtil::moveDir('a/','b/c');                    测试移动文件夹 建立一个b/c文件夹,并把a文件夹下的内容移动进去，并删除a文件夹
 * FileUtil::moveFile('b/1/2/3.exe','b/d/3.exe'); 测试移动文件        建立一个b/d文件夹，并把b/1/2中的3.exe移动进去
 * FileUtil::unlinkFile('b/d/3.exe');             测试删除文件        删除b/d/3.exe文件
 * FileUtil::unlinkDir('d');                      测试删除文件夹 删除d文件夹
 */
class FileUtil
{

    /**
     * 建立文件夹
     *
     * @param string $aimUrl
     * @return viod
     */
    static function createDir($aimUrl)
    {
        if (!empty($aimUrl)) {
            return is_dir($aimUrl) or mkdir($aimUrl, 0755, true);
        }
    }

    /**
     * 建立文件
     *
     * @param string $aimUrl
     * @param boolean $overWrite 该参数控制是否覆盖原文件
     * @return boolean
     */
    static function createFile($aimUrl, $overWrite = false)
    {
        if (file_exists($aimUrl) && $overWrite == false) {
            return false;
        } elseif (file_exists($aimUrl) && $overWrite == true) {
            FileUtil:: unlinkFile($aimUrl);
        }
        $aimDir = dirname($aimUrl);
        FileUtil:: createDir($aimDir);
        touch($aimUrl);
        return true;
    }

    /**
     * 移动文件夹
     *
     * @param string $oldDir
     * @param string $aimDir
     * @return boolean
     */
    static function moveDir($oldDir, $aimDir)
    {
        if (FileUtil::copyDir($oldDir, $aimDir)) {
            return FileUtil::unlinkDir($oldDir);
        }
    }

    /**
     * 移动文件
     *
     * @param string $fileUrl
     * @param string $aimUrl
     * @param boolean $overWrite 该参数控制是否覆盖原文件
     * @return boolean
     */
    static function moveFile($fileUrl, $aimUrl, $overWrite = false)
    {
        is_dir($aimUrl) or mkdir($aimUrl, 0755, true);
        if (is_file($fileUrl) && is_dir($aimUrl)) {
            copy($fileUrl, $aimUrl.'/'.basename($fileUrl));
            return unlink($fileUrl);
        }
    }

    /**
     * 删除文件夹
     *
     * @param string $dir
     * @return boolean
     */
    static function unlinkDir($dir)
    {
        if (!is_dir($dir)) {
            return true;
        }
        foreach (glob($dir."/*") as $v) {
            is_dir($v) ? FileUtil::unlinkDir($v) : unlink($v);
        }
        return rmdir($dir);
    }

    /**
     * 删除文件
     *
     * @param string $aimUrl
     * @return boolean
     */
    static function unlinkFile($aimUrl)
    {
        if (is_file($aimUrl)) {
            return unlink($aimUrl);
        }
        return true;
    }

    /**
     * 复制文件夹
     *
     * @param string $oldDir
     * @param string $aimDir
     * @return boolean
     */
    static function copyDir($oldDir, $aimDir)
    {
        is_dir($aimDir) or mkdir($aimDir, 0755, true);
        foreach (glob($oldDir.'/*') as $v) {
            $to = $aimDir.'/'.basename($v);
            is_file($v) ? copy($v, $to) : FileUtil::copyDir($v, $to);
        }
        return true;
    }

    /**
     * 复制文件
     *
     * @param string $fileUrl
     * @param string $aimUrl
     * @param boolean $overWrite 该参数控制是否覆盖原文件
     * @return boolean
     */
    static function copyFile($fileUrl, $aimUrl)
    {
        if (!is_file($fileUrl)) {
            return false;
        }
        //创建目录
        FileUtil::createDir(dirname($aimUrl));
        return copy($fileUrl, $aimUrl);
    }


    //遍历目录文件
    static public function getDir($dir, $exts = [])
    {
        $list = [];
        if (empty($dir)) {
            return $list;
        }
        foreach (glob($dir.'/*') as $id => $v) {
            $info                     = pathinfo($v);
            $listDir = 0;
            if(empty($exts)){
                $listDir = 1;
            }else{
                if (in_array($info['extension'], $exts)) {
                    $listDir = 1;
                }
            }
            if ($listDir==1) {
                $list [$id] ['path']      = $v;
                $list [$id] ['type']      = filetype($v);
                $list [$id] ['dirname']   = $info['dirname'];
                $list [$id] ['basename']  = $info['basename'];
                $list [$id] ['filename']  = $info['filename'];
                $list [$id] ['extension'] = isset($info['extension']) ? $info['extension'] : '';
                $list [$id] ['filemtime'] = filemtime($v);
                $list [$id] ['fileatime'] = fileatime($v);
                $list [$id] ['size']      = is_file($v) ? filesize($v) : FileUtil::size($v);
                $list [$id] ['iswrite']   = is_writeable($v);
                $list [$id] ['isread']    = is_readable($v);
            }
        }
        return array_values($list);
    }

    /**
     * 遍历树型文件目录
     * @param $dir
     * @return array
     */
    static public function tree($dir) {
        $files = [];
        $i=0;
        if(@$handle = opendir($dir)) { //注意这里要加一个@，不然会有warning错误提示：）
            while(($file = readdir($handle)) !== false) {
                if($file != ".." && $file != ".") { //排除根目录；
                    if(is_dir($dir."/".$file)) { //如果是子文件夹，就进行递归
                        $files[$i]['name'] = $file;
                        $files[$i]['open'] = false;
                        $files[$i]["children"] =  self::tree($dir."/".$file);
                    } else { //不然就将文件的名字存入数组；
                        $files[$i] = ['name'=>$file];
                    }
                    $i+=1;
                }
            }
            //children
            closedir($handle);
            return $files;
        }
        return [];
    }

    //获取目录大小
    static public function size($dir)
    {
        $s = 0;
        foreach (glob($dir.'/*') as $v) {
            $s += is_file($v) ? filesize($v) : FileUtil::size($v);
        }
        return $s;
    }

}
