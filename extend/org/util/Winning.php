<?php
/**
 * 中奖概率算法
 * 奖品编号 奖品名称 中奖等级 奖品数量
 * 中奖等级和奖品数量必须，其他辅助
$arr = [
    ['id'=>1,'name'=>'平板电脑','level'=>1,'num'=>1],
    ['id'=>2,'name'=>'数码相机','level'=>1,'num'=>5],
    ['id'=>3,'name'=>'音箱设备','level'=>1,'num'=>10],
    ['id'=>4,'name'=>'4G优盘','level'=>1,'num'=>12],
    ['id'=>5,'name'=>'10Q币','level'=>1,'num'=>22],
    ['id'=>6,'name'=>'下次没准就能中哦','level'=>1,'num'=>50],
];
$res = Winning::calc($arr);
 */

namespace org\util;

class Winning {
    /*
     * 奖项数组
     * 是一个二维数组，记录了所有本次抽奖的奖项信息，
     * 其中level表示中奖等级，name表示奖品，v表示中奖概率。
     * 注意其中的v必须为整数，你可以将对应的 奖项的v设置成0，即意味着该奖项抽中的几率是0，
     * 数组中v的总和（基数），基数越大越能体现概率的准确性。
     * 本例中v的总和为100，那么平板电脑对应的 中奖概率就是1%，
     * 如果v的总和是10000，那中奖概率就是万分之一了。
     */
    static public function calc($prizeArr){
        $arr=[];
        foreach ($prizeArr as $key => $val) {
            $arr[$val['level']] = $val['num'];
        }
        $rid = self::getRand($arr); //根据概率获取奖项id
        $res['yes'] = $prizeArr[$rid-1]; //中奖项
        unset($prizeArr[$rid-1]); //将中奖项从数组中剔除，剩下未中奖项
        shuffle($prizeArr); //打乱数组顺序
        $pr=[];
        for($i=0;$i<count($prizeArr);$i++){
            $pr[] = $prizeArr[$i];
        }
        $res['no'] = $pr;
        return $res;
    }
    /*
     * 经典的概率算法，
     * $proArr是一个预先设置的数组，
     * 假设数组为：array(100,200,300，400)，
     * 开始是从1,1000 这个概率范围内筛选第一个数是否在他的出现概率范围之内，
     * 如果不在，则将概率空间，也就是k的值减去刚刚的那个数字的概率空间，
     * 在本例当中就是减去100，也就是说第二个数是在1，900这个范围内筛选的。
     * 这样 筛选到最终，总会有一个数满足要求。
     * 就相当于去一个箱子里摸东西，
     * 第一个不是，第二个不是，第三个还不是，那最后一个一定是。
     * 这个算法简单，而且效率非常 高，
     * 关键是这个算法已在我们以前的项目中有应用，尤其是大数据量的项目中效率非常棒。
     */
    static public function getRand($proArr) {
        $result = '';
        //概率数组的总概率精度
        $proSum = array_sum($proArr);
        //概率数组循环
        foreach ($proArr as $key => $proCur) {
            $randNum = mt_rand(1, $proSum);
            if ($randNum <= $proCur) {
                $result = $key;
                break;
            } else {
                $proSum -= $proCur;
            }
        }
        unset ($proArr);
        return $result;
    }
}