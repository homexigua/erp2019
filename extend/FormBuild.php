<?php

use think\Facade;
/**
 * @see \form\FormHelper
 * @mixin \form\FormHelper
 * @method string _input($type, $name, $value = null, $options = []) static 生成文本框(按类型)
 * @method string _text($name, $value = null, $options = []) static 生成普通文本框
 * @method string _password($name, $value = null, $options = []) static 生成密码文本框
 * @method string _hidden($name, $value = null, $options = []) static 生成隐藏文本框
 * @method string _switch($name, $value = null, $options = []) static 生成switch组件
 * @method string _tags($name, $value = null, $options = []) static 生成tags组件
 * @method string _color($name, $value = null, $options = []) static 生成颜色选择框
 * @method string _textarea($name, $value = null, $options = []) static 生成多行文本框
 * @method string _ckeditor($name, $value = null, $options = []) static 生成富文本编辑器
 * @method string _select($name, $value = null, $options = []) static 生成下拉列表框
 * @method string _selectpage($name, $value = null, $options = []) static 生成selectPage控件
 * @method string _selectpages($name, $value = null, $options = []) static 生成多选selectPage控件 多选
 * @method string _laydate($name, $value, $options = []) static 生成日期选择器
 * @method string _input_array($name, $value, $options = []) static 生成项目列表控件
 * @method string _checkbox($name, $value = null, $options = []) static 生成复选框
 * @method string _radio($name, $value = null, $options = []) static 生成单选框
 * @method string _image($name, $value, $options=[]) static 生成上传图片(单图)
 * @method string _file($name, $value, $options=[]) static 生成上传附件(单附件)
 * @method string _images($name, $value, $options=[]) static 生成多图上传组件
 * @method string _images_desc($name, $value, $options=[]) static 生成多图带描述上传组件
 * @method string _files($name, $value, $options=[]) static 上传多附件
 * @method string _cxselect($name, $value, $options=[]) static 多级级联
 * @method string _button($value = null, $options = []) static 生成按钮
 * @method array getFormTypes() static 获取表单类型
 * @method array getValidRules() static 获取验证规则
 * @method array formatRules($data) static 获取验证规则
 * @method array transformationValid($valid,$split=',') static 换算前端验证规则
 */

class FormBuild extends Facade {
    protected static function getFacadeClass() {
        return 'form\FormHelper';
    }
}