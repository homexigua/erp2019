<div class="container text-center" style="padding:30px 0;">
    <ul class="mc-avg-sm-3">
        <li>
            <h3 class="text-size16">客户服务</h3>
            <a href="javascript:;" class="mc-icon-item service-link">
                <i class="iconfont icon-icon-kefu"></i>
                <span class="text-size14 block line-height30">在线客服</span>
            </a>
            <a href="javascript:;" class="mc-icon-item service-link">
                <i class="iconfont icon-kefu2"></i>
                <span class="text-size14 block line-height30">用户反馈</span>
            </a>
        </li>
        <li style="height:180px;border:1px solid #e8e8e8;border-width:0 1px;">
            <div style="padding:0 30px;">
                <h3 class="text-size16">何为严选</h3>
                <p class="text-justify text-size14 line-height30 text-gray">
                    网易原创生活类电商品牌，秉承网易一贯的严谨态度，我们深入世界各地，从源头全程严格把控商品生产环节，力求帮消费者甄选到优质的商品
                </p>
            </div>
        </li>
        <li>
            <h3 class="text-size16">扫码下载严选APP</h3>
            <img src="/static/home/pc/images/qrcode.png" style="width:100px;" alt="">
            <p class="text-size12 text-main line-height30">下载领取1000元新人礼包</p>
        </li>
    </ul>
</div>
<div class="footer-box">
    <div class="container">
        <div class="height-20"></div>
        <ul class="mc-avg-sm-4 mc-avg-between">
            <li>
                <div class="mc-icon-item text-main">
                    <i class="iconfont border-main-2 circle icon-houtai1"></i>
                    <span class="text-size18 single-cut"> 本地电商平台</span>
                </div>
            </li>
            <li>
                <div class="mc-icon-item text-main">
                    <i class="iconfont border-main-2 circle icon-wuyoushouhou"></i>
                    <span class="text-size18 single-cut">30天无忧退货</span>
                </div>
            </li>
            <li>
                <div class="mc-icon-item text-main">
                    <i class="iconfont border-main-2 circle icon-iconfontevaluate"></i>
                    <span class="text-size18 single-cut"> 品牌实体服务保障</span>
                </div>
            </li>
            <li>
                <div class="mc-icon-item text-main">
                    <i class="iconfont border-main-2 circle icon-jifen"></i>
                    <span class="text-size18 single-cut"> 线上线下积分共享</span>
                </div>
            </li>
        </ul>
        <div class="height-20"></div>
        <div class="split-line-solid"></div>
        <div class="footer-link">
            <a href="javascript:;" class="mc-text-link">关于我们</a>
            <span class="mc-text-split">|</span>
            <a href="javascript:;" class="mc-text-link">帮助中心</a>
            <span class="mc-text-split">|</span>
            <a href="javascript:;" class="mc-text-link">售后服务</a>
            <span class="mc-text-split">|</span>
            <a href="javascript:;" class="mc-text-link">配送与验收</a>
            <span class="mc-text-split">|</span>
            <a href="javascript:;" class="mc-text-link">商务合作</a>
            <span class="mc-text-split">|</span>
            <a href="javascript:;" class="mc-text-link">企业采购</a>
            <span class="mc-text-split">|</span>
            <a href="javascript:;" class="mc-text-link">搜索推荐</a>
            <span class="mc-text-split">|</span>
            <a href="javascript:;" class="mc-text-link">友情链接</a>
        </div>
        <p class="copyright">
            版权所有 © 1997-2018   食品经营许可证：JY13301080111719 出版物经营许可证：新出发滨字第0132号 单用途商业预付卡备案证：330100AAC0024
        </p>
    </div>
</div>