<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>{block name="title"}{/block}</title>
    <meta name="description" content="{block name='description'}{/block}">
    <meta name="keywords" content="{block name='keywords'}{/block}">
    <link rel="stylesheet" href="/static/plugins/bootstrap/css/bootstrap.min.css">
    <link href="/static/home/pc/css/extend.css" rel="stylesheet">
    <link href="/static/home/pc/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="//at.alicdn.com/t/font_539359_mr6vh79uln.css">
    <script src="//at.alicdn.com/t/font_539359_mr6vh79uln.js"></script>
    <!--[if lt IE 9]>
    <script src="http://apps.bdimg.com/libs/html5shiv/3.7/html5shiv.min.js"></script>
    <script src="http://apps.bdimg.com/libs/respond.js/1.4.2/respond.js"></script>
    <![endif]-->
    <script src="/static/plugins/jquery/jquery-2.2.4.min.js"></script>
    <script src="/static/plugins/lodash/lodash.min.js"></script>
    <script src="/static/plugins/juicer/juicer-min.js"></script>
    <script src="/static/plugins/layer/layer.js"></script>
    <script src="/static/plugins/in/in.min.js"></script>
    <script src="/static/home/pc/js/in-config.js?v=1.0"></script>
    <!--大话主席js-->
    <script src="/static/home/pc/js/jquery.SuperSlide.2.1.1.js"></script>
    <!-- 自定义头部 -->
    {block name="head"}{/block}
    <!-- layer弹框 -->
    <script src="/static/home/pc/js/utils.js?v=1.7"></script>
</head>
<body>
{block name="body"}{/block}
<script src="/static/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="/static/home/pc/js/extend.js"></script>
{block name="script"}{/block}
</body>
</html>