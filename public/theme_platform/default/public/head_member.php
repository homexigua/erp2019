<div class="header-box">
    {include file='public/h_top'}
    {include file='public/h_main'}
</div>
<div class="member-nav-bar height-40 bg-main">
    <div class="container">
        <ul id="index-nav" class="mc-index-nav">
            <li class="nLi">
                <a href="/home/member.index/index.html" class="menu-link">会员首页</a>
            </li>
            <li class="nLi">
                <a href="/home/member.order/index.html" class="menu-link">订单中心</a>
            </li>
            <li class="nLi">
                <a href="/home/member.index/safe.html" class="menu-link">账户安全</a>
            </li>
            <li class="nLi ">
                <a href="javascript:;" class="menu-link">我的资产</a>
                <ul class="sub">
                    <li><a href="/home/member.account/index.html?type=point" class="sub-link">我的积分</a></li>
                    <li><a href="/home/member.account/index.html?type=coin" class="sub-link">我的金币</a></li>
                    <li><a href="/home/member.account/index.html?type=yuanb" class="sub-link">我的元宝</a></li>
                    <li><a href="/home/member.account/blackcard.html" class="sub-link">我的黑卡</a></li>
                    <li><a href="/home/member.account/coupon.html" class="sub-link">优惠券</a></li>
                </ul>
            </li>
            <li class="nLi">
                <a href="/home/member.account/exchange.html" class="menu-link">兑换中心</a>
            </li>
        </ul>
    </div>
</div>
<script>
    jQuery("#index-nav").slide({
        type:"menu",// 效果类型，针对菜单/导航而引入的参数（默认slide）
        titCell:".nLi", //鼠标触发对象
        targetCell:".sub", //titCell里面包含的要显示/消失的对象
        effect:"slideDown", //targetCell下拉效果
        delayTime:300 , //效果时间
        triggerTime:0, //鼠标延迟触发时间（默认150）
        returnDefault:true //鼠标移走后返回默认状态，例如默认频道是“预告片”，鼠标移走后会返回“预告片”（默认false）
    });
</script>