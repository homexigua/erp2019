{extend name="public:layout" /}
{block name="title"}会员登录{/block}
{block name="head"}{/block}
{block name="body"}
{include file='public/head_base'}
<div class="login-banner">
    <div class="login-box">
        <a href="javascript:;" class="iconfont icon-erweima2 corner-icon"></a>
        <div class="m-d-30">
            <h3 class="text-center text-gray">会员登录</h3>
            <div class="height-20"></div>
            <form class="form-horizontal icon-form icon-form-lg" id="form">
                <div class="form-group">
                    <span class="iconfont icon-huiyuan1"></span>
                    <input type="text" class="form-control input-lg" name="account" placeholder="请输入登录账号或手机号码" datatype="*">
                </div>
                <div class="form-group">
                    <span class="iconfont icon-mima"></span>
                    <input type="password" class="form-control input-lg" name="password" placeholder="请输入密码" datatype="*">
                </div>
                <div class="form-group">
                    <span class="iconfont icon-msnui-auth-code"></span>
                    <div class="input-group input-group-lg">
                        <input type="text" class="form-control" name="vcode" placeholder="请输入验证码">
                        <span class="input-group-btn">
                            <img src="/captcha.html" onclick="$(this).attr('src','/captcha.html?rnd=' + Math.random());" class="img-captcha" />
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <button type="button" class="btn btn-sub btn-lg btn-block btn-main">立即登录</button>
                </div>
            </form>
            <p>
                <a href="/platform/public/reg.html" class="mc-text-link">还没有账号，去注册</a>
                <a href="/platform/public/findpassword.html" class="mc-text-link pull-right">忘记密码</a>
            </p>
        </div>
    </div>
</div>
{include file='public/foot'}
{/block}
{block name='script'}
<script>
    $(function () {
        utils.ajaxSubForm({
            callback: function (res) {
                utils.closeLoading();
                $('#form').find('.btn-sub').removeAttr("disabled"); //启用按钮
                $('#form').find('.btn-reset').removeAttr("disabled"); //启用按钮
                if (res.code == 1) {
                    location.href=res.url;
                } else {
                    utils.msg(res.msg);
                }
            }
        })
    })
</script>
{/block}