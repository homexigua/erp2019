<div class="header-main" style="padding:40px 0">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <a href="/"><img src="/static/home/pc/images/logo2.png" class="height-55 pull-left" alt=""></a>
                <div class="slogan-box pull-left">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="mc-cart-box pull-right" id="pop-cart-box" style="width:40px;margin-top:8px;">

                </div>
                <form action="" class="mc-search-box" style="width:240px;float:right;">
                    <div class="input-group yanxuan-search">
                        <input type="text" class="form-control" placeholder="请输入关键词进行检索">
                        <span class="input-group-btn">
                                <button class="btn" type="button"><i class="iconfont icon-search"></i></button>
                            </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
{include file='cart/cart_list'}
<script>
    $(function () {
        utils.getAjax({
            url:'/home/cart/index.html',
            success:function (res) {
                var html = juicer($('#pop_cart_list').html(),{data:res,num:res.length});
                $('#pop-cart-box').html(html);
            }
        })
    })
</script>