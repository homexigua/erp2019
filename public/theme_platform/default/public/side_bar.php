<div class="mc-side-bar">
    <div class="action-box">
        <a href="/home/member.index/index.html" class="link-item">
            <i class="iconfont icon-huiyuan1 link-icon"></i>
            <span class="text-size12">账号</span>
        </a>
        <a href="/home/cart/index.html" class="link-item cart-link active">
            <i class="iconfont icon-gouwugouwuchedinggou link-icon"></i>
            <span class="text-size12">购<br>物<br>车</span>
        </a>
        <a href="/home/member.account/coupon.html" class="link-item">
            <i class="iconfont icon-coupon link-icon"></i>
            <span class="text">我的优惠券</span>
        </a>
        <a href="" class="link-item">
            <i class="iconfont icon-yduishoucangkongxin link-icon"></i>
            <span class="text">店铺收藏</span>
        </a>
        <a href="" class="link-item">
            <i class="iconfont icon-yduixingxingkongxin link-icon"></i>
            <span class="text">商品收藏</span>
        </a>
        <a href="" class="link-item">
            <i class="iconfont icon-yduizuji link-icon"></i>
            <span class="text">浏览足迹</span>
        </a>
    </div>
    <div class="feedback-box">
        <a href="" class="link-item">
            <i class="iconfont icon-fankui1 link-icon"></i>
            <span class="text">会员反馈</span>
        </a>
        <a href="javascript:;" class="link-item"  onclick='$("body,html").animate({"scrollTop":0},500)'>
            <i class="iconfont icon-arrow_up link-icon"></i>
            <span class="text">返回顶部</span>
        </a>
    </div>
</div>