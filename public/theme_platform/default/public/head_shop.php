<div class="header-box">
    {include file='public/head_top'}
    <div class="header-main" style="padding:20px 0">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <a href="/" class="block pull-left"><img src="/static/home/pc/images/logo2.png" class="height-55 pull-left" alt=""></a>
                <div class="shop-info-box pull-left" style="padding:0 15px;margin:10px 15px 10px;height:45px;border-left:1px solid #e8e8e8">
                    <div class="text-size16">
                        {$shop_info.config.sho_name|default=$shop_info.shop_name}
                        {notempty name='shop_info.category_info'}
                            <span class="text-gray text-size12">主营业务：</span>
                            {volist name='shop_info.category_info' id='vo' offset='' length=''}
                            <span class="btn btn-main btn-xs">{$vo}</span>
                            {/volist}
                        {/notempty}
                    </div>
                    <div class="text-size14 text-gray line-height30">实体店地址： {$shop_info.prov}{$shop_info.city}{$shop_info.dist}{$shop_info.address}</div>
                </div>
            </div>
            <div class="col-sm-4">
                <form action="" class="mc-search-box" style="width:240px;float:right;">
                    <div class="input-group yanxuan-search">
                        <input type="text" class="form-control" placeholder="请输入关键词进行检索">
                        <span class="input-group-btn">
                                <button class="btn" type="button"><i class="iconfont icon-search"></i></button>
                            </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
</div>
<div class="split-line-solid split-line-sm"></div>
{include file='public/side_bar'}