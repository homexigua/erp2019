<div class="top-bar">
    <div class="container">
        <div class="row">
            <div class="col-sm-5">
                <div class="mc-scroll-top mc-scroll-notice">
                    <div class="bd">
                        <ul class="picList">
                            <li><a href="javascript:;" class="mc-text-link">【严选x中行联名卡】达标领80元礼品卡</a></li>
                            <li><a href="javascript:;" class="mc-text-link">【福利】下载严选APP，抢1元包邮团</a></li>
                            <li><a href="javascript:;" class="mc-text-link">【严选x中行联名卡】达标领80元礼品卡</a></li>
                        </ul>
                    </div>
                </div>
                <script type="text/javascript">
                    jQuery(".mc-scroll-top").slide({titCell:".hd ul",mainCell:".bd ul",autoPage:true,effect:"top",autoPlay:true,vis:1,trigger:"click"});
                </script>
            </div>
            <div class="col-sm-7 text-right">
                {gt name='uid' value='0'}
                <div class="mc-drop-down">
                    <a href="javascript:;" class="mc-text-link">
                        {$loginInfo.username} <i class="arrow iconfont"></i>
                    </a>
                    <ul class="sub-menu">
                        <li> <a href="javascript:;" class="menu-link-item">账户设置</a></li>
                        <li> <a href="javascript:;" class="menu-link-item">订单中心</a></li>
                        <li> <a href="javascript:;" class="menu-link-item">优惠卡券</a></li>
                        <li> <a href="javascript:;" class="menu-link-item">我的积分</a></li>
                        <li> <a href="{:url('home/member.index/exitlogin')}" class="menu-link-item">退出登录</a></li>
                    </ul>
                </div>
                <span class="mc-text-split">|</span>
                <a href="javascript:;" class="mc-text-link" style="position:relative">
                    消息
                    <span style="width:6px;height:6px;border-radius:50%;position:absolute;right:-5px;top:-3px;background:#bd2d28"></span>
                </a>
                <span class="mc-text-split">|</span>
                <div class="mc-drop-down">
                    <a href="javascript:;" class="mc-text-link">签到有礼 <i class="arrow iconfont"></i></a>
                    <div class="sub-menu sign-box text-left" style="width:460px;left:-230px;">
                        <div class="p-d">
                            <div class="mc-step-bar-sm">
                                <ul class="mc-avg-sm-7">
                                    <li class="step-item step-over">
                                        <a href="javascript:;" class="">
                                            <div class="icon-box">
                                                <span class="iconfont-o iconfont-o-xs step-number"></span>
                                                <i class="iconfont icon-finish"></i>
                                            </div>
                                            <p class="text">1天</p>
                                        </a>
                                    </li>
                                    <li class="step-item step-over">
                                        <a href="javascript:;">
                                            <div class="icon-box">
                                                <span class="iconfont-o iconfont-o-xs step-number"></span>
                                                <i class="iconfont icon-finish"></i>
                                            </div>
                                            <p class="text">2天</p>
                                        </a>
                                    </li>
                                    <li class="step-item">
                                        <a href="javascript:;">
                                            <div class="icon-box">
                                                <span class="iconfont-o iconfont-o-xs step-number"></span>
                                                <i class="iconfont icon-finish"></i>
                                            </div>
                                            <p class="text">3天</p>
                                        </a></li>
                                    <li class="step-item">
                                        <a href="javascript:;">
                                            <div class="icon-box">
                                                <span class="iconfont-o iconfont-o-xs step-number"></span>
                                                <i class="iconfont icon-finish"></i>
                                            </div>
                                            <p class="text">4天</p>
                                        </a>
                                    </li>
                                    <li class="step-item">
                                        <a href="javascript:;">
                                            <div class="icon-box">
                                                <span class="iconfont-o iconfont-o-xs step-number"></span>
                                                <i class="iconfont icon-finish"></i>
                                            </div>
                                            <p class="text">5天</p>
                                        </a>
                                    </li>
                                    <li class="step-item">
                                        <a href="javascript:;">
                                            <div class="icon-box">
                                                <span class="iconfont-o iconfont-o-xs step-number"></span>
                                                <i class="iconfont icon-finish"></i>
                                            </div>
                                            <p class="text">6天</p>
                                        </a>
                                    </li>
                                    <li class="step-item">
                                        <a href="javascript:;">
                                            <div class="icon-box">
                                                <span class="iconfont-o iconfont-o-xs step-number"></span>
                                                <i class="iconfont icon-finish"></i>
                                            </div>
                                            <p class="text">7天</p>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="split-line-solid split-line-sm"></div>
                            <a href="javascript:;" class="text-size12 line-height30 text-gray">每天签到送惊喜， 连续签到7天即可获得【10元优惠券】</a>
                            <a href="javascript:;" class="btn btn-main pull-right">立即签到</a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <span class="mc-text-split">|</span>
                <a href="/home/member.index/index.html" class="mc-text-link">会员中心</a>
                {else}
                <a href="/home/public/login.html" class="mc-text-link">登录</a>
                <span class="mc-text-split">|</span>
                <a href="/home/public/reg.html" class="mc-text-link">注册</a>
                {/gt}
                <span class="mc-text-split">|</span>
                <a href="javascript:;" class="mc-text-link">同城社区</a>
                <span class="mc-text-split">|</span>
                <div class="mc-drop-down">
                    <a href="javascript:;" class="mc-text-link">
                        客户服务 <i class="arrow iconfont"></i>
                    </a>
                    <ul class="sub-menu">
                        <li> <a href="javascript:;" class="menu-link-item">在线客服</a></li>
                        <li> <a href="/home/index/you.html?tpl=help" class="menu-link-item">帮助中心</a></li>
                    </ul>
                </div>
                <span class="mc-text-split">|</span>
                <div class="mc-drop-down">
                    <a href="javascript:;" class="mc-text-link">
                        <i class="iconfont icon-shouji2"></i> 手机版 <i class="arrow iconfont"></i>
                    </a>
                    <div class="sub-menu" style="width:100px;">
                        <div class="m-d">
                            <img src="/static/home/pc/images/qrcode.png" class="am-img-responsive" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>