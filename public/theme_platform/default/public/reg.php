{extend name="public:layout" /}
{block name="title"}会员注册{/block}
{block name="head"}{/block}
{block name="body"}
{include file='public/head_base'}
<div class="login-banner">
    <div class="login-box reg-box">
        <div class="m-d-30">
            <h3 class="text-center text-gray">会员注册</h3>
            <div class="height-20"></div>
            <form class="form-horizontal icon-form icon-form-lg" id="form" action="">

                <div class="form-group">
                    <span class="iconfont icon-shouji1"></span>
                    <input type="tel" class="form-control input-lg" name="mobile" placeholder="请输入手机号码" datatype="m" nullmsg="请输入手机号码">
                </div>
                <div class="form-group">
                    <span class="iconfont icon-msnui-auth-code"></span>
                    <div class="input-group input-group-lg">
                        <input type="text" class="form-control" name="vcode" placeholder="请输入验证码">
                        <span class="input-group-btn">
                    <img src="/captcha.html" onclick="$(this).attr('src','/captcha.html?rnd=' + Math.random());" class="img-captcha" />
                </span>
                    </div>
                </div>
                <div class="form-group">
                    <span class="iconfont icon-duanxin"></span>
                    <div class="input-group input-group-lg">
                        <input type="text" class="form-control" name="sms_vcode" placeholder="请输入短信验证码" datatype="*">
                        <span class="input-group-btn">
                            <a href="javascript:;" class="btn btn-white btn-lg getsms"   style="width:120px;height:46px; font-size: 14px; line-height: 25px">发送验证码</a>

                </span>
                    </div>
                </div>
                <div class="form-group">
                    <span class="iconfont icon-mima"></span>
                    <input type="password" class="form-control input-lg" name="password" placeholder="请输入密码" datatype="*">
                </div>
                <div class="form-group">
                    <span class="iconfont icon-mima"></span>
                    <input type="password" class="form-control input-lg" name="password" placeholder="请再次输入密码" datatype="*">
                </div>
                <div class="form-group">
                    <label  class="i-checks pull-left">
                        <input type="checkbox" class="f-icheck " name="agree" value="1" checked>
                    </label>
                    <div class="pull-left" style="margin:2px 0 0 5px;">我同意<a href="javascript:;" class="mc-text-link">用户注册协议</a></div>
                </div>
                <div class="form-group">
                    <button type="button" class="btn btn-sub btn-lg  btn-block btn-main">立即注册</button>
                </div>
            </form>
            <p>
                <a href="/home/public/login.html" class="mc-text-link">已有账号，去登录</a>
                <a href=/home/public/findpassword.html" class="mc-text-link pull-right">忘记密码</a>
            </p>
        </div>

    </div>
</div>
{include file='public/foot'}
{/block}
{block name='script'}
<script>
    $(function () {
        formHelper.initForm();
        utils.ajaxSubForm(function () {
            utils.msg('注册成功！',function () {
                location.href='/home/public/login.html';
            })
        })
    });
    In('layer',function(){
        //获取短信验证码
        $('.getsms').click(function(){
            var o=$(this);
            if(o.hasClass("disabled")) return;
            var mobile=$("input[name='mobile']").val();
            var vcode=$("input[name='vcode']").val();
            if(mobile==""){
                layer.msg('请输入手机号！');
                return false;
            }
            if(vcode==""){
                layer.msg('请输入图形验证码！');
                return false;
            }
            utils.getAjax({
                url:'/platform/public/smsen.html',
                data:{
                    mobile:mobile,
                    vcode:vcode,
                    scene:'reg'
                },
                success:function(res){
                    if(res.code==1){
                        var countdown=179;
                        var iCount=setInterval(function(){
                            o.text(countdown--+"s后重试");
                            o.attr("disabled",true);
                            if(countdown==0){
                                o.removeAttr("disabled");
                                o.text("获取验证码");
                                clearInterval(iCount);//清除倒计时
                                countdown=180;//设置倒计时时间
                                $('.captcha').click();
                            }
                        },1000);
                    }else{
                        layer.msg(res.msg);
                        $('.captcha').click();
                    }
                }
            });
        });
    });
</script>
{/block}