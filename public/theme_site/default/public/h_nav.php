<div class="nav-box">
    <div class="container">
        <div class="mc-category-box pull-left">
            <a href="javascript:;" class="all-link">
                商品分类
            </a>
            <ul class="category-menu">
                {volist name='goods_tree_class' id='vo' offset='0' length='10'}
                <li class="item">
                    <div class="item-main">
                        <span class="block p-d-lr text-size14 line-height45">{$vo.name}</span>
                        <div class="clearfix"></div>
                    </div>
                    {notempty name='vo.children'}
                    <div class="sub-box">
                        {volist name='vo.children' id='v'}
                        <div class="mc-cat-item">
                            <span class="cat-tit">{$v.name}</span>
                            {volist name='v.children' id='item'}
                            <a href="/site/goods/index.html?class_id={$item.id}" class="mc-text-link">{$item.name}</a> 
                            {/volist}
                        </div>
                        {/volist}
                    </div>
                    {/notempty}
                </li>
                {/volist}
            </ul>
        </div>
        <ul id="index-nav" class="mc-index-nav pull-left">
            <li class="nLi"><a href="/" class="menu-link">首页</a></li>
            {volist name='goods_tree_class' id='vo' offset='0' length='8'}
            <li class="nLi {eq name='vo.id' value='$Request.param.class_id'}on{/eq}">
                <a href="/site/goods/index.html?class_id={$vo.id}" class="menu-link">{$vo.name}</a>
                {notempty name='vo.children'}
                <ul class="sub">
                    {volist name='vo.children' id='v'}
                    <li><a href="/site/goods/index.html?class_id={$v.id}" class="sub-link">{$v.name}</a></li>
                    {/volist}
                </ul>
                {/notempty}
            </li>
            {/volist}
            <li class="nLi">
                <a href="javascript:;" class="menu-link">更多</a>
                <ul class="sub">
                    {volist name='goods_tree_class' id='vo' offset='8' length='100'}
                    <li><a href="/site/goods/index.html?class_id={$vo.id}" class="sub-link">{$vo.name}</a></li>
                    {/volist}
                </ul>
            </li>
        </ul>
        <script>
            jQuery("#index-nav").slide({
                type:"menu",// 效果类型，针对菜单/导航而引入的参数（默认slide）
                titCell:".nLi", //鼠标触发对象
                targetCell:".sub", //titCell里面包含的要显示/消失的对象
                effect:"slideDown", //targetCell下拉效果
                delayTime:300 , //效果时间
                triggerTime:0, //鼠标延迟触发时间（默认150）
                returnDefault:true //鼠标移走后返回默认状态，例如默认频道是“预告片”，鼠标移走后会返回“预告片”（默认false）
            });
        </script>
    </div>
</div>