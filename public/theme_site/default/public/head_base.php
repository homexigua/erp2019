<div class="header-box">
    {include file='public/h_top'}
    <div class="header-main" style="padding:25px 0;border-bottom:2px solid #be9a60">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <a href="/"><img src="{$webconfig.logo|default='/static/home/pc/images/logo.png'}" class="height-55 pull-left" alt=""></a>
                    <div class="slogan-box pull-right">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>