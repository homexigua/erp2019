<div class="top-bar">
    <div class="container">
        <div class="row">
            <div class="col-sm-5">
                <div class="mc-scroll-top mc-scroll-notice">
                    <div class="bd">
                        <ul class="picList">
                            <li><a href="javascript:;" class="mc-text-link">【严选x中行联名卡】达标领80元礼品卡</a></li>
                            <li><a href="javascript:;" class="mc-text-link">【福利】下载严选APP，抢1元包邮团</a></li>
                            <li><a href="javascript:;" class="mc-text-link">【严选x中行联名卡】达标领80元礼品卡</a></li>
                        </ul>
                    </div>
                </div>
                <script type="text/javascript">
                    jQuery(".mc-scroll-top").slide({titCell:".hd ul",mainCell:".bd ul",autoPage:true,effect:"top",autoPlay:true,vis:1,trigger:"click"});
                </script>
            </div>
            <div class="col-sm-7 text-right">

                {notempty name='member'}
                <div class="mc-drop-down">
                    <a href="javascript:;" class="mc-text-link">
                        {$member.username} <i class="arrow iconfont"></i>
                    </a>
                    <ul class="sub-menu">
                        <li> <a href="javascript:;" class="menu-link-item">账户设置</a></li>
                        <li> <a href="javascript:;" class="menu-link-item">订单中心</a></li>
                        <li> <a href="javascript:;" class="menu-link-item">优惠卡券</a></li>
                        <li> <a href="javascript:;" class="menu-link-item">我的积分</a></li>
                        <li> <a href="/site/member/loginout.html" class="menu-link-item">退出登录</a></li>
                    </ul>
                </div>
                <span class="mc-text-split">|</span>
                <a href="javascript:;" class="mc-text-link" style="position:relative">
                    消息
                    <span style="width:6px;height:6px;border-radius:50%;position:absolute;right:-5px;top:-3px;background:#bd2d28"></span>
                </a>
                <span class="mc-text-split">|</span>
                <a href="/site/member.member/index.html" class="mc-text-link">会员中心</a>
                {else}
                <a href="/site/public/login.html" class="mc-text-link">登录</a>
                <span class="mc-text-split">|</span>
                <a href="/site/public/reg.html" class="mc-text-link">注册</a>
                {/notempty}
                <span class="mc-text-split">|</span>
                <div class="mc-drop-down">
                    <a href="javascript:;" class="mc-text-link">
                        客户服务 <i class="arrow iconfont"></i>
                    </a>
                    <ul class="sub-menu">
                        <li> <a href="javascript:;" class="menu-link-item">在线客服</a></li>
                        <li> <a href="/site/index/you.html?tpl=help" class="menu-link-item">帮助中心</a></li>
                    </ul>
                </div>
                <span class="mc-text-split">|</span>
                <div class="mc-drop-down">
                    <a href="javascript:;" class="mc-text-link">
                        <i class="iconfont icon-shouji2"></i> 手机版 <i class="arrow iconfont"></i>
                    </a>
                    <div class="sub-menu" style="width:100px;">
                        <div class="m-d">
                            <img src="/static/home/pc/images/qrcode.png" class="am-img-responsive" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>