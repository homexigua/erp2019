{extend name="home@public:layout" /}
{block name="title"}领取优惠劵{/block}
{block name="head"}
<style>
    .g-bd5{margin:0 0 10px;}
    .g-sd51,.g-sd52{position:relative;float:left;width:230px;margin:0 -230px 0 0;}
    .g-sd52{float:right;width:190px;margin:0 0 0 -190px;}
    .g-mn5{float:left;width:100%;}
    .g-mn5c{margin:0 200px 0 240px;}

</style>
{/block}
{block name="body"}
<section class="mc-view-box">
    <div class="mc-header-box">
        <div class="mc-header bg-white">
            <a href="javascript:;" onclick="utils.back()" class="head-icon">
                <i class="iconfont icon-arrow-left"></i>
            </a>
            <div class="main-box head-title text-center">
                领取优惠劵
            </div>
            <a href="/home/goods/category.html" class="head-icon">
                <i class="iconfont icon-tupianliebiao"></i>
            </a>
        </div>
    </div>
    <div class="mc-scrollview">
        <div class="weui-msg">
            <div class="g-bd5 f-cb">
                <div class="g-sd51">
                    <div style="margin: 10px auto";>
                        <h2 style="font-size: 68px" class="weui-msg__title bg-light-blue">20元</h2>
                        <h4 style="font-size: 32px">满300元可用</h4>
                    </div>

                </div>
                <div class="g-mn5">
                    <div class="g-mn5c">
                        <h4 class="weui-msg__title">庆国庆，迎中秋厂家钜惠</h4>
                        <p class="weui-msg__desc">有效期：2019年10月1日~11月<a href="/home/member/rule";>使用规则</a></p>

                    </div>
                </div>
                <div class="g-sd52">

                    <h2 class="weui-msg__title bg-light-blue">    <a href="javascript:;" class="weui-btn weui-btn_primary">未使用</a>
                    </h2>



                </div>
            </div>

        </div>


    </div>
</section>
{/block}
{block name="script"}
{/block}