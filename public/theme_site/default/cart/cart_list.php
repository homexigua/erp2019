<script type="text/html" id="pop_cart_list">
    <a href="/home/cart/index.html" class="cart-link-box2">
        <span class="iconfont icon-gouwugouwuchedinggou"></span>
        {@if num>0}
        <span class="badge badge-primary badge-xs">${data[1]['goods_sum']}</span>
        {@/if}
    </a>
    {@if num>0}
    <div class="cart-main-box2">
        <div class="cart-body">
            <div class="cart-goods-group">
                {@each data[0] as value}
                {@each value.cart_list as val}
                <div class="mc-cart-goods-item">
                    <a href="/home/goods/goodsinfo.html?goods_id=${val.goods_id}" class="goods-info" title="${val.goods_name}">
                        <img class="goods-thumb" src="${val.thumb}" alt="">
                        <div class="text-size14 font-weight500 single-cut">${val.goods_name}</div>
                        <span class="text-size12 text-gray">${val.sku_name}</span>
                    </a>
                    <div class="price-box">
                        <span class="block text-size14">¥${val.sale_price}×${val.cart_num}</span>
                        <a href="javascript:;" class="del">删除</a>
                    </div>
                </div>
                {@/each}
                {@/each}
            </div>
        </div>
        <div class="cart-foot">
            <span class="text-size12 text-gray pull-left">
                商品合计<br>
                <span class="text-size16 text-red">¥ ${data[1]['goods_amount']}</span>
            </span>
            <a href="/home/cart/index.html" class="btn btn-main pull-right">购物车结算</a>
        </div>
    </div>
    {@/if}
</script>