{extend name="public:layout" /}
{block name="title"}购物车_{/block}
{block name="head"}{/block}
{block name="body"}
{include file='public/head'}
<div class="channel-page">
    <div class="container" id="cart-box">
    </div>
</div>
{include file='public/foot'}
{/block}
{block name='script'}
<script type="text/html" id="cart-tpl">
    {@if data==''}
    <div class="height-100"></div>
    <div class="row">
        <div class="col-sm-5">
            <img src="/static/home/pc/images/cart_null.gif" class="am-img-responsive" alt="">
        </div>
        <div class="col-sm-6 col-sm-offset-1">
            <div class="height-60"></div>
            <h3 class="text-size40 text-gray font-weight600">您的购物车还是空的！</h3>
            <p class="text-size16 text-gray">请先添加商品到购物车</p>
            <div class="p-d-tf">
                <a href="/" class="btn btn-warning btn-lg no-radius" style="margin-right:20px;">马上购物</a>
                <a href="/home/member.order/index.html" class="btn btn-info btn-lg no-radius">订单中心</a>
            </div>
        </div>
    </div>
    <div class="height-50"></div>
    {@else}
    <div class="mc-step-drop-bar" style="width:600px;margin:0 auto;">
        <ul class="mc-avg-sm-3">
            <li class="step-item step-half">
                <a href="javascript:;" class="">
                    <div class="icon-box">
                        <span class="step-number">1</span>
                    </div>
                    <p class="text">查看购物车</p>
                </a>
            </li>
            <li class="step-item">
                <a href="javascript:;">
                    <div class="icon-box">
                        <span class="step-number">2</span>
                    </div>
                    <p class="text">确认订单信息</p>
                </a>
            </li>
            <li class="step-item">
                <a href="javascript:;">
                    <div class="icon-box">
                        <span class="step-number">3</span>
                    </div>
                    <p class="text">成功提交订单</p>
                </a></li>
        </ul>
    </div>
    <div class="height-30"></div>
    <table class="mc-cart-list-item list-title bg-gray">
        <tr>
            <td class="checked-box">
            </td>
            <td class="goods-info">商品信息</td>
            <td class="goods-price">单价</td>
            <td class="goods-num">数量</td>
            <td class="total-price">小计</td>
            <td class="action-box">操作</td>
        </tr>
    </table>
    <div id="cart-list-box">
        <div class="shop-item">
            <div class="mc-cart-shop">
                <div class="check-box pull-left">
                    <label class="mc-check-item"">
                    <input type="checkbox" name="selectAll" data-box="shop-item-${data.site_info.site_id}" onclick="doAction.checkAll(this)"/>
                    <span></span>
                    </label>
                </div>
                <a href="http://${data.site_info.domain}" target="_blank" class="shop-name pull-left">
                    <i class="iconfont icon-mendian"></i> ${data['site_info']['site_name']}
                </a>
            </div>
            <div class="mc-cart-list-group" id="shop-item-${data.site_info.site_id}">
                {@each data.item as value,index}
                <table class="mc-cart-list-item">
                    <tr>
                        <td class="checked-box">
                            <label class="mc-check-item">
                                <input type="checkbox" name="select" data-cartid="${value.id}" onclick="doAction.ischeckall(this)" ${value.check}>
                                <span></span>
                            </label>
                        </td>
                        <td class="goods-info">
                            <a href="/site/goods/goodsinfo.html?goods_id=${value.goods_id}" class="mc-width-auto left-box mc-goods-info">
                                <img class="float-box goods-thumb" src="${value.thumb}" alt="">
                                <div class="text-size14 font-weight500 single-cut">${value.goods_name}</div>
                                <span class="text-size12 text-gray">${value.sku_name}</span>
                            </a>
                        </td>
                        <td class="goods-price">
                            <span class="text-size14">¥${value.sale_price}</span>
                        </td>
                        <td class="goods-num">
                              <span class="m-spinner" data-ydui-spinner="">
                                  <a href="javascript:;" class="J_Del"></a>
                                  <input type="text" name="number" class="J_Input" value="${value.goods_number}" placeholder=""/>
                                  <a href="javascript:;" class="J_Add"></a>
                              </span>
                        </td>
                        <td class="total-price">
                            <span class="text-size16 text-red">¥<span id="sub-total-${value.id}">${value.sub_total}</span></span>
                        </td>
                        <td class="action-box">
                            <a href="javascript:;">移入收藏</a><br>
                            <a href="javascript:;" data-type="${value.id}" onclick="doAction.delCart(this)">删除</a>
                        </td>
                    </tr>
                </table>
                <div class="split-line-dashed"></div>
                {@/each}
            </div>
        </div>
        <div class="height-20"></div>
    </div>
    <div class="mc-cart-foot">
        <div class="check-box pull-left">
            <label class="mc-check-item"">
            <input type="checkbox" name="selectAll" data-box="cart-list-box" onclick="doAction.checkAll(this)"/>
            <span></span>
            </label>
        </div>
        <div class="action-box pull-left">
            <a href="javascript:;" class="action-link" data-type="check" onclick="doAction.delCart(this)">删除选中</a>
            <a href="javascript:;" class="action-link">添加到收藏</a>
            <a href="javascript:;" class="action-link">清空失效</a>
        </div>
        <div class="total-box pull-right text-right">
            <a href="javascript:;" onclick="doAction.buy()"  class="btn cart-btn pull-right">立即结算</a>
            <div class="pull-right" style="line-height:20px;padding:5px;">
                已选商品 <b class="text-size16 text-red" id="select_num">0</b> 件
                合计（不含运费）： <span class="text-size14 text-red">￥<b class="text-size18" id="total_money">0.00</b></span>
                <br><span class="text-gray">交易完成可得积分<span id="get_points">0.00</span></span>
            </div>
        </div>
    </div>
    {@/if}
</script>
<script>
    $(function () {
        doAction.getCart();
    });
    var doAction = {
        /*获取购物车列表*/
        getCart:function () {
            /*传递已选择的购物车ids*/
            var cart_ids=localStorage.getItem('cart_ids');
            utils.getAjax({
                url:'index.html',
                data:{cart_ids:cart_ids},
                success:function (res) {
                    console.log(res);
                    var html = juicer($('#cart-tpl').html(),{data:res});
                    $('#cart-box').html(html);
                    doAction.ischeckall();
                }
            })
        },
        /*全选*/
        checkAll:function(obj){
            var box =$(obj).data('box');
            $(obj).is(':checked')?utils.checkAll('#'+box):utils.checkOther('#'+box);
            doAction.ischeckall();
        },
        /*检测是否全选中*/
        ischeckall:function () {
            var check =[];
            $('.mc-cart-list-group').each(function () {
                var sub_check =[];
                $(this).find('input[name="select"]').each(function () {
                    $(this).is(":checked")?sub_check.push(1):sub_check.push(0);
                });
                $(this).closest('.shop-item').find('input[name="selectAll"]').prop('checked',eval(sub_check.join('*'))==1?true:false);
                check.push(eval(sub_check.join('*')))
            })
            $('.mc-cart-foot').find('input[name="selectAll"]').prop('checked',eval(check.join('*'))==1?true:false);
            doAction.cartTotal();
        },
        /*删除购物车*/
        delCart:function (obj) {
            var type=$(obj).data('type');
            var cart_id = type=='check'?localStorage.getItem('cart_ids'):type;
            console.log(type);
            utils.confirm('确认删除吗？',function () {
                utils.getAjax({
                    url:'/site/cart/cartdel.html',
                    data:{cart_id:cart_id},
                    success:function (res) {
                        console.log(res);
                        if(res.code==1){
                            layer.msg('已删除',{icon:1,shift:-1},function () {
                                doAction.getCart();
                            })
                        }else{
                            layer.msg(res.msg,{icon:2})
                        }
                        layer.close(index);
                    }
                })
            })
        },
        /*计算所有选中商品的价格和数量*/
        cartTotal:function () {
            var check=[0];
            var money=[0];
            //var points=[0];
            var cart_ids=[];
            $('#cart-list-box input[name="select"]:checked').each(function () {
                var cart_id =$(this).data('cartid');
                money.push($('#sub-total-'+cart_id).text());
                //points.push($('#points-'+cart_id).text());
                check.push(1);
                cart_ids.push(cart_id);
            })
            localStorage.setItem('cart_ids',cart_ids);
            money = eval(money.join('+'));
            check = eval(check.join('+'));
            //points = eval(points.join('+'));
            $('#select_num').text(check);
            $('#total_money').text(money.toFixed(2));
            //$('#get_points').text(points.toFixed(2));
        },
        /*更新购物车数量*/
        cartNumAction:function(val) {
            console.log(val);
            utils.getAjax({
                url:'/home/cart/cartadd.html',
                data:{sku_sn:sku_sn,number:cart_num,type:'update'},
                success:function (res) {
                    console.log(res);
                }
            });
        },
        buy:function () {
            var cart_ids = localStorage.getItem('cart_ids');
            if(cart_ids==''){
                layer.msg('请选择要购买的商品！',{icon:2});
            }else{
                location.href='/site/cart/orderconfirm.html';
            }
        }
    }
</script>
{/block}