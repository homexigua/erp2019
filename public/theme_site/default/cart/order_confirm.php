{extend name="public:layout" /}
{block name="title"}订单确认_{/block}
{block name="head"}{/block}
{block name="body"}
{include file='public/head'}
<div class="channel-page">
    <div class="container">
        <div class="mc-step-drop-bar" style="width:600px;margin:0 auto;">
            <ul class="mc-avg-sm-3">
                <li class="step-item step-over">
                    <a href="javascript:;" class="">
                        <div class="icon-box">
                            <span class="step-number">1</span>
                        </div>
                        <p class="text">查看购物车</p>
                    </a>
                </li>
                <li class="step-item step-half">
                    <a href="javascript:;">
                        <div class="icon-box">
                            <span class="step-number">2</span>
                        </div>
                        <p class="text">确认订单信息</p>
                    </a>
                </li>
                <li class="step-item">
                    <a href="javascript:;">
                        <div class="icon-box">
                            <span class="step-number">3</span>
                        </div>
                        <p class="text">成功提交订单</p>
                    </a></li>
            </ul>
        </div>
        <div class="height-30"></div>
        <form  id="form">
            <input type="hidden" name="use_points" value="0" datatype="*">
            <input type="hidden" name="address_id" value="0" datatype="*">
            <input type="hidden" name="is_zt" value="0" datatype="*" nullmsg="请选择配送方式">
            <div class="panel panel-sm no-radius panel-default">
                <div class="panel-heading">
                    配送方式
                </div>
                <div class="panel-body">
                    <div class="height-20"></div>
                    <div class="clearfix">
                        <a href="javascript:;" data-type="address" onclick="doAction.shipping(this)" class="pay-item select-item active">
                            <svg class="icon" aria-hidden="true">
                                <use xlink:href="#icon-mianyunfei"></use>
                            </svg>
                            <span>快递配送</span>
                        </a>
                        <a href="javascript:;" data-type="is_zt" onclick="doAction.shipping(this)" class="pay-item select-item">
                            <svg class="icon" aria-hidden="true">
                                <use xlink:href="#icon-shangpin2"></use>
                            </svg>
                            <span>到店自提</span>
                        </a>
                    </div>
                    <div class="height-20"></div>
                    <div class="m-d" id="address-box">
                        <div class="row">
                            <div class="col-sm-8" id="address-info" style="border-right:1px solid #e8e8e8"></div>
                            <div class="col-sm-4 text-center">
                                <div class="height-10"></div>
                                <a href="javascript:;" onclick="addrAction.selectAddr()" class="btn btn-white">切换地址</a>
                                <a href="javascript:;" onclick="addrAction.addAddr()" class="btn btn-white">新建地址</a>
                                <div class="height-10"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="shop-item-box"></div>
            <div class="split-line-dashed"></div>
            <div class="row">
                <div class="col-sm-8"></div>
                <div class="col-sm-4">
                    <table class="line-height30 text-right" style="width:100%;">
                        <tr>
                            <td><span class="text-red" id="total_goods_sum"></span>件商品，总商品金额</td>
                            <td><span class="text-size16">￥<span id="total_goods_amount"></span></span></td>
                        </tr>
                        <tr>
                            <td>运费总计</td>
                            <td><span class="text-size16">￥<span id="total_freight">0</span></span></td>
                        </tr>
                        <tr class="discount-box">
                            <td>优惠金额</td>
                            <td><span class="text-size16">-￥<span id="total_discount_money">0.00</span></span></td>
                        </tr>
                        <tr>
                            <td>应付总额</td>
                            <td>
                                <b class="text-size24 text-red">￥<span id="total_order_money"></span></b>
                            </td>
                        </tr>
                        <tr id="total_getpoints_box">
                            <td colspan="2">
                                <div class="text-size12 text-gray get_points">可获得积分<span class="text-red" id="total_points"></span>个</div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <a href="javascript:;" class="btn btn-sub btn-main btn-lg no-radius">提交订单</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </form>
    </div>
</div>
{include file='public/foot'}
{include file='member/address/add'}
{include file='member/address/edit'}
{include file='member/address/list'}
{/block}
{block name='script'}
<script src="/lib/home/address.js"></script>
<script type="text/html" id="address-info-tpl">
    {@if data!='0'}
    <div class="row line-height24 text-size12 text-gray">
        <div class="col-sm-2">收件人：</div>
        <div class="col-sm-10">${data.consignee_name}  <a href="javascript:;" data-info="${json2string(data)}" onclick="addrAction.editAddr(this)" class="text-gray pull-right" style="margin-right:10px;"><i class="iconfont icon-fankui1"></i>编辑</a></div>
        <div class="col-sm-2">联系电话：</div>
        <div class="col-sm-10">${data.consignee_phone}</div>
        <div class="col-sm-2">收货地址：</div>
        <div class="col-sm-10">${data.prov}${data.city}${data.dist}${data.address}</div>
    </div>
    {@else}
    <div class="row line-height24 text-size12 text-gray">
        <div class="height-10"></div>
        <p class="m-d-20">您还没有添加收货地址，请先添加后选择！</p>
        <div class="height-10"></div>
    </div>
    {@/if}
</script>
<script type="text/html" id="shop-item-tpl">
    {@each data as value,index}
    <div class="panel panel-sm no-radius panel-default shop-item">
        <div class="panel-heading">
            <a href="/home/goods/shopindex.html?shop_id=${value.shopinfo.shop_id}" class="text-size14 text-black">
                <i class="iconfont icon-mendian"></i> ${value.shopinfo.shop_name}
            </a>
        </div>
        <div class="panel-body">
            <div class="shop-item">
                <table class="mc-cart-list-item list-title bg-gray">
                    <tr>
                        <td class="goods-info">商品信息</td>
                        <td class="goods-price">单价</td>
                        <td class="goods-num">数量</td>
                        <td class="total-price">小计</td>
                    </tr>
                </table>
                <div class="height-20"></div>
                <div class="mc-cart-list-group">
                    <table class="mc-cart-list-item">
                        {@each value.cart_list as goods}
                        <input type="hidden" name="goods[${index}][goods_list][${goods.sku_sn}]" value="${goods.cart_num}">
                        <tr>
                            <td class="goods-info">
                                <a href="/home/goods/goodsinfo.html?shop_id=${value.shopinfo.shop_id}&goods_id=${goods.goods_id}" class="mc-width-auto left-box mc-goods-info">
                                    <img class="float-box goods-thumb" src="${goods.thumb}" alt="">
                                    <div class="text-size14 font-weight500 single-cut">${goods.goods_name}</div>
                                    <span class="text-size12 text-gray">${goods.sku_name}</span>
                                </a>
                            </td>
                            <td class="goods-price">
                                <span class="text-size14">¥${moneyFormat(goods.sale_price)}</span>
                            </td>
                            <td class="goods-num">
                                <span class="text-size14">${goods.cart_num}</span>
                            </td>
                            <td class="total-price">
                                <span class="text-size16 text-red">¥${moneyFormat(goods.sub_total)}</span>
                            </td>
                        </tr>
                        <tr><td colspan="4" style="height:15px;"></td></tr>
                        {@/each}
                    </table>
                    <div class="split-line-dashed" style="margin-left:0;"></div>
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label font-weight500">备注</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control input-sm" name="goods[${index}][bill_remark]" placeholder="请输入订单备注">
                                    </div>
                                </div>
                                {@if value.coupon_list!=''}
                                <div class="form-group">
                                    <label class="col-sm-2 control-label font-weight500">优惠券</label>
                                    <div class="col-sm-6">
                                        <select class="form-control input-sm coupon-select" name="goods[${index}][coupon_id]" onchange="doAction.itemTotal()">
                                            <option value="">请选择优惠券</option>
                                            {@each value.coupon_list as coupon}
                                            <option value="${coupon.id}" data-money="${coupon.card_money}">${coupon.card_title}[${coupon.card_sub_title}]</option>
                                            {@/each}
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                    </div>
                                </div>
                                {@/if}
                                <div class="form-group points_box">
                                    <label class="col-sm-2 control-label font-weight500">积分</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control input-sm input-points" name="goods[${index}][points]" onblur="doAction.pointsAction(this)" placeholder="请输入要抵扣的积分数量" data-max="${value.total.usable_points}">
                                    </div>
                                    <div class="col-sm-4">
                                        <span class="text-size12 line-height30 text-gray">积分余额 <span class="text-size16 text-red points_sum">${moneyFormat(points)}</span> 个</span>
                                        <a href="javascript:;" onclick="utils.msg('使用积分进行抵扣后，本订单将不再进行积分')" title="使用积分进行抵扣后，本订单将不再进行积分！" style="font-size:14px;" class="iconfont icon-wenhao text-danger"></a>
                                    </div>
                                </div>
                                <div class="form-group black_box">
                                    <label class="col-sm-2 control-label font-weight500">黑卡</label>
                                    <div class="col-sm-6">
                                        <input type="hidden" class="form-control input-sm input-black-key" name="goods[${index}][black_card]"  readonly>
                                        <input type="text" class="form-control input-sm input-black" name="goods[${index}][black_card_money]" placeholder="请选择黑卡抵用现金" readonly>
                                    </div>
                                    <div class="col-sm-4">
                                        <span class="text-size12 line-height34 text-gray">
                                            黑卡金额 <span class="text-size16 text-red">￥<span class="black_card_money">${moneyFormat(black_money)}</span></span> 元
                                        </span>
                                        <a href="javascript:;" onclick="doAction.blackAction(this)" data-index="${index}" class="btn btn-main btn-xs">选择黑卡</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <table class="line-height30 text-right" style="width:100%;">
                                <tr>
                                    <td>商品总价</td>
                                    <td><span class="text-size16">￥<span class="goods_amount">${moneyFormat(value.total.goods_amount)}</span></span></td>
                                </tr>
                                <tr>
                                    <td>运费</td>
                                    <td><span class="text-size16">￥<span class="freight_money">0.00</span></span></td>
                                </tr>
                                <tr class="discount-box">
                                    <td>优惠金额</td>
                                    <td><span class="text-size16">-￥<span class="discount_money">0.00</span></span></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div class="text-size12 text-gray discount_text"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>合计</td>
                                    <td>
                                        <b class="text-size24 text-red">￥<span class="item-total">${moneyFormat(value.total.goods_amount)}</span></b>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div class="text-size12 text-gray get_points">可获得积分<span class="text-red">${moneyFormat(value.total.get_points)}</span>个</div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {@/each}
</script>
<script type="text/html" id="black-card-tpl">
    <div class="black-card-box" style="position:relative;width:100%;height:100%;overflow-y:auto;padding-bottom:50px;">
        <div class="m-d-30" id="address_list">
            {@if rows==''}
            <div class="mc-no-msg">
                <span class="iconfont icon-duanxin"></span>
                <p class="text">没有可用的黑卡</p>
            </div>
            {@else}
            <ul class="mc-avg-sm-3 mc-avg-between-lg" id="select-black-box">
                {@each rows as value}
                {@if value.disabled==0}
                <li>
                    <label class="mc-check-item">
                        <input type="checkbox" name="card_key" value="${value.card_key}" onclick="doAction.selectBlack(this)" data-money="${value.card_money}" data-index="${index}" ${value.checked}>
                        <div class="mc-coupon-black" style="height:163px;position:relative;">
                            <div class="money text-center">
                                <div class="text-size40 single-cut">
                                    ${value.card_money}<b class="text-size14">元</b>
                                </div>
                            </div>
                            <div class="coupon-number text-size16">SN.${value.card_key}</div>
                            <div class="text-size12 line-height36 text-right">${value.begin_time}-${value.end_time}</div>
                        </div>
                    </label>
                </li>
                {@/if}
                {@/each}
            </ul>
            {@/if}
        </div>
    </div>
    <div class="fixed-foot" style="width:100%;padding:10px 20px;position:absolute;bottom:0;left:0;background:#f8f8f8;border-top:1px solid #e8e8e8">
        <div class="pull-left line-height40">
            已选择 ￥<span id="select-black-money">0.00</span>
        </div>
        <div class="pull-right">
            <button type="button" class="btn btn-info" onclick="doAction.itemTotal()">确认</button>
        </div>
    </div>
</script>
<script>
    var address_scene='buy';
    var points = "{$user_info.points}";//积分
    var black_money = "{$user_info.black_money}";//黑卡的钱
    $(function () {
        formHelper.initForm();
        doAction.getGoods();
        addrAction.getAddress();
        utils.ajaxSubForm(function (res) {
            console.log(res);
        })
    })
    var doAction = {
        getGoods:function () {
            var cart_ids = localStorage.getItem('cart_ids');
            utils.getAjax({
                url:'/site/cart/index.html',
                data:{scene:'buy',cart_ids:cart_ids},
                success:function (res) {
                    console.log(res);
                    if(typeof(res[0])=='undefined'){
                       /* utils.msg('订单商品为空',2,function () {
                            location.href='/home/cart/index.html';
                        })*/
                    }
                    var html=juicer($('#shop-item-tpl').html(),{data:res});
                    $('#shop-item-box').html(html);
                    $('#total_goods_sum').text(res[1]['goods_sum']);
                    $('#total_goods_amount').text(utils.moneyFormat(res[1]['goods_amount']));
                    $('#total_order_money').text(utils.moneyFormat(res[1]['goods_amount']));
                    $('#total_points').text(res[1]['get_points']);
                }
            })
        },
        /*配送方式*/
        shipping:function (obj) {
            var type=$(obj).data('type');
            $(obj).addClass('active').siblings().removeClass('active');
            if(type=='is_zt'){
                $('#address-box').hide();
                $('input[name="is_zt"]').val(1);
                $('input[name="address_id"]').val(0);
            }else{
                $('#address-box').show();
                addrAction.getAddress('buy');
            }
        },
        /*积分操作*/
        pointsAction:function (obj) {
            var value= $(obj).val();
            console.log(value);
            var max =$(obj).data('max');//最多可用多少积分
            if(typeof(value) != "undefined" && !valid.isFloat(value)){
                utils.msg('必须输入数字！',2);
                $(obj).val('');
                return false;
            }
            if(value>max){
                utils.msg('最多可以使用'+max+'进行抵扣');
                $(obj).val('');
                return false;
            }
            doAction.monitorPoints();
            if(doAction.monitorPoints()==false){
                $(obj).val('');
            }
            doAction.itemTotal();
        },
        /*检测积分变动*/
        monitorPoints:function () {
            var input_value=0;
            $('.input-points').each(function (i,v) {
                var c= $(this).val()==''?0:$(this).val();
                input_value+=parseFloat(c);
            })
            if(input_value>points){
                utils.msg('积分不足');
                return false;
            }else{
                $('.points_sum').text(utils.moneyFormat(parseFloat(points)-parseFloat(input_value)));
                input_value>0?$('.get_points').hide():$('.get_points').show();
                $('input[name="use_points"]').val(input_value);
                return true;
            }
        },
        /*计算单店金额*/
        itemTotal:function () {
            $('.shop-item').each(function (i,v) {
                /*优惠减免计算*/
                var coupon_money = $(this).find('.coupon-select option:selected').data('money');//优惠券金额
                var use_points = $(this).find('.input-points').val();//积分金额
                var use_black_money = $(this).find('.input-black').val();//黑卡金额
                coupon_money = typeof(coupon_money) == "undefined"?0:coupon_money;
                use_points = use_points==''?0:use_points;
                use_black_money = use_black_money==''?0:use_black_money;
                var discount_money =parseFloat(coupon_money)+parseFloat(use_points)+parseFloat(use_black_money);
                discount_money>0?$(this).find('.discount-box').show():$(this).find('.discount-box').hide();
                $(this).find('.discount_money').text(discount_money);
                var html ='';
                html+=coupon_money>0?'优惠券抵扣<span class="text-red">￥'+coupon_money+'</span> ':'';
                html+=use_points>0?'积分抵扣<span class="text-red">￥'+use_points+'</span> ':'';
                html+=use_black_money>0?'黑卡抵扣<span class="text-red">￥'+use_black_money+'</span> ':'';
                discount_money>0?$(this).find('.discount_text').html(html):'';
                /*单个店铺计算*/
                var goods_amount = $(this).find('.goods_amount').text();
                goods_amount = goods_amount==''?0:goods_amount.replace(',','');
                var freight_money = $(this).find('.freight_money').text();
                freight_money = freight_money==''?0:freight_money.replace(',','');
                var item_total = parseFloat(goods_amount)+parseFloat(freight_money)-discount_money;
                item_total=item_total<0?0:item_total;
                $(this).find('.item-total').text(utils.moneyFormat(item_total));
            });
            doAction.total();
            layer.closeAll();
        },
        /*计算总费用*/
        total:function () {
            var total_goods_amount=[];
            var total_freight_money=[];
            var total_discount_money=[];
            $('.goods_amount').each(function () {
                var value=$(this).text();
                total_goods_amount.push(value.replace(',',''));
            });
            $('.freight_money').each(function () {
                var value=$(this).text();
                total_freight_money.push(value.replace(',',''));
            });
            $('.discount_money').each(function () {
                var value=$(this).text();
                total_discount_money.push(value.replace(',',''));
            })
            total_goods_amount=eval(total_goods_amount.join('+'));
            total_freight_money=eval(total_freight_money.join('+'));
            total_discount_money=eval(total_discount_money.join('+'));
            var total_order_money = parseFloat(total_goods_amount)+parseFloat(total_freight_money)-parseFloat(total_discount_money);
            $('#total_freight').text(utils.moneyFormat(total_freight_money));
            $('#total_discount_money').text(utils.moneyFormat(total_discount_money));
            $('#total_order_money').text(utils.moneyFormat(total_order_money));
            console.log(total_discount_money);
        },
        /*选择黑卡弹窗*/
        blackAction:function (obj) {
            var index = $(obj).data('index');
            var params = {filter:{'is_use':0,'status':1},op:{'is_use':'=','status':'='}};
            utils.getAjax({
                url:'/home/member.black_card/getlist.html',
                data:params,
                success:function (res) {
                    console.log(res);
                    /*加工数据，过滤掉另外店铺已选择黑卡*/
                    doAction.formatBlack(res.data,index);
                    var html = juicer($('#black-card-tpl').html(),{rows:res.data,index:index});
                    layer.open({
                        type: 1,
                        title:'选择黑卡进行抵扣',
                        skin: 'layui-layer-rim', //加上边框
                        area: ['800px', '600px'], //宽高
                        content: html,
                        success:function () {
                            doAction.getBlack(index);
                        }
                    });
                }
            })
        },
        /*选择黑卡*/
        selectBlack:function (obj) {
            var index=$(obj).data('index');
            doAction.getBlack(index);
        },
        /*获取选中的黑卡*/
        getBlack:function (index) {
            var value= utils.getCheckboxValue('#select-black-box');
            var black_card_money=[0];
            $('#select-black-box').find("input[type='checkbox']").each(function () {
                if ($(this).prop("checked")) {
                    var money = $(this).data('money');
                    black_card_money.push(money)
                }
            });
            black_card_money =eval(black_card_money.join('+'));
            $('input[name="goods['+index+'][black_card]"]').val(value);
            $('input[name="goods['+index+'][black_card_money]"]').val(utils.moneyFormat(black_card_money));
            $('#select-black-money').text(utils.moneyFormat(black_card_money));
        },
        /*格式化可选黑卡*/
        formatBlack:function (data,index) {
            var other_black_key=[];
            /*过滤其他店铺已选择的黑卡*/
            $('.input-black-key').each(function (i,v) {
                var name=$(this).attr('name');
                if(name!='goods['+index+'][black_card]') other_black_key.push($(this).val());
            });
            other_black_key = other_black_key.join(',');
            var selected = $('input[name="goods['+index+'][black_card]"]').val();
            $.each(data,function (i,v) {
                data[i]['disabled']=other_black_key.indexOf(v['card_key']) != -1?'1':'0';
                data[i]['checked']=selected.indexOf(v['card_key']) != -1?'checked':'';
            });
            return data;
        }
    }
</script>
{/block}