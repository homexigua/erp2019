{extend name="public:layout" /}
{block name="title"}{/block}
{block name="head"}{/block}
{block name="body"}
{include file='public/head'}
<input type="hidden" name="class_id" value="{$Request.param.class_id}">
<div class="channel-page">
    <div class="container">
        <div id="location_cat" style="display:none">
            <ol class="breadcrumb">
                <li><a href="/">首页</a></li>
                {volist name='location_cat' id='vo'}
                <li><a href="/site/goods/index.html?class_id={$vo.id}">{$vo.name}</a></li>
                {/volist}
            </ol>
        </div>
        <div id="searchBox"></div>
    </div>
    <div class="height-20"></div>
    <div class="container">
        <div class="mc-sort-box">
            <div class="btn-group sort-btn-group pull-left" role="group" >
                <button type="button" class="btn btn-white active"> 新品 </button>
                <button type="button" class="btn btn-white"> 综合 </button>
                <button type="button" class="btn btn-white"> 销量  </button>
                <button type="button" class="btn btn-white asc"> 价格  <i class="iconfont icon-jiantou1"></i></button>
            </div>
            <div class="page-box pull-right">
                <span class="text-red">1</span>/<span>589</span>
                <div class="btn-group" role="group" style="margin-left:10px;" >
                    <button type="button" class="btn btn-white">上一页</button>
                    <button type="button" class="btn btn-white">下一页</button>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="height-20"></div>
    <div class="container">
        <div id="goods-list"></div>
        <div id="page"></div>
    </div>
</div>
{include file='public/foot'}
{/block}
{block name='script'}
<script type="text/html" id="goods-tpl">
    {@if rows==''}
    <div class="mc-no-msg">
        <div class="iconfont icon-duanxin"></div>
        <p class="text">没有相关商品</p>
    </div>
    {@else}
    <ul class="mc-avg-sm-5 mc-avg-between-lg">
        {@each rows as value,index}
        <li>
            <a href="/site/goods/goodsinfo.html?goods_id=${value.id}" class="mc-goods-item2 {@if value.alubm_num>1}album-slide{@/if}">
                <div class="goods-thumb">
                    <div class="mc-goods-album-slide">
                        {@each value.album as album}
                        <img src="${album}" class="thumb" alt="">
                        <img src="${album}" class="big-img" alt="">
                        {@/each}
                    </div>
                </div>
                <div class="goods-info">
                    <p class="text-size16 text-red pull-left">
                        ¥ ${value.sku_first.sale_price}
                        <span class="del-line text-gray text-size12">¥ ${value.sku_first.market_price}</span>
                    </p>
                    <div class="clearfix"></div>
                    <h3 class="text-size14 line-height24 two-cut">${value.goods_name}</h3>
                </div>
            </a>
        </li>
        {@/each}
    </ul>
    {@/if}
</script>
<script>
    $(function () {
        var title = $('#location_cat').html();
        var commonSearchData = {
            url : "",//查询url
            title : title,//这里可以放当前位置
            dataList : [{
                id: "class_id",
                title: "分类",
                selectorList: [{
                        id: "11",
                        name: "加碘"
                    },
                    {
                        id: "12",
                        name: "未加碘"
                    }],
                multipleSelect: false,
                more: true
            },{
                id: "brand_id",
                title: "品牌",
                selectorList: [{
                    id: "1",
                    name: "晋盐"
                },
                    {
                        id: "2",
                        name: "井盐"
                    },{
                        id: "3",
                        name: "嘎嘎"
                    },{
                        id: "4",
                        name: "哈哈"
                    }],
                multipleSelect: true,
                more: true
            }],
            total:0
        };
        $("#searchBox").commonSearch(commonSearchData,function(param){
            //TODO 查询回调函数
            console.log(param);
        });
        initPage();
    })
    function initPage() {
        var class_id = $('input[name="class_id"]').val();
        utils.initPage({
            dataSource: 'index.html', //默认取rows和total
            params:{class_id:class_id},
            /* header:function(currentPage,totalPage,totalNumber){
                 $('#total-num').text(totalNumber);
                 $('#pagebox').html('<span class="page-num">'+currentPage+'</span>/'+totalPage);
             },*/
            callback: function(data, pagination) {
                console.info(data);
                var html=juicer($('#goods-tpl').html(),{rows:data});
                $('#goods-list').html(html);
            }
        });
    }
</script>
{/block}