{extend name="public:layout" /}
{block name="title"}{$shop_info.config.sho_name|default=$shop_info.shop_name}{/block}
{block name="head"}{/block}
{block name="body"}
{include file='public/head_shop'}
<div class="height-20"></div>
<div class="container shop-index-container">
    {include file='goods/shop_info'}
    <div class="side-right-box">
        <div id="searchBox"></div>
        <div class="mc-sort-box">
            <div class="btn-group sort-btn-group pull-left" role="group" >
                <button type="button" class="btn btn-white active"> 新品 </button>
                <button type="button" class="btn btn-white"> 综合 </button>
                <button type="button" class="btn btn-white"> 销量  </button>
                <button type="button" class="btn btn-white asc"> 价格  <i class="iconfont icon-jiantou1"></i></button>
            </div>
            <div class="page-box pull-right">
                <span class="text-red">1</span>/<span>589</span>
                <div class="btn-group" role="group" style="margin-left:10px;" >
                    <button type="button" class="btn btn-white">上一页</button>
                    <button type="button" class="btn btn-white">下一页</button>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="height-20"></div>
        <div id="goods-list"></div>
        <div id="page"></div>
    </div>
</div>
{include file='public/foot'}
{/block}
{block name='script'}
<script type="text/html" id="goods-tpl">
    <ul class="mc-avg-sm-4 mc-avg-between-lg">
        {@each rows as value,index}
        <li>
            <a href="/home/goods/goodsinfo.html?shop_id=${value.shop_id}&goods_id=${value.id}" class="mc-goods-item2 {@if value.alubm_num>1}album-slide{@/if}">
                <div class="goods-thumb">
                    <div class="mc-goods-album-slide">
                        {@each value.album as album}
                        <img src="${album}" class="thumb" alt="">
                        <img src="${album}" class="big-img" alt="">
                        {@/each}
                    </div>
                </div>
                <div class="goods-info">
                    <p class="text-size16 text-red pull-left">
                        ¥ ${value.sku_first.sale_price}
                        <span class="del-line text-gray text-size12"> ¥${value.sku_first.market_price}</span>
                    </p>
                    <div class="clearfix"></div>
                    <h3 class="text-size14 line-height24 two-cut">${value.goods_name}</h3>
                </div>
            </a>
        </li>
        {@/each}
    </ul>
</script>
<script>
    $(function () {
        formHelper.initForm();
        var title ='全部商品';
        var commonSearchData = {
            url : "",//查询url
            title : title,//这里可以放当前位置
            dataList : [{
                id: "demo1",
                title: "裙型",
                selectorList: [{
                    id: "selector1",
                    name: "蛋糕裙"
                },
                    {
                        id: "selector2",
                        name: "铅笔裙"
                    },{
                        id: "selector3",
                        name: "伞裙"
                    },{
                        id: "selector4",
                        name: "仙女裙"
                    },{
                        id: "selector5",
                        name: "运动群"
                    },{
                        id: "selector6",
                        name: "鱼尾裙"
                    }, {
                        id: "selector7",
                        name: "蓬蓬裙"
                    },{
                        id: "selector8",
                        name: "花苞裙"
                    },{
                        id: "selector9",
                        name: "A字裙"
                    },{
                        id: "selector10",
                        name: "不对称"
                    },{
                        id: "selector11",
                        name: "褶皱裙"
                    }],
                multipleSelect: false,
                more: true
            },{
                id: "demo2",
                title: "版型",
                selectorList: [{
                    id: "selector21",
                    name: "紧身"
                },
                    {
                        id: "selector22",
                        name: "修身"
                    },{
                        id: "selector23",
                        name: "常规"
                    },{
                        id: "selector24",
                        name: "直筒"
                    },{
                        id: "selector25",
                        name: "宽松"
                    },{
                        id: "selector26",
                        name: "收腰"
                    }],
                multipleSelect: true,
                more: true
            },
                {
                    id: "demo3",
                    title: "裙长",
                    selectorList: [{
                        id: "selector31",
                        name: "长裙"
                    },
                        {
                            id: "selector32",
                            name: "中长裙"
                        },{
                            id: "selector33",
                            name: "短裙"
                        },{
                            id: "selector34",
                            name: "超短裙"
                        },{
                            id: "selector35",
                            name: "前短后长"
                        },{
                            id: "selector36",
                            name: "其他"
                        }],
                    multipleSelect: false,
                    more: false
                }],
            total:0
        };
        $("#searchBox").commonSearch(commonSearchData,function(param){
            //TODO 查询回调函数
            console.log(param);
        });
        initPage();
    })
    function initPage() {
        utils.initPage({
            dataSource: 'pagelist.html', //默认取rows和total
            /*params:params,*/
            /* header:function(currentPage,totalPage,totalNumber){
                 $('#total-num').text(totalNumber);
                 $('#pagebox').html('<span class="page-num">'+currentPage+'</span>/'+totalPage);
             },*/
            callback: function(data, pagination) {
                console.info(data);
                var html=juicer($('#goods-tpl').html(),{rows:data});
                $('#goods-list').html(html);
            }
        });
    }
</script>
{/block}