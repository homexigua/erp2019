{extend name="public:layout" /}
{block name="title"}{$current_cat.name}{/block}
{block name='description'}{$description}{/block}
{block name='keywords'}{$keyword}{/block}
{block name="head"}{/block}
{block name="body"}
{include file='public/head'}
<div class="mc-full-slide" style="height:380px;">
    <div class="bd">
        <ul>
            <li _src="url(/static/home/pc/images/2.jpg)" style="background:#DED5A1 center 0 no-repeat;"><a href="javascript:;"></a></li>
            <li _src="url(/static/home/pc/images/3.jpg)" style="background:#B8CED1 center 0 no-repeat;"><a href="javascript:;"></a></li>
            <li _src="url(/static/home/pc/images/4.jpg)" style="background:#98918E center 0 no-repeat;"><a href="javascript:;"></a></li>
        </ul>
    </div>
    <div class="hd"><ul></ul></div>
    <span class="prev"><i class="iconfont icon-arrow-left"></i></span>
    <span class="next"><i class="iconfont icon-arrow-right"></i></span>
</div>
<script>
    /* 控制左右按钮显示 */
    jQuery(".mc-full-slide").hover(function(){ jQuery(this).find(".prev,.next").stop(true,true).fadeTo("show",0.5) },function(){ jQuery(this).find(".prev,.next").fadeOut() });
    /* 调用SuperSlide */
    jQuery(".mc-full-slide").slide({ titCell:".hd ul", mainCell:".bd ul", effect:"fold",  autoPlay:true, autoPage:true, trigger:"click",
        startFun:function(i){
            var curLi = jQuery(".mc-full-slide .bd li").eq(i); /* 当前大图的li */
            if( !!curLi.attr("_src") ){
                curLi.css("background-image",curLi.attr("_src")).removeAttr("_src") /* 将_src地址赋予li背景，然后删除_src */
            }
        }
    });
</script>
<div class="channel-page">
    <div class="container">
        <ul class="mc-avg-sm-2 mc-avg-between-lg">
            <li><a href="javascript:;"><img class="img-responsive" src="/static/home/pc/images/hot_sort.jpg" /></a></li>
            <li><a href="javascript:;"><img class="img-responsive" src="/static/home/pc/images/brand.jpg" /></a></li>
        </ul>
    </div>
    <div class="container">

    </div>
    <div class="container">
        {notempty name='shop_list'}
        <div class="mc-index-title">
            <a href="javascript:;" class="tit">
                全部品牌
            </a>
        </div>
        <ul class="mc-avg-sm-2 mc-avg-between-lg">
            {volist name='shop_list' id='vo'}
            <li>
                <a href="/home/goods/shopindex.html?shop_id={$vo.id}" class="mc-brand-item">
                    <div class="brand-img mc-fade-in2">
                        <img src="{$vo.config.shop_logo}" class="am-img-responsive" alt="">
                        <span  onclick="alert('添加收藏')" class="mc-collect-link"></span>
                    </div>
                    <div class="brand-info">
                        <span class="shop-name">{$vo.config.shop_slogan|default=$vo.shop_name}</span>
                    </div>
                </a>
            </li>
            {/volist}
        </ul>
        {/notempty}
    </div>
</div>
{include file='public/foot'}
{/block}