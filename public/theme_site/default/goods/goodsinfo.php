{extend name="public:layout" /}
{block name="title"}{/block}
{block name="head"}
<link rel="stylesheet" href="/static/home/pc/plugins/cloudzoom/cloudzoom.css">
<script type="text/javascript" src="/static/home/pc/plugins/cloudzoom/cloudzoom.js"></script>
<script type="text/javascript">
    CloudZoom.quickStart();
</script>
<style>
    .goods-details-item p{margin:0;}
    .cloudzoom-zoom{background:#f4f4f4;}
    .step_price_box{background:#fff;padding:10px;border:1px solid #e8e8e8;width:240px;position:absolute;left:350px;top:0;z-index:99;
        box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);-webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);
        transform:translateY(-50%);display:none;
    }
    .step_price_box .table{margin-bottom:0;}
</style>
{/block}
{block name="body"}
{include file='public/head'}
<div class="channel-page">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="/">首页</a></li>
            {volist name='location_cat' id='vo'}
            <li><a href="/site/goods/index.html?class_id={$vo.id}">{$vo.name}</a></li>
            {/volist}
            <li class="active">{$info.goods_name}</li>
        </ol>
        <div class="goods-info-head clearfix">
            <div class="goods-album-box">
                <div class="mc-zoom-focus">
                    <ul class="bigImg">
                        {volist name='info.album' id='vo'}
                        <li>
                            <img title="鼠标滚轮向上或向下滚动，能放大或缩小图片哦~" class="cloudzoom"
                                 src="{$vo}"
                                 data-cloudzoom="zoomSizeMode:'image',zoomImage: '{$vo}',autoInside: 30"/>
                        </li>
                        {/volist}
                    </ul>
                    <div class="smallScroll">
                        <a class="sPrev" href="javascript:void(0)"><span class="mc-arrow"></span></a>
                        <div class="smallImg">
                            <ul>
                                {volist name='info.album' id='vo'}
                                <li><a><img src="{$vo}" /></a></li>
                                {/volist}
                            </ul>
                        </div>
                        <a class="sNext" href="javascript:void(0)"><span class="mc-arrow"></span></a>
                    </div>
                </div>
                <script type="text/javascript">
                    //大图切换
                    jQuery(".mc-zoom-focus").slide({ titCell:".smallImg li", mainCell:".bigImg", effect:"fold", autoPlay:false,delayTime:200,
                        startFun:function(i,p){
                            //控制小图自动翻页
                            if(i==0){ jQuery(".mc-zoom-focus .sPrev").click() } else if( i%4==0 ){ jQuery(".mc-zoom-focus .sNext").click()}
                        }
                    });
                    //小图左滚动切换
                    jQuery(".mc-zoom-focus .smallScroll").slide({ mainCell:"ul",delayTime:100,vis:4,scroll:4,effect:"left",autoPage:true,prevCell:".sPrev",nextCell:".sNext",pnLoop:false });
                </script>
            </div>
            <div class="goods-info-box">
                <h3 class="text-size24">{$info.goods_name}</h3>
                <p class="text-size12 text-gray">{$info.desc}</p>
                <div class="price-box bg-gray">
                   <!-- <div class="mc-promotion-bar">
                        <span class="bar-tit text-size18"><i class="iconfont icon-yduinaozhong"></i> 限时秒杀</span>
                        <span class="text-size14 p-d-lr">距优惠结束剩余</span>
                        <div class="yomibox text-size18" data-emsg="已结束" data-etime="{php}echo time()+43200{/php}"></div>
                        <a href="javascript:;" class="mc-text-link pull-right">更多秒杀&gt;</a>
                    </div>-->
                    <div class="p-d-20">
                        <div class="mc-attr-item" style="line-height:40px!important;">
                            <span class="tit">销售价格</span>
                            <span class="con">
                                <span class="text-size24 text-main">¥<span id="goods_price">{:number_format($info.sku_first.goods_price,2)}</span></span>
                                <s class="del-line text-gray">¥<span id="market_price">{:number_format($info.sku_first.market_price,2)}</span></s>
                            </span>
                        </div>
                        <div id="shop-coupon"></div>
                        {eq name='info.goods_price_type' value='1'}
                        <div class="mc-attr-item" style="position:relative">
                            <span class="tit">阶梯价格</span>
                            <span class="con">
                                本商品为阶梯价格，根据购买数量享受不同价格。<a href="javascript:;" onclick="$('#step_price_box').toggle();" class="iconfont icon-icon3"></a>
                            </span>
                            <div id="step_price_box" class="step_price_box">
                                <table class="table">
                                    <tr>
                                        <td>最小购买量</td>
                                        <td>单价</td>
                                    </tr>
                                    {volist name='$info.sku_first.goods_price_group' id='vo' offset='' length=''}
                                    <tr>
                                        <td>{$vo.buy_number}</td>
                                        <td>{$vo.goods_price}</td>
                                    </tr>
                                    {/volist}
                                </table>
                            </div>
                        </div>
                        {/eq}
                        <div class="mc-attr-item">
                            <span class="tit">积分规则</span>
                            <span class="con">
                                购买商品可获得**积分
                            </span>
                        </div>
                        <div class="split-line-dashed split-line-sm"></div>
                        <div class="mc-attr-item">
                            <span class="tit">服务说明</span>
                            <span class="con">
                                <ul class="service-items">
                                    <li>･ 支持30天无忧退换货</li>
                                    <li>･ 48小时快速退款</li>
                                    <li>･ 满88元免邮费</li>
                                    <li>･ 网易自营品牌</li>
                                    <li>･ 国内部分地区无法配送</li>
                                </ul>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="sku-info-box">
                    {volist name='info.sku_select' id='vo'}
                    <div class="goods_attr">
                        <span class="label">{$vo.attr_name}</span>
                        <ul>
                            {volist name='vo.attr_list' id='attr'}
                            <li class="text goods_sku" val="{$attr}"><span>{$attr}</span><s></s></li>
                            {/volist}
                        </ul>
                    </div>
                    {/volist}
                </div>
                <div class="action-box p-d-20">
                    <span class="m-spinner" data-ydui-spinner="{unit:'1',max:'10000'}">
                        <a href="javascript:;" class="J_Del"></a>
                        <input type="text" name="number" class="J_Input" placeholder=""/>
                        <a href="javascript:;" class="J_Add"></a>
                    </span>
                    <span class="pull-right">
                        <input type="hidden" name="sku_sn" id="sku_sn" value="{$info.sku_first.sku_sn}">
                         <a href="javascript:;" data-action="add-buy"  onclick="buyAction(this)" class="btn btn-lg btn-warning no-radius">立即购买</a>
                         <a href="javascript:;" data-action="add-cart" onclick="buyAction(this)" class="btn btn-lg btn-main no-radius">加入购物车</a>
                    </span>
                </div>
            </div>
        </div>
        <div class="goods-info-main">
            <div class="goods-details-box">
                <div class="goods-details-bar clearfix">
                    <div class="container-box">
                        <ul class="mc-navbar pull-left">
                            <li class="item active">商品详情</li>
                            <li class="item">规格参数</li>
                            <li class="item">商品评价</li>
                        </ul>
                        <div class="cart-box pull-right">
                            <span class="text-size16 text-main line-height40" style="margin-right:15px;">¥299</span>
                            <a href="javascript:;" class="btn btn-main">加入购物车</a>
                        </div>
                    </div>
                </div>
                <div>
                    <!--商品详情-->
                    <div class="goods-details-item">
                        <h3>商品详情</h3>
                        <div class="split-line-solid"></div>
                        {$info.goods_content|raw}
                    </div>
                    <!--规格参数-->
                    <div class="goods-details-item">
                        <h3>规格参数</h3>
                        <div class="split-line-solid"></div>
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <th class="active" style="width:15%">适用季节：</th>
                                <td>冬</td>
                                <th class="active" style="width:15%">款式：</th>
                                <td>开衫</td>
                            </tr><tr>                    <th class="active" style="width:15%">流行元素：</th>
                                <td>口袋,纽扣</td>
                                <th class="active" style="width:15%">弹性：</th>
                                <td>无弹</td>
                            </tr><tr>                    <th class="active" style="width:15%">适用人群：</th>
                                <td>青年</td>
                                <th class="active" style="width:15%">上市时间-枚举：</th>
                                <td>2018冬</td>
                            </tr><tr>                    <th class="active" style="width:15%">选购热点：</th>
                                <td>线上专供款</td>
                                <th class="active" style="width:15%">袖型：</th>
                                <td>蝙蝠袖</td>
                            </tr><tr>                    <th class="active" style="width:15%">风格：</th>
                                <td>通勤</td>
                                <th class="active" style="width:15%">适用场合：</th>
                                <td>通勤</td>
                            </tr><tr>                    <th class="active" style="width:15%">厚薄：</th>
                                <td>厚</td>
                                <th class="active" style="width:15%">版型：</th>
                                <td>直筒</td>
                            </tr><tr>                    <th class="active" style="width:15%">衣门襟：</th>
                                <td>暗门襟</td>
                                <th class="active" style="width:15%">衣长：</th>
                                <td>中长</td>
                            </tr><tr>                    <th class="active" style="width:15%">图案：</th>
                                <td>纯色</td>
                                <th class="active" style="width:15%">面料：</th>
                                <td>聚酯纤维</td>
                            </tr><tr>                    <th class="active" style="width:15%">品牌名称：</th>
                                <td>欧昵雪</td>
                                <th class="active" style="width:15%">商品名称：</th>
                                <td>【2018冬季新款】毛绒大衣时尚秋冬修身休闲中长款女装外套</td>
                            </tr><tr>                    <th class="active" style="width:15%">产地：</th>
                                <td>中国</td>
                                <th class="active" style="width:15%">材质：</th>
                                <td>面料:100%聚酯纤维</td>
                            </tr><tr>                    <th class="active" style="width:15%">洗涤说明：</th>
                                <td>"手洗，最高洗涤温度40℃
                                    不可漂白
                                    悬挂晾干
                                    熨斗底板最高温度110度
                                    不可干洗
                                    不可绞拧"</td>
                                <th class="active" style="width:15%">商品编号：</th>
                                <td>C5218Y06</td>
                            </tr>        </tbody>
                        </table>

                    </div>
                    <!--商品评价-->
                    <div class="goods-details-item">
                        <h3>商品评价</h3>
                        <div class="split-line-solid"></div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
{include file='public/foot'}
{/block}
{block name='script'}
<script src="/static/home/pc/js/home_sku.js"></script>
<script src="/static/home/pc/js/cart.js"></script>
<script type="text/html" id="coupon-tpl">
    <div class="mc-attr-item">
        <span class="tit">优惠卡券</span>
        <span class="con">
            {@each data as value}
            <a  href="javascript:;" onclick="couponAction.receiveCoupon(${value.id})" class="mc-quan-item" title="${value.direction}">
                <s></s><b></b>
                <span class="text">${value.sub_title}</span>
            </a>
            {@/each}
        </span>
    </div>
</script>
<script>
    $(function () {
        $('.goods-details-bar').navfix(0,'fixed-box');
        $('.goods-details-bar').floor('.goods-details-item');
        var sku_list=$.parseJSON('{:json_encode($info.sku_item)}');
        skuSelect.init({
            sku_list:sku_list,
            success:function (skuinfo) {
                console.info(skuinfo);
                /*sku改变后动作 修改销售价 市场价 修改skuid*/
                $('#goods_price').text(utils.moneyFormat(skuinfo['goods_price'],2));
                $('#market_price').text(utils.moneyFormat(skuinfo['market_price'],2));
                $('#sku_sn').val(skuinfo['sku_sn']);
            },
            error:function () {}
        });
        couponAction.getCoupon();
    })
    //购买动作
    function buyAction(obj) {
        var action = $(obj).data('action');
        var sku_sn = $('input[name="sku_sn"]').val();
        var number = $('input[name="number"]').val();
        if(sku_sn==''){
            utils.msg('请选择商品参数！');
            return false;
        }
        /*立即购买*/
        if(action=='add-buy'){
            utils.getAjax({
                url:'/site/cart/cartadd.html',
                data:{sku_sn:sku_sn,number:number,type:'update'}, //更新数量
                success:function (res) {
                    console.log(res);
                    if(res.code==1){
                        location.href="/site/cart/buy.html?cart_ids="+res.data;
                    }else{
                        layer.msg(res.msg,{icon:2});
                    }
                }
            });
        }
        /*加入购物车*/
        if(action=='add-cart'){
            utils.getAjax({
                url:'/site/cart/cartadd.html',
                data:{sku_sn:sku_sn,number:number,type:'plus'},
                success:function (res) {
                    console.log(res);
                    if(res.code==1){
                        layer.msg('加入购物车成功！',{icon:1,shift:-1},function () {
                            /*utils.getCartNum();*/
                        });
                    }else{
                        layer.msg(res.msg,{icon:2});
                    }
                }
            });
        }
        return false;
    }
    var couponAction = {
        /*获取店内优惠券*/
        getCoupon:function () {
            utils.getAjax({
                url:'/site/goods/getshopcoupon.html',
                data:{shop_id:shop_id},
                success:function (res) {
                    console.log(res);
                    if(res.length>1){
                        var html = juicer($('#coupon-tpl').html(),{data:res});
                        $('#shop-coupon').html(html);
                    }

                }
            })
        },
        /*领取优惠券*/
        receiveCoupon:function (coupon_id) {
            utils.getAjax({
                url:'/site/member.index/receivecoupon.html',
                data:{coupon_id:coupon_id},
                success:function (res) {
                    console.log(res);
                    if(res.msg=='nologin'){
                        utils.msg('请先登录再领取')
                    }else{
                        utils.msg(res.msg)
                    }
                }
            })
        }
    }
</script>
{/block}