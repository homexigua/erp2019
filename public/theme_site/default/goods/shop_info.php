<div class="side-left-box">
    <div class="panel panel-sm panel-default no-radius shop-info-box">
        <div class="panel-heading single-cut">{$shop_info.config.sho_name|default=$shop_info.shop_name}</div>
        <div class="panel-body">
            <div class="mc-attr-item line-height18">
                <div class="tit">实体地址</div>
                <span class="con">
                        {$shop_info.prov}{$shop_info.city}{$shop_info.dist}{$shop_info.address}
                    </span>
            </div>
            <div class="mc-attr-item line-height18">
                <span class="tit">联系电话</span>
                <span class="con">
                        {$shop_info.config.shop_tel}
                    </span>
            </div>
            <div class="mc-attr-item line-height18">
                <span class="tit">客服QQ</span>
                <span class="con">
                        {volist name='shop_info.config.shop_qq' id='vo'}
                        <a target="_blank" href="http://wpa.qq.com/msgrd?v=3&amp;uin={$vo.qq}&amp;site=qq&amp;menu=yes" class="mc-flex-box mc-flex-c">
                            <svg class="qq-icon" aria-hidden="true">
                                <use xlink:href="#icon-qq4"></use>
                            </svg>
                            <span class="text-size12 text-gray"> {$vo.name}</span>
                        </a>
                        {/volist}
                    </span>
            </div>
            <div class="split-line-solid split-line-md"></div>
            <ul class="mc-avg-sm-3 text-center">
                <li>
                    <span class="text-size12 text-gray">商品</span>
                    <div class="text-size14 text-main line-height24">4.9</div>
                </li>
                <li>
                    <span class="text-size12 text-gray">服务</span>
                    <div class="text-size14 text-main line-height24">4.9</div>
                </li>
                <li>
                    <span class="text-size12 text-gray">售后</span>
                    <div class="text-size14 text-main line-height24">4.9</div>
                </li>
            </ul>
            <div class="split-line-solid split-line-md"></div>
            <div class="action-box text-center">
                <a href="/home/goods/shopindex.html?shop_id={$shop_info.id}" class="btn btn-white">进入店铺</a>
                <a href="javascript:;" class="btn btn-white">收藏店铺</a>
            </div>
        </div>
    </div>
    <div class="panel panel-sm panel-default no-radius">
        <div class="panel-heading single-cut">店内搜索</div>
        <div class="panel-body">
            <form class="form-horizontal">
                <div class="form-group">
                    <div class="col-sm-12">
                        <input type="text" class="form-control input-sm"  placeholder="请输入关键词">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <input type="text" class="form-control input-sm pull-left" style="width:80px;" placeholder="价格起">
                        <div class="pull-left text-center text-gray" style="width:18px;">-</div>
                        <input type="text" class="form-control input-sm pull-right" style="width:80px;" placeholder="价格止">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <button type="button" class="btn btn-sm btn-block btn-default">立即搜索</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="panel panel-sm panel-default no-radius">
        <div class="panel-heading single-cut">精品推荐</div>
        <div class="panel-body">
            <a href="javascript:;" class="mc-goods-item goods-item-sm mc-scale">
                <div class="goods-thumb">
                    <img src="/static/home/pc/images/goods01.png" class="" alt="">
                </div>
                <div class="goods-info">
                    <h3 class="text-size14 line-height30 single-cut">地表强温 男式鹅绒迷彩羽绒服</h3>
                    <p class="text-size14 text-red">¥ 1299.00</p>
                </div>
            </a>
            <div class="split-line-solid split-line-sm"></div>
            <a href="javascript:;" class="mc-goods-item goods-item-sm mc-scale">
                <div class="goods-thumb">
                    <img src="/static/home/pc/images/goods02.png" class="" alt="">
                </div>
                <div class="goods-info">
                    <h3 class="text-size14 line-height30 single-cut">地表强温 男式鹅绒迷彩羽绒服</h3>
                    <p class="text-size14 text-red">¥ 1299.00</p>
                </div>
            </a>
            <div class="split-line-solid split-line-sm"></div>
            <a href="javascript:;" class="mc-goods-item goods-item-sm mc-scale">
                <div class="goods-thumb">
                    <img src="/static/home/pc/images/goods03.png" class="" alt="">
                </div>
                <div class="goods-info">
                    <h3 class="text-size14 line-height30 single-cut">地表强温 男式鹅绒迷彩羽绒服</h3>
                    <p class="text-size14 text-red">¥ 1299.00</p>
                </div>
            </a>
        </div>
    </div>
</div>