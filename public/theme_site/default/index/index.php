{extend name="public:layout" /}
{block name="title"}{/block}
{block name="head"}{/block}
{block name="body"}
{include file='public/head'}
<div class="mc-full-slide">
    <div class="bd">
        <ul>
            <li _src="url(/static/home/pc/images/2.jpg)" style="background:#DED5A1 center 0 no-repeat;"><a href="javascript:;"></a></li>
            <li _src="url(/static/home/pc/images/3.jpg)" style="background:#B8CED1 center 0 no-repeat;"><a href="javascript:;"></a></li>
            <li _src="url(/static/home/pc/images/4.jpg)" style="background:#98918E center 0 no-repeat;"><a href="javascript:;"></a></li>
        </ul>
    </div>
    <div class="hd"><ul></ul></div>
    <span class="prev"><i class="iconfont icon-arrow-left"></i></span>
    <span class="next"><i class="iconfont icon-arrow-right"></i></span>
</div>
<script>
    /* 控制左右按钮显示 */
    jQuery(".mc-full-slide").hover(function(){ jQuery(this).find(".prev,.next").stop(true,true).fadeTo("show",0.5) },function(){ jQuery(this).find(".prev,.next").fadeOut() });
    /* 调用SuperSlide */
    jQuery(".mc-full-slide").slide({ titCell:".hd ul", mainCell:".bd ul", effect:"fold",  autoPlay:true, autoPage:true, trigger:"click",
        startFun:function(i){
            var curLi = jQuery(".mc-full-slide .bd li").eq(i); /* 当前大图的li */
            if( !!curLi.attr("_src") ){
                curLi.css("background-image",curLi.attr("_src")).removeAttr("_src") /* 将_src地址赋予li背景，然后删除_src */
            }
        }
    });
</script>
<div class="index-row">
    <div class="container">
        <div class="mc-index-title">
            <a href="javascript:;" class="tit">
                新品首发
                <small>进口商品 爆款商品 买全球！</small>
            </a>
            <a href="/home/index/you.html?tpl=goods_list" class="more-link">更多新品  <i class="iconfont icon-arrow-right"></i></a>
        </div>
    </div>
    <div style="width:1225px;margin:0 auto;">
        <div class="mc-scroll-left">
            <a class="next"><i class="iconfont icon-arrow-right"></i></a>
            <a class="prev"><i class="iconfont icon-arrow-left"></i></a>
            <div class="bd">
                <ul class="picList">
                    <li>
                        <a href="/home/index/you.html?tpl=goods_info" class="mc-goods-item mc-hover-shadow-sm mc-fade-in">
                            <div class="goods-thumb">
                                <img src="/static/home/pc/images/goods_album001.png" class="a-item" alt="">
                                <img src="/static/home/pc/images/goods_album002.jpg" class="b-item" alt="">
                            </div>
                            <div class="goods-info">
                                <h3 class="text-size16 line-height30 single-cut">女装品牌</h3>
                                <p class="text-size12">已有<span class="text-red">2,300</span>个专业买手入驻</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="/home/index/you.html?tpl=goods_info" class="mc-goods-item mc-hover-shadow-sm mc-fade-in">
                            <div class="goods-thumb">
                                <img src="/static/home/pc/images/goods01.png" class="a-item" alt="">
                                <img src="/static/home/pc/images/goods02.jpg" class="b-item" alt="">
                            </div>
                            <div class="goods-info">
                                <h3 class="text-size16 line-height30 single-cut">女装专场</h3>
                                <p class="text-size12"><span class="text-red">2,300</span>个专业为您买买买</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="/home/index/you.html?tpl=goods_info" class="mc-goods-item mc-hover-shadow-sm mc-fade-in">
                            <div class="goods-thumb">
                                <img src="/static/home/pc/images/goods02.png" class="a-item" alt="">
                                <img src="/static/home/pc/images/goods02_01.jpg" class="b-item" alt="">
                            </div>
                            <div class="goods-info">
                                <h3 class="text-size16 line-height30 single-cut">男装专场</h3>
                                <p class="text-size12"><span class="text-red">1,200</span>个专业买手为您精选</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="/home/index/you.html?tpl=goods_info" class="mc-goods-item mc-hover-shadow-sm mc-fade-in">
                            <div class="goods-thumb">
                                <img src="/static/home/pc/images/goods03.png" class="a-item" alt="">
                                <img src="/static/home/pc/images/goods03_01.jpg" class="b-item" alt="">
                            </div>
                            <div class="goods-info">
                                <h3 class="text-size16 line-height30 single-cut">美国专场</h3>
                                <p class="text-size12"><span class="text-red">66</span>个美国品牌入驻</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="/home/index/you.html?tpl=goods_info" class="mc-goods-item mc-hover-shadow-sm mc-fade-in">
                            <div class="goods-thumb">
                                <img src="/static/home/pc/images/goods04.png" class="a-item" alt="">
                                <img src="/static/home/pc/images/goods04_01.jpg" class="b-item" alt="">
                            </div>
                            <div class="goods-info">
                                <h3 class="text-size16 line-height30 single-cut">韩国专场</h3>
                                <p class="text-size12">已有<span class="text-red">2,300</span>个专业买手入驻</p>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        jQuery(".mc-scroll-left").slide({titCell:".hd ul",mainCell:".bd ul",autoPage:true,effect:"left",autoPlay:true,vis:4,trigger:"click"});
    </script>
</div>
<div class="index-row bg-gray">
    <div class="container">
        <div class="mc-index-title">
            <a href="javascript:;" class="tit">
                限时秒杀
            </a>
            <a href="javascript:;" class="more-link">更多秒杀  <i class="iconfont icon-arrow-right"></i></a>
        </div>
        <div class="index-sale-box">
            <div class="item text-center">
                <h3 class="text-size28 text-black">18点场</h3>
                <p class="text-size18 text-red line-height40">距离结束还剩</p>
                <div class="yomibox mc-yomi1 mc-yomi1-lg" data-emsg="已结束" data-etime="{php}echo time()+43200{/php}"></div>
                <div class="height-60"></div>
                <a href="javascript:;" class="btn btn-main no-radius">查看全部</a>
            </div>
            <div class="item">
                <a href="javascript:;" class="mc-goods-item3 mc-scale">
                    <div class="goods-thumb">
                        <img src="/static/home/pc/images/goods03.png" class="" alt="">
                    </div>
                    <div class="goods-info">
                        <h3 class="text-size16 line-height30 single-cut">地表强温 男式鹅绒迷彩羽绒服</h3>
                        <p class="text-size12 text-gray">优衣库制造商，进口抗菌纱</p>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="progress  progress-bar-default progress-small" style="margin:10px auto;">
                                    <div style="width: 43%" aria-valuemax="100"
                                         aria-valuemin="0" aria-valuenow="43"
                                         role="progressbar" class="progress-bar progress-bar-light-red">
                                        <span class="sr-only">43% Complete (success)</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <p class="text-size12 text-gray line-height30">剩1200件</p>
                            </div>
                        </div>
                        <div class="text-size14 text-red">
                            限时价 ¥<b class="text-size24">85.10</b> <s class="text-size12 text-gray del-line">¥109</s>
                        </div>
                        <div class="height-10"></div>
                        <button class="btn btn-danger no-radius">立即抢购</button>
                    </div>
                </a>
            </div>
            <div class="item">
                <a href="javascript:;" class="mc-goods-item3 mc-scale">
                    <div class="goods-thumb">
                        <img src="/static/home/pc/images/goods02.png" class="" alt="">
                    </div>
                    <div class="goods-info">
                        <h3 class="text-size16 line-height30 single-cut">地表强温 男式鹅绒迷彩羽绒服</h3>
                        <p class="text-size12 text-gray">优衣库制造商，进口抗菌纱</p>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="progress  progress-bar-default progress-small" style="margin:10px auto;">
                                    <div style="width: 43%" aria-valuemax="100"
                                         aria-valuemin="0" aria-valuenow="43"
                                         role="progressbar" class="progress-bar progress-bar-light-red">
                                        <span class="sr-only">43% Complete (success)</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <p class="text-size12 text-gray line-height30">剩1200件</p>
                            </div>
                        </div>
                        <div class="text-size14 text-red">
                            限时价 ¥<b class="text-size24">85.10</b> <s class="text-size12 text-gray del-line">¥109</s>
                        </div>
                        <div class="height-10"></div>
                        <button class="btn btn-danger no-radius">立即抢购</button>
                    </div>
                </a>
            </div>
            <div class="item">
                <a href="javascript:;" class="mc-goods-item3 mc-scale">
                    <div class="goods-thumb">
                        <img src="/static/home/pc/images/goods01.png" class="" alt="">
                    </div>
                    <div class="goods-info">
                        <h3 class="text-size16 line-height30 single-cut">地表强温 男式鹅绒迷彩羽绒服</h3>
                        <p class="text-size12 text-gray">优衣库制造商，进口抗菌纱</p>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="progress  progress-bar-default progress-small" style="margin:10px auto;">
                                    <div style="width: 43%" aria-valuemax="100"
                                         aria-valuemin="0" aria-valuenow="43"
                                         role="progressbar" class="progress-bar progress-bar-light-red">
                                        <span class="sr-only">43% Complete (success)</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <p class="text-size12 text-gray line-height30">剩1200件</p>
                            </div>
                        </div>
                        <div class="text-size14 text-red">
                            限时价 ¥<b class="text-size24">85.10</b> <s class="text-size12 text-gray del-line">¥109</s>
                        </div>
                        <div class="height-10"></div>
                        <button class="btn btn-danger no-radius">立即抢购</button>
                    </div>
                </a>
            </div>
            <div class="item">
                <a href="javascript:;" class="mc-goods-item3 mc-scale">
                    <div class="goods-thumb">
                        <img src="/static/home/pc/images/goods04.png" class="" alt="">
                    </div>
                    <div class="goods-info">
                        <h3 class="text-size16 line-height30 single-cut">地表强温 男式鹅绒迷彩羽绒服</h3>
                        <p class="text-size12 text-gray">优衣库制造商，进口抗菌纱</p>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="progress  progress-bar-default progress-small" style="margin:10px auto;">
                                    <div style="width: 43%" aria-valuemax="100"
                                         aria-valuemin="0" aria-valuenow="43"
                                         role="progressbar" class="progress-bar progress-bar-light-red">
                                        <span class="sr-only">43% Complete (success)</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <p class="text-size12 text-gray line-height30">剩1200件</p>
                            </div>
                        </div>
                        <div class="text-size14 text-red">
                            限时价 ¥<b class="text-size24">85.10</b> <s class="text-size12 text-gray del-line">¥109</s>
                        </div>
                        <div class="height-10"></div>
                        <button class="btn btn-danger no-radius">立即抢购</button>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="index-row">
    <div class="container">
        <div class="mc-index-title">
            <a href="javascript:;" class="tit">
                热卖推荐
            </a>
            <a href="javascript:;" class="more-link">更多推荐  <i class="iconfont icon-arrow-right"></i></a>
        </div>
        <div class="index-recommend-box">
            <div class="item">
                <a href="javascript:;" class="mc-goods-item mc-scale">
                    <div class="goods-thumb">
                        <img src="/static/home/pc/images/goods04_01.jpg" class="" alt="">
                    </div>
                    <div class="goods-info">
                        <span class="label label-danger">新品</span>
                        <h3 class="text-size18 line-height45 single-cut font-weight600">地表强温 男式鹅绒迷彩羽绒服</h3>
                        <b class="text-size16 text-red">¥ 1299.00</b>
                    </div>
                </a>
            </div>
            <div class="item">
                <a href="javascript:;" class="mc-goods-item mc-scale">
                    <div class="goods-thumb">
                        <img src="/static/home/pc/images/goods01.png" class="" alt="">
                    </div>
                    <div class="goods-info">
                        <span class="label label-danger">新品</span>
                        <h3 class="text-size14 line-height30 single-cut">地表强温 男式鹅绒迷彩羽绒服</h3>
                        <p class="text-size14 text-red">¥ 1299.00</p>
                    </div>
                </a>
            </div>
            <div class="item">
                <a href="javascript:;" class="mc-goods-item mc-scale">
                    <div class="goods-thumb">
                        <img src="/static/home/pc/images/goods02.png" class="" alt="">
                    </div>
                    <div class="goods-info">
                        <span class="label label-danger">新品</span>
                        <h3 class="text-size14 line-height30 single-cut">地表强温 男式鹅绒迷彩羽绒服</h3>
                        <p class="text-size14 text-red">¥ 1299.00</p>
                    </div>
                </a>
            </div>
            <div class="item">
                <a href="javascript:;" class="mc-goods-item mc-scale">
                    <div class="goods-thumb">
                        <img src="/static/home/pc/images/goods03.png" class="" alt="">
                    </div>
                    <div class="goods-info">
                        <span class="label label-danger">新品</span>
                        <h3 class="text-size14 line-height30 single-cut">地表强温 男式鹅绒迷彩羽绒服</h3>
                        <p class="text-size14 text-red">¥ 1299.00</p>
                    </div>
                </a>
            </div>
            <div class="item">
                <a href="javascript:;" class="mc-goods-item mc-scale">
                    <div class="goods-thumb">
                        <img src="/static/home/pc/images/goods04.png" class="" alt="">
                    </div>
                    <div class="goods-info">
                        <span class="label label-danger">新品</span>
                        <h3 class="text-size14 line-height30 single-cut">地表强温 男式鹅绒迷彩羽绒服</h3>
                        <p class="text-size14 text-red">¥ 1299.00</p>
                    </div>
                </a>
            </div>
            <div class="item">
                <a href="javascript:;" class="mc-goods-item mc-scale">
                    <div class="goods-thumb">
                        <img src="/static/home/pc/images/goods02_01.jpg" class="" alt="">
                    </div>
                    <div class="goods-info">
                        <span class="label label-danger">新品</span>
                        <h3 class="text-size14 line-height30 single-cut">地表强温 男式鹅绒迷彩羽绒服</h3>
                        <p class="text-size14 text-red">¥ 1299.00</p>
                    </div>
                </a>
            </div>
            <div class="item">
                <a href="javascript:;" class="mc-goods-item mc-scale">
                    <div class="goods-thumb">
                        <img src="/static/home/pc/images/goods03_01.jpg" class="" alt="">
                    </div>
                    <div class="goods-info">
                        <span class="label label-danger">新品</span>
                        <h3 class="text-size14 line-height30 single-cut">地表强温 男式鹅绒迷彩羽绒服</h3>
                        <p class="text-size14 text-red">¥ 1299.00</p>
                    </div>
                </a>
            </div>

        </div>
    </div>
</div>
<ul class="floor-scroll-nav">
    {volist name='category' id='vo'}
    <li class="item"><i></i><span>{$vo.name}</span></li>
    {/volist}
    <li class="item" onclick='$("body,html").animate({"scrollTop":0},500)'>返回</li>
</ul>
<div class="floor-scroll-box">
    {volist name='category' id='vo'}
    <div class="index-row index-floor">
        <div class="container">
            <div class="mc-index-title">
                <a href="javascript:;" class="tit">
                    {$vo.name}品牌
                </a>
                <a href="javascript:;" class="more-link">全部{$vo.name}品牌  <i class="iconfont icon-arrow-right"></i></a>
            </div>
            <ul class="mc-avg-sm-2 mc-avg-between-lg">
                <li>
                    <a href="javascript:;" class="mc-brand-item">
                        <div class="brand-img mc-fade-in2">
                            <img src="/static/home/pc/images/brand01.jpg" class="am-img-responsive" alt="">
                            <span  onclick="alert('添加收藏')" class="mc-collect-link"></span>
                        </div>
                        <div class="brand-info">
                            <b class="text-size24 text-red">1</b> 折起
                            <span class="shop-name">对白女装旗舰店</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:;" class="mc-brand-item">
                        <div class="brand-img mc-fade-in2">
                            <img src="/static/home/pc/images/brand02.jpg" class="am-img-responsive" alt="">
                            <span  onclick="alert('添加收藏')" class="mc-collect-link active"></span>
                        </div>
                        <div class="brand-info">
                            <b class="text-size24 text-red">1.2</b> 折起
                            <span class="shop-name">白鹿语特卖旗舰店</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:;" class="mc-brand-item">
                        <div class="brand-img mc-fade-in2">
                            <img src="/static/home/pc/images/brand03.jpg" class="am-img-responsive" alt="">
                            <span  onclick="alert('添加收藏')" class="mc-collect-link"></span>
                        </div>
                        <div class="brand-info">
                            <b class="text-size24 text-red">2</b> 折起
                            <span class="shop-name">梵希蔓Vimly女装特卖旗舰店</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:;" class="mc-brand-item">
                        <div class="brand-img mc-fade-in2">
                            <img src="/static/home/pc/images/brand04.jpg" class="am-img-responsive" alt="">
                            <span  onclick="alert('添加收藏')" class="mc-collect-link active"></span>
                        </div>
                        <div class="brand-info">
                            <b class="text-size24 text-red">3</b> 折起
                            <span class="shop-name">韩风时尚牛仔</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:;" class="mc-brand-item">
                        <div class="brand-img mc-fade-in2">
                            <img src="/static/home/pc/images/brand05.jpg" class="am-img-responsive" alt="">
                            <span  onclick="alert('添加收藏')" class="mc-collect-link"></span>
                        </div>
                        <div class="brand-info">
                            <b class="text-size24 text-red">1</b> 折起
                            <span class="shop-name">对白女装旗舰店</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:;" class="mc-brand-item">
                        <div class="brand-img mc-fade-in2">
                            <img src="/static/home/pc/images/brand06.jpg" class="am-img-responsive" alt="">
                            <span  onclick="alert('添加收藏')" class="mc-collect-link"></span>
                        </div>
                        <div class="brand-info">
                            <b class="text-size24 text-red">1.2</b> 折起
                            <span class="shop-name">白鹿语特卖旗舰店</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:;" class="mc-brand-item">
                        <div class="brand-img mc-fade-in2">
                            <img src="/static/home/pc/images/brand07.jpg" class="am-img-responsive" alt="">
                            <span  onclick="alert('添加收藏')" class="mc-collect-link"></span>
                        </div>
                        <div class="brand-info">
                            <b class="text-size24 text-red">2</b> 折起
                            <span class="shop-name">梵希蔓Vimly女装特卖旗舰店</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:;" class="mc-brand-item">
                        <div class="brand-img mc-fade-in2">
                            <img src="/static/home/pc/images/brand08.jpg" class="am-img-responsive" alt="">
                            <span  onclick="alert('添加收藏')" class="mc-collect-link"></span>
                        </div>
                        <div class="brand-info">
                            <b class="text-size24 text-red">3</b> 折起
                            <span class="shop-name">韩风时尚牛仔</span>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    {/volist}
</div>
{include file='public/foot'}
{/block}
{block name="script"}
<script>
    $(document).ready(function(e) {
        $('.nav-box').navfix(0,'nav-fixed-box');
        var height = $(".floor-scroll-box").offset().top;
        $('.floor-scroll-nav').floor('.floor-scroll-box .index-floor',height);
    });
</script>
{/block}