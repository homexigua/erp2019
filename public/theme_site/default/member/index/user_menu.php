<div class="page-left-box">
    <div class="mc-side-menu cross-icon">
        <h3><em></em>资金管理</h3>
        <ul>
            <li><a href="/site/index/you.html?tpl=member_coupon" class="mc-text-link">我的资金</a></li>
            <li><a href="/site/index/you.html?tpl=member_coupon" class="mc-text-link">资金明细</a></li>
            <li><a href="/site/index/you.html?tpl=member_coupon" class="mc-text-link">提现申请</a></li>
        </ul>
        <h3><em></em>我的账户</h3>
        <ul>
            <li><a href="/site/member.member/info" class="mc-text-link">我的资料</a></li>
            <li><a href="/site/index/you.html?tpl=member_safe" class="mc-text-link">我的评价</a></li>
            <li><a href="/site/index/you.html?tpl=member_safe" class="mc-text-link">账户安全</a></li>
            <li><a href="/site/index/you.html?tpl=member_safe" class="mc-text-link">发票管理</a></li>
            <li><a href="/site/index/you.html?tpl=member_msg" class="mc-text-link">消息中心</a></li>
            <li><a href="/site/member.address/index.html" class="mc-text-link">地址管理</a></li>
            <li><a href="/site/index/you.html?tpl=help" class="mc-text-link">帮助中心</a></li>
        </ul>
        <h3><em></em>我的资产</h3>
        <ul>
            <li><a href="/site/index/you.html?tpl=member_coupon" class="mc-text-link">优惠券</a></li>
            <li><a href="/site/index/you.html?tpl=member_black_card" class="mc-text-link">我的积分</a></li>
            <li><a href="/site/index/you.html?tpl=member_exchange" class="mc-text-link">兑换中心</a></li>
        </ul>
        <h3><em></em>我的关注</h3>
        <ul>
            <li><a href="/site/index/you.html?tpl=member_black_card" class="mc-text-link">关注商品</a></li>
            <li><a href="/site/index/you.html?tpl=member_exchange" class="mc-text-link">关注店铺</a></li>
            <li><a href="/site/index/you.html?tpl=member_coupon" class="mc-text-link">内容收藏</a></li>
        </ul>
        <h3><em></em>我的交易</h3>
        <ul>
            <li><a href="/site/index/you.html?tpl=member_order_list" class="mc-text-link">订单管理</a></li>
            <li><a href="/site/index/you.html?tpl=member_order_list" class="mc-text-link">退换/售后</a></li>
        </ul>


    </div>
    <script>
        $('.mc-side-menu').find('h3').click(function () {
            $(this).hasClass('on')?$(this).removeClass('on'):$(this).addClass('on');
            $(this).next().toggle();
        })
    </script>
</div>