{extend name="public:layout" /}
{block name="title"}我的资料_会员中心{/block}
{block name="head"}{/block}
{block name="body"}
{include file='public/head'}
<div class="memeber-page">
    <div class="container">
        <div class="page-left-box">
            <div class="mc-side-menu cross-icon">
                <h3><em></em>我的交易</h3>
                <ul>
                    <li><a href="/home/index/you.html?tpl=member_order_list" class="mc-text-link">订单管理</a></li>
                    <li><a href="/home/index/you.html?tpl=member_order_list" class="mc-text-link">退换/售后</a></li>
                    <li><a href="/home/index/you.html?tpl=member_recommend" class="mc-text-link">推荐返利</a></li>
                </ul>
                <h3><em></em>我的资产</h3>
                <ul>
                    <li><a href="/home/index/you.html?tpl=member_point" class="mc-text-link active">我的积分</a></li>
                    <li><a href="/home/index/you.html?tpl=member_coin" class="mc-text-link">我的金币</a></li>
                    <li><a href="/home/index/you.html?tpl=member_yuanb" class="mc-text-link">我的元宝</a></li>
                    <li><a href="/home/index/you.html?tpl=member_black_card" class="mc-text-link">我的黑卡</a></li>
                    <li><a href="/home/index/you.html?tpl=member_coupon" class="mc-text-link">优惠券</a></li>
                    <li><a href="/home/index/you.html?tpl=member_exchange" class="mc-text-link">兑换中心</a></li>
                </ul>
                <h3><em></em>我的账户</h3>
                <ul>
                    <li><a href="/home/index/you.html?tpl=member_info" class="mc-text-link">我的资料</a></li>
                    <li><a href="/home/index/you.html?tpl=member_safe" class="mc-text-link">账户安全</a></li>
                    <li><a href="/home/index/you.html?tpl=member_msg" class="mc-text-link">消息中心</a></li>
                    <li><a href="/home/index/you.html?tpl=member_address" class="mc-text-link">地址管理</a></li>
                    <li><a href="/home/index/you.html?tpl=help" class="mc-text-link">帮助中心</a></li>
                </ul>
            </div>
            <script>
                $('.mc-side-menu').find('h3').click(function () {
                    $(this).hasClass('on')?$(this).removeClass('on'):$(this).addClass('on');
                    $(this).next().toggle();
                })
            </script>
        </div>
        <div class="page-right-box">
            <div class="panel panel-default panel-main no-radius">
                <ul class="mc-navbar">
                    <li class="item active">积分兑换黑卡</li>
                    <li class="item">金币兑换元宝</li>
                    <li class="item">元宝申请提现</li>
                    <a href="javascript:;" class="text-size12 text-main line-height40 pull-right"><i class="iconfont icon-yduigantanhaoshixin"></i> 兑换规则</a>
                </ul>
                <div class="panel-body">
                    <div class="alert alert-warning alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <b>为什么要兑换黑卡？</b><br>
                        购买时直接使用积分抵扣的话，本次购物将不再赠送积分。积分兑换黑卡后，使用黑卡进行购买可以继续获得积分。
                    </div>
                    <ul class="mc-avg-sm-3 mc-avg-between-lg">
                        <li>
                            <div class="mc-coupon-black text-center">
                                <div class="money">
                                            <span class="text-size60 single-cut">
                                                100<span class="text-size14">元</span>
                                            </span>
                                </div>
                                <div class="coupon-number">
                                    跨店使用，不设找零
                                </div>
                                <div class="text-size12 line-height36">领取后即刻生效，7日内有效</div>
                            </div>
                            <div class="row line-height40">
                                <div class="col-sm-6 text-size14 text-gray">
                                    需积分 <span class="text-red text-size18">100.00</span> 兑换
                                </div>
                                <div class="col-sm-6 text-right">
                                    <a href="javascript:;" class="btn btn-main btn-sm">立即兑换</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="mc-coupon-black text-center">
                                <div class="money">
                                            <span class="text-size60 single-cut">
                                                50<span class="text-size14">元</span>
                                            </span>
                                </div>
                                <div class="coupon-number">
                                    跨店使用，不设找零
                                </div>
                                <div class="text-size12 line-height36">领取后即刻生效，7日内有效</div>
                            </div>
                            <div class="row line-height40">
                                <div class="col-sm-6 text-size14 text-gray">
                                    需积分 <span class="text-red text-size18">50.00</span> 兑换
                                </div>
                                <div class="col-sm-6 text-right">
                                    <a href="javascript:;" class="btn btn-main btn-sm">立即兑换</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="mc-coupon-black text-center">
                                <div class="money">
                                            <span class="text-size60 single-cut">
                                                20<span class="text-size14">元</span>
                                            </span>
                                </div>
                                <div class="coupon-number">
                                    跨店使用，不设找零
                                </div>
                                <div class="text-size12 line-height36">领取后即刻生效，7日内有效</div>
                            </div>
                            <div class="row line-height40">
                                <div class="col-sm-6 text-size14 text-gray">
                                    需积分 <span class="text-red text-size18">20.00</span> 兑换
                                </div>
                                <div class="col-sm-6 text-right">
                                    <a href="javascript:;" class="btn btn-main btn-sm">立即兑换</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="mc-coupon-black text-center">
                                <div class="money">
                                            <span class="text-size60 single-cut">
                                                10<span class="text-size14">元</span>
                                            </span>
                                </div>
                                <div class="coupon-number">
                                    跨店使用，不设找零
                                </div>
                                <div class="text-size12 line-height36">领取后即刻生效，7日内有效</div>
                            </div>
                            <div class="row line-height40">
                                <div class="col-sm-6 text-size14 text-gray">
                                    需积分 <span class="text-red text-size18">10.00</span> 兑换
                                </div>
                                <div class="col-sm-6 text-right">
                                    <a href="javascript:;" class="btn btn-main btn-sm">立即兑换</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
{include file='public/foot'}
{/block}
{block name='script'}
<script>
    $(function () {
        formHelper.initForm();
        utils.ajaxSubForm()
    });
    var  doAction={
        cell:function () {
            utils.openDialog({
                title:'绑定手机',
                offset:'auto',
                area:['500px','300px'],
                content: '/site/member/editcell.html'

            })
        }
    };
</script>
{/block}