{extend name="public:layout" /}
{block name="title"}我的资料_会员中心{/block}
{block name="head"}{/block}
{block name="body"}
{include file='public/head'}
<div class="memeber-page">
    <div class="container">
        <div class="page-left-box">
            <div class="mc-side-menu cross-icon">
                <h3><em></em>我的交易</h3>
                <ul>
                    <li><a href="/home/index/you.html?tpl=member_order_list" class="mc-text-link">订单管理</a></li>
                    <li><a href="/home/index/you.html?tpl=member_order_list" class="mc-text-link">退换/售后</a></li>
                    <li><a href="/home/index/you.html?tpl=member_recommend" class="mc-text-link">推荐返利</a></li>
                </ul>
                <h3><em></em>我的资产</h3>
                <ul>
                    <li><a href="/home/index/you.html?tpl=member_point" class="mc-text-link active">我的积分</a></li>
                    <li><a href="/home/index/you.html?tpl=member_coin" class="mc-text-link">我的金币</a></li>
                    <li><a href="/home/index/you.html?tpl=member_yuanb" class="mc-text-link">我的元宝</a></li>
                    <li><a href="/home/index/you.html?tpl=member_black_card" class="mc-text-link">我的黑卡</a></li>
                    <li><a href="/home/index/you.html?tpl=member_coupon" class="mc-text-link">优惠券</a></li>
                    <li><a href="/home/index/you.html?tpl=member_exchange" class="mc-text-link">兑换中心</a></li>
                </ul>
                <h3><em></em>我的账户</h3>
                <ul>
                    <li><a href="/home/index/you.html?tpl=member_info" class="mc-text-link">我的资料</a></li>
                    <li><a href="/home/index/you.html?tpl=member_safe" class="mc-text-link">账户安全</a></li>
                    <li><a href="/home/index/you.html?tpl=member_msg" class="mc-text-link">消息中心</a></li>
                    <li><a href="/home/index/you.html?tpl=member_address" class="mc-text-link">地址管理</a></li>
                    <li><a href="/home/index/you.html?tpl=help" class="mc-text-link">帮助中心</a></li>
                </ul>
            </div>
            <script>
                $('.mc-side-menu').find('h3').click(function () {
                    $(this).hasClass('on')?$(this).removeClass('on'):$(this).addClass('on');
                    $(this).next().toggle();
                })
            </script>
        </div>
        <div class="page-right-box">
            <div class="panel panel-default panel-main no-radius">
                <ul class="mc-navbar">
                    <li class="item active">可使用</li>
                    <li class="item">已使用</li>
                    <li class="item">已失效</li>
                </ul>
                <div class="panel-body">
                    <ul class="mc-avg-sm-3 mc-avg-between-lg">
                        <li>
                            <a href="javascript:;" class="mc-poker-block" style="height:120px;">
                                <div class="a-item">
                                    <div class="mc-coupon">
                                        <div class="main-box">
                                            <div class="money text-center">
                                        <span class="text-size40 single-cut">
                                            300<span class="text-size14">元</span>
                                        </span>
                                            </div>
                                            <div class="info">
                                                <div class="text-size16 font-weight600">全场满300减30</div>
                                                <span class="text-size12">2018.11.16-2018.11.16</span>
                                            </div>
                                        </div>
                                        <div class="desc-box">
                                            <div class="row">
                                                <div class="col-sm-8">
                                                    <div class="single-cut text-black text-size14">梵希蔓Vimly女装特卖旗舰店梵希蔓Vimly女装特卖旗舰店</div>
                                                </div>
                                                <div class="col-sm-4 text-right">
                                                    <span class="text-size12 text-black">使用说明</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="b-item">
                                    <div class="mc-coupon" style="padding:10px;">
                                        <div class="desc-info">
                                            <div class="text-size16 line-height36 text-center">使用说明</div>
                                            <p>
                                                1.本券仅限在发行卡券的店铺内使用<br>
                                                2.有效期内购买商品折抵相应金额，超出或不到有效期无法使用<br>
                                                3.优惠卡券金额不设找零。
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" class="mc-poker-block" style="height:120px;">
                                <div class="a-item">
                                    <div class="mc-coupon">
                                        <div class="main-box">
                                            <div class="money text-center">
                                        <span class="text-size40 single-cut">
                                            300<span class="text-size14">元</span>
                                        </span>
                                            </div>
                                            <div class="info">
                                                <div class="text-size16 font-weight600">全场满300减30</div>
                                                <span class="text-size12">2018.11.16-2018.11.16</span>
                                            </div>
                                        </div>
                                        <div class="desc-box">
                                            <div class="row">
                                                <div class="col-sm-8">
                                                    <div class="single-cut text-black text-size14">梵希蔓Vimly女装特卖旗舰店梵希蔓Vimly女装特卖旗舰店</div>
                                                </div>
                                                <div class="col-sm-4 text-right">
                                                    <span class="text-size12 text-black">使用说明</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="b-item">
                                    <div class="mc-coupon" style="padding:10px;">
                                        <div class="desc-info">
                                            <div class="text-size16 line-height36 text-center">使用说明</div>
                                            <p>
                                                1.本券仅限在发行卡券的店铺内使用<br>
                                                2.有效期内购买商品折抵相应金额，超出或不到有效期无法使用<br>
                                                3.优惠卡券金额不设找零。
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" class="mc-poker-block" style="height:120px;">
                                <div class="a-item">
                                    <div class="mc-coupon">
                                        <div class="main-box">
                                            <div class="money text-center">
                                        <span class="text-size40 single-cut">
                                            300<span class="text-size14">元</span>
                                        </span>
                                            </div>
                                            <div class="info">
                                                <div class="text-size16 font-weight600">全场满300减30</div>
                                                <span class="text-size12">2018.11.16-2018.11.16</span>
                                            </div>
                                        </div>
                                        <div class="desc-box">
                                            <div class="row">
                                                <div class="col-sm-8">
                                                    <div class="single-cut text-black text-size14">梵希蔓Vimly女装特卖旗舰店梵希蔓Vimly女装特卖旗舰店</div>
                                                </div>
                                                <div class="col-sm-4 text-right">
                                                    <span class="text-size12 text-black">使用说明</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="b-item">
                                    <div class="mc-coupon" style="padding:10px;">
                                        <div class="desc-info">
                                            <div class="text-size16 line-height36 text-center">使用说明</div>
                                            <p>
                                                1.本券仅限在发行卡券的店铺内使用<br>
                                                2.有效期内购买商品折抵相应金额，超出或不到有效期无法使用<br>
                                                3.优惠卡券金额不设找零。
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
</div>
{include file='public/foot'}
{/block}
{block name='script'}
<script>
    $(function () {
        formHelper.initForm();
        utils.ajaxSubForm()
    });
    var  doAction={
        cell:function () {
            utils.openDialog({
                title:'绑定手机',
                offset:'auto',
                area:['500px','300px'],
                content: '/site/member/editcell.html'

            })
        }
    };
</script>
{/block}