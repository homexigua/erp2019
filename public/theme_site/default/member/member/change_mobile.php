{extend name="public/layout" /}
{block name='title'}更改手机号{/block}
{block name="head"}
<style>
    body{min-width:500px;width:100%;}
    .ct-info{width: 300px;margin: 0 auto;}
    .info-tt{padding: 20px 0 10px 0;}
    .input-bg{border: 1px solid #e8e8e8;font-weight: normal;margin-top: 2px;}
    .message-code{border: none;width: 170px;}
    .send-btn{margin-left: 10px;height: 35px;padding: 0 10px;display: inline-block;border: 1px solid #e8e8e8;line-height: 35px;}
    .ct-btn{height: 35px;padding-top: 25px;}
    .code-btn{color: #000000;display: block;}
    .code-btn:hover{color: #000000;}
    .send-btn:hover{background: #ececec;}
    .mh-btn{background: #ff6700;border: 1px solid #ff6700;color: #fff;min-width:100px;height: 33px;line-height: 33px;margin-top: 5px;}
</style>
{/block}
{block name="body"}
<div class="popup-box">
    <div class="popup-content">
        <div class="ct-info">
            <div class="info-tt">
                <h5 style="font-size: 15px;font-weight: bold;">为确认身份，我们仍需验证您的手机</h5>
            </div>
            <div class="info-ct">
                <p style="color: #666">点击发送短信按钮，将会发送一条有验证码的短信至手机<span style="color: #ff6700;padding-left: 3px;">{:substr_replace($memberInfo['mobile'],'******',3,6)}</span></p>
            </div>
        </div>
        <div class="ct-btn text-center">
            <button type="button" id="send" class="mh-btn">发送短信</button>
        </div>
    </div>
</div>
<script>
    $('#send').click(function () {
        var mobile = '{$memberInfo['mobile']}';
        $.get('/home/public/sendsms.html',{mobile:mobile,desc:'更换手机号'},function (res) {
            if(res.code==1){
                //跳转到文本框界面
                location.href = '/home/member/editcell2.html';
                //把发送按钮设置disabled

                //把发送按钮文字改成倒计时

            }else{
                alert(res.msg);
            }
        });
    });
</script>
{/block}
