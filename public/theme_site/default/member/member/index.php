{extend name="public:layout" /}
{block name="title"}我的资料_会员中心{/block}
{block name="head"}{/block}
{block name="body"}
{include file='public/head'}
<div class="memeber-page">
    <div class="container">
        <div class="page-left-box">
            {include file='member/index/user_menu'}
        </div>
        <div class="page-right-box">
            <div class="panel panel-default panel-main radian-bottom no-radius user-head-box">
                <div class="panel-body">
                    <div class="user-info-head mc-flex-box mc-flex-s">
                        <div class="head-img">
                            <img src="/static/head_img.png" class="img-responsive" alt="">
                        </div>
                        <div class="user-info">
                            <h3 class="user_name">{$memberInfo.username}</h3>
                            <p>绑定手机：{$memberInfo.mobile} <a href="javascript:;" class="btn btn-main btn-xs">更换绑定</a></p>
                            <p>推广链接：<a href="javascript:;" class="text-main">http://jqweui.site?tpl=member&amp;num=23432454</a> <a href="javascript:;" title="推广二维码" class="text-main iconfont icon-erweima1"></a></p>
                        </div>
                        <div class="msg-box">
                        </div>
                    </div>
                    <div class="asset-box">
                        <div class="row">
                            <div class="col-sm-10">
                                <ul class="mc-avg-sm-3 am-thumbnails">
                                    <li>我的积分：<a href="javascript:;" class="number">3,120.90</a></li>
                                    <li>我的金币：<a href="javascript:;" class="number">1,030.00</a></li>
                                    <li>我的元宝：<a href="javascript:;" class="number">¥120.90</a></li>
                                    <li>我的黑卡：<a href="javascript:;" class="number">¥300.00</a>(3张)</li>
                                    <li>优惠卡券：  <a href="javascript:;" class="number">3</a>张</li>
                                    <li>我的推荐：  <a href="javascript:;" class="number">300</a>人</li>
                                </ul>
                            </div>
                            <div class="col-sm-2 text-center">
                                <a href="javascript:;" class="btn btn-main btn-lg">兑换中心</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default panel-main no-radius">
                <div class="panel-heading">
                    我的订单
                    <a href="javascript:;" class="mc-text-link text-size12 pull-right">全部订单</a>
                </div>
                <div class="panel-body">
                    <ul class="mc-avg-sm-5">
                        <li>
                            <a href="javascript:;" class="mc-icon-link">
                                <i class="iconfont icon-daifukuan">
                                    <span class="badge badge-danger">2</span>
                                </i>
                                <p class="text">待付款</p>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" class="mc-icon-link">
                                <i class="iconfont icon-icon2"></i>
                                <p class="text">待发货</p>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" class="mc-icon-link">
                                <i class="iconfont icon-daishouhuo">
                                    <span class="badge badge-danger">8</span>
                                </i>
                                <p class="text">待收货</p>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" class="mc-icon-link">
                                <i class="iconfont icon-daipingjia"></i>
                                <p class="text">待评价</p>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" class="mc-icon-link">
                                <i class="iconfont icon-tuikuan"></i>
                                <p class="text">退款/售后</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="panel panel-default panel-main no-radius">
                <div class="panel-heading">
                    我的收藏
                    <a href="javascript:;" class="mc-text-link text-size12 pull-right">查看全部</a>
                </div>
                <div class="panel-body">
                    <ul class="mc-avg-sm-4 mc-avg-between">
                        <li>
                            <a href="javascript:;" class="mc-goods-item goods-collect-item mc-scale">
                                <span onclick="alert('删除成功')" class="del-icon iconfont icon-yduishanchu"></span>
                                <div class="goods-thumb">
                                    <img src="/static/pc/images/goods01.png" class="" alt="">
                                </div>
                                <div class="goods-info">
                                    <h3 class="text-size14 line-height30 single-cut">地表强温 男式鹅绒迷彩羽绒服</h3>
                                    <p class="text-size14 text-red">¥ 1299.00</p>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" class="mc-goods-item goods-collect-item mc-scale">
                                <span onclick="alert('删除成功')" class="del-icon iconfont icon-yduishanchu"></span>
                                <div class="goods-thumb">
                                    <img src="/static/pc/images/goods02.png" class="" alt="">
                                </div>
                                <div class="goods-info">
                                    <h3 class="text-size14 line-height30 single-cut">地表强温 男式鹅绒迷彩羽绒服</h3>
                                    <p class="text-size14 text-red">¥ 1299.00</p>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" class="mc-goods-item goods-collect-item mc-scale">
                                <span onclick="alert('删除成功')" class="del-icon iconfont icon-yduishanchu"></span>
                                <div class="goods-thumb">
                                    <img src="/static/pc/images/goods03.png" class="" alt="">
                                </div>
                                <div class="goods-info">
                                    <h3 class="text-size14 line-height30 single-cut">地表强温 男式鹅绒迷彩羽绒服</h3>
                                    <p class="text-size14 text-red">¥ 1299.00</p>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" class="mc-goods-item goods-collect-item mc-scale">
                                <span onclick="alert('删除成功')" class="del-icon iconfont icon-yduishanchu"></span>
                                <div class="goods-thumb">
                                    <img src="/static/pc/images/goods04.png" class="" alt="">
                                </div>
                                <div class="goods-info">
                                    <h3 class="text-size14 line-height30 single-cut">地表强温 男式鹅绒迷彩羽绒服</h3>
                                    <p class="text-size14 text-red">¥ 1299.00</p>
                                </div>
                            </a>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
{include file='public/foot'}
{/block}
{block name='script'}
<script>
    $(function () {
        formHelper.initForm();
        utils.ajaxSubForm()
    })
</script>
{/block}