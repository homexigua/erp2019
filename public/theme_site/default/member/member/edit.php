{extend name="public:layout" /}
{block name="title"}我的资料_会员中心{/block}
{block name="head"}{/block}
{block name="body"}
{include file='public/head'}
<div class="memeber-page">
    <div class="container">
        <div class="page-left-box">
            {include file='member/index/user_menu'}
        </div>
        <div class="page-right-box">
            <div class="panel panel-default panel-main no-radius">
                <div class="panel-heading">
                    我的资料
                </div>
                <div class="panel-body">
                    <div style="width:600px;margin:20px auto;">
                        <form class="form-horizontal" id="form">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">用户头像</label>
                                <div class="col-sm-10">
                                    <div class="input-group">
                                        <input type="hidden" class="form-control" name="headimgurl" id="input-upload-img" placeholder="请上传缩略图" />
                                        <span class="img-upload-box headimg-upload f-uploadimg" id="upload-img" data-option='{"uploaded":"formHelper.showThumb"}'>
                                            <a href="javascript:;" onclick="formHelper.delimg('upload-img')" class="del-icon">×</a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">登录账号</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="username" value="{$memberInfo.username}" placeholder="登录账号为字母与数字组合" datatype="*" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">昵称</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="nickname" value="{$memberInfo.nickname}" placeholder="请输入您的昵称" datatype="*">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">性别</label>
                                <div class="col-sm-9">
                                    <label class="checkbox-inline i-checks"><input type="radio" class="f-icheck" name="sex" value="1" {eq name='memberInfo.sex' value='1'}checked{/eq} /> 男 </label>
                                    <label class="checkbox-inline i-checks"><input type="radio" class="f-icheck" name="sex" value="2" {eq name='memberInfo.sex' value='2'}checked{/eq}/> 女 </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">手机号码</label>
                                <div class="col-sm-9">
                                    {empty name='memberInfo.mobile'}
                                    <p class="form-control-static"><a href="javascript:;" class="btn btn-main btn-xs">绑定手机</a></p>
                                    {else}
                                    <p class="form-control-static">{:hideStar($memberInfo.mobile)}<a href="javascript:;" class="btn btn-main btn-xs" onclick="doAction.cell()">更换绑定</a></p>
                                    {/empty}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">电子邮箱</label>
                                <div class="col-sm-9">
                                    {empty name='memberInfo.email'}
                                    <p class="form-control-static"><a href="javascript:;" class="btn btn-main btn-xs">绑定邮箱</a></p>
                                    {else}
                                    <p class="form-control-static">{:hideStar($memberInfo.email)}<a href="javascript:;" class="btn btn-main btn-xs">更换绑定</a></p>
                                    {/empty}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">出生日期</label>
                                <div class="col-sm-9">
                                    <input class="form-control f-laydate" name="birthday" value="" data-option='{}' readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"></label>
                                <div class="col-sm-9">
                                    <button type="button" class="btn btn-sub btn-lg btn-block btn-main">立即保存</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{include file='public/foot'}
{/block}
{block name='script'}
<script>
    $(function () {
        formHelper.initForm();
        utils.ajaxSubForm()
    });
    var  doAction={
        cell:function () {
            utils.openDialog({
                title:'绑定手机',
                offset:'auto',
                area:['500px','300px'],
                content: '/site/member/editcell.html'

            })
        }
    };
</script>
{/block}