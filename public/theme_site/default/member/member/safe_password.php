<script type="text/html" id="safe-password-tpl">
    <div class="m-d-30">
        <form class="form-horizontal" id="safe-form" action="/home/member.index/setpassword.html">
            <div class="form-group">
                <label class="col-sm-2 control-label">登录账号</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="username" value="{$user_info.username}" readonly>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">原密码</label>
                <div class="col-sm-9">
                    <input type="password" class="form-control" name="old_password" placeholder="请输入原密码" datatype="*">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">新密码</label>
                <div class="col-sm-9">
                    <input type="password" class="form-control" name="password" placeholder="请输入新密码" datatype="*">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">确认密码</label>
                <div class="col-sm-9">
                    <input type="password" class="form-control" name="confirm_password" placeholder="请再次输入新密码" datatype="*" recheck="password">
                </div>
            </div>
            <div class="fixed-foot text-right clearfix" style="width:100%;padding:10px 20px;position:absolute;bottom:0;left:0;background:#f8f8f8;border-top:1px solid #e8e8e8">
                <span class="valid-msg text-danger pull-left" style="margin-left:10px;display:none;"></span>
                <button type="button" class="btn btn-sub btn-info">确认修改</button>
                <button type="button" class="btn btn-default" onclick="layer.closeAll()">取消</button>
            </div>
        </form>
    </div>
</script>