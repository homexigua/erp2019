{extend name="public:layout" /}
{block name="title"}会员中心{/block}
{block name="head"}{/block}
{block name="body"}
{include file='public/head'}
<div class="memeber-page">
    <div class="container">
        <div class="page-left-box">
            {include file='member/index/user_menu'}
        </div>
        <div class="page-right-box">
            <div class="panel panel-default panel-main no-radius">
                <div class="panel-heading">
                    账户安全
                </div>
                <div class="panel-body">
                    <div style="width:800px;margin:20px auto;">
                        <div class="safe-info" style="padding:40px 50px">
                            <div class="row" style="margin-bottom:50px;">
                                <div class="col-md-2 text-right">
                                    <span class="text-danger">安全评级</span>
                                </div>
                                <div class="col-md-4">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100" style="width: 33%">
                                            <span class="sr-only"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <small>建议您经常更换密码，保障账号安全。</small>
                                </div>
                            </div>
                            <div class="row safe-item">
                                <div class="col-md-3">
                                    <div class="safe-tit"><i class="iconfont icon-mima"></i>登录密码</div>
                                </div>
                                <div class="col-md-6">
                                    建议您经常重置密码，防止密码泄露造成风险。
                                </div>
                                <div class="col-md-2 text-center">
                                    <a href="javascript:;" onclick="safeAction.setPassword()" class="mc-text-link">修改</a>
                                </div>
                            </div>
                            <div class="row safe-item">
                                <div class="col-md-3">
                                    <div class="safe-tit">
                                        <i class="iconfont icon-shouji2"></i>
                                        手机验证</div>
                                </div>
                                <div class="col-md-6">
                                    {notempty name='user_info.mobile'}
                                    已验证手机号码：{:hideStar($user_info.mobile)}
                                    {else}
                                    <span class="text-danger">未绑定手机号码</span>
                                    {/notempty}
                                </div>
                                <div class="col-md-2 text-center">
                                    {notempty name='user_info.mobile'}
                                    <a href=javascript:;" class="mc-text-link">更换验证</a>
                                    {else}
                                    <a href=javascript:;" class="mc-text-link">绑定邮箱</a>
                                    {/notempty}
                                </div>
                            </div>
                            <div class="row safe-item">
                                <div class="col-md-3">
                                    <div class="safe-tit">
                                        <i class="iconfont icon-youjian"></i>
                                        邮箱验证</div>
                                </div>
                                <div class="col-md-6">
                                    {notempty name='user_info.email'}
                                    已验证邮箱：{:hideStar($user_info.email)}
                                    {else}
                                    <span class="text-danger">未绑定邮箱</span>
                                    {/notempty}
                                </div>
                                <div class="col-md-2 text-center">
                                    {notempty name='user_info.email'}
                                    <a href="javascript:;" class="mc-text-link">更换验证</a>
                                    {else}
                                    <a href="javascript:;" class="mc-text-link">绑定邮箱</a>
                                    {/notempty}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{include file='public/foot'}
{include file='member/member/safe_password'}
{/block}
{block name='script'}
<script>
    var safeAction = {
        setPassword:function () {
            var html = juicer($('#safe-password-tpl').html(),{});
            layer.open({
                type: 1,
                title:'修改密码',
                skin: 'layui-layer-rim', //加上边框
                area: ['600px', '360px'], //宽高
                content:html,
                success:function () {
                    utils.ajaxSubForm(function () {
                        layer.closeAll();
                    },'#safe-form')
                }
            });

        }
    }
</script>
{/block}