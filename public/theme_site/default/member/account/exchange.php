{extend name="public:layout" /}
{block name="title"}兑换中心_我的资产{/block}
{block name="head"}{/block}
{block name="body"}
{include file='public/head'}
<div class="memeber-page">
    <div class="container">
        <div class="page-left-box">
            {include file='member/index/user_menu'}
        </div>
        <div class="page-right-box">
            <div class="panel panel-default panel-main no-radius">
                <ul class="mc-navbar">
                    <li class="item active">积分兑换优惠券</li>
                    <li class="item">积分兑换物品</li>
                    <li class="item">积分申请提现</li>
                    <a href="javascript:;" class="text-size12 text-main line-height40 pull-right"><i class="iconfont icon-yduigantanhaoshixin"></i> 兑换规则</a>
                </ul>
                <div class="panel-body">
                    <div class="alert alert-warning alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <b>为什么要兑换优惠券？</b><br>
                        购买时直接使用积分抵扣的话，本次购物将不再赠送积分。积分兑换黑卡后，使用黑卡进行购买可以继续获得积分。
                    </div>
                    <ul class="mc-avg-sm-3 mc-avg-between-lg">
                        <li>
                            <div class="mc-coupon-black text-center">
                                <div class="money">
                                            <span class="text-size60 single-cut">
                                                100<span class="text-size14">元</span>
                                            </span>
                                </div>
                                <div class="coupon-number">
                                    跨店使用，不设找零
                                </div>
                                <div class="text-size12 line-height36">领取后即刻生效，7日内有效</div>
                            </div>
                            <div class="row line-height40">
                                <div class="col-sm-6 text-size14 text-gray">
                                    需积分 <span class="text-red text-size18">100.00</span> 兑换
                                </div>
                                <div class="col-sm-6 text-right">
                                    <a href="javascript:;" class="btn btn-main btn-sm">立即兑换</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="mc-coupon-black text-center">
                                <div class="money">
                                            <span class="text-size60 single-cut">
                                                50<span class="text-size14">元</span>
                                            </span>
                                </div>
                                <div class="coupon-number">
                                    跨店使用，不设找零
                                </div>
                                <div class="text-size12 line-height36">领取后即刻生效，7日内有效</div>
                            </div>
                            <div class="row line-height40">
                                <div class="col-sm-6 text-size14 text-gray">
                                    需积分 <span class="text-red text-size18">50.00</span> 兑换
                                </div>
                                <div class="col-sm-6 text-right">
                                    <a href="javascript:;" class="btn btn-main btn-sm">立即兑换</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="mc-coupon-black text-center">
                                <div class="money">
                                            <span class="text-size60 single-cut">
                                                20<span class="text-size14">元</span>
                                            </span>
                                </div>
                                <div class="coupon-number">
                                    跨店使用，不设找零
                                </div>
                                <div class="text-size12 line-height36">领取后即刻生效，7日内有效</div>
                            </div>
                            <div class="row line-height40">
                                <div class="col-sm-6 text-size14 text-gray">
                                    需积分 <span class="text-red text-size18">20.00</span> 兑换
                                </div>
                                <div class="col-sm-6 text-right">
                                    <a href="javascript:;" class="btn btn-main btn-sm">立即兑换</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="mc-coupon-black text-center">
                                <div class="money">
                                            <span class="text-size60 single-cut">
                                                10<span class="text-size14">元</span>
                                            </span>
                                </div>
                                <div class="coupon-number">
                                    跨店使用，不设找零
                                </div>
                                <div class="text-size12 line-height36">领取后即刻生效，7日内有效</div>
                            </div>
                            <div class="row line-height40">
                                <div class="col-sm-6 text-size14 text-gray">
                                    需积分 <span class="text-red text-size18">10.00</span> 兑换
                                </div>
                                <div class="col-sm-6 text-right">
                                    <a href="javascript:;" class="btn btn-main btn-sm">立即兑换</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
{include file='public/foot'}
{/block}
{block name='script'}
<script>
    $(function () {
        formHelper.initForm();
    })
</script>
{/block}