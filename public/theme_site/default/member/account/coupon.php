{extend name="public:layout" /}
{block name="title"}优惠券_我的资产{/block}
{block name="head"}{/block}
{block name="body"}
{include file='public/head'}
<div class="memeber-page">
    <div class="container">
        <div class="page-left-box">
            {include file='member/index/user_menu'}
        </div>
        <div class="page-right-box">
            <div class="panel panel-default panel-main no-radius">
                <ul class="mc-navbar">
                    <li class="item active">可使用</li>
                    <li class="item">已使用</li>
                    <li class="item">已失效</li>
                </ul>
                <div class="panel-body">
                    <ul class="mc-avg-sm-3 mc-avg-between-lg">
                        <li>
                            <a href="javascript:;" class="mc-poker-block" style="height:120px;">
                                <div class="a-item">
                                    <div class="mc-coupon">
                                        <div class="main-box">
                                            <div class="money text-center">
                                        <span class="text-size40 single-cut">
                                            300<span class="text-size14">元</span>
                                        </span>
                                            </div>
                                            <div class="info">
                                                <div class="text-size16 font-weight600">全场满300减30</div>
                                                <span class="text-size12">2018.11.16-2018.11.16</span>
                                            </div>
                                        </div>
                                        <div class="desc-box">
                                            <div class="row">
                                                <div class="col-sm-8">
                                                    <div class="single-cut text-black text-size14">梵希蔓Vimly女装特卖旗舰店梵希蔓Vimly女装特卖旗舰店</div>
                                                </div>
                                                <div class="col-sm-4 text-right" >
                                                    <span class="text-size12 text-black">使用说明</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="b-item">
                                    <div class="mc-coupon" style="padding:10px;">
                                        <div class="desc-info">
                                            <div class="text-size16 line-height36 text-center">使用说明</div>
                                            <p>
                                                1.本券仅限在发行卡券的店铺内使用<br>
                                                2.有效期内购买商品折抵相应金额，超出或不到有效期无法使用<br>
                                                3.优惠卡券金额不设找零。
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" class="mc-poker-block" style="height:120px;">
                                <div class="a-item">
                                    <div class="mc-coupon">
                                        <div class="main-box">
                                            <div class="money text-center">
                                        <span class="text-size40 single-cut">
                                            300<span class="text-size14">元</span>
                                        </span>
                                            </div>
                                            <div class="info">
                                                <div class="text-size16 font-weight600">全场满300减30</div>
                                                <span class="text-size12">2018.11.16-2018.11.16</span>
                                            </div>
                                        </div>
                                        <div class="desc-box">
                                            <div class="row">
                                                <div class="col-sm-8">
                                                    <div class="single-cut text-black text-size14">梵希蔓Vimly女装特卖旗舰店梵希蔓Vimly女装特卖旗舰店</div>
                                                </div>
                                                <div class="col-sm-4 text-right" >
                                                    <span class="text-size12 text-black">使用说明</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="b-item">
                                    <div class="mc-coupon" style="padding:10px;">
                                        <div class="desc-info">
                                            <div class="text-size16 line-height36 text-center">使用说明</div>
                                            <p>
                                                1.本券仅限在发行卡券的店铺内使用<br>
                                                2.有效期内购买商品折抵相应金额，超出或不到有效期无法使用<br>
                                                3.优惠卡券金额不设找零。
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" class="mc-poker-block" style="height:120px;">
                                <div class="a-item">
                                    <div class="mc-coupon">
                                        <div class="main-box">
                                            <div class="money text-center">
                                        <span class="text-size40 single-cut">
                                            300<span class="text-size14">元</span>
                                        </span>
                                            </div>
                                            <div class="info">
                                                <div class="text-size16 font-weight600">全场满300减30</div>
                                                <span class="text-size12">2018.11.16-2018.11.16</span>
                                            </div>
                                        </div>
                                        <div class="desc-box">
                                            <div class="row">
                                                <div class="col-sm-8">
                                                    <div class="single-cut text-black text-size14">梵希蔓Vimly女装特卖旗舰店梵希蔓Vimly女装特卖旗舰店</div>
                                                </div>
                                                <div class="col-sm-4 text-right" >
                                                    <span class="text-size12 text-black">使用说明</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="b-item">
                                    <div class="mc-coupon" style="padding:10px;">
                                        <div class="desc-info">
                                            <div class="text-size16 line-height36 text-center">使用说明</div>
                                            <p>
                                                1.本券仅限在发行卡券的店铺内使用<br>
                                                2.有效期内购买商品折抵相应金额，超出或不到有效期无法使用<br>
                                                3.优惠卡券金额不设找零。
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
</div>
{include file='public/foot'}
{/block}
{block name='script'}
<script>
    $(function () {
        formHelper.initForm();
    })
</script>
{/block}