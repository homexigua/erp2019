{extend name="public:layout" /}
{block name="title"}我的返利_我的交易{/block}
{block name="head"}{/block}
{block name="body"}
{include file='public/head'}
<div class="memeber-page">
    <div class="container">
        <div class="page-left-box">
            {include file='member/index/user_menu'}
        </div>
        <div class="page-right-box">
            <div class="panel panel-sm panel-default no-radius point-head-box bg-gray">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-4 text-center left-box">
                            <div class="text-size18 line-height36">我的推荐码</div>
                            <img src="/static/pc/images/qrcode.png" style="width:80px;" alt="">

                        </div>
                        <div class="col-sm-8">
                            <div class="height-10"></div>
                            <h4 class="text-size18">推荐返利统计
                                <a href="javascript:;" class="text-size12 text-main pull-right"><i class="iconfont icon-yduigantanhaoshixin"></i> 推荐返利规则</a>
                            </h4>
                            <div class="height-10"></div>
                            <ul class="mc-avg-sm-3 mc-avg-between">
                                <li>
                                    <span class="text-size12">我的推荐</span>
                                    <div class="text-size18 line-height36">310 <span class="text-size12">人</span></div>
                                </li>
                                <li>
                                    <span class="text-size12">累计返利</span>
                                    <div class="text-size18 line-height36">￥300.00</div>
                                </li>
                                <li>
                                    <span class="text-size12">已提现金额</span>
                                    <div class="text-size18 line-height36">￥100.00</div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-sm panel-default no-radius">
                <div class="panel-heading">
                    推荐返利明细
                </div>
                <div class="panel-body">
                    <div class="order-search-box p-d-tf">
                        <div role="form" class="form-inline">
                            <div class="form-group">
                                <div class="input-group">
                                    <select name="type" class="form-control" style="width:120px;float:left">
                                        <option value="1">所有类型</option>
                                        <option value="1">支出</option>
                                        <option value="1">收入</option>
                                    </select>
                                    <input type="text" placeholder="请选择日期区间" class="form-control" name="date_range" value="" style="width:165px;float:left;margin-left:5px;">
                                    <span class="input-group-btn">
                                <button class="btn btn-white" onclick="getOrderList()">检索</button>
                            </span>
                                </div>
                            </div>
                            <div class="form-group pull-right">
                                <label class="sr-only">下单时间</label>
                                <select class="form-control" name="datetype" id="datetype" onchange="getOrderList()">
                                    <option value="0">全部</option>
                                    <option value="1">一月内</option>
                                    <option value="2">三月内</option>
                                    <option value="3">半年内</option>
                                    <option value="4">一年内</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="p-d-tf">
                        <table class="table point-table">
                            <thead>
                            <tr>
                                <th class="text-center">时间</th>
                                <th class="text-center">收入/支出</th>
                                <th class="text-center">详细说明</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="c1">2018-11-04 11:50:37</td>
                                <td class="c2"><span class="text-red">5</span></td
                                ><td class="c3">参加[科沃斯京东自营旗舰店]店铺活动-奖励</td>
                            </tr>
                            <tr>
                                <td class="c1">2018-10-10 17:38:31</td>
                                <td class="c2"><span class="text-red">16</span></td>
                                <td class="c3">购物送京豆(商品号:7545792)</td>
                            </tr>
                            <tr>
                                <td class="c1">2018-10-10 09:45:32</td>
                                <td class="c2"><span class="text-red">1</span></td>
                                <td class="c3">无线端签到领京豆</td>
                            </tr>
                            <tr>
                                <td class="c1">2018-10-07 18:06:50</td>
                                <td class="c2"><span class="text-red">20</span></td>
                                <td class="c3">商品评价奖励京豆(商品号:2943358)</td>
                            </tr>
                            <tr>
                                <td class="c1">2018-10-07 12:55:31</td>
                                <td class="c2"><span class="text-red">11</span></td>
                                <td class="c3">购物送京豆(商品号:2943358)</td>
                            </tr>
                            <tr>
                                <td class="c1">2018-10-06 08:09:39</td>
                                <td class="c2"><span class="text-green">-1000</span></td>
                                <td class="c3">订单80336182757使用京豆1000个</td>
                            </tr>
                            <tr>
                                <td class="c1">2018-08-23 10:04:46</td>
                                <td class="c2"><span class="text-red">19</span></td>
                                <td class="c3">购物送京豆(商品号:18449203616)</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{include file='public/foot'}
{/block}
{block name='script'}
<script>
    $(function () {
        formHelper.initForm();
    })
</script>
{/block}