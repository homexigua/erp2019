{extend name="public:layout" /}
{block name="title"}地址管理_会员中心{/block}
{block name="head"}{/block}
{block name="body"}
{include file='public/head'}
<div class="memeber-page">
    <div class="container">
        <div class="page-left-box">
            {include file='member/index/user_menu'}
        </div>
        <div class="page-right-box">
            <div class="panel panel-default panel-main no-radius">
                <div class="panel-heading">
                    地址管理
                    <a href="javascript:;" onclick="addrAction.addAddr()" class="btn btn-main btn-xs pull-right">添加地址</a>
                </div>
                <div class="panel-body">
                    <div id="address_list_box">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{include file='public/foot'}
{include file='member/address/add'}
{include file='member/address/edit'}
{/block}
{block name='script'}
<script type="text/html" id="addr-list-tpl">
    <table class="table address-table">
        <thead>
        <tr>
            <td>收货人</td>
            <td>联系电话</td>
            <td>收货地址</td>
            <td>操作</td>
        </tr>
        </thead>
        <tbody>
        {@each data as value }
        <tr>
            <td>${value.consignee_name}</td>
            <td>${value.consignee_phone}</td>
            <td>${value.prov}${value.city}${value.dist}${value.address}</td>
            <td>
                <a href="javascript:;" data-info="${json2string(value)}" class="action-btn" onclick="addrAction.editAddr(this)">编辑</a>
                <a href="javascript:;" data-id="${value.id}" class="action-btn" onclick="addrAction.delAddr(this)">删除</a>
                {@if value.is_default==1}
                <span style="padding:5px;border:1px solid #be9a60;color:#be9a60;background:#f4f4f4;margin:0 5px;">默认地址</span>
                {@else}
                <a href="javascript:;" data-id="${value.id}" class="action-btn" onclick="addrAction.setDefault(this)">设为默认</a>
                {@/if}
            </td>
        </tr>
        {@/each}
        </tbody>
    </table>
</script>
<script src="/lib/home/address.js"></script>
<script>
    var address_scene='address';
    $(function () {
        addrAction.getAddress();
    })
</script>
{/block}