<script type="text/html" id="add-addr-tpl">
    <div class="m-d-30">
        <form class="form-horizontal" id="address_form" action="/site/member.address/add.html">
            <input type="hidden" name="buyer_uid" value="{$loginInfo.id}">
            <div class="form-group">
                <label class="col-sm-2 control-label require">收货人</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="consignee_name" placeholder="请输入收货人姓名" datatype="*" nullmsg="请输入收货人姓名">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label require">联系电话</label>
                <div class="col-sm-10">
                    <input type="tel" class="form-control" name="consignee_phone" placeholder="请输入收货人联系电话" datatype="*" nullmsg="请输入收货人联系电话">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label require">所在区域</label>
                <div class="col-sm-10">
                    <div id="cxselect" class="f-cx-select">
                        <select class="prov" name="prov" datatype="*" nullmsg="请选择省份"></select>
                        <select class="city" name="city" datatype="*" nullmsg="请选择城市"></select>
                        <select class="dist" name="dist"></select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label require">详细地址</label>
                <div class="col-sm-10">
                    <input type="tel" class="form-control" name="address" placeholder="请输入详细地址" datatype="*">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-6 col-sm-offset-2">
                    <input type="checkbox" name="is_default" class="f-switch"
                           data-option='{"onText":"设为默认","offText":"普通地址","onColor":"info","offColor":"default","size":"lg"}'
                           placeholder="" value="1" checked/> 设为默认
                </div>
                <div class="col-sm-4">
                    <button type="button" class="btn btn-sub btn-block btn-main no-radius">立即保存</button>
                </div>
            </div>
        </form>
    </div>
</script>