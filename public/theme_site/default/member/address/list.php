<script type="text/html" id="addr-list-tpl">
    <div class="address-box" style="position:relative;width:100%;height:100%;overflow-y:auto;padding-bottom:50px;">
        <div class="m-d-30" id="address_list">
            {@each data as value }
            <div class="mc-address-item {@if value.is_default==1} active {@/if}" data-info="${json2string(value)}">
                <div class="row line-height24 text-size14 text-gray">
                    <div class="col-sm-3">收件人：</div>
                    <div class="col-sm-8 text-black">${value.consignee_name}</div>
                    <div class="col-sm-3">联系电话：</div>
                    <div class="col-sm-8 text-black">${value.consignee_phone}</div>
                    <div class="col-sm-3">收货地址：</div>
                    <div class="col-sm-8 text-black">${value.prov}${value.city}${value.dist}${value.address}</div>
                </div>
            </div>
            {@/each}
        </div>
    </div>
    <div class="fixed-foot text-right" style="width:100%;padding:10px 20px;position:absolute;bottom:0;left:0;background:#f8f8f8;border-top:1px solid #e8e8e8">
        <button type="button" class="btn btn-info" id="selectAddr">确认</button>
        <button type="button" class="btn btn-default" onclick="layer.closeAll()">取消</button>
    </div>
</script>