{extend name="public:layout" /}
{block name="title"}订单列表_我的交易{/block}
{block name="head"}{/block}
{block name="body"}
{include file='public/head'}
<div class="memeber-page">
    <div class="container">
        <div class="page-left-box">
            {include file='member/index/user_menu'}
        </div>
        <div class="page-right-box">
            <div class="panel panel-default panel-main no-radius">
                <div class="panel-heading">
                    <span class="text-size16 pull-left">我的订单</span>
                    <ul class="tabs pull-left">
                        <li class="item active"> 全 部 </li>
                        <li class="item"> 待付款 <span class="badge badge-danger">3</span> </li>
                        <li class="item"> 待确认 </li>
                        <li class="item"> 待发货 </li>
                        <li class="item"> 待评价 </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <div class="order-search-box p-d-tf">
                        <div role="form" class="form-inline">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" placeholder="请输入订单编号检索" class="form-control" id="order_par" name="order_sn" value="" style="width:165px;float:left">
                                    <input type="text" placeholder="请输入商品名称检索" class="form-control" id="goods_name" name="goods_name" value="" style="width:165px;float:left;margin-left:5px;">
                                    <span class="input-group-btn">
                                <button class="btn btn-white" onclick="getOrderList()">检索</button>
                            </span>
                                </div>
                            </div>
                            <div class="form-group pull-right">
                                <label class="sr-only">下单时间</label>
                                <select class="form-control" name="datetype" id="datetype" onchange="getOrderList()">
                                    <option value="0">全部</option>
                                    <option value="1">一月内</option>
                                    <option value="2">三月内</option>
                                    <option value="3">半年内</option>
                                    <option value="4">一年内</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="order-list-group" id="order-list">
                        <table class="order-table">
                            <thead>
                            <tr>
                                <td colspan="6" style="text-align:left">
                                    订单编号: <a href="/home/index/you.html?tpl=order_info" class="order-sn">180510506724033901</a>
                                    <span class="pull-right">
                                        <span class="datetime">下单时间：2018-05-10 19:11</span>
                                    </span>
                                </td>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th class="goods-info">商品信息</th>
                                <th class="goods-price">单价</th>
                                <th class="goods-num">数量</th>
                                <th class="goods-total">订单总额</th>
                                <th class="status-text">订单状态</th>
                                <th class="action">操作</th>
                            </tr>
                            <tr>
                                <td>
                                    <a href="javascript:;" class="mc-width-auto left-box mc-goods-info">
                                        <img class="float-box goods-thumb" src="http://img1.shikee.com/try/2016/06/19/09430120929215230041.jpg_220x220.jpg" alt="">
                                        <div class="text-size14 font-weight500 single-cut">地表强温 男式鹅绒迷彩羽绒服</div>
                                        <span class="text-size12 text-gray">黑色迷彩 XXL:185/100A</span>
                                    </a>
                                </td>
                                <td>￥222.00</td>
                                <td>1</td>
                                <td rowspan="2">￥333.00</td>
                                <td rowspan="2" style="text-align:left;line-height:1.8">
                                    订单已确认，合同生成中
                                </td>
                                <td rowspan="2">
                                    <a href="/home/index/you.html?tpl=order_info" class="btn btn-xs btn-block btn-info">订单详情</a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="javascript:;" class="mc-width-auto left-box mc-goods-info">
                                        <img class="float-box goods-thumb" src="http://img1.shikee.com/try/2016/06/19/09430120929215230041.jpg_220x220.jpg" alt="">
                                        <div class="text-size14 font-weight500 single-cut">地表强温 男式鹅绒迷彩羽绒服</div>
                                        <span class="text-size12 text-gray">黑色迷彩 XXL:185/100A</span>
                                    </a>
                                </td>
                                <td>￥333.00</td>
                                <td>1</td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="order-table">
                            <thead>
                            <tr>
                                <td colspan="6" style="text-align:left">
                                    订单编号: <a href="/home/index/you.html?tpl=order_info" class="order-sn">180509652029657401</a>
                                    <span class="pull-right">
                                        <span class="datetime">下单时间：2018-05-09 19:26</span>
                                    </span>
                                </td>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th class="goods-info">商品信息</th>
                                <th class="goods-price">单价</th>
                                <th class="goods-num">数量</th>
                                <th class="goods-total">订单总额</th>
                                <th class="status-text">订单状态</th>
                                <th class="action">操作</th>
                            </tr>
                            <tr>
                                <td>
                                    <a href="javascript:;" class="mc-width-auto left-box mc-goods-info">
                                        <img class="float-box goods-thumb" src="http://img1.shikee.com/try/2016/06/19/09430120929215230041.jpg_220x220.jpg" alt="">
                                        <div class="text-size14 font-weight500 single-cut">地表强温 男式鹅绒迷彩羽绒服</div>
                                        <span class="text-size12 text-gray">黑色迷彩 XXL:185/100A</span>
                                    </a>
                                </td>
                                <td>￥10500.00</td>
                                <td>1</td>
                                <td rowspan="2">￥10833.00</td>
                                <td rowspan="2" style="text-align:left;line-height:1.8">
                                    已提交订单，请等待商家确认！
                                </td>
                                <td rowspan="2">
                                    <a href="javascript:;" onclick="delOrder(this)" data-ordersn="180509652029657401" class="btn btn-xs btn-block btn-danger">取消订单</a>
                                    <a href="/home/index/you.html?tpl=order_info" class="btn btn-xs btn-block btn-info">订单详情</a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="javascript:;" class="mc-width-auto left-box mc-goods-info">
                                        <img class="float-box goods-thumb" src="http://img1.shikee.com/try/2016/06/19/09430120929215230041.jpg_220x220.jpg" alt="">
                                        <div class="text-size14 font-weight500 single-cut">地表强温 男式鹅绒迷彩羽绒服</div>
                                        <span class="text-size12 text-gray">黑色迷彩 XXL:185/100A</span>
                                    </a>
                                </td>
                                <td>￥333.00</td>
                                <td>1</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="mc-no-msg">
                        <span class="iconfont icon-duanxin"></span>
                        <p class="text">没有相应的订单信息</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{include file='public/foot'}
{/block}
{block name='script'}
<script>
    $(function () {
        formHelper.initForm();
    })
</script>
{/block}