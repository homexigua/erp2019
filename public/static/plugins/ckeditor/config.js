CKEDITOR.editorConfig = function( config ) {
    config.toolbar = 'Full';
    config.toolbar_Full =
        [
            { name: 'document', items : [ 'Source','-' ] },
            { name: 'clipboard', items : [ 'Undo','Redo' ,'-','Cut','Copy','Paste','PasteText','PasteFromWord' ] },
            { name: 'editing', items : [ 'SelectAll','Replace','-' ] },

            { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
            { name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent', '-','JustifyLeft','JustifyCenter','JustifyRight' ] },
            '/',
            { name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
            { name: 'colors', items : [ 'TextColor','BGColor' ] },
            { name: 'links', items : [ 'Link','Unlink','-','Blockquote' ] },
            { name: 'insert', items : [ 'Image','html5video','Table','HorizontalRule','SpecialChar','PageBreak' ] },
            { name: 'tools', items : [  'ShowBlocks','Maximize' ] }
        ];
    config.toolbar_Basic =
        [
            ['Bold', 'Italic','Underline','-','JustifyLeft','JustifyCenter','JustifyRight','-','Link','Unlink','-','Image']
        ];
    // 界面语言
    config.language = 'zh-cn';
    config.font_names = '宋体/SimSun;微软雅黑/Microsoft YaHei;'+config.font_names;
    //上传插件替换
    config.extraPlugins = 'image2,widget,dialog,html5video,widgetselection,clipboard,lineutils';
    // 编辑器的z-index值
    config.baseFloatZIndex = 10000;
    //去除上传窗口多余按钮
    config.removeDialogTabs = 'image:advanced;image:Link;link:advanced;link:target;link:Link;flash:advanced;flash:Link;';
    //上传相关
    //config.filebrowserBrowseUrl = '/admin/upload/browse.html?opener=ckeditor&ext_type=all'; //文件浏览
    //config.filebrowserImageBrowseUrl = '/admin/upload/browse.html?opener=ckeditor&ext_type=img'; //图片浏览
    config.filebrowserUploadUrl = '/admin/upload/file.html?opener=ckeditor&ext_type=file'; //文件上传
    config.filebrowserImageUploadUrl = '/admin/upload/file.html?opener=ckeditor&ext_type=img'; //图片上传
};