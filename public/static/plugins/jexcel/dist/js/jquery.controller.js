var selectGoods={ //商品选择扩展
    // Methods
    closeEditor : function(cell, save) {
        //移除编辑状态
        $(cell).removeClass('edition');
        var position = $(cell).prop('id').split('-');
        var row = $.fn.jexcel.defaults['edit-grid'].data[position[1]][header.length];
        if(typeof row=='undefined'){
            $(cell).text('');
        }else{
            var field = header[position[0]]['field'];
            $(cell).text(row[field]);
        }
        jexcelExtend.editRow(position[1],row);
        //保存操作记录
        if(typeof row=='undefined'){
            return '';
        }else{
            return row[field];
        }
    },
    openEditor : function(cell) {
        $(cell).addClass('edition');
        //获取当前内容
        var value = $(cell).text();
        //创建编辑器
        var position = $(cell).prop('id').split('-');
        var field = header[position[0]]['field'];
        var editor = document.createElement('input');
        $(editor).prop('class', 'editor');
        $(editor).css('width', $(cell).width());
        $(editor).css('height', $(cell).height());
        $(editor).val($.trim(value));
        $(cell).html(editor);
        //显示匹配商品
        var left= $(cell).offset().left+'px';
        var top= ($(cell).offset().top);
        if(top>500){
            top = top-225+'px';
        }else{
            top = top+28+'px';
        }
        $(editor).on('focus input propertychange', function() {
            var changText = $(this).val();
            layer.open({
                type:1,
                id:"select-layer",
                area:['860px','235px'],
                title:false,
                closeBtn:0,
                offset: [top, left],
                shade:0.01,
                shadeClose:true,
                end: function(index, layero){
                    $('#' + $.fn.jexcel.current).jexcel('closeEditor', $(cell), true);
                }
            });
            var html='<div class="table-responsive" style="padding:4px 8px;"><table id="select-grid" class="table-condensed"></table></div>';
            $('#select-layer').html(html);
            In('btable',function () {
                $('#select-grid').bootstrapTable({
                    url:'/admin/mall.goods_sku/select.html',
                    method: 'post', //请求方法
                    contentType:'application/x-www-form-urlencoded', //请求方式
                    sidePagination: 'server',
                    queryParamsType:'page',
                    height:'220',
                    pageSize: 15,
                    pageList: [10, 25, 50, 'All'],
                    showExport: false,
                    pagination: true,
                    clickToSelect: true, //是否启用点击选中
                    singleSelect: true, //是否启用单选
                    showRefresh: false,
                    showToggle: false,
                    showColumns: false,
                    columns:[
                        { field:'state',checkbox:true,width:50 },
                        { field:'bar_code',title:'商品条码',width:100},
                        { field:'goods_name',title:'商品名称'},
                        { field:'goods_price',title:'成本价',width:100,sortable:true},
                        { field:'goods_price',title:'零售价',width:100,sortable:true},
                        { field:'usable_stock',title:'可用量',width:100,sortable:true},
                        { field:'goods_stock',title:'现存量',width:100,sortable:true},
                    ],
                    queryParams:function (params) {
                        params[field]=$.trim(changText);
                        return params;
                    },
                    onCheck:function (row, $el) {
                        $.fn.jexcel.defaults['edit-grid'].data[position[1]][header.length]=row;
                        layer.closeAll('page');
                    }
                });
            })
        });
        $(editor).focus();
    },
    getValue : function(cell) {
        return $(cell).text();
    },
    setValue : function(cell, value) {
        $(cell).text(value);
        return true;
    }
};