//统一复选框数据
var jexcelExtend={
    init:function(option){
        var config = jexcelExtend.getConfig();
        var data=jexcelExtend.initItems();
        var defaultOption={
            data: data,
            colHeaders: config.colHeaders,
            colWidths: config.colWidths,
            columns: config.columns,
            colAlignments:config.colAlignments,
            tableHeight:'600px',
            contextMenu: function (type, row) { //row or col,当前位置
                if (type == 'row') {
                    var contextMenuContent = "<a onclick=\"jexcelExtend.addBlankRow()\">新增行</a>";
                    contextMenuContent += "<a onclick=\"jexcelExtend.delRow("+row+")\">删除行</a>";
                    contextMenuContent += "<a onclick=\"$('#edit-grid').jexcel('undo')\">撤销/还原(Ctrl+Z)</a>";
                    contextMenuContent += "<a onclick=\"$('#edit-grid').jexcel('download')\">导出数据(Ctrl+S)</a>";
                    return contextMenuContent;
                }else{
                    return '';
                }
            }
        };
        var options = option || {};
        var jexcelOption = $.extend({},defaultOption,options);
        $('#edit-grid').jexcel(jexcelOption);
    },
    /**
     * 添加空行
     */
    addBlankRow:function(){
        var data = $.fn.jexcel.defaults['edit-grid'].data; //获取数据
        data = _.dropRight(data); //去除合计行
        data.push([]); //增加空行
        var foot=this.getFooter(data.length);
        data.push(foot);
        $('#edit-grid').jexcel('setData',data,false);
    },
    /**
     * 添加数据
     * @param items 增加的数据项
     * @returns {boolean}
     */
    addRow:function(items){
        if(typeof items=='undefined') return;
        var that=this;
        //处理表格数据
        var data = $.fn.jexcel.defaults['edit-grid'].data; //获取数据
        var skuIndex = that.getIndex('sku_id');
        //去除最后一行合计行循环插入
        var j=0;
        for(var i=0;i<data.length-2;i++){
            if(data[i][skuIndex]=='' || typeof data[i][skuIndex]=='undefined'){  //判断是否有skuid
                var item = items[j];
                if(typeof item!='undefined'){
                    data[i] = that.formatRow(item,i+1); //更新data数据
                    data[i][header.length]=item;
                    j++;
                }
            }
        }
        $('#edit-grid').jexcel('setData',data,false);
    },
    /**
     * 更新数据
     */
    editRow:function(index,editItem){
        if(typeof editItem=='undefined') return;
        var cols=['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ'];
        for (var j = 0; j < header.length; j++) {
            var cell = cols[j]+(parseInt(index)+1);
            if (editItem.hasOwnProperty(header[j]['field'])) {  //只修改存在数据的项目
                $('#edit-grid').jexcel('setValue', cell, editItem[header[j]['field']],true);
            }else{
                if (header[j].hasOwnProperty('default')) {
                    var defaultValue = header[j]['default'];
                    if (isNaN(defaultValue)) {
                        defaultValue = defaultValue.replace(/\[i\]/g,parseInt(index)+1);
                    }
                    $('#edit-grid').jexcel('setValue', cell, defaultValue,true);
                }
            }
        }
    },
    /**
     * 删除行
     * @param index
     * @returns {boolean}
     */
    delRow:function(index){
        var data = $('#edit-grid').jexcel('getData', false);
        if (index == data.length - 1) {
            layer.msg('不允许删除合计行！');
            return false;
        }
        $('#edit-grid').jexcel('deleteRow',index); //删除数据
    },
    //设置合计行
    getFooter:function(index){
        var foot=[];
        for (var i = 0; i < header.length; i++) {
            if(i==0){
                foot.push('合计');
            }else{
                //补充合计行
                if (header[i].hasOwnProperty("col_calc")) {
                    var calc = header[i]['col_calc'];
                    calc = calc.replace(/\[END\]/g, index);
                    foot.push(calc);
                } else {
                    foot.push('');
                }
            }
        }
        return foot;
    },
    //格式化一条数据
    formatRow:function(row,index){
        var arr=[];
        for (var j = 0; j < header.length; j++) {
            if (row.hasOwnProperty(header[j]['field'])) {
                arr.push(row[header[j]['field']]); //push数据
            } else {
                if (header[j].hasOwnProperty('default')) {
                    var defaultValue = header[j]['default'];
                    if (isNaN(defaultValue)) {
                        defaultValue = defaultValue.replace(/\[i\]/g, index);
                    }
                    arr.push(defaultValue);
                } else {
                    arr.push("");
                }
            }
        }
        return arr;
    },
    //初始化20行空数据
    initItems:function(){
        var data=[];
        for(var i=0;i<18;i++){
            data.push([]);
        }
        //插入合计行
        var foot=this.getFooter(data.length);
        data.push(foot);
        return data;
    },
    /**
     * 获取表格配置
     */
    getConfig:function(){
        var res={
            colField:[],
            colHeaders:[],
            colWidths:[],
            columns:[],
            colAlignments:[] //左中右
        };
        for (var i = 0; i < header.length; i++) {
            res.colField.push(header[i].field);
            res.colHeaders.push(header[i].label);
            var width = header[i].hasOwnProperty("width") ? header[i].width : 100;
            res.colWidths.push(width);
            var column = header[i].hasOwnProperty("column") ? header[i].column : {type: "text"};
            if(column.hasOwnProperty('filter')){ //有对应过滤方法
                column.filter = eval(column.filter);
            }
            res.columns.push(column);
            var align = header[i].hasOwnProperty("align") ? header[i].align : "center";
            res.colAlignments.push(align);
        }
        return res;
    },
    /**
     * 反向获取复选框值
     */
    getBackCheckboxValue:function(val) {
        var value = this.getCheckboxValue(val);
        return value==1 ? 0 : 1;
    },
    /**
     * 正向获取复选框值
     */
    getCheckboxValue:function(val) {
        if (val == "true" || val == "1") return 1;
        if (val == "false" || val == "0") return 0;
    },
    /**
     * 格式化列计算
     */
    formatCols:function (instance, cell, col, row, val, id,type) {
        var m = typeof(type)=="undefined" || type=='' || type.indexOf('退货')==-1?1:-1;
        var item = $.fn.jexcel.defaults['edit-grid'].data[row];
        if ((header[col].hasOwnProperty("format") && typeof(item[header.length])!='undefined')) {
            var format = header[col]['format'];
            txt = $(cell).text();
            txt = numeral(txt).format(format.format);
            if (format.display.length > 3) { //大于3表示有计算公式
                txt = eval(format.display);
                txt = numeral(txt).format(format.format); //算完再格式化一次
            }
            //补充前缀
            if (format.hasOwnProperty("prefix")) {
                txt = format.prefix + txt;
            }
            //补充后缀
            if (format.hasOwnProperty("suffix")) {
                txt = txt + format.suffix;
            }
            if(header[col]['field']=='goods_num' &&m==-1){
                txt = '<span style="color:#ff0000">'+txt+'</span>';
            }
            $(cell).html('<input type="hidden" value="' + val + '"> ' +txt);
        }
        if($(cell).text()=='合计'){
            $('tr#row-'+row+' td').addClass('readonly').css({'background-color': '#edf3ff', 'color': '#249D7F'});
        }
    },
    round:function (number, pre) {
        var num = number.toFixed(pre);
        number = numeral(num).format('0.00');
        return number;
    },
    getCol:function (col) {
        var cols=['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ'];
        if(isNaN(col)){ //字段名
            for(var i=0;i<header.length;i++){
                if(header[i].field==col){
                    return cols[i];
                }
            }
        }else{
            return cols[col];
        }
    },
    getIndex:function (fieldName) { //根据字段名获取索引
        var len = header.length;
        for(var i=0;i<len;i++){
            if(header[i].field==fieldName){
                return i;
            }
        }
    }
};
