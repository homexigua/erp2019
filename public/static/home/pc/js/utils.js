var utils = {
    //刷新页面
    refreshPage: function () {
        window.location.reload();
        return false;
    },
    //返回上一页
    back: function () {
        window.history.back(-1);
    },
    //跳转地址
    urlHref: function (url) {
        window.location.href = url;
    },
    //复选框全选
    checkAll: function (dom) {
        var obj = dom || '#grid';
        $(obj + " :checkbox").prop("checked", true);
    },
    //复选框反选
    checkOther: function (dom) {
        var obj = dom || '#grid';
        $(obj + " :checkbox").each(function () {
            $(this).prop("checked", !$(this).prop("checked"));
        });
    },
    //获取复选框值
    getCheckboxValue: function (dom) {
        var obj = dom || '#grid';
        var reVal = '';
        $(obj).find("input[type='checkbox']").each(function () {
            if ($(this).prop("checked")) {
                reVal += $(this).val() + ",";
            }
        });
        reVal = reVal.substr(0, reVal.length - 1);
        return reVal;
    },
    //获取复选框值
    getCheckboxArray: function (dom) {
        var obj = dom || '#grid';
        var reVal = [];
        $(obj).find("input[type='checkbox']").each(function () {
            if ($(this).prop("checked")) {
                reVal.push($(this).val());
            }
        });
        return reVal;
    },
    //检测一条记录
    checkOne: function (reVal) {
        var flag = true;
        if (reVal == undefined || reVal == "") {
            alert('请选择要操作的数据');
            flag = false;
        } else if (reVal.split(",").length > 1) {
            alert('对不起，只能选择一条数据');
            flag = false;
        }
        return flag;
    },
    //检测多条记录
    checkMore: function (reVal) {
        var flag = true;
        if (reVal == undefined || reVal == "") {
            alert('请选择要操作的数据!');
            flag = false;
        }
        return flag;
    },
    //获取json指定列数据
    jsonCol: function (json, col, split) {
        var len = json.length;
        if (len == 0) {
            return [];
        } else {
            var reval = [];
            for (var i = 0; i < len; i++) {
                reval.push(json[i][col]);
            }
            split = split || ','
            return reval.join(split);
        }
    },
    //json转字符串
    json2string: function (json) {
        return JSON.stringify(json);
    },
    //时间戳友好显示
    dateDiff: function (timestamp) {
        // 补全为13位
        var arrTimestamp = (timestamp + '').split('');
        for (var start = 0; start < 13; start++) {
            if (!arrTimestamp[start]) {
                arrTimestamp[start] = '0';
            }
        }
        timestamp = arrTimestamp.join('') * 1;
        var minute = 1000 * 60;
        var hour = minute * 60;
        var day = hour * 24;
        var halfamonth = day * 15;
        var month = day * 30;
        var now = new Date().getTime();
        var diffValue = now - timestamp;
        // 如果本地时间反而小于变量时间
        if (diffValue < 0) {
            return '不久前';
        }
        // 计算差异时间的量级
        var monthC = diffValue / month;
        var weekC = diffValue / (7 * day);
        var dayC = diffValue / day;
        var hourC = diffValue / hour;
        var minC = diffValue / minute;

        // 数值补0方法
        var zero = function (value) {
            if (value < 10) {
                return '0' + value;
            }
            return value;
        };
        // 使用
        if (monthC > 12) {
            // 超过1年，直接显示年月日
            return (function () {
                var date = new Date(timestamp);
                return date.getFullYear() + '年' + zero(date.getMonth() + 1) + '月' + zero(date.getDate()) + '日';
            })();
        } else if (monthC >= 1) {
            return parseInt(monthC) + "月前";
        } else if (weekC >= 1) {
            return parseInt(weekC) + "周前";
        } else if (dayC >= 1) {
            return parseInt(dayC) + "天前";
        } else if (hourC >= 1) {
            return parseInt(hourC) + "小时前";
        } else if (minC >= 1) {
            return parseInt(minC) + "分钟前";
        }
        return '刚刚';
    },
    //格式化时间戳
    dateFormat: function (timestamp, formats) {
        // formats格式包括
        // 1. Y-m-d
        // 2. Y-m-d H:i:s
        // 3. Y年m月d日
        // 4. Y年m月d日 H时i分
        formats = formats || 'Y-m-d';

        var zero = function (value) {
            if (value < 10) {
                return '0' + value;
            }
            return value;
        };
        timestamp = timestamp * 1000;
        var myDate = timestamp ? new Date(timestamp) : new Date();

        var year = myDate.getFullYear();
        var month = zero(myDate.getMonth() + 1);
        var day = zero(myDate.getDate());

        var hour = zero(myDate.getHours());
        var minite = zero(myDate.getMinutes());
        var second = zero(myDate.getSeconds());

        return formats.replace(/Y|m|d|H|i|s/ig, function (matches) {
            return ({
                Y: year,
                m: month,
                d: day,
                H: hour,
                i: minite,
                s: second
            })[matches];
        });
    },
    //获取url参数
    getQueryString: function (name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
        var r = window.location.search.substr(1).match(reg);
        if (r != null) return unescape(r[2]);
        return null;
    },
    //是否手机
    isMobile: function () {
        var ua = navigator.userAgent;
        var ipad = ua.match(/(iPad).*OS\s([\d_]+)/),
            isIphone = !ipad && ua.match(/(iPhone\sOS)\s([\d_]+)/),
            isAndroid = ua.match(/(Android)\s+([\d.]+)/),
            isMobile = isIphone || isAndroid;
        if (isMobile) {
            return 1;
        } else {
            return 0;
        }
    },
    //设置滚动条
    scroll: function (dom, decHeight) {
        var bodyWidth = $(document.body).width();
        var windowHeight = $(window).height();
        if (bodyWidth > 990) {
            var boxHeight = windowHeight - decHeight;
            $(dom).slimScroll({
                height: boxHeight + 'px',
                size: '4px'
            });
        }
    },
    //获取表格高度
    getTableHeight: function (decHeight) {
        var bodyWidth = $(document.body).width();
        var height = undefined;
        if (bodyWidth > 990) {
            height = $(window).height() - decHeight;
        }
        return height;
    },
    //alert框
    alert: function (content, options, yes) {
        if (typeof  options === 'function') {
            yes = options;
        }
        if (typeof  options !== 'object') {
            options = {icon: 7};
        }
        return layer.alert(content, options, yes);
    },
    //确认框
    confirm: function (content, options, yes,no) {
        if (typeof  options === 'function') {
            yes = options;
        }
        if (typeof  options !== 'object') {
            options = {icon: 3, title: '提示信息'};
        }
        return layer.confirm(content, options, yes,no);
    },
    //消息框
    msg: function (content, icon, end) {
        if (typeof  icon === 'function') {
            end = icon;
        }
        var options = {time: 1500};
        if (!isNaN(icon)) {
            options = {icon: icon, time: 1200};
        }
        return layer.msg(content, options, end);
    },
    //加载层
    loading: function () {
        layer.load(1);
    },
    //关闭加载层
    closeLoading: function () {
        layer.closeAll('loading');
    },
    //打开页面层
    open: function (options) {
        var defaultOptions = {
            type: 1,
            offset:'8%',
            area: '980px'
        };
        var options = $.extend({}, defaultOptions, options);
        var index = layer.open(options);
        if (utils.isMobile() == 1 || options.full==1) {
            layer.full(index);
        }
    },
    openFullPage:function(url,title){
        var index = layer.open({
            type:2,
            title:title,
            shade:0.1,
            btnAlign:'l',
            area:['1000px','400px'],
            maxmin:true,
            resize:false,
            content:url
        });
        layer.full(index);
    },
    openPage:function (url,title) { //后台打开对应页面
        //弹框打开
        if(openTab=='dialog'){
            layer.open({
                type:2,
                title:title,
                shade:0.1,
                btnAlign:'l',
                maxmin:true,
                resize:false,
                area:['980px','580px'],
                content:url
            });
        }
        if(openTab=='iframe'){
            //判断当前页面是否在iframe中
            if (isIframe===true) {
                top.addTabs({id: url,url:url, title: title});
            }else{
                utils.urlHref(url);
            }
        }
    },
    closeOpen: function(){ //关闭打开窗口
        var isIframe = top == this ? false : true;
        if(isIframe===true){
            var index = parent.layer.getFrameIndex(window.name);
            parent.layer.close(index);
        }else{
            layer.close(layer.index);
        }
    },
    //ajax分页封装
    pagination: function (options, dom) {
        var obj = dom || '#page';
        var params = options.params || {};
        In('pagination', function () {
            var defaultConfig = {
                locator: 'rows',
                totalNumberLocator: function (res) {
                    return res.total;
                },
                className: 'paginationjs-theme-blue paginationjs-small',
                pageSize: 15,
                prevText: '上一页',
                nextText: '下一页',
                autoHidePrevious: true, //隐藏上一页
                autoHideNext: true, //隐藏下一页
                ajax: {
                    type: 'post',
                    data: params,
                    beforeSend: function () {
                        utils.loading();
                    },
                    complete: function () {
                        utils.closeLoading();
                    }
                }
            };
            var config = $.extend({}, defaultConfig, options);
            $(obj).pagination(config);
        });
    },
    //表单验证
    ajaxSubForm: function (options, dom) {
        var obj = dom || '#form';
        var that = this;
        var defaultConfig = {
            btnSubmit: ".btn-sub",
            btnReset: ".btn-reset",
            ajaxPost: true,
            postonce: true,
            tipSweep: true,
            label: '.control-label',
            tiptype: function (msg, o, cssctl) {
                if (o.type == 3) {
                    o.obj.closest('.form-group').addClass('has-error');
                    $(obj).find('.valid-msg').show().html('<i class="fa fa-info-circle"></i> ' + msg);
                } else {
                    o.obj.closest('.form-group').removeClass('has-error');
                    $(obj).find('.valid-msg').html('').hide();
                }
            },
            beforeCheck: function (curform) { //表单验证前同步编辑器
                if (typeof CKEDITOR != 'undefined') {
                    for (instance in CKEDITOR.instances) {
                        CKEDITOR.instances[instance].updateElement();
                    }
                }
            },
            beforeSubmit: function (curform) {
                $(obj).find('.btn-sub').attr("disabled", "disabled"); //禁用按钮
                $(obj).find('.btn-reset').attr("disabled", "disabled"); //禁用按钮
                that.loading();
            },
            callback: function (res) {
                that.closeLoading();
                $(obj).find('.btn-sub').removeAttr("disabled"); //启用按钮
                $(obj).find('.btn-reset').removeAttr("disabled"); //启用按钮
                if (res.code == 1) {
                    if (typeof options == 'function') {
                        options(res);
                    }
                } else {
                    utils.msg(res.msg);
                }
            }
        };
        var config = defaultConfig;
        if (typeof options == 'object') {
            config = $.extend({}, defaultConfig, options);
        }
        In('valid', function () {
            $(obj).Validform(config);
        });
    },
    //表单验证
    majaxSubForm: function (options, dom) {
        var obj = dom || '#form';
        var that = this;
        var defaultConfig = {
            btnSubmit: ".btn-sub",
            btnReset: ".btn-reset",
            ajaxPost: true,
            postonce: true,
            tipSweep: true,
            label: '.control-label',
            tiptype: function (msg, o, cssctl) {
                if (o.type == 3) {
                    that.msg(msg);
                }
            },
            beforeCheck: function (curform) { //表单验证前同步编辑器
                if (typeof CKEDITOR != 'undefined') {
                    for (instance in CKEDITOR.instances) {
                        CKEDITOR.instances[instance].updateElement();
                    }
                }
            },
            beforeSubmit: function (curform) {
                $(obj).find('.btn-sub').attr("disabled", "disabled"); //禁用按钮
                $(obj).find('.btn-reset').attr("disabled", "disabled"); //禁用按钮
                $(obj).find("[type='submit']").attr("disabled", "disabled"); //禁用按钮
                $(obj).find("[type='reset']").attr("disabled", "disabled"); //禁用按钮
                that.loading();
            },
            callback: function (res) {
                that.closeLoading();
                $(obj).find('.btn-sub').removeAttr("disabled"); //启用按钮
                $(obj).find('.btn-reset').removeAttr("disabled"); //启用按钮
                $(obj).find("[type='submit']").removeAttr("disabled"); //启用按钮
                $(obj).find("[type='reset']").removeAttr("disabled"); //启用按钮
                if (res.code == 1) {
                    if (typeof options == 'function') {
                        options(res);
                    }
                } else {
                    that.msg(res.msg);
                }
            }
        };
        var config = defaultConfig;
        if (typeof options == 'object') {
            config = $.extend({}, defaultConfig, options);
        }
        In('valid', function () {
            $(obj).Validform(config);
        });
    },
    //ajax封装
    getAjax: function (options) {
        var that = this;
        var defaultOptions = {
            type: 'post',
            cache: true,
            async: true,
            dataType: 'json',
            beforeSend: function () {
                that.loading();
            },
            error: function () {
                that.alert('与服务器断开连接！');
                return false;
            },
            complete: function () {
                that.closeLoading();
            }
        };
        var option = $.extend({}, defaultOptions, options);
        $.ajax(option);
    },
    //确认后操作
    doAction: function (obj, callfn) {
        var that = this;
        var msg = $(obj).data('msg');
        var url = $(obj).data('url');
        var params = $(obj).data('params');
        that.confirm(msg, function (index) {
            that.getAjax({
                url: url,
                data: params,
                success: function (res) {
                    layer.close(index);
                    if (res.code == 1) {
                        if (typeof callfn === "function") {
                            callfn(obj, res);
                        } else {
                            if (callfn == "refreshPage") that.refreshPage();
                            if (callfn == 'refreshTable') that.refreshTable();
                            if (callfn == 'url') that.urlHref(res.url);
                        }
                    } else {
                        that.alert(res.msg);
                    }
                }
            });
        });
    },
    //表格控件
    btable:function(option,dom){
        var grid=dom || '#grid';
        In.config('serial',true); //串行加载
        In('btable','btable-export','editable','btable-editable','btable-resizable',function () {
            var defaultOptions={
                url: '',
                method: 'post', //请求方法
                contentType:'application/x-www-form-urlencoded', //请求方式
                toolbar: ".toolbar", //工具栏
                showExport: true,
                exportDataType: "all",
                exportTypes: ['json', 'xml', 'csv', 'txt', 'doc', 'excel'],
                sidePagination: 'server',
                queryParamsType:'page',
                pageSize: 10,
                pageList: [10, 25, 50, 'All'],
                pagination: true,
                clickToSelect: true, //是否启用点击选中
                singleSelect: true, //是否启用单选
                showRefresh: true,
                showToggle: false,
                showColumns: true,
                pk: 'id',
                sortName: 'id',
                sortOrder: 'desc',
                mobileResponsive:true,
                queryParams:function (params) {
                    var formData = $('#searchForm').serializeArray();
                    var filter={};
                    var op={};
                    for (var i in formData) {
                        var opObj = $("[name='"+formData[i].name+"']");
                        //filter: {"createtime":"2018-10-26 00:00:00 - 2018-10-26 23:59:59","uid":1}
                        //op: {"createtime":"BETWEEN","uid":"="}
                        op[formData[i].name]=opObj.data('op');
                        filter[formData[i].name]=formData[i]['value'];
                    }
                    params.searchKey=$('#searchKey').val();
                    params.searchValue=$('#searchValue').val();
                    params.filter=filter;
                    params.op=op;
                    return params;
                }
            };
            var options=$.extend({},defaultOptions,option);
            $(grid).bootstrapTable(options);
        });
    },
    //树型表格控件
    bTreeTable:function(option,dom){
        var grid=dom || '#grid';
        In.config('serial',true); //串行加载
        In('btable','treegrid','btable-treegrid','cookie','editable','btable-editable',function () {
            var defaultOptions={
                url: '',
                toolbar: ".toolbar", //工具栏
                clickToSelect: true, //是否启用点击选中
                singleSelect: true, //是否启用单选
                showRefresh: true,
                showToggle: false,
                showColumns: true,
                treeShowField: 'name',
                parentIdField: 'pid',
                onLoadSuccess: function(data) {
                    $(grid).treegrid({
                        treeColumn: 1,
                        saveState:true,
                        initialState:'collapsed',
                        onChange: function() {
                            $('#grid').bootstrapTable('resetWidth');
                        }
                    });
                }
            };
            var options=$.extend({},defaultOptions,option);
            $(grid).bootstrapTable(options);
        });
    },
    initPage:function (options,dom) {
        var obj = dom || '#page';
        var params = options.params || {};
        In('paginationjs',function () {
            var defaultConfig = {
                locator:'rows',
                totalNumberLocator:function (response) {
                    return response.total;
                },
                className:'paginationjs-theme-green',
                pageSize: 12,
                prevText:'上一页',
                nextText:'下一页',
                ajax: {
                    type:'post',
                    data:params,
                    beforeSend: function() {
                        layer.load(2);
                    },
                    complete:function () {
                        layer.closeAll('loading');
                    }
                }
            };
            var config = $.extend({}, defaultConfig, options);
            $(obj).pagination(config);
        });
    },
    //图片预览
    imgPreview: function (url) {
        if (url == '') {
            utils.msg('请先上传图片');
            return false;
        }
        if (utils.isMobile() == 1) {
            area = "80%";
            width = '100%';
        } else {
            area = '600px';
            width = '600px';
        }
        utils.open({
            title: false,
            closeBtn: 0,
            offset:'60px',
            area: area,
            skin: 'layui-layer-nobg', //没有背景色
            shadeClose: true,
            content: '<img src="' + url + '" style="width:' + width + ';" />'
        });
    },
    //附件下载
    filePreview: function (url) {
        var $form = $('<form method="GET" target="_blank"></form>');
        $form.attr('action', url);
        $form.appendTo($('body'));
        $form.submit();
    },
    //删除行 父级元素是 .item-row 时通用
    delRow: function (obj, callFn) {
        $(obj).closest('.item-row').remove();
        if (typeof callFn != 'undefined') {
            callFn(obj);
        }
    },
    //上移 父级元素是 .item-row 时通用
    upMove: function (obj) {
        var leftMove = $(obj).closest('.item-row');   //当前行
        var prevMove = leftMove.prev('.item-row');               //上一个同级元素
        if (prevMove.length > 0) {                        //存在上一个同级元素
            prevMove.insertAfter(leftMove);           //就把它移动到前面
        }
    },
    //下移 父级元素是 .item-row 时通用
    downMove: function (obj) {
        var rightMove = $(obj).closest('.item-row');
        var nextMove = rightMove.next('.item-row');
        if (nextMove.length > 0) {
            nextMove.insertBefore(rightMove);
        }
    },
    /*截取字符串*/
    msubstr:function (str,start,length,suffix) {
        var start = start || 0;
        var length = length || 10;
        var str_length=str.length;/*字符长度*/
        var new_str=str.substring(parseInt(start),parseInt(start)+parseInt(length));
        if(suffix==1 && str_length>length){
            return new_str+'...';
        }else{
            return new_str;
        }
    },
    /**
     * 格式化金额
     * moneyFormat("12345.675910", 3)，返回12,345.676
     * */
    moneyFormat:function (s,n) {
        n = n > 0 && n <= 20 ? n : 2;
        s = parseFloat((s + "").replace(/[^\d\.-]/g, "")).toFixed(n) + "";
        var l = s.split(".")[0].split("").reverse(), r = s.split(".")[1];
        t = "";
        for (i = 0; i < l.length; i++) {
            t += l[i] + ((i + 1) % 3 == 0 && (i + 1) != l.length ? "," : "");
        }
        return t.split("").reverse().join("") + "." + r;

    }
};

/**
 * 验证
 */
var valid = {
    //是否为空
    isEmpty: function (str) {
        if (str == "" || str == null || str == undefined) { // "",null,undefined
            return true;
        } else {
            return false;
        }
    },
    //是否为邮件
    isEmail: function (str) {
        var reg = /^\w+((-w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
        if (reg.test(str)) return true;
    },
    //是否为手机号
    isPhone: function (str) {
        var a = /^1\d{10}$/, s = $.trim(str);
        if (a.test(s)) return true;
    },
    //是否汉字
    isCN: function (str) {
        var s = $.trim(str), reg = /^[\u4e00-\u9fa5]+$/;
        if (reg.test(s)) return true;
        else return false;
    },
    //是否密码
    isPwd: function (str) {
        var reg = /^[A-Za-z0-9_-]+$/;
        if (reg.test(str)) return true;
    },
    //是否为微信号
    isWx: function (str) {
        var rex = /^[a-zA-Z\d_]{5,}$/;
        if (rex.test($.trim(str))) return true;
        return false;
    },
    //是否全为数字
    isNum: function (num) {
        var rex = /^[0-9]+$/;
        if (rex.test($.trim(num))) return true;
        return false;
    },
    //是否为热线电话
    isPhone: function (num) {
        var rex = /^[0-9]([0-9]|-)*[0-9]$/;
        if (rex.test($.trim(num))) return true;
        return false;
    },
    //是否为钱数
    isFloat: function (n) {
        return !isNaN(n);
    },
    //判断是否为url地址
    isUrl: function (str) {
        var rex = /^(http|https){1}:\/\/[^\s]+$/;
        if (rex.test($.trim(str))) return true;
        else return false;
    }
};
var cxSelectApi;
var formHelper = {
    initForm: function () {
        var that = this;
        that.iCheck();
        that.ckeditor();
        that.laydate();
        that.selectpage();
        that.uploadImage();
        that.uploadFile();
        that.uploadMultiImage();
        that.uploadMultiImageDesc();
        that.tags();
        that.cxselect();
        that.inputArray();
        that.daterange();
        that.uploadMultiFile();
        that.select2();
        that.iconpicker();
        that.switch();
    },
    //icheck
    iCheck: function (skin) {
        if ($('.f-icheck').length > 0) {
            In('iCheck', function () {
                var selectSkin = skin || "square-blue";
                $('.f-icheck').iCheck({
                    checkboxClass: 'icheckbox_' + selectSkin,
                    radioClass: 'iradio_' + selectSkin
                });
            });
        }
    },
    /*switch*/
    switch:function () {
        if ($('.f-switch').length > 0) {
            In('switch', function () {
                $('.f-switch').each(function () {
                    var $option = $(this).data('option');
                    var name = $(this).attr('name');
                    var option = valid.isEmpty($option) ? {} : $option;
                    var defaultOptions = {
                        onText:"启用",
                        offText:"禁用",
                        onColor:'success',
                        offColor:'danger',
                        size:"mini",
                        onSwitchChange:function(event,state){
                            if (typeof $option['callfn'] != 'undefined') {
                                eval($option['callfn'] + '(event, state)');
                            }
                        }
                    };
                    var options = $.extend({}, defaultOptions, option);
                    $('input[name="'+name+'"]').bootstrapSwitch(options);
                })
            });
        }
    },
    //icheck
    select2: function () {
        if ($('.f-select2').length > 0) {
            In('select2', function () {
                $('.f-select2').each(function () {
                    var $option = $(this).data('option');
                    var name = $(this).attr('name');
                    var option = valid.isEmpty($option) ? {} : $option;
                    var defaultOptions = {
                        placeholder: '请选择',
                        minimumResultsForSearch:Infinity // 隐藏搜索框
                    };
                    var options = $.extend({}, defaultOptions, option);
                    $('select[name="'+name+'"]').select2(options);
                    /*设置初始值*/
                    var value = $(this).data('value');
                    if(!valid.isEmpty(value) && !valid.isNum(value)) {
                        value = value.split(',');
                    }
                    $('select[name="'+name+'"]').val(value).trigger('change');
                })
            });
        }
    },
    //富文本编辑器
    ckeditor: function () {
        if ($('.f-ckeditor').length > 0) {
            In('ckeditor', function () {
                $('.f-ckeditor').each(function () {
                    var name = $(this).attr('name');
                    var $option = $(this).data('option');
                    var toolbar = $(this).data('toolbar');
                    if(valid.isEmpty(toolbar)){
                        var bodyWidth = $(document.body).width();
                        toolbar = bodyWidth <= 1024 ? 'Basic' : 'Full';
                    }
                    var option = valid.isEmpty($option) ? {} : $option;
                    var defaultOptions = {
                        toolbar: toolbar
                    };
                    var options = $.extend({}, defaultOptions, option);
                    CKEDITOR.replace(name, options);
                });
            });
        }
    },
    //日期控件
    laydate: function () {
        if ($('.f-laydate').length > 0) {
            In('laydate', function () {
                $('.f-laydate').each(function () {
                    var $option = $(this).data('option');
                    var option = valid.isEmpty($option) ? {} : $option;
                    var defaultOptions = {
                        elem: this
                    };
                    var options = $.extend({}, defaultOptions, option);
                    laydate.render(options);
                });
            })
        }
    },
    /*日期范围控件*/
    daterange:function () {
        //定义locale汉化插件
        var locale = {
            "format": 'YYYY-MM-DD',
            "separator": " - ",
            "applyLabel": "确定",
            "cancelLabel": "取消",
            "fromLabel": "起始时间",
            "toLabel": "结束时间'",
            "customRangeLabel": "自定义",
            "weekLabel": "W",
            "daysOfWeek": ["日", "一", "二", "三", "四", "五", "六"],
            "monthNames": ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
            "firstDay": 1
        };
        if ($('.f-daterange').length > 0){
            In('daterange',function () {
                $('.f-daterange').each(function () {
                    var $type = $(this).data('type');
                    var format = $(this).data('format');
                    var $option = $(this).data('option');
                    var option = valid.isEmpty($option) ? {} : $option;
                    var type = valid.isEmpty($type) ? 'range' : $type;
                    locale.format = valid.isEmpty(format)?'YYYY-MM-DD':format;
                    /*自定义日期范围*/
                    var defaultOptions = {
                        "custom":{
                            'locale': locale,
                            ranges: {
                                '今日': [moment(), moment()],
                                '昨日': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                                '最近7日': [moment().subtract(6, 'days'), moment()],
                                '最近30日': [moment().subtract(29, 'days'), moment()],
                                '本月': [moment().startOf('month'), moment().endOf('month')],
                                '上月': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                            },
                            "opens":"right",
                            "timePicker24Hour": true,
                            "timePickerIncrement" : 60,
                            startDate: moment().subtract(29, 'days'),
                            endDate: moment()
                        },
                        "range":{
                            'locale': locale,
                            "opens":"right",
                            "timePicker24Hour": true,
                            "timePickerIncrement" : 60,
                            startDate: moment().subtract(29, 'days'),
                            endDate: moment()
                        },
                        "single":{
                            'locale': locale,
                            "singleDatePicker": true,
                            "timePicker": true,
                            "timePicker24Hour": true,
                        }
                    };
                    var options = $.extend({}, defaultOptions[type], option);
                    $(this).daterangepicker(
                        options,
                        function (start, end) {
                            $(this).val(start.format() + ' - ' + end.format());
                            if (typeof $option['callfn'] != 'undefined') {
                                eval($option['callfn'] + '(start, end)');
                            }
                        }
                    );
                })
            })
        }
    },
    //selectpage控件
    selectpage: function () {
        if ($('.f-selectpage').length > 0) {
            In('selectpage', function () {
                $('.f-selectpage').each(function () {
                    var url = $(this).data('url');
                    var showField = $(this).data('show');
                    var keyField = $(this).data('key');
                    var searchField = $(this).data('search');
                    searchField = searchField || showField;
                    var option = $(this).data('option');
                    option = option || {};
                    var custom = $(this).data('custom');
                    custom = custom || '';
                    var defaultOptions = {
                        data: url,
                        showField: showField,
                        keyField: keyField,
                        dropButton: true,
                        multiple: false,
                        maxSelectLimit: 0,
                        andOr: 'AND',
                        pageSize: 10,
                        params: function () {
                            return {searchField:searchField,custom: custom};
                        },
                        eAjaxSuccess: function (d) {
                            return d ? d : undefined;
                        },
                    };
                    if (typeof option['formatItemFun'] != 'undefined') {
                        defaultOptions.formatItem = function (data) {
                            return eval(option['formatItemFun'] + '(data)');
                        }
                    }
                    if (typeof option['selectFun'] != 'undefined') {
                        defaultOptions.eSelect = function (data) {
                            return eval(option['selectFun'] + '(data)');
                        }
                    }
                    var options = $.extend({}, defaultOptions, option);
                    $(this).selectPage(options);
                });
            });
        }
    },
    //上传图片
    uploadImage: function () {
        if ($('.f-uploadimg').length > 0) {
            In('upload', function () {
                $('.f-uploadimg').each(function () {
                    var id = $(this).attr('id');
                    var $option = $(this).data('option'); //maxsize
                    $option = $option || {};
                    var maxsize = $option['maxsize'] || '10mb';
                    var ext = $option['ext'] || 'jpg,gif,png';
                    var options={
                        runtimes: 'html5,flash,html4',
                        multi_selection: false, //不允许多选
                        browse_button: id,
                        url: '/system/file/upload.html?ext_type=img',
                        flash_swf_url: '/static/plupload/Moxie.swf',
                        multipart_params: {},
                        filters: {
                            max_file_size: maxsize,
                            mime_types: [
                                {title: "请选择图片", extensions: ext}
                            ]
                        },
                        init: {
                            FilesAdded: function (up, files) {
                                utils.loading();
                                up.start();
                            },
                            FileUploaded: function (up, file, responseObject) {
                                utils.closeLoading();
                                if (responseObject.status === 200) {
                                    var res = JSON.parse(responseObject.response);
                                    if (res.code == 1) {
                                        $('#input-' + id).val(res.data.file_url);
                                        if (typeof $option['uploaded'] != 'undefined') {
                                            eval($option['uploaded'] + '(id,res.data)');
                                        }
                                    } else {
                                        utils.msg(res.msg);
                                    }
                                } else {
                                    utils.msg('上传服务器无响应！');
                                }
                            }
                        }
                    };
                    var uploader = new plupload.Uploader(options);
                    uploader.init();
                });
            });
        }
    },
    //上传附件
    uploadFile: function () {
        if ($('.f-uploadfile').length > 0) {
            In('upload', function () {
                $('.f-uploadfile').each(function () {
                    var id = $(this).attr('id');
                    var $option = $(this).data('option'); //maxsize
                    $option = $option || {};
                    var maxsize = $option['maxsize'] || '50mb';
                    var ext = $option['ext'] || 'rar,zip,tar,gz,7z,bz2,cab,iso,pem,doc,docx,xls,xlsx,ppt,pptx,pdf,txt,md,jpg,jpeg,png,gif';
                    var options={
                        runtimes: 'html5,flash,html4',
                        multi_selection: false, //不允许多选
                        browse_button: id,
                        url: '/system/file/upload.html?ext_type=file',
                        flash_swf_url: '/static/plupload/Moxie.swf',
                        multipart_params: {},
                        filters: {
                            max_file_size: maxsize,
                            mime_types: [
                                {title: "请选择附件", extensions: ext}
                            ]
                        },
                        init: {
                            FilesAdded: function (up, files) {
                                utils.loading();
                                up.start();
                            },
                            FileUploaded: function (up, file, responseObject) {
                                utils.closeLoading();
                                if (responseObject.status === 200) {
                                    var res = JSON.parse(responseObject.response);
                                    if (res.code == 1) {
                                        $('#input-' + id).val(res.data.file_url);
                                        if (typeof $option['uploaded'] != 'undefined') {
                                            eval($option['uploaded'] + '(id,res.data)');
                                        }
                                    } else {
                                        utils.msg(res.msg);
                                    }
                                } else {
                                    utils.msg('上传服务器无响应！');
                                }
                            }
                        }
                    };
                    var uploader = new plupload.Uploader(options);
                    uploader.init();
                });
            });
        }
    },
    //多图上传
    uploadMultiImage: function () {
        if ($('.f-upload-images-box').length > 0) {
            In('upload', function () {
                $('.f-upload-images-box').each(function () {
                    var itemDom=$(this).find('.f-upload-images-item');
                    var id = $(this).data('upload');
                    var name = $(this).data('name');
                    var $option = $(this).data('option'); //maxsize
                    var datatype = $(this).attr('datatype');
                    if(typeof datatype=='undefined'){
                        datatype='';
                    }else{
                        datatype='datatype="'+datatype+'"';
                    }
                    $option = $option || {};
                    var maxsize = $option['maxsize'] || '50mb';
                    var ext = $option['ext'] || 'jpg,gif,png';
                    var options={
                        runtimes: 'html5,flash,html4',
                        multi_selection: true, //允许多选
                        browse_button: id,
                        url: '/system/file/upload.html?ext_type=img',
                        flash_swf_url: '/static/plupload/Moxie.swf',
                        multipart_params: {},
                        filters: {
                            max_file_size: maxsize,
                            mime_types: [
                                {title: "请选择图片", extensions: ext}
                            ]
                        },
                        init: {
                            FilesAdded: function (up, files) {
                                utils.loading();
                                up.start();
                            },
                            FileUploaded: function (up, file, responseObject) {
                                utils.closeLoading();
                                if (responseObject.status === 200) {
                                    $('.moxie-shim-html5').hide();
                                    var res = JSON.parse(responseObject.response);
                                    if (res.code == 1) {
                                        if (typeof $option['uploaded'] != 'undefined') {
                                            eval($option['uploaded'] + '(id,res.data)');
                                        } else {
                                            var itemrow = '<li class="item-row">\n' +
                                                '               <input type="hidden" name="' + name + '[]" value="' + res.data.file_url + '" '+datatype+'/>\n' +
                                                '               <div class="img-box" style="background-image:url(' + res.data.file_url + ')">\n' +
                                                '                    <a href="javascript:;" onclick="utils.upMove(this);"><i class="fa fa-arrow-left"></i></a>\n' +
                                                '                    <a href="javascript:;" onclick="utils.downMove(this);"><i class="fa fa-arrow-right"></i></a>\n' +
                                                '                    <a href="javascript:;" onclick="utils.delRow(this);"><i class="fa fa-trash-o"></i></a>\n' +
                                                '               </div>\n' +
                                                '          </li>';
                                            itemDom.append(itemrow);
                                        }
                                    } else {
                                        utils.msg(res.msg);
                                    }
                                } else {
                                    utils.msg('上传服务器无响应！');
                                }
                            }
                        }
                    };
                    var uploader = new plupload.Uploader(options);
                    uploader.init();
                });
            });
        }
    },
    //多图上传带描述
    uploadMultiImageDesc: function () {
        if ($('.f-upload-images-remark-box').length > 0) {
            In('upload', function () {
                $('.f-upload-images-remark-box').each(function () {
                    var itemDom=$(this).find('.f-upload-images-item');
                    var id = $(this).data('upload');
                    var name = $(this).data('name');
                    var dom = $('#' + id);
                    var datatype = $(this).attr('datatype');
                    if(typeof datatype=='undefined'){
                        datatype='';
                    }else{
                        datatype='datatype="'+datatype+'"';
                    }
                    var $option = $(this).data('option'); //maxsize
                    $option = $option || {};
                    var maxsize = $option['maxsize'] || '50mb';
                    var ext = $option['ext'] || 'jpg,gif,png';
                    var options={
                        runtimes: 'html5,flash,html4',
                        multi_selection: true, //允许多选
                        browse_button: id,
                        url: '/system/file/upload.html?ext_type=img',
                        flash_swf_url: '/static/plupload/Moxie.swf',
                        multipart_params: {},
                        filters: {
                            max_file_size: maxsize,
                            mime_types: [
                                {title: "请选择图片", extensions: ext}
                            ]
                        },
                        init: {
                            FilesAdded: function (up, files) {
                                utils.loading();
                                up.start();
                            },
                            FileUploaded: function (up, file, responseObject) {
                                utils.closeLoading();
                                if (responseObject.status === 200) {
                                    $('.moxie-shim-html5').hide();
                                    var res = JSON.parse(responseObject.response);
                                    if (res.code == 1) {
                                        if (typeof $option['uploaded'] != 'undefined') {
                                            eval($option['uploaded'] + '(id,res.data)');
                                        } else {
                                            var itemrow = '<div class="col-lg-6 col-md-12 col-sm-12 item-row">\n' +
                                                '               <div class="img-box"  style="background-image:url(' + res.data.file_url + ')">\n' +
                                                '                        <a href="javascript:;" onclick="utils.upMove(this);"><i class="fa fa-arrow-left"></i></a>\n' +
                                                '                        <a href="javascript:;" onclick="utils.downMove(this);"><i class="fa fa-arrow-right"></i></a>\n' +
                                                '                        <a href="javascript:;" onclick="utils.delRow(this);"><i class="fa fa-trash-o"></i></a>\n' +
                                                '               </div>\n' +
                                                '               <div class="form">\n' +
                                                '                   <div class="form-group">\n' +
                                                '                        <input type="hidden" class="form-control" name="'+name+'[url][]" value="'+res.data.file_url+'" '+datatype+'>\n' +
                                                '                        <input type="text" class="form-control" name="'+name+'[title][]" placeholder="请输入图片标题">\n' +
                                                '                   </div>\n' +
                                                '                   <div class="form-group">\n' +
                                                '                        <textarea class="form-control" rows="2" name="'+name+'[remark][]" placeholder="请输入图片摘要"></textarea>\n' +
                                                '                   </div>\n' +
                                                '                   <div class="input-group link-input">\n' +
                                                '                        <span class="input-group-addon"><i class="fa fa-link"></i></span>\n' +
                                                '                        <input type="text" class="form-control" name="'+name+'[href][]" placeholder="请输入链接地址">\n' +
                                                '                   </div>\n' +
                                                '                </div>\n' +
                                                '          </div>';
                                            itemDom.append(itemrow);
                                        }
                                    } else {
                                        utils.msg(res.msg);
                                    }
                                } else {
                                    utils.msg('上传服务器无响应！');
                                }
                            }
                        }
                    };
                    var uploader = new plupload.Uploader(options);
                    uploader.init();
                });
            });
        }
    },
    //多附件上传
    uploadMultiFile: function () {
        if ($('.f-upload-file-box').length > 0) {
            In('upload', function () {
                $('.f-upload-file-box').each(function () {
                    var itemDom=$(this).find('.f-upload-file-item');
                    var id = $(this).data('upload');
                    var $option = $(this).data('option'); //maxsize
                    var name = $(this).data('name');
                    $option = $option || {};
                    var maxsize = $option['maxsize'] || '50mb';
                    var ext = $option['ext'] || 'rar,zip,tar,gz,7z,bz2,cab,iso,pem,doc,docx,xls,xlsx,ppt,pptx,pdf,txt,md,jpg,jpeg,png,gif';
                    var options={
                        runtimes: 'html5,flash,html4',
                        multi_selection: true, //允许多选
                        browse_button: id,
                        url: '/system/file/upload.html?ext_type=file',
                        flash_swf_url: '/static/plupload/Moxie.swf',
                        multipart_params: {},
                        filters: {
                            max_file_size: maxsize,
                            mime_types: [
                                {title: "请选择附件", extensions: ext}
                            ]
                        },
                        init: {
                            FilesAdded: function (up, files) {
                                utils.loading();
                                up.start();
                            },
                            FileUploaded: function (up, file, responseObject) {
                                utils.closeLoading();
                                if (responseObject.status === 200) {
                                    $('.moxie-shim-html5').hide();
                                    var res = JSON.parse(responseObject.response);
                                    if (res.code == 1) {

                                        if (typeof $option['uploaded'] != 'undefined') {
                                            eval($option['uploaded'] + '(id,res.data)');
                                        } else {
                                            var itemrow = '<div class="item-row">\n' +
                                                '               <input type="hidden" name="' + name + '[]" value="' + res.data.file_url + '" />\n' +
                                                '               <a href="'+res.data.file_url+'" target="_blank" class="file-name '+res.data.ext+'">' + res.data.original_name +'</a>\n' +
                                                '               <div class="action-box">\n' +
                                                '                    <a href="javascript:;" onclick="utils.upMove(this);"><i class="fa fa-arrow-up"></i></a>\n' +
                                                '                    <a href="javascript:;" onclick="utils.downMove(this);"><i class="fa fa-arrow-down"></i></a>\n' +
                                                '                    <a href="javascript:;" onclick="utils.delRow(this);"><i class="fa fa-trash-o"></i></a>\n' +
                                                '               </div>\n' +
                                                '          </div>';
                                            itemDom.append(itemrow);
                                        }
                                    } else {
                                        utils.msg(res.msg);
                                    }
                                } else {
                                    utils.msg('上传服务器无响应！');
                                }
                            }
                        }
                    };
                    var uploader = new plupload.Uploader(options);
                    uploader.init();
                });
            });
        }
    },
    /*tags标签*/
    tags: function () {
        if ($('.f-tags').length > 0) {
            In('tags', function () {
                $('.f-tags').each(function () {
                    var id = $(this).attr('id');
                    var disable = $(this).data('disable');
                    var value = $(this).data('value');
                    var tag = new Tag(id);
                    tag.tagValue  = valid.isEmpty(value)?'':value;
                    if(disable){
                        tag.isDisable = true;
                    }
                    tag.initView();
                });
            })
        }
    },
    /*cxselect*/
    cxselect:function () {
        if($('.f-cx-select').length >0 ){
            In('cxselect',function () {
                $('.f-cx-select').each(function () {
                    var id=$(this).attr('id');
                    var selects = $(this).data('selects');
                    var url = $(this).data('url');
                    valid.isEmpty(selects)?$(this).data('selects','prov,city,dist'):'';
                    valid.isEmpty(url)?$(this).data('url','/static/plugins/cxselect/js/cityData.min.json'):'';
                    $('#'+id).cxSelect({
                    },function(api) {
                        cxSelectApi=api;
                    });
                });
            })
        }
    },
    iconpicker:function(){
        if($('.f-iconpicker').length >0 ){
            In('iconpicker',function () {
                $('.f-iconpicker').fontIconPicker({
                    source: fa_icons,
                    searchSource: fa_icons,
                    theme: 'fip-bootstrap',
                    emptyIconValue: 'none',
                    Uncategorized:'未分类',
                    allCategoryText:'全部图标'
                });
            });
        }
    },
    /**
     * 多行数据数组。点击增加一行,
     * 思路：将每行数据内的name使用data-name代替，执行新增和删除行时循环更新每行内的name，加上总的字段名和顺序数字
     * */
    inputArray:function () {
        if($('.f-input-array').length >0 ){
            $('.f-input-array').each(function () {
                var name=$(this).data('name');
                $(this).find('.data-row').each(function (index,element) {
                    $(this).attr('data-index',index);
                    var del ='<a href="javascript:;" onclick="formHelper.inputArrayDelrow(this)" class="del">×</a>';
                    if(index>0 && $(this).find('.del').length==0){
                        $(this).append(del);
                    }
                    $(this).children().each(function (i,v) {
                        var children_name = $(this).data('name');
                        $(this).hasClass('del')?'':$(this).attr('name',name+'['+index+']['+children_name+']');
                    })
                })
            })
        }
    },
    inputArrayAddrow:function (obj) {
        var box =$(obj).closest('.f-input-array');
        var html = box.find('.data-row').html();
        html = '<div class="data-row">'+html+'</div>';
        $(obj).before(html);
        formHelper.inputArray();
        /*清空最后一个元素内的值*/
        box.find('.data-row:last').children().each(function () {
            $(this).val('');
        });


    },
    inputArrayDelrow:function (obj) {
        $(obj).closest('.data-row').remove();
        formHelper.inputArray();
    },
    /*单图上传后显示缩略图*/
    showThumb:function (id,data) {
        $('#'+id).addClass('img-upload-over');
        $('#'+id).css('backgroundImage','url('+data.file_url+')');
    },
    /*单图上传后删除*/
    delimg:function (id) {
        $('#'+id).removeClass('img-upload-over');
        $('#'+id).css('backgroundImage','');
        $('#input-'+id).val('');
    },
    keepDecimal:function (num,fix) {
        var res = parseFloat(num);
        return res.toFixed(fix);
    }
};

/**
 * 级联带回
 */
var selectBox = {
    goods: function (callFn) {
        var html='<section class="content"><div class="box box-solid color-palette-box"><div class="box-body"><div id="select-goods-toolbar"><form id="select-goods-search-form" class="form-inline">' +
            '<div class="form-group">\n' +
            '                    <select class="form-control" id="searchKey">\n' +
            '                        <option value="goods_name">商品名称</option>\n' +
            '                        <option value="sku_sn">商品条码</option>\n' +
            '                    </select>\n' +
            '                </div>\n' +
            '                <div class="form-group">\n' +
            '                    <input type="text" class="form-control" id="searchValue" placeholder="请输入检索关键词" />\n' +
            '                </div>' +
            '<div class="form-group">' +
            '<button type="button" class="btn btn-default" onclick="$(\'#select-goods-grid\').bootstrapTable(\'refresh\');">' +
            '<i class="fa fa-search"></i> 检索' +
            '</button>' +
            '</div>' +
            '</form></div>';
        html+='<div class="table-responsive"><table id="select-goods-grid"></table></div></div></div></section>';
        utils.open({
            title: '选择商品',
            content: html,
            offset: '100px',
            btn: '确认',
            success: function (layero, index) {
                $('#select-goods-grid').bootstrapTable({
                    mobileResponsive: true,
                    toolbar: '#select-goods-search-form',
                    pagination: true,
                    url: '/system/goods.goods/getBillGoods.html',
                    method:'post',
                    contentType:'application/x-www-form-urlencoded',
                    sidePagination:'server',
                    queryParamsType:'page',
                    pageSize:15,
                    showColumns:true,
                    showRefresh:true,
                    showToggle:true,
                    clickToSelect:true,
                    sortName:'id',
                    sortOrder:'desc',
                    height: utils.getTableHeight(400),
                    columns:[
                        {field:'state',checkbox:true},
                        {field:'goods_name',title:'商品名称',width:250,formatter:selectFormatter.goods_name},
                        {field:'goods_spec',title:'商品规格',width:150},
                        {field:'goods_unit',title:'单位',width:60,align:'center'},
                        {field:'sale_price',title:'销售价',width:120},
                        {field:'cost_price',title:'成本价',width:120},
                        {field:'goods_unit_group',title:'包装组',width:200}
                    ],
                    queryParams:function (params) {
                        var formData = $('#select-goods-search-form').serializeArray();
                        console.info(formData);
                        var filter={};
                        var op={};
                        for (var i in formData) {
                            var opObj = $("[name='"+formData[i].name+"']");
                            //filter: {"createtime":"2018-10-26 00:00:00 - 2018-10-26 23:59:59","uid":1}
                            //op: {"createtime":"BETWEEN","uid":"="}
                            op[formData[i].name]=opObj.data('op');
                            filter[formData[i].name]=formData[i]['value'];
                        }
                        params.searchKey=$('#searchKey').val();
                        params.searchValue=$('#searchValue').val();
                        params.filter=filter;
                        params.op=op;
                        return params;
                    }
                });
            },
            yes: function (index, layero) {
                var rows = $('#select-goods-grid').bootstrapTable('getSelections');
                if (rows.length <= 0) {
                    utils.msg('请选择商品！');
                    return false;
                }
                callFn(rows, index);
            }
        });
    },
    customer: function (callFn) {
        var html = $('#select-customer').html();
        utils.open({
            title: '选择客户',
            content: html,
            offset: '100px',
            btn: '确认',
            success: function (layero, index) {
                $('#select-customer-grid').bootstrapTable({
                    url: '/system/select/customerlist.html',
                    height: utils.getTableHeight(400)
                });
            },
            yes: function (index, layero) {
                var rows = $('#select-customer-grid').bootstrapTable('getSelections');
                if (rows.length <= 0) {
                    utils.msg('请选择客户！');
                    return false;
                }
                callFn(rows, index);
            }
        });
    },
    supplier: function (callFn) {
        var html = $('#select-supplier').html();
        utils.open({
            title: '选择供应商',
            content: html,
            offset: '100px',
            btn: '确认',
            success: function (layero, index) {
                $('#select-supplier-grid').bootstrapTable({
                    url: '/system/select/supplylist.html',
                    height: utils.getTableHeight(400)
                });
            },
            yes: function (index, layero) {
                var rows = $('#select-supplier-grid').bootstrapTable('getSelections');
                if (rows.length <= 0) {
                    utils.msg('请选择供应商！');
                    return false;
                }
                callFn(rows, index);
            }
        });
    },
    user: function (callFn) {
        var html = $('#select-user').html();
        utils.open({
            title: '选择经手人',
            content: html,
            offset: '100px',
            btn: '确认',
            success: function (layero, index) {
                $('#select-user-grid').bootstrapTable({
                    url: '/system/select/userlist.html',
                    height: utils.getTableHeight(400)
                });
            },
            yes: function (index, layero) {
                var rows = $('#select-user-grid').bootstrapTable('getSelections');
                if (rows.length <= 0) {
                    utils.msg('请选择经手人！');
                    return false;
                }
                callFn(rows, index);
            }
        });
    },
    stock: function (callFn) {
        var html = $('#select-stock').html();
        utils.open({
            title: '选择商品',
            content: html,
            offset: '100px',
            btn: '确认',
            success: function (layero, index) {
                $('#select-stock-grid').bootstrapTable({
                    mobileResponsive: true,
                    toolbar: '#select-stock-toolbar',
                    pagination: true,
                    url: '/system/select/stocklist.html',
                    method:'post',
                    contentType:'application/x-www-form-urlencoded',
                    sidePagination:'server',
                    queryParamsType:'page',
                    pageSize:15,
                    showColumns:true,
                    showRefresh:true,
                    showToggle:true,
                    clickToSelect:true,
                    sortName:'id',
                    sortOrder:'desc',
                    height: utils.getTableHeight(400),
                    columns:[
                        {field:'state',checkbox:true},
                        {field:'goods_name',title:'商品名称',width:250,formatter:selectFormatter.goods_name},
                        {field:'goods_spec',title:'规格',width:150},
                        {field:'goods_unit',title:'单位',width:60,align:'center'},
                        {field:'goods_stock',title:'可用量',width:120},
                        {field:'position_stock',title:'占用量',width:120},
                        {field:'goods_avg_price',title:'平均成本',width:120},
                        {field:'last_goods_price',title:'最新成本',width:120},
                    ],
                });
                formHelper.initForm();
            },
            yes: function (index, layero) {
                var rows = $('#select-stock-grid').bootstrapTable('getSelections');
                if (rows.length <= 0) {
                    utils.msg('请选择商品！');
                    return false;
                }
                callFn(rows, index);
            }
        });
    },
    so_bill: function (callFn) {
        var html = $('#select-bill').html();
        /*单据类型*/
        utils.open({
            title: '选择销售单据',
            content: html,
            offset: '100px',
            btn: '确认',
            success: function (layero, index) {
                $('#select-bill-grid').bootstrapTable({
                    mobileResponsive: true,
                    toolbar: '#select-bill-toolbar',
                    pagination: true,
                    url: '/system/select/sobill.html',
                    method:'post',
                    contentType:'application/x-www-form-urlencoded',
                    sidePagination:'server',
                    queryParamsType:'page',
                    pageSize:15,
                    showColumns:true,
                    showRefresh:true,
                    showToggle:true,
                    clickToSelect:true,
                    sortName:'id',
                    sortOrder:'desc',
                    queryParams:selectBox.bill_params,
                    /*   fixedColumns: true,
                       fixedNumber:3,*/
                    height: utils.getTableHeight(400),
                    columns:[
                        {field:'state',checkbox:true},
                        {field:'bill_sn',title:'单据编号',width:150},
                        {field:'goods_name',title:'商品名称',width:250,formatter:selectFormatter.goods_name},
                        {field:'customer_name',title:'供应商',width:200},
                        {field:'goods_spec',title:'商品规格',width:150},
                        {field:'goods_unit',title:'单位',width:60,align:'center'},
                        {field:'sale_price',title:'销售价',width:120},
                        {field:'goods_num',title:'销售数量',width:120},
                        {field:'shipping_num',title:'已发货数量',width:120,formatter:selectFormatter.shipping_num},
                        {field:'goods_unit_group',title:'包装组',width:200},
                        {field:'detail_remark',title:'备注',width:200}
                    ],
                });
                formHelper.initForm();
            },
            yes: function (index, layero) {
                var rows = $('#select-bill-grid').bootstrapTable('getSelections');
                console.log(rows);
                if (rows.length <= 0) {
                    utils.msg('请选择单据信息！');
                    return false;
                }
                /*不能选择不同供应商的明细*/
                var customer_id = rows[0]['customer_id'];
                console.log(customer_id);
                var flag=1;
                $.each(rows,function (index,value) {
                    console.log(value.customer_id);
                    if(value.customer_id!=customer_id){
                        utils.msg('只能选择同一个供应商的单据信息');
                        flag=0;
                        return false;
                    }
                });
                if(flag==1) callFn(rows, index);
            }
        });
    },
    bill_params:function (params) {
        params.customer_id=$('#customer_id2').val();
        params.search_key=$('#select-bill-searchKey').val();
        params.search_value=$('#select-bill-searchValue').val();
        return params;
    }
};
/*级联格式化*/
var selectFormatter={
    goods_name:function (value,row,index) {
        return '<a href="javascript:;" title="'+value+'">' + utils.msubstr(value,0,14,1) + '</a>';
    },
    /*已发货数量，等于或大于显示红色*/
    shipping_num:function (value,row,index) {
        if(value>=row.goods_num){
            return '<span class="text-danger">'+value+'</span>';
        } else{
            return value;
        }
    }
};
var mselectBox = {
    user: function (map,number,callFn) {//条件 数量 回调
        var html = $('#select-user').html();
        utils.open({
            title: '联系人',
            content: html,
            area: ['100vw', '100vh'], //宽高
            offset:0,
            btn: '确认',
            success: function (layero, index) {
                utils.initPage({
                    dataSource: '/system/select/userlist.html', //默认取rows和total
                    params:{},
                    pageSize:1000,
                    header:function(currentPage,totalPage,totalNumber){
                        console.log(totalPage);
                        if(totalPage==0) $('#page').hide();
                    },
                    callback: function(data, pagination) {
                        console.info(data);
                        var html=juicer($('#system-user-tpl').html(),{rows:data});
                        $('#system-user-group').html(html);
                    }
                });
            },
            yes: function (index, layero) {
                var selected=utils.getCheckboxArray('#system-user-group');
                if (selected.length <= 0) {
                    utils.msg('没有选择相关信息！');
                    return false;
                }
                if(valid.isNum(number)){
                    if (selected.length > number) {
                        utils.msg('最多只能选择'+number+'条记录！');
                        return false;
                    }
                }
                if (typeof  callFn === 'function') {
                    callFn(selected, index);
                }
                /*如果是审核人*/
                if (callFn == 'audit') {
                    var userinfo=JSON.parse(selected[0]);
                    if(userinfo!=''){
                        $('input[name="audit_name"]').val(userinfo.name)
                        $('input[name="audit_uid"]').val(userinfo.id)
                    }
                    layer.close(index);
                }
            }
        });
    },
    selectUser:function () {
        var selected=utils.getCheckboxArray('#system-user-group');
        if(selected.length>0){
            $('#select-text').text('已选择 '+selected.length+' 人');
        }
    }
};
//注册模板使用函数
juicer.register('dateDiff', utils.dateDiff);
juicer.register('dateFormat', utils.dateFormat);
juicer.register('json2string', utils.json2string);
juicer.register('moneyFormat', utils.moneyFormat);


