//laydate
In.add('laydate',{path:'/static/plugins/laydate/laydate.js',type:'js',charset:'utf-8'});
//layer
In.add('layer',{path:'/static/plugins/layer/layer.js',type:'js',charset:'utf-8'});
//webuploader
In.add('webuploader-css',{path:'/static/plugins/webuploader/webuploader.css',type:'css',charset:'utf-8'});
In.add('webuploader',{path:'/static/plugins/webuploader/webuploader.min.js',type:'js',charset:'utf-8',rely:['webuploader-css']});
//icheck
In.add('iCheck-css',{path:'/static/plugins/iCheck/all.css',type:'css',charset:'utf-8'});
In.add('iCheck',{path:'/static/plugins/iCheck/icheck.min.js',type:'js',charset:'utf-8',rely:['iCheck-css']});

//valid
In.add('valid-js',{path:'/static/plugins/validform/js/Validform.js',type:'js',charset:'utf-8'});
In.add('valid',{path:'/static/plugins/validform/js/Validform_Datatype.js',type:'js',charset:'utf-8',rely:['valid-js']});
//chosen
In.add('chosen-css',{path:'/static/plugins/chosen/chosen.css',type:'css',charset:'utf-8'});
In.add('chosen',{path:'/static/plugins/chosen/chosen.jquery.js',type:'js',charset:'utf-8',rely:['chosen-css']});
//treetable
In.add('treetable',{path:'/static/plugins/bootstrap-treetable/src/bootstrap-treetable.js',type:'js',charset:'utf-8'});
//nestable
In.add('nestable',{path:'/static/plugins/nestable/jquery.nestable.js',type:'js',charset:'utf-8'});

//ztree
In.add('ztree-css',{path:'/static/plugins/ztree/css/metroStyle/metroStyle.css',type:'css',charset:'utf-8'});
In.add('ztree',{path:'/static/plugins/ztree/js/jquery.ztree.all-3.5.min.js',type:'js',charset:'utf-8',rely:['ztree-css']});
//ueditor
In.add('ueditor-config',{path:'/static/plugins/ueditor/ueditor.config.js',type:'js',charset:'utf-8'});
In.add('ueditor',{path:'/static/plugins/ueditor/ueditor.all.min.js',type:'js',charset:'utf-8',rely:['ueditor-config']});
//codemirror
In.add('codemirror-css',{path:'/static/plugins/codemirror/codemirror.css',type:'css',charset:'utf-8'});
In.add('codemirror-js',{path:'/static/plugins/codemirror/codemirror.js',type:'js',charset:'utf-8',rely:['codemirror-css']});
In.add('htmlmixed',{path:'/static/plugins/codemirror/mode/htmlmixed/htmlmixed.js',type:'js',charset:'utf-8',rely:['codemirror-js']});
In.add('xml',{path:'/static/plugins/codemirror/mode/xml/xml.js',type:'js',charset:'utf-8',rely:['htmlmixed']});
In.add('javascript',{path:'/static/plugins/codemirror/mode/javascript/javascript.js',type:'js',charset:'utf-8',rely:['xml']});
In.add('css',{path:'/static/plugins/codemirror/mode/css/css.js',type:'js',charset:'utf-8',rely:['javascript']});
In.add('clike',{path:'/static/plugins/codemirror/mode/clike/clike.js',type:'js',charset:'utf-8',rely:['css']});
In.add('codemirror',{path:'/static/plugins/codemirror/mode/php/php.js',type:'js',charset:'utf-8',rely:['clike']});
//echo
In.add('echo',{path:'/static/plugins/echo/echo.js',type:'js',charset:'utf-8'});
//paginationjs
In.add('paginationjs-css',{path:'/static/plugins/paginationjs/pagination.css',type:'css',charset:'utf-8'});
In.add('paginationjs',{path:'/static/plugins/paginationjs/pagination.min.js',type:'js',charset:'utf-8',rely:['paginationjs-css']});
//wform
In.add('wform',{path:'/static/plugins/widget/form.js',type:'js',charset:'utf-8'});
//address
In.add('address',{path:'/static/plugins/jsAddress/jsAddress.js',type:'js',charset:'utf-8'});
//ajaxtree
In.add('ajaxtree',{path:'/static/plugins/ajaxtree/ajaxtree.js',type:'js',charset:'utf-8'});
//selectpage
In.add('selectpage-css',{path:'/static/plugins/selectpage/selectpage.css',type:'css',charset:'utf-8'});
In.add('selectpage',{path:'/static/plugins/selectpage/selectpage.min.js',type:'js',charset:'utf-8',rely:['selectpage-css']});
//btable
In.add('btable-css',{path:'/static/plugins/bootstraptable/bootstrap-table.min.css',type:'css',charset:'utf-8'});
In.add('btable-js',{path:'/static/plugins/bootstraptable/bootstrap-table.min.js',type:'js',charset:'utf-8',rely:['btable-css']});
In.add('btable-export',{path:'/static/plugins/bootstraptable/extensions/export/bootstrap-table-export.js',type:'js',charset:'utf-8',rely:['btable-js']});
In.add('btable-export-plugins',{path:'/static/plugins/tableExport.jquery.plugin/tableExport.js',type:'js',charset:'utf-8',rely:['btable-export']});
In.add('btable',{path:'/static/plugins/bootstraptable/locale/bootstrap-table-zh-CN.js',type:'js',charset:'utf-8',rely:['btable-export-plugins']});
//cxselect
In.add('cxselect',{path:'/static/plugins/cxselect/js/jquery.cxselect.min.js',type:'js',charset:'utf-8'});
//table-rowspan
In.add('table-rowspan',{path:'/static/plugins/table-rowspan/jquery.table.rowspan.js',type:'js',charset:'utf-8'});
//sku
In.add('sku',{path:'/static/plugins/sku/sku-select.js',type:'js',charset:'utf-8'});
//cart
In.add('cart',{path:'/static/plugins/sku/cart-action.js',type:'js',charset:'utf-8'});
//qrcode
In.add('qrcode',{path:'/static/plugins/qrcodejs/qrcode.min.js',type:'js',charset:'utf-8'});
//swiper
In.add('swiper-css',{path:'/static/plugins/swiper/css/swiper.min.css',type:'css',charset:'utf-8'});
In.add('swiper',{path:'/static/plugins/swiper/js/swiper.min.js',type:'js',charset:'utf-8',rely:['swiper-css']});
//lightbox
In.add('lightbox-css',{path:'/static/plugins/lightbox/css/lightbox.css',type:'css',charset:'utf-8'});
In.add('lightbox',{path:'/static/plugins/lightbox/js/lightbox.js',type:'js',charset:'utf-8',rely:['lightbox-css']});

/*商品相册*/
In.add('cloudzoom-css',{path:'/static/plugins/cloudzoom/cloudzoom.css',type:'css',charset:'utf-8'});
In.add('cloudzoom',{path:'/static/plugins/cloudzoom/cloudzoom.js',type:'js',charset:'utf-8',rely:['cloudzoom-css']});

//tag插件
In.add('tags-css',{path:'/static/plugins/tags/css/tag.css?v=1.1',type:'css',charset:'utf-8'});
In.add('tags',{path:'/static/plugins/tags/js/tag.js?v=1.1',type:'js',charset:'utf-8',rely:['tags-css']});
//daterange日期范围插件
In.add('daterange-css',{path:'/static/plugins/daterangepicker/daterangepicker.css?v=1.1',type:'css',charset:'utf-8'});
In.add('daterange-js',{path:'/static/plugins/daterangepicker/moment.min.js',type:'js',charset:'utf-8',rely:['daterange-css']});
In.add('daterange',{path:'/static/plugins/daterangepicker/daterangepicker.js?v=1.1',type:'js',charset:'utf-8',rely:['daterange-js']});
/*select2*/
In.add('select2-css',{path:'/static/plugins/select2/select2.min.css',type:'css',charset:'utf-8'});
In.add('select2',{path:'/static/plugins/select2/select2.full.min.js',type:'js',charset:'utf-8',rely:['select2-css']});
//plupload
In.add('upload-js',{path:'/static/plugins/plupload/plupload.full.min.js',type:'js',charset:'utf-8'});
In.add('upload',{path:'/static/plugins/plupload/i18n/zh_CN.js',type:'js',charset:'utf-8',rely:['upload-js']});

In.add('switch-css',{path:'/static/plugins/bootstrap-switch/bootstrap-switch.min.css',type:'css',charset:'utf-8'});
In.add('switch',{path:'/static/plugins/bootstrap-switch/bootstrap-switch.min.js',type:'js',charset:'utf-8',rely:['switch-css']});