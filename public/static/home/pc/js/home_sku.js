/*前端sku*/
/*
{volist name="goodsinfo.sku_arr" id="one"}
<div class="goods_attr"><span class="label">{$one.attr_name}</span>
    <ul>
        {volist name="one.attr_list" id="attr"}
        <li class="text goods_sku" val="{$attr}"><span>{$attr}</span><s></s></li>
        {/volist}
    </ul>
</div>
{/volist}
<script>
skuSelect.init({
    sku_list:$.parseJSON('{:json_encode($goodsinfo.sku_item)}'),
    success:function (skuinfo) {
        console.info(skuinfo);
    },
    error:function () {}
});
</script>
 */
var skuSelect = {
    init:function (options) {
        var defaultConfig = {
            select:0,
            sku_list:[],
            success:function () {},
            error:function () {}
        };
        var config = $.extend({},defaultConfig,options);
        var that=this;
        //默认选中第一条
        if(config.sku_list.length>1){
            var default_sku=config.sku_list[config.select];
            default_sku_name = default_sku['sku_name'].split(',');
            $('.goods_attr .goods_sku').each(function () {
                var value = $(this).attr('val');
                if($.inArray(value, default_sku_name)>=0){
                    $(this).siblings().removeClass('sel');
                    $(this).addClass('sel');
                }
            });
            that.skuClick(config.sku_list,config.success,config.error); //默认先执行一次
        }
        $('.goods_attr .goods_sku').click(function () {
            if ($(this).hasClass('b')) {
                return;//被锁定了
            }
            if ($(this).hasClass('sel')) {
                $(this).removeClass('sel');
            } else {
                $(this).siblings().removeClass('sel');
                $(this).addClass('sel');
            }
            that.skuClick(config.sku_list,config.success,config.error);
        });
    },
    skuClick:function (sku_list,successfn,errorfn) {
        var that=this;
        var select_ids = that._getSelAttrId();
        //已经选择了的规格
        var $_sel_goods_attr = $('.goods_sku.sel').parents('.goods_attr');
        // step 1
        var all_ids = that.filterAttrs(sku_list,select_ids);
        //比较选择的规格个数是否和键值个数是否一样
        if ($('.goods_sku.sel').length == all_ids.length) {
            //根据键值查询数据对应信息,并赋值
            $.each(sku_list, function (idx, obj) {
                sel_sku_key = all_ids.join(',');
                if (obj['sku_name'] == sel_sku_key) {
                    successfn(obj);
                }
            });
        } else {
            errorfn();
        }
        //获取未选择的
        var $other_notsel_attr = $('.goods_attr').not($_sel_goods_attr);
        //设置为选择属性中的不可选节点
        $other_notsel_attr.each(function () {
            that.set_block($(this), all_ids);
        });
        //step 2
        //设置已选节点的同级节点是否可选
        $_sel_goods_attr.each(function () {
            that.update_sku(sku_list,$(this));
        });
    },
    //获取所有包含指定节点的路线
    filterProduct: function (sku_list,ids) {
        var result = [];
        $(sku_list).each(function (k, v) {
            _attr = ',' + v['sku_name'] + ',';
            _all_ids_in = true;
            for (k in ids) {
                if (_attr.indexOf(',' + ids[k] + ',') == -1) {
                    _all_ids_in = false;
                    break;
                }
            }
            if (_all_ids_in) {
                result.push(v);
            }

        });
        return result;
    },
    //获取 经过已选节点 所有线路上的全部节点
    // 根据已经选择得属性值，得到余下还能选择的属性值
    filterAttrs: function (sku_list,ids) {
        var that=this;
        var products = that.filterProduct(sku_list,ids);
        var result = [];
        $(products).each(function (k, v) {
            result = result.concat(v['sku_name'].split(','));
        });
        return result;
    },
    //已选择的节点数组
    _getSelAttrId: function () {
        var list = [];
        $('.goods_attr .goods_sku.sel').each(function () {
            list.push($(this).attr('val'));
        });
        return list;
    },
    update_sku: function (sku_list,$goods_attr) {
        var that=this;
        // 若该属性值 $li 是未选中状态的话，设置同级的其他属性是否可选
        var select_ids = that._getSelAttrId();
        var $li = $goods_attr.find('.goods_sku.sel');
        var select_ids2 = that.del_array_val(select_ids, $li.attr('val'));
        var all_ids = that.filterAttrs(sku_list,select_ids2);
        that.set_block($goods_attr, all_ids);
    },
    set_block: function ($goods_attr, all_ids) {
        //根据 $goods_attr下的所有节点是否在可选节点中（all_ids） 来设置可选状态
        $goods_attr.find('.goods_sku').each(function (k2, li2) {
            if ($.inArray($(li2).attr('val'), all_ids) == -1) {
                $(li2).addClass('b');
            } else {
                $(li2).removeClass('b');
            }
        });
    },
    del_array_val: function (arr, val) {
        //去除 数组 arr中的 val ，返回一个新数组
        var a = [];
        for (k in arr) {
            if (arr[k] != val) {
                a.push(arr[k]);
            }
        }
        return a;
    }
};