var carts={
    parentClass: '.goods-spinner',
    incClass: '.goods-inc',
    decClass: '.goods-dec',
    inputClass: '.goods-num',
    init:function (options) {
        var that=this;
        var incDom = that.parentClass+' '+that.incClass;
        var decDom = that.parentClass+' '+that.decClass;
        var defaultConfig={
            changeNum:function () {
            }
        };
        var config = $.extend({},defaultConfig,options);
        //增加
        $(document).on('click',incDom,function () {
            var parentObj = $(this).closest(that.parentClass);
            var inputDom = parentObj.find(that.inputClass);
            var currentSku = parentObj.data('skuinfo');
            if(currentSku==""){
                alert('当前选项不明确！');
                return false;
            }
            var num = parseInt(inputDom.val()) + parseInt(currentSku.step);
            if (num > parseInt(currentSku.goods_stock)) {
                //inputDom.val(currentSku.mininum);
                alert('库存不足！');
                return false;
            }
            inputDom.val(num);
            config.changeNum(currentSku,num);
            return that.valid(inputDom,num,currentSku);
        });
        //减少
        $(document).on('click',decDom,function () {
            var parentObj = $(this).closest(that.parentClass);
            var inputDom = parentObj.find(that.inputClass);
            var currentSku = parentObj.data('skuinfo');
            if(currentSku==""){
                alert('当前选项不明确！');
                return false;
            }
            var num = parseInt(inputDom.val()) - parseInt(currentSku.step);
            if (num < parseInt(currentSku.mininum)) {
                //inputDom.val(currentSku.mininum);
                alert('起订量不能小于:' + currentSku.mininum);
                return false;
            }
            inputDom.val(num);
            config.changeNum(currentSku,num);
            return that.valid(inputDom,num,currentSku);
        });
        //数值改变
        $(document).on('change',that.inputClass,function () {
            var parentObj = $(this).closest(that.parentClass);
            var currentSku = parentObj.data('skuinfo');
            if(currentSku==""){
                alert('当前选项不明确！');
                return false;
            }
            config.changeNum(currentSku,$(this).val());
            return that.valid($(this),$(this).val(),currentSku);
        });
    },
    valid: function (inputDom,num,currentSku) { //验证
        if (isNaN(num)) {
            //inputDom.val(currentSku.mininum);
            alert('请输入数字！');
            return false;
        }
        if (num > parseInt(currentSku.goods_stock)) {
            //inputDom.val(currentSku.mininum);
            alert('库存不足！');
            return false;
        }
        if (num < parseInt((currentSku.mininum))) {
            //inputDom.val(currentSku.mininum);
            alert('起订量不能小于:' + currentSku.mininum);
            return false;
        }
    }
};