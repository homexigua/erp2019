与其他插件不一样，Cloud Zoom 不需要再在 js 代码里调用了，只需要给 a 标签加上 class 为 “cloud-zoom” 就可以实现调用。

只加一个 class 效果是默认的，想要其他效果的话，需要给 a 标签的 rel 属性上加上一定的值，具体的值如下：
参数	描述	默认值
zoomWidth	放大镜窗口的宽度，如果是默认值，则和小图片一样。不需要单位，有单位会出错	'auto'
zoomHeight	放大镜窗口的高度，如果是默认值，则和小图片一样。不需要单位，有单位会出错	'auto'
position	放大镜窗口的位置，可选 left 、 right 、 top 、 bottom ，也可以指定某个 ID，如 position: 'element1'	'right'
adjustX	距离小图片的水平位置。不需要单位，有单位会出错	0
adjustY	距离小图片的垂直位置。不需要单位，有单位会出错	0
tint	非放大区域的颜色，必须是十六进制颜色，如 '#aa00aa',不能和 softFocus 一起使用	false
tintOpacity	不透明度，0 是完全透明，1 是完全不透明	0.5
lensOpacity	镜头鼠标指针不透明度，0 是完全透明，1 是完全不透明，在 tint 和 softFocus 模式下始终透明	0.5
softFocus	微微的模糊效果，可选 true 或 false，不能和 tint 一起使用	false
smoothMove	放大区域图片移动的平滑度，数字越高越平滑，为 1 则不平滑	3
showTitle	显示图片标题，可选 true 或 false	true
titleOpacity	标题不透明度，0 是完全透明，1 是完全不透明	0.5