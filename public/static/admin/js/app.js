//确保jQuery已经在app.js之前加载
if (typeof jQuery === "undefined") {
    throw new Error("AdminLTE requires jQuery");
}

$.AdminLTE = {};

/* --------------------
 * - AdminLTE 选项 -
 * --------------------
 * 修改这些选项以满足你的需要
 */
$.AdminLTE.options = {
    //将 slimscroll 添加到导航栏菜单
    //你需要加载 slimscroll 插件
    //在所有页面之前加载 app.js
    navbarMenuSlimscroll: true,
    navbarMenuSlimscrollWidth: "3px", //滚动条的宽度
    navbarMenuHeight: "200px", //内部菜单的高度
    //一般为JS动画速度，如框折叠/展开和侧边栏向上/向下滑动。
    //单位为毫秒，
    //或者使用 'fast', 'normal', 或 'slow'
    animationSpeed: 'fast',
    //侧边栏菜单按钮选择器
    sidebarToggleSelector: "[data-toggle='offcanvas']",
    // 激活侧边栏推菜单
    sidebarPushMenu: true,
    //激活边栏slimscroll如果固定的布局设置（需要SlimScroll插件）
    sidebarSlimScroll: true,
    //启用迷你侧边栏并固定
    //如果固定布局和迷你侧边栏启用，
    //则此选项被强制为真
    sidebarExpandOnHover: true,
    //BoxRefresh 插件
    enableBoxRefresh: true,
    //启用快速单击。
    //Fastclick 让触摸设备拥有等同于本地的触摸体验。
    //如果您选择启用该插件，
    //请确保在 AdminLTE app.js 前加载了该脚本
    enableFastclick: false,
    //控制栏树视图
    enableControlTreeView: true,
    //Box 小部件插件。启用这个插件
    //允许折叠和（或）删除
    enableBoxWidget: true,
    //Box 小部件配置
    boxWidgetOptions: {
        boxWidgetIcons: {
            //折叠图标
            collapse: 'fa-minus',
            //展开图标
            open: 'fa-plus',
            //删除图标
            remove: 'fa-times'
        },
        boxWidgetSelectors: {
            //删除按钮选择器
            remove: '[data-widget="remove"]',
            //折叠按钮选择器
            collapse: '[data-widget="collapse"]'
        }
    },
    //bootstrap使用的标准屏幕大小。
    //If you change these in the variables.less file, change
    //them here too.
    screenSizes: {
        xs: 480,
        sm: 768,
        md: 992,
        lg: 1200
    }
};

/* ------------------
 * - Implementation -
 * ------------------
 * The next block of code implements AdminLTE's
 * functions and plugins as specified by the
 * options above.
 */
$(function () {
    "use strict";

    //Fix for IE page transitions
    $("body").removeClass("hold-transition");

    //Extend options if external options exist
    if (typeof AdminLTEOptions !== "undefined") {
        $.extend(true, $.AdminLTE.options, AdminLTEOptions);
    }

    //Easy access to options
    var o = $.AdminLTE.options;

    //Set up the object
    _init();

    //Activate the layout maker
    $.AdminLTE.layout.activate();

    //Enable sidebar tree view controls
    if (o.enableControlTreeView) {
        $.AdminLTE.tree('.sidebar');
    }

    //Add slimscroll to navbar dropdown
    if (o.navbarMenuSlimscroll && typeof $.fn.slimscroll != 'undefined') {
        $(".navbar .menu").slimscroll({
            height: o.navbarMenuHeight,
            alwaysVisible: false,
            size: o.navbarMenuSlimscrollWidth
        }).css("width", "100%");
    }

    // 激活侧边栏推菜单
    if (o.sidebarPushMenu) {
        $.AdminLTE.pushMenu.activate(o.sidebarToggleSelector);
    }
    //Activate box widget
    if (o.enableBoxWidget) {
        $.AdminLTE.boxWidget.activate();
    }
    //Activate fast click
    if (o.enableFastclick && typeof FastClick != 'undefined') {
        FastClick.attach(document.body);
    }

    /*
     * INITIALIZE BUTTON TOGGLE
     * ------------------------
     */
    $('.btn-group[data-toggle="btn-toggle"]').each(function () {
        var group = $(this);
        $(this).find(".btn").on('click', function (e) {
            group.find(".btn.active").removeClass("active");
            $(this).addClass("active");
            e.preventDefault();
        });

    });
});

/* ----------------------------------
 * - Initialize the AdminLTE Object -
 * ----------------------------------
 * All AdminLTE functions are implemented below.
 */
function _init() {
    'use strict';
    /* Layout
     * ======
     * Fixes the layout height in case min-height fails.
     *
     * @type Object
     * @usage $.AdminLTE.layout.activate()
     *        $.AdminLTE.layout.fixSidebar()
     */
    $.AdminLTE.layout = {
        activate: function () {
            var _this = this;
            _this.fixSidebar();
            $('body, html, .wrapper').css('height', 'auto');
            $(window, ".wrapper").resize(function () {
                _this.fixSidebar();
            });
        },
        fixSidebar: function () {
            //Make sure the body tag has the .fixed class
            if (!$("body").hasClass("fixed")) {
                if (typeof $.fn.slimScroll != 'undefined') {
                    $(".sidebar").slimScroll({destroy: true}).height("auto");
                }
                return;
            } else if (typeof $.fn.slimScroll == 'undefined' && window.console) {
                window.console.error("Error: the fixed layout requires the slimscroll plugin!");
            }
            //Enable slimscroll for fixed layout
            if ($.AdminLTE.options.sidebarSlimScroll) {
                if (typeof $.fn.slimScroll != 'undefined') {
                    //Destroy if it exists
                    $(".sidebar").slimScroll({destroy: true}).height("auto");
                    //Add slimscroll
                    $(".sidebar").slimScroll({
                        height: ($(window).height() - $(".main-header").height()) + "px",
                        color: "rgba(0,0,0,0.2)",
                        size: "3px"
                    });
                }
            }
        }
    };

    /* PushMenu()
     * ==========
     * Adds the push menu functionality to the sidebar.
     *
     * @type Function
     * @usage: $.AdminLTE.pushMenu("[data-toggle='offcanvas']")
     */
    $.AdminLTE.pushMenu = {
        activate: function (toggleBtn) {
            //Get the screen sizes
            var screenSizes = $.AdminLTE.options.screenSizes;
            //Enable sidebar toggle
            $(document).on('click', toggleBtn, function (e) {
                e.preventDefault();
                //Enable sidebar push menu
                if ($(window).width() > (screenSizes.sm - 1)) {
                    if ($("body").hasClass('sidebar-collapse')) {
                        $("body").removeClass('sidebar-collapse').trigger('expanded.pushMenu');
                    } else {
                        $("body").addClass('sidebar-collapse').trigger('collapsed.pushMenu');
                    }
                }
                //Handle sidebar push menu for small screens
                else {
                    if ($("body").hasClass('sidebar-open')) {
                        $("body").removeClass('sidebar-open').removeClass('sidebar-collapse').trigger('collapsed.pushMenu');
                    } else {
                        $("body").addClass('sidebar-open').trigger('expanded.pushMenu');
                    }
                }
            });

            $(".content-wrapper").click(function () {
                //Enable hide menu when clicking on the content-wrapper on small screens
                if ($(window).width() <= (screenSizes.sm - 1) && $("body").hasClass("sidebar-open")) {
                    $("body").removeClass('sidebar-open');
                }
            });

            //Enable expand on hover for sidebar mini
            if ($.AdminLTE.options.sidebarExpandOnHover || ($('body').hasClass('fixed') && $('body').hasClass('sidebar-mini'))) {
                this.expandOnHover();
            }
        },
        expandOnHover: function () {
            var _this = this;
            var screenWidth = $.AdminLTE.options.screenSizes.sm - 1;
            //Expand sidebar on hover
            $('.main-sidebar').hover(function () {
                if ($('body').hasClass('sidebar-mini')
                    && $("body").hasClass('sidebar-collapse')
                    && $(window).width() > screenWidth) {
                    _this.expand();
                }
            }, function () {
                if ($('body').hasClass('sidebar-mini')
                    && $('body').hasClass('sidebar-expanded-on-hover')
                    && $(window).width() > screenWidth) {
                    _this.collapse();
                }
            });
        },
        expand: function () { //展开
            $("body").removeClass('sidebar-collapse').addClass('sidebar-expanded-on-hover');
        },
        collapse: function () { //合并
            if ($('body').hasClass('sidebar-expanded-on-hover')) {
                $('body').removeClass('sidebar-expanded-on-hover').addClass('sidebar-collapse');
            }
        }
    };

    /* Tree()
     * ======
     * Converts the sidebar into a multilevel
     * tree view menu.
     *
     * @type Function
     * @Usage: $.AdminLTE.tree('.sidebar')
     */
    $.AdminLTE.tree = function (menu) {
        var _this = this;
        var animationSpeed = $.AdminLTE.options.animationSpeed;
        $(document).off('click', menu + ' li a')
            .on('click', menu + ' li a', function (e) {
                //Get the clicked link and the next element
                var $this = $(this);
                var checkElement = $this.next();

                //Check if the next element is a menu and is visible
                if ((checkElement.is('.treeview-menu')) && (checkElement.is(':visible')) && (!$('body').hasClass('sidebar-collapse'))) {
                    //Close the menu
                    checkElement.slideUp(animationSpeed, function () {
                        checkElement.removeClass('menu-open');
                    });
                    checkElement.parent("li").removeClass("active");
                }
                //If the menu is not visible
                else if ((checkElement.is('.treeview-menu')) && (!checkElement.is(':visible'))) {
                    //Get the parent menu
                    var parent = $this.parents('ul').first();
                    //Close all open menus within the parent
                    var ul = parent.find('ul:visible').slideUp(animationSpeed);
                    //Remove the menu-open class from the parent
                    ul.removeClass('menu-open');
                    //Get the parent li
                    var parent_li = $this.parent("li");

                    //Open the target menu and add the menu-open class
                    checkElement.slideDown(animationSpeed, function () {
                        //Add the class active to the parent li
                        checkElement.addClass('menu-open');
                        parent.find('li.active').removeClass('active');
                        parent_li.addClass('active');
                    });
                }
                //if this isn't a link, prevent the page from being redirected
                if (checkElement.is('.treeview-menu')) {
                    e.preventDefault();
                }
            });
    };

    /* Box 部件
     * =========
     * BoxWidget 插件用来处理屏幕上的
     * 折叠和移除框。
     *
     * @type Object
     * @usage $.AdminLTE.boxWidget.activate()
     *        在$ .AdminLTE.options对象中设置所有选项
     */
    $.AdminLTE.boxWidget = {
        selectors: $.AdminLTE.options.boxWidgetOptions.boxWidgetSelectors,
        icons: $.AdminLTE.options.boxWidgetOptions.boxWidgetIcons,
        animationSpeed: $.AdminLTE.options.animationSpeed,
        activate: function (_box) {
            var _this = this;
            if (!_box) {
                _box = document; // 默认情况下激活所有Box
            }
            //监听折叠事件
            $(_box).on('click', _this.selectors.collapse, function (e) {
                e.preventDefault();
                _this.collapse($(this));
            });

            //监听删除事件
            $(_box).on('click', _this.selectors.remove, function (e) {
                e.preventDefault();
                _this.remove($(this));
            });
        },
        collapse: function (element) {
            var _this = this;
            //查找 box 父级
            var box = element.parents(".box").first();
            //找到 body 和 footer
            var box_content = box.find("> .box-body, > .box-footer, > form  >.box-body, > form > .box-footer");
            if (!box.hasClass("collapsed-box")) {
                //转换减号为加号图标
                element.children(":first")
                    .removeClass(_this.icons.collapse)
                    .addClass(_this.icons.open);
                //隐藏内容
                box_content.slideUp(_this.animationSpeed, function () {
                    box.addClass("collapsed-box");
                });
            } else {
                //转换加号为减号图标
                element.children(":first")
                    .removeClass(_this.icons.open)
                    .addClass(_this.icons.collapse);
                //显示内容
                box_content.slideDown(_this.animationSpeed, function () {
                    box.removeClass("collapsed-box");
                });
            }
        },
        remove: function (element) {
            //查找 box 父级
            var box = element.parents(".box").first();
            box.slideUp(this.animationSpeed);
        }
    };
}

/*
 * BOX 刷新按钮
 * ------------------
 * 这是BOX的自定义插件。它允许您在BOX中添加一个刷新按钮。
 * 将BOX状态转换为加载状态。
 *
 * @type plugin
 * @usage $("#box-widget").boxRefresh( options );
 */
(function ($) {

    "use strict";

    $.fn.boxRefresh = function (options) {

        // 渲染选项
        var settings = $.extend({
            //刷新按钮选择器
            trigger: ".refresh-btn",
            //要加载的源文件 (如 ajax/src.php)
            source: "",
            //回调
            onLoadStart: function (box) {
                return box;
            }, //在按钮被点击之后
            onLoadDone: function (box) {
                return box;
            } //当内容加载完毕

        }, options);

        //覆盖层
        var overlay = $('<div class="overlay"><div class="fa fa-refresh fa-spin"></div></div>');

        return this.each(function () {
            //如果指定了源
            if (settings.source === "") {
                if (window.console) {
                    window.console.log("Please specify a source first - boxRefresh()");
                }
                return;
            }
            //这个box
            var box = $(this);
            //这个按钮
            var rBtn = box.find(settings.trigger).first();

            //触发点击
            rBtn.on('click', function (e) {
                e.preventDefault();
                //添加覆盖层
                start(box);

                //执行ajax调用
                box.find(".box-body").load(settings.source, function () {
                    done(box);
                });
            });
        });

        function start(box) {
            //添加覆盖层和加载图片
            box.append(overlay);

            settings.onLoadStart.call(box);
        }

        function done(box) {
            //移除覆盖层和加载图片
            box.find(overlay).remove();

            settings.onLoadDone.call(box);
        }

    };

})(jQuery);
/*
 * 显示 BOX 控制
 * -----------------------
 * 这是和BOX一起使用的插件，
 * 允许你在激活、删除BOX后，在DOM中插入 BOX。
 *
 * @type plugin
 * @usage $("#box-widget").activateBox();
 * @usage $("#box-widget").toggleBox();
 * @usage $("#box-widget").removeBox();
 */
(function ($) {

    'use strict';

    $.fn.activateBox = function () {
        $.AdminLTE.boxWidget.activate(this);
    };

    $.fn.toggleBox = function () {
        var button = $($.AdminLTE.boxWidget.selectors.collapse, this);
        $.AdminLTE.boxWidget.collapse(button);
    };

    $.fn.removeBox = function () {
        var button = $($.AdminLTE.boxWidget.selectors.remove, this);
        $.AdminLTE.boxWidget.remove(button);
    };

})(jQuery);

var isIframe = top == this ? false : true;
$(function () {
    $('.tab-href').click(function () {
        var url = $(this).data('url');
        var title = $(this).data('title');
        if(isIframe){
            top.addTabs({
                id: url,
                title: title,
                url: url,
                close:true,
                targetType: 'iframe-tab'
            });
        }else{
            location.href=url;
        }
    });
});

