//animate
In.add('animate',{path:'/static/plugins/animate/animate.css',type:'css',charset:'utf-8'});
//btable
In.add('btable-css',{path:'/static/plugins/bootstrap-table/bootstrap-table.min.css',type:'css',charset:'utf-8'});
In.add('btable-js',{path:'/static/plugins/bootstrap-table/bootstrap-table.min.js',type:'js',charset:'utf-8',rely:['btable-css']});
In.add('btable-mobile',{path:'/static/plugins/bootstrap-table/extensions/mobile/bootstrap-table-mobile.min.js',type:'js',charset:'utf-8',rely:['btable-js']});
In.add('btable',{path:'/static/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.js',type:'js',charset:'utf-8',rely:['btable-mobile']});
//export-table
In.add('btable-export-js',{path:'/static/plugins/bootstrap-table/extensions/export/bootstrap-table-export.js',type:'js',charset:'utf-8'});
In.add('btable-export',{path:'/static/plugins/bootstrap-table/tableExport/tableExport.min.js',type:'js',charset:'utf-8',rely:['btable-export-js']});
//edittable
In.add('btable-editable',{path:'/static/plugins/bootstrap-table/extensions/editable/bootstrap-table-editable.min.js',type:'js',charset:'utf-8'});
//editable
In.add('editable-css',{path:'/static/plugins/bootstrap-editable/css/bootstrap-editable.css',type:'css',charset:'utf-8'});
In.add('editable',{path:'/static/plugins/bootstrap-editable/js/bootstrap-editable.min.js',type:'js',charset:'utf-8',rely:['editable-css']});
//tree-table
In.add('btable-treegrid',{path:'/static/plugins/bootstrap-table/extensions/treegrid/bootstrap-table-treegrid.min.js',type:'js',charset:'utf-8'});
//colorpicker
In.add('colorpicker-css',{path:'/static/plugins/colorpicker/bootstrap-colorpicker.min.css',type:'css',charset:'utf-8'});
In.add('colorpicker',{path:'/static/plugins/colorpicker/bootstrap-colorpicker.min.js',type:'js',charset:'utf-8',rely:['colorpicker-css']});
//cookie
In.add('cookie',{path:'/static/plugins/cookie/jquery.cookie.js',type:'js',charset:'utf-8'});
//cxselect
In.add('cxselect',{path:'/static/plugins/cxselect/js/jquery.cxselect.min.js',type:'js',charset:'utf-8'});
//echo
In.add('echo',{path:'/static/plugins/echo/echo.js',type:'js',charset:'utf-8'});
//icheck
In.add('iCheck-css',{path:'/static/plugins/iCheck/all.css',type:'css',charset:'utf-8'});
In.add('iCheck',{path:'/static/plugins/iCheck/icheck.min.js',type:'js',charset:'utf-8',rely:['iCheck-css']});
//treegrid
In.add('treegrid-css',{path:'/static/plugins/jquery-treegrid/css/jquery.treegrid.css',type:'css',charset:'utf-8'});
In.add('treegrid',{path:'/static/plugins/jquery-treegrid/js/jquery.treegrid.js',type:'js',charset:'utf-8',rely:['treegrid-css']});
//template
In.add('template',{path:'/static/plugins/juicer/juicer-min.js',type:'js',charset:'utf-8'});
//laydate
In.add('laydate',{path:'/static/plugins/laydate/laydate.js',type:'js',charset:'utf-8'});
//paginationjs
In.add('pagination-css',{path:'/static/plugins/paginationjs/pagination.css',type:'css',charset:'utf-8'});
In.add('pagination',{path:'/static/plugins/paginationjs/pagination.min.js',type:'js',charset:'utf-8',rely:['pagination-css']});
//plupload
In.add('upload-js',{path:'/static/plugins/plupload/plupload.full.min.js',type:'js',charset:'utf-8'});
In.add('upload',{path:'/static/plugins/plupload/i18n/zh_CN.js',type:'js',charset:'utf-8',rely:['upload-js']});
//selectpage
In.add('selectpage-css',{path:'/static/plugins/selectpage/selectpage.css',type:'css',charset:'utf-8'});
In.add('selectpage',{path:'/static/plugins/selectpage/selectpage.min.js',type:'js',charset:'utf-8',rely:['selectpage-css']});
//tags
In.add('tags-css',{path:'/static/plugins/tags/tag.css',type:'css',charset:'utf-8'});
In.add('tags',{path:'/static/plugins/tags/tag.js',type:'js',charset:'utf-8',rely:['tags-css']});
//valid
In.add('valid-js',{path:'/static/plugins/validform/js/Validform.js',type:'js',charset:'utf-8'});
In.add('valid',{path:'/static/plugins/validform/js/Validform_Datatype.js',type:'js',charset:'utf-8',rely:['valid-js']});
//form转json
In.add('jquery-json',{path:'/static/plugins/jquery.serializeJSON-master/jquery.serializejson.js',type:'js',charset:'utf-8'});
//ztree
In.add('ztree-css',{path:'/static/plugins/ztree/css/metroStyle/metroStyle.css',type:'css',charset:'utf-8'});
In.add('ztree',{path:'/static/plugins/ztree/js/jquery.ztree.all-3.5.min.js',type:'js',charset:'utf-8',rely:['ztree-css']});
//tag插件
In.add('tags-css',{path:'/static/plugins/tags/css/tag.css',type:'css',charset:'utf-8'});
In.add('tags',{path:'/static/plugins/tags/js/tag.js',type:'js',charset:'utf-8',rely:['tags-css']});
/*switch*/
In.add('switch-css',{path:'/static/plugins/bootstrap-switch/bootstrap-switch.min.css',type:'css',charset:'utf-8'});
In.add('switch',{path:'/static/plugins/bootstrap-switch/bootstrap-switch.min.js',type:'js',charset:'utf-8',rely:['switch-css']});
//ckeditor
In.add('ckeditor',{path:'/static/plugins/ckeditor/ckeditor.js',type:'js',charset:'utf-8'});
//mask
In.add('mask',{path:'/static/plugins/jquery-mask/jquery.mask.min.js',type:'js',charset:'utf-8'});
//jexcel
In.add('numeral',{path:'/static/plugins/jexcel/dist/js/numeral.min.js',type:'js',charset:'utf-8'});
In.add('jexcel-js',{path:'/static/plugins/jexcel/dist/js/jquery.jexcel.js',type:'js',charset:'utf-8',rely:['numeral']});
In.add('jexcel-css',{path:'/static/plugins/jexcel/dist/css/jquery.jexcel.css',type:'css',charset:'utf-8',rely:['jexcel-js']});
In.add('jcalendar-css',{path:'/static/plugins/jexcel/dist/css/jquery.jcalendar.css',type:'css',charset:'utf-8',rely:['jexcel-css']});
In.add('jcalendar-js',{path:'/static/plugins/jexcel/dist/js/jquery.jcalendar.js',type:'js',charset:'utf-8',rely:['jcalendar-css']});
In.add('jexcel',{path:'/static/plugins/jexcel/dist/js/excel-formula.min.js',type:'js',charset:'utf-8',rely:['jcalendar-js']});
//math
In.add('math',{path:'/static/plugins/math/math.min.js',type:'js',charset:'utf-8'});











