utils.ajaxSubForm();
$(function () {
    if(scene=='add'){
        /*初始化价格表格*/
        doAction.createNoSkuTable();
    }
    if(scene=='edit'){
        /*初始化价格表格*/
        doAction.editSkuTable();
    }
});
/*选择sku选项时触发*/
$('#select-attr').on('click','input[type=checkbox]',function () {
    if(scene=='add'){
        doAction.createSkuTable();
        removeskuItem={};
        doAction.showRSkuitem();
    }
});
var doAction={
    /*新增品牌、单位、运费模板*/
    addOther:function (title,url) {
        utils.openDialog({
            title: title,
            area: ['800px', '420px'],
            content:url
        });
    },
    /*选择运费模板*/
    selectDelivery:function (data) {
        var html='<div class="form-group">\n' +
            '                    <div class="col-sm-10 col-sm-offset-1">\n' +
            '                         <small>'+data.freight_type_text+'</small>\n'+
            '                    </div>\n' +
            '                </div>';
        $('#freight_type_msg').html(html);
    },
    /*清除运费模板*/
    clearDelivery:function () {
        $('#freight_type_msg').empty();
    },
    /*阶梯价*/
    ladderPrice:function(obj) {
        var value=$(obj).closest('.input-price').find('input').val();
        utils.openDialog({
            title: '设置阶梯价格',
            area: ['600px', '350px'],
            content:'/admin/mall.goods/ladderprice.html?price='+value,
            btn: '确认',
            yes:function (index, layero) {
                var iframeWin = window[layero.find('iframe')[0]['name']];
                var price = iframeWin.stepAction.getLadderPrice();
                console.log(price);
                $(obj).closest('.input-price').find('input').val(price);
                layer.close(index);
            }
        });
    },
    /*开启sku*/
    switchSku:function (event,state) {
        console.log(state);
        if(state==true){
            /*关闭sku*/
            $('input[name="is_goods_attr"]').val(0);
            $('#goods_attr_ids').selectPageClear();
            skuAttr={};
            $('#sku-box').hide();
            doAction.createNoSkuTable();
            doAction.setAttr();
        }else{
            /*开启sku*/
            $('input[name="is_goods_attr"]').val(1);
            $('#sku-box').show();
        }
    },
    /*开启一口价/阶梯价*/
    switchPrice:function (event,state) {
        if(state==true){
            /*一口价*/
            $('.input-price').find('input').val('').removeAttr("readOnly");
            $('.input-price').removeClass('step-price');
            $('input[name="goods_price_type"]').val(0);
        }else{
            /*阶梯价*/
            $('.input-price').find('input').val('').attr("readOnly","readOnly");
            $('.input-price').addClass('step-price');
            $('input[name="goods_price_type"]').val(1);
        }
    },
    /*无sku*/
    createNoSkuTable:function(){
        var goods_price_type = $('input[name="goods_price_type"]').val();
        var noSkuTable = juicer($('#no-sku-table-tpl').html(),{goods_price_type:goods_price_type});
        $('#sku-table').empty().html(noSkuTable);
    },
    /*选择sku属性后*/
    selectSkuAttr:function (data) {
        /*返回值是数组为移除，对象为选择*/
        if(toString.call(data)=='[object Array]'){
            $('#sku-table').empty();
            delete skuAttr[data[0]['id']];
            doAction.setAttr();
        }
        if(toString.call(data)=='[object Object]'){
            $('#sku-table').empty();
            skuAttr[data.id]=data;
            doAction.setAttr();
        }
    },
    //拼装表格
    createSkuTable:function () {
        var arrayInfo = this.getCheckboxSelect();
        var skuitem = this.doGroup(arrayInfo); //获取到的skuitem
        var goods_price_type = $('input[name="goods_price_type"]').val();
        console.log(goods_price_type);
        var skuTable = juicer($('#sku-table-tpl').html(),{data:skuitem,num:skuitem.length,goods_price_type:goods_price_type});
        $('#sku-table').empty().html(skuTable);
    },
    /*编辑时创建表格*/
    editSkuTable:function () {
        var goods_price_type = $('input[name="goods_price_type"]').val();
        var skuTable = juicer($('#sku-table-tpl').html(),{data:editSku,num:editSku.length,goods_price_type:goods_price_type});
        $('#sku-table').empty().html(skuTable);
    },
    /*设置sku属性*/
    setAttr:function(){
        if($.isEmptyObject(skuAttr)){
            $('#select-attr').empty();
        }else{
            var attrGroup = juicer($('#attr_group_tpl').html(),{ data:skuAttr });
            $('#select-attr').empty().html(attrGroup);
        }
    },
    //获取选中值
    getCheckboxSelect:function () {
        var arrayInfo=[]; //每组存放数据
        $('#select-attr .attr-item').each(function () {
            var that=$(this);
            var checkArr=[];
            that.find("input[type='checkbox']:checked").each(function () {
                checkArr.push($(this).val());
            });
            console.log(checkArr);
            arrayInfo.push(checkArr);
        });
        return arrayInfo;
    },
    //组合数组
    doGroup: function (doubleArrays) {
        var len = doubleArrays.length;
        if(len==0) return [];
        if (len >= 2) {
            var arr1 = doubleArrays[0];
            var arr2 = doubleArrays[1];
            var len1 = doubleArrays[0].length;
            var len2 = doubleArrays[1].length;
            var newlen = len1 * len2;
            var temp = new Array(newlen);
            var index = 0;
            for (var i = 0; i < len1; i++) {
                for (var j = 0; j < len2; j++) {
                    temp[index] = arr1[i] + "," + arr2[j];
                    index++;
                }
            }
            var newArray = new Array(len - 1);
            newArray[0] = temp;
            if (len > 2) {
                var _count = 1;
                for (var i = 2; i < len; i++) {
                    newArray[_count] = doubleArrays[i];
                    _count++;
                }
            }
            return doAction.doGroup(newArray);
        } else {
            return doubleArrays[0];
        }
    },
    delSkuItem:function (obj) {
        var sku_item = $('#sku-table .item').length;
        var index= $(obj).parents('tr').data('index');
        var value= $(obj).parents('tr').data('value');
        if(sku_item>1){
            removeskuItem[index]=value;
            doAction.showRSkuitem();
            $(obj).parents('tr').remove();
        }else {
            layer.msg('至少要保留一条商品信息！',{icon:2});
        }
    },
    /*显示已移除的sku项目，点击还原*/
    showRSkuitem:function () {
        var html='<span style="font-size:12px;color:#989898">已移除：</span> ';
        if($.isEmptyObject(removeskuItem)){
            $('#remove-sku-box').empty();
        }else{
            if(scene=='add'){
                $.each(removeskuItem,function (index,value) {
                    html+=' <a href="javascript:;" title="点击可还原" data-index="'+index+'" data-value="'+value+'" onclick="doAction.restoreSkuitem(this)" class="btn btn-xs btn-default">'+value+'</a> ';
                });
            }
            if(scene=='edit'){
                $.each(removeskuItem,function (index,value) {
                    html+=' <a href="javascript:;" title="点击可还原" data-index="'+index+'" data-value=\''+JSON.stringify(value)+'\' onclick="doAction.restoreSkuitem(this)" class="btn btn-xs btn-default">'+value.sku_name+'</a> ';
                });
            }
            $('#remove-sku-box').empty().html(html);
        }
    },
    /*还原已移除的sku项目*/
    restoreSkuitem:function (obj) {
        var index=$(obj).data('index');
        var value=$(obj).data('value');
        var goods_price_type = $('input[name="goods_price_type"]').val();
        var html=juicer($('#sku-item-tpl').html(),{index:index,value:value,goods_price_type:goods_price_type});
        if(index>0){
            var num = parseInt(index)-1;
            var box = $('#sku-table [data-index="'+num+'"]');
            box.after(html);
        }else{
            $('#sku-table [class="item"]:first-child').before(html);
        }
        delete removeskuItem[index];
        doAction.showRSkuitem();
    },
    /*编辑时如果勾选的sku存在移除选项，点重置恢复已移除的项目*/
    resetSku:function () {
        removeskuItem={};
        editSku=[];
        doAction.showRSkuitem();
        var arrayInfo = this.getCheckboxSelect();
        var skuitem = this.doGroup(arrayInfo); //获取到的skuite
        exskuArray = utils.jsonCol(exskuItem,'sku_name','###');
        exskuArray = exskuArray.split('###');
        var resetsku = _.difference(skuitem,exskuArray);/*所选sku中已经不包含的选项*/
        $.each(resetsku,function (index,value) {
            var item={id:0,sku_name:value,goods_stock:0};
            editSku.push(item);
        });
        editSku =exskuItem.concat(editSku);
        doAction.editSkuTable();
    },
    /*批量设置价格*/
    batchPrice:function (type) {
        var title;
        switch (type){
            case 'market_price':
                title ='批量设置市场价';
                break;
            case 'sale_price':
                title ='批量设置销售价';
                break;
        }
        var goods_price_type = $('input[name="goods_price_type"]').val();
        if(goods_price_type==1 && type=='sale_price'){
            utils.openDialog({
                title: '设置阶梯价格',
                area: ['600px', '350px'],
                content:'/admin/mall.goods/ladderprice.html?price=',
                btn: '确认',
                yes:function (index, layero) {
                    var iframeWin = window[layero.find('iframe')[0]['name']];
                    var price = iframeWin.stepAction.getLadderPrice();
                    $('input[data-name="'+type+'"]').val(price);
                    layer.close(index);
                }
            });
        }else{
            layer.prompt({title:title, formType:0}, function(text, index){
                if(!valid.isFloat(text)){
                    utils.msg('请输入数字！');
                    return false;
                }
                $('input[data-name="'+type+'"]').val(text);
                layer.close(index);
            });
        }

    },
};