In.config('serial',true); //串行加载
In('btable','editable','btable-editable',function () {
    $('#prov-grid').bootstrapTable({
        url:'getDataList.html?pid=0',
        toolbar:'.prov-toolbar',
        clickToSelect:false,
        singleSelect:true,
        selectItemName:'prov-radio',
        idField:'id',
        editableUrl:'setfield.html',
        columns:[
            {field:'state',radio:true},
            {field:'name',title:'省份',editable:formatter.name},
            {field:'show_order',title:'排序',editable:formatter.show_order},
        ],
        onCheck:function (row) {
            $('#city-grid').bootstrapTable('refresh',{
                url:'getDataList.html?pid='+row.id
            });
            $('#dist-grid').bootstrapTable('refresh',{
                url:'getDataList.html?pid=-1'
            });
        }
    });
    $('#city-grid').bootstrapTable({
        toolbar:'.city-toolbar',
        clickToSelect:false,
        singleSelect:true,
        selectItemName:'city-radio',
        idField:'id',
        editableUrl:'setfield.html',
        columns:[
            {field:'state',radio:true},
            {field:'name',title:'城市',editable:formatter.name},
            {field:'show_order',title:'排序',editable:formatter.show_order},
        ],
        onCheck:function (row) {
            $('#dist-grid').bootstrapTable('refresh',{
                url:'getDataList.html?pid='+row.id
            });
        }
    });
    $('#dist-grid').bootstrapTable({
        toolbar:'.dist-toolbar',
        clickToSelect:false,
        singleSelect:true,
        selectItemName:'dist-radio',
        idField:'id',
        editableUrl:'setfield.html',
        columns:[
            {field:'state',radio:true},
            {field:'name',title:'地区',editable:formatter.name},
            {field:'show_order',title:'排序',editable:formatter.show_order},
        ]
    });
});
var area={
    add:function (obj) {
        var label=$(obj).data('label');
        var dom=$(obj).data('dom');
        var pid=0;
        if(dom=='#city-grid'){ //添加城市
            var rows = $('#prov-grid').bootstrapTable('getSelections');
            if(rows.length==0){
                utils.msg('请选择省份');
                return false;
            }
            pid=rows[0].id;
        }
        if(dom=='#dist-grid'){ //添加地区
            var rows = $('#city-grid').bootstrapTable('getSelections');
            if(rows.length==0){
                utils.msg('请选择城市');
                return false;
            }
            pid=rows[0].id;
        }
        var data={label:label,pid:pid};
        var html=juicer($('#area-add-tpl').html(), data);
        utils.open({
            title:'添加'+label,
            area:'400px',
            content:html,
            success: function(layero, index){
                utils.ajaxSubForm(function (res) {
                    layer.close(index);
                    $(dom).bootstrapTable('refresh',{
                        url:'getDataList.html?pid='+pid
                    });
                });
            }
        });
    },
    del:function (obj) {
        var label=$(obj).data('label');
        var dom=$(obj).data('dom');
        var rows = $(dom).bootstrapTable('getSelections');
        if(rows.length==0){
            utils.msg('请选择'+label);
            return false;
        }
        var id=rows[0].id;
        utils.confirm('确认删除吗？',function (index) {
            layer.close(index);
            utils.getAjax({
                url:'del.html',
                data:{id:id},
                success:function (res) {
                    if(res.code==1){
                        $(dom).bootstrapTable('refresh');
                    }else{
                        utils.msg(res.msg);
                    }
                }
            });
        })
    }
};
var formatter={
    name:{
        type: 'text',
        validate: function (v) { //验证规则自定义
            if (valid.isEmpty(v)) return '区域名称不能为空！';
        }
    },
    show_order:{
        type: 'text',
        validate: function (v) { //验证规则自定义
            if (!valid.isNum(v)) return '排序只能为数字！';
        }
    }
};