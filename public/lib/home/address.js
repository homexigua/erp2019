var addrAction = {
    /*新增收货地址*/
    addAddr:function () {
        var html = juicer($('#add-addr-tpl').html(),{});
        layer.open({
            type: 1,
            title:'新增收货地址',
            skin: 'layui-layer-rim', //加上边框
            area: ['600px', '350px'], //宽高
            content: html,
            success:function () {
                formHelper.initForm();
                utils.ajaxSubForm(function (res) {
                    if(res.code==1){
                        if(address_scene=='buy')  addrAction.setAddress(res.data);
                        if(address_scene=='address')   addrAction.getAddress();
                    }else{
                        utils.msg(res.msg);
                    }
                    layer.closeAll();
                },'#address_form')
            }
        });
    },
    /*编辑收货地址*/
    editAddr:function (obj) {
        var data = $(obj).data('info');
        var html = juicer($('#edit-addr-tpl').html(),{data:data});
        layer.open({
            type: 1,
            title:'编辑收货地址',
            skin: 'layui-layer-rim', //加上边框
            area: ['600px', '350px'], //宽高
            content: html,
            success:function () {
                formHelper.initForm();
                utils.ajaxSubForm(function (res) {
                    if(res.code==1){
                        if(address_scene=='buy')  addrAction.setAddress(res.data);
                        if(address_scene=='address')   addrAction.getAddress();
                    }else{
                        utils.msg(res.msg);
                    }
                    layer.closeAll();
                },'#address_form')
            }
        });
    },
    /*切换地址*/
    selectAddr:function () {
        utils.getAjax({
            url:'/site/member.address/getaddrlist.html',
            success:function (res) {
                if(res.length<=1){
                    utils.msg('没有可选择的其他地址',2);
                }else{
                    var html = juicer($('#addr-list-tpl').html(),{data:res});
                    layer.open({
                        type: 1,
                        title:'选择收货地址',
                        scrollbar: false,
                        skin: 'layui-layer-rim', //加上边框
                        area: ['600px', '400px'], //宽高
                        content: html,
                        success:function () {
                            $('#address_list .mc-address-item').click(function () {
                                $(this).addClass('active').siblings().removeClass('active');
                            })
                            $('#selectAddr').click(function () {
                                var data=$('#address_list .active').data('info');
                                addrAction.setAddress(data);
                                layer.closeAll();
                            })
                        }
                    });
                }
            }
        })
    },
    /*获取全部收货地址*/
    getAddress:function (type) {
        var scene =type||'address_list';
        utils.getAjax({
            url:'/site/member.address/getaddrlist.html',
            success:function (res) {
                if(address_scene == 'buy'){
                    var data =res.length>=1?res[0]:'0';
                    addrAction.setAddress(data);
                }else{
                    var html = juicer($('#addr-list-tpl').html(),{data:res});
                    $('#address_list_box').html(html);
                }
            }
        })
    },
    /*删除收货地址*/
    delAddr:function (obj) {
        var id=$(obj).data('id');
        utils.getAjax({
            url:'/site/member.address/del.html',
            data:{id:id},
            success:function (res) {
                addrAction.getAddress();
            }
        })
    },
    /*设置默认*/
    setDefault:function (obj) {
        var id=$(obj).data('id');
        utils.getAjax({
            url:'/site/member.address/setdefault.html',
            data:{id:id},
            success:function (res) {
                addrAction.getAddress();
            }
        })
    },
    /*设置收货地址*/
    setAddress:function (data) {
        var html = juicer($('#address-info-tpl').html(),{data:data});
        $('#address-info').html(html);
        if(data!=''){
            $('input[name="is_zt"]').val(0);
            $('input[name="address_id"]').val(data.id);
        }
    }
}